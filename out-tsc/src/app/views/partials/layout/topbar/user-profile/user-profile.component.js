import * as tslib_1 from "tslib";
// Angular
import { Component, Input } from '@angular/core';
// NGRX
import { select, Store } from '@ngrx/store';
import { currentUser, Logout } from '../../../../../core/auth';
var UserProfileComponent = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param store: Store<AppState>
     */
    function UserProfileComponent(store) {
        this.store = store;
        this.avatar = true;
        this.greeting = true;
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    UserProfileComponent.prototype.ngOnInit = function () {
        this.user$ = this.store.pipe(select(currentUser));
    };
    /**
     * Log out
     */
    UserProfileComponent.prototype.logout = function () {
        this.store.dispatch(new Logout());
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], UserProfileComponent.prototype, "avatar", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], UserProfileComponent.prototype, "greeting", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], UserProfileComponent.prototype, "badge", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], UserProfileComponent.prototype, "icon", void 0);
    UserProfileComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-user-profile',
            templateUrl: './user-profile.component.html',
        }),
        tslib_1.__metadata("design:paramtypes", [Store])
    ], UserProfileComponent);
    return UserProfileComponent;
}());
export { UserProfileComponent };
//# sourceMappingURL=user-profile.component.js.map