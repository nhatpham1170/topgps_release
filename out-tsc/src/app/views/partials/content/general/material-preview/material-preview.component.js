import * as tslib_1 from "tslib";
// Angular
import { Component, Input } from '@angular/core';
var MaterialPreviewComponent = /** @class */ (function () {
    /**
     * Component constructor
     */
    function MaterialPreviewComponent() {
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    MaterialPreviewComponent.prototype.ngOnInit = function () {
    };
    /**
     * Toggle visibility
     */
    MaterialPreviewComponent.prototype.changeCodeVisibility = function () {
        this.viewItem.isCodeVisible = !this.viewItem.isCodeVisible;
    };
    /**
     * Check examples existing
     */
    MaterialPreviewComponent.prototype.hasExampleSource = function () {
        if (!this.viewItem) {
            return false;
        }
        if (!this.viewItem.cssCode && !this.viewItem.htmlCode && !this.viewItem.scssCode && !this.viewItem.tsCode) {
            return false;
        }
        return true;
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], MaterialPreviewComponent.prototype, "viewItem", void 0);
    MaterialPreviewComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-material-preview',
            templateUrl: './material-preview.component.html',
            styleUrls: ['./material-preview.component.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], MaterialPreviewComponent);
    return MaterialPreviewComponent;
}());
export { MaterialPreviewComponent };
//# sourceMappingURL=material-preview.component.js.map