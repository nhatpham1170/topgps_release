import * as tslib_1 from "tslib";
// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// Config
import { AccordionControlConfig } from './accordion-control.config';
import { AccordionControlComponent, AccordionControlPanelDirective, AccordionControlPanelTitleDirective, AccordionControlPanelContentDirective } from './accordion-control.component';
export { AccordionControlConfig } from './accordion-control.config';
export { AccordionControlComponent, AccordionControlPanelDirective, AccordionControlPanelTitleDirective, AccordionControlPanelContentDirective } from './accordion-control.component';
var ACCORDION_CONTROL_DIRECTIVES = [
    AccordionControlComponent,
    AccordionControlPanelDirective,
    AccordionControlPanelTitleDirective,
    AccordionControlPanelContentDirective
];
var AccordionControlModule = /** @class */ (function () {
    function AccordionControlModule() {
    }
    AccordionControlModule_1 = AccordionControlModule;
    AccordionControlModule.forRoot = function () {
        return { ngModule: AccordionControlModule_1, providers: [AccordionControlConfig] };
    };
    var AccordionControlModule_1;
    AccordionControlModule = AccordionControlModule_1 = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule
            ],
            exports: ACCORDION_CONTROL_DIRECTIVES,
            declarations: ACCORDION_CONTROL_DIRECTIVES
        })
    ], AccordionControlModule);
    return AccordionControlModule;
}());
export { AccordionControlModule };
//# sourceMappingURL=accordion-control.module.js.map