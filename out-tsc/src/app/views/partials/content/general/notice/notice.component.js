import * as tslib_1 from "tslib";
// Angular
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
var NoticeComponent = /** @class */ (function () {
    /**
     * Component constructor
     */
    function NoticeComponent() {
        // Public properties
        this.classes = '';
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    NoticeComponent.prototype.ngOnInit = function () {
        if (this.icon) {
            this.classes += ' kt-alert--icon';
        }
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], NoticeComponent.prototype, "classes", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], NoticeComponent.prototype, "icon", void 0);
    NoticeComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-notice',
            templateUrl: './notice.component.html',
            changeDetection: ChangeDetectionStrategy.OnPush
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], NoticeComponent);
    return NoticeComponent;
}());
export { NoticeComponent };
//# sourceMappingURL=notice.component.js.map