import * as tslib_1 from "tslib";
// Angular
import { Component, HostBinding, Input } from '@angular/core';
var PortletFooterComponent = /** @class */ (function () {
    function PortletFooterComponent() {
        // Public properties
        this.classList = 'kt-portlet__foot';
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    PortletFooterComponent.prototype.ngOnInit = function () {
        if (this.class) {
            this.classList += ' ' + this.class;
        }
    };
    tslib_1.__decorate([
        HostBinding('class'),
        tslib_1.__metadata("design:type", Object)
    ], PortletFooterComponent.prototype, "classList", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], PortletFooterComponent.prototype, "class", void 0);
    PortletFooterComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-portlet-footer',
            template: "\n\t\t<ng-content></ng-content>"
        })
    ], PortletFooterComponent);
    return PortletFooterComponent;
}());
export { PortletFooterComponent };
//# sourceMappingURL=portlet-footer.component.js.map