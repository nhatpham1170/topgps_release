import * as tslib_1 from "tslib";
// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressBarModule, MatProgressSpinnerModule } from '@angular/material';
// Module
import { CoreModule } from '../../../../../core/core.module';
// Portlet
import { PortletComponent } from './portlet.component';
import { PortletHeaderComponent } from './portlet-header.component';
import { PortletBodyComponent } from './portlet-body.component';
import { PortletFooterComponent } from './portlet-footer.component';
var PortletModule = /** @class */ (function () {
    function PortletModule() {
    }
    PortletModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                CoreModule,
                MatProgressSpinnerModule,
                MatProgressBarModule
            ],
            declarations: [
                PortletComponent,
                PortletHeaderComponent,
                PortletBodyComponent,
                PortletFooterComponent,
            ],
            exports: [
                PortletComponent,
                PortletHeaderComponent,
                PortletBodyComponent,
                PortletFooterComponent,
            ]
        })
    ], PortletModule);
    return PortletModule;
}());
export { PortletModule };
//# sourceMappingURL=portlet.module.js.map