import * as tslib_1 from "tslib";
import { KtDialogService, StickyDirective } from '../../../../../core/_base/layout';
// Angular
import { Component, ElementRef, HostBinding, HostListener, Inject, Input, PLATFORM_ID, ViewChild } from '@angular/core';
// RXJS
import { Observable } from 'rxjs';
var PortletHeaderComponent = /** @class */ (function () {
    function PortletHeaderComponent(el, platformId, ktDialogService) {
        this.el = el;
        this.platformId = platformId;
        this.ktDialogService = ktDialogService;
        this.viewLoading = false;
        this.classes = 'kt-portlet__head';
        this.lastScrollTop = 0;
        this.subscriptions = [];
        this.isScrollDown = false;
        this.stickyDirective = new StickyDirective(this.el, this.platformId);
    }
    PortletHeaderComponent.prototype.onResize = function () {
        this.updateStickyPosition();
    };
    PortletHeaderComponent.prototype.onScroll = function () {
        this.updateStickyPosition();
        var st = window.pageYOffset || document.documentElement.scrollTop;
        this.isScrollDown = st > this.lastScrollTop;
        this.lastScrollTop = st <= 0 ? 0 : st;
    };
    PortletHeaderComponent.prototype.updateStickyPosition = function () {
        var _this = this;
        if (this.sticky) {
            Promise.resolve(null).then(function () {
                // get boundary top margin for sticky header
                var headerElement = document.querySelector('.kt-header');
                var subheaderElement = document.querySelector('.kt-subheader');
                var headerMobileElement = document.querySelector('.kt-header-mobile');
                var height = 0;
                // mobile header
                if (window.getComputedStyle(headerElement).height === '0px') {
                    height += headerMobileElement.offsetHeight;
                }
                else {
                    // desktop header
                    if (document.body.classList.contains('kt-header--minimize-topbar')) {
                        // hardcoded minimized header height
                        height = 60;
                    }
                    else {
                        // normal fixed header
                        if (document.body.classList.contains('kt-header--fixed')) {
                            height += headerElement.offsetHeight;
                        }
                        if (document.body.classList.contains('kt-subheader--fixed')) {
                            height += subheaderElement.offsetHeight;
                        }
                    }
                }
                _this.stickyDirective.marginTop = height;
            });
        }
    };
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    PortletHeaderComponent.prototype.ngOnInit = function () {
        if (this.sticky) {
            this.stickyDirective.ngOnInit();
        }
        // append custom class
        this.classes += this.class ? ' ' + this.class : '';
        // hide icon's parent node if no icon provided
        this.hideIcon = this.refIcon.nativeElement.children.length === 0;
        // hide tools' parent node if no tools template is provided
        this.hideTools = this.refTools.nativeElement.children.length === 0;
    };
    PortletHeaderComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        if (this.sticky) {
            this.updateStickyPosition();
            this.stickyDirective.ngAfterViewInit();
        }
        // initialize loading dialog
        if (this.viewLoading$) {
            var loadingSubscription = this.viewLoading$.subscribe(function (res) { return _this.toggleLoading(res); });
            this.subscriptions.push(loadingSubscription);
        }
    };
    PortletHeaderComponent.prototype.toggleLoading = function (_incomingValue) {
        this.viewLoading = _incomingValue;
        if (_incomingValue && !this.ktDialogService.checkIsShown()) {
            this.ktDialogService.show();
        }
        if (!this.viewLoading && this.ktDialogService.checkIsShown()) {
            this.ktDialogService.hide();
        }
    };
    PortletHeaderComponent.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (sb) { return sb.unsubscribe(); });
        if (this.sticky) {
            this.stickyDirective.ngOnDestroy();
        }
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], PortletHeaderComponent.prototype, "class", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], PortletHeaderComponent.prototype, "title", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], PortletHeaderComponent.prototype, "icon", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], PortletHeaderComponent.prototype, "noTitle", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Boolean)
    ], PortletHeaderComponent.prototype, "sticky", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Observable)
    ], PortletHeaderComponent.prototype, "viewLoading$", void 0);
    tslib_1.__decorate([
        HostBinding('class'),
        tslib_1.__metadata("design:type", String)
    ], PortletHeaderComponent.prototype, "classes", void 0);
    tslib_1.__decorate([
        HostBinding('attr.ktSticky'),
        tslib_1.__metadata("design:type", StickyDirective)
    ], PortletHeaderComponent.prototype, "stickyDirective", void 0);
    tslib_1.__decorate([
        ViewChild('refIcon', { static: true }),
        tslib_1.__metadata("design:type", ElementRef)
    ], PortletHeaderComponent.prototype, "refIcon", void 0);
    tslib_1.__decorate([
        ViewChild('refTools', { static: true }),
        tslib_1.__metadata("design:type", ElementRef)
    ], PortletHeaderComponent.prototype, "refTools", void 0);
    tslib_1.__decorate([
        HostListener('window:resize', ['$event']),
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", []),
        tslib_1.__metadata("design:returntype", void 0)
    ], PortletHeaderComponent.prototype, "onResize", null);
    tslib_1.__decorate([
        HostListener('window:scroll', ['$event']),
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", []),
        tslib_1.__metadata("design:returntype", void 0)
    ], PortletHeaderComponent.prototype, "onScroll", null);
    PortletHeaderComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-portlet-header',
            styleUrls: ['portlet-header.component.scss'],
            template: "\n\t\t<div class=\"kt-portlet__head-label\" [hidden]=\"noTitle\">\n\t\t\t<span class=\"kt-portlet__head-icon\" #refIcon [hidden]=\"hideIcon\">\n\t\t\t\t<ng-content *ngIf=\"!icon\" select=\"[ktPortletIcon]\"></ng-content>\n\t\t\t\t<i *ngIf=\"icon\" [ngClass]=\"icon\"></i>\n\t\t\t</span>\n\t\t\t<ng-content *ngIf=\"!title\" select=\"[ktPortletTitle]\"></ng-content>\n\t\t\t<h3 *ngIf=\"title\" class=\"kt-portlet__head-title\" [innerHTML]=\"title\"></h3>\n\t\t</div>\n\t\t<div class=\"kt-portlet__head-toolbar\" #refTools [hidden]=\"hideTools\">\n\t\t\t<ng-content select=\"[ktPortletTools]\"></ng-content>\n\t\t</div>"
        }),
        tslib_1.__param(1, Inject(PLATFORM_ID)),
        tslib_1.__metadata("design:paramtypes", [ElementRef, String, KtDialogService])
    ], PortletHeaderComponent);
    return PortletHeaderComponent;
}());
export { PortletHeaderComponent };
//# sourceMappingURL=portlet-header.component.js.map