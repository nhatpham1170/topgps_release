import * as tslib_1 from "tslib";
// Angular
import { Component, ElementRef, Input, ViewChild } from '@angular/core';
// Loading bar
import { LoadingBarService } from '@ngx-loading-bar/core';
// RxJS
import { Observable } from 'rxjs';
// Portlet
import { PortletBodyComponent } from './portlet-body.component';
import { PortletHeaderComponent } from './portlet-header.component';
import { PortletFooterComponent } from './portlet-footer.component';
// Layout
import { LayoutConfigService } from '../../../../../core/_base/layout';
var PortletComponent = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param el: ElementRef
     * @param loader: LoadingBarService
     * @param layoutConfigService: LayoutConfigService
     */
    function PortletComponent(el, loader, layoutConfigService) {
        this.el = el;
        this.loader = loader;
        this.layoutConfigService = layoutConfigService;
        this.loader.complete();
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    PortletComponent.prototype.ngOnInit = function () {
    };
    /**
     * After view init
     */
    PortletComponent.prototype.ngAfterViewInit = function () {
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Observable)
    ], PortletComponent.prototype, "loading$", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], PortletComponent.prototype, "options", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], PortletComponent.prototype, "class", void 0);
    tslib_1.__decorate([
        ViewChild('portlet', { static: true }),
        tslib_1.__metadata("design:type", ElementRef)
    ], PortletComponent.prototype, "portlet", void 0);
    tslib_1.__decorate([
        ViewChild(PortletHeaderComponent, { static: true }),
        tslib_1.__metadata("design:type", PortletHeaderComponent)
    ], PortletComponent.prototype, "header", void 0);
    tslib_1.__decorate([
        ViewChild(PortletBodyComponent, { static: true }),
        tslib_1.__metadata("design:type", PortletBodyComponent)
    ], PortletComponent.prototype, "body", void 0);
    tslib_1.__decorate([
        ViewChild(PortletFooterComponent, { static: true }),
        tslib_1.__metadata("design:type", PortletFooterComponent)
    ], PortletComponent.prototype, "footer", void 0);
    PortletComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-portlet',
            templateUrl: './portlet.component.html',
            exportAs: 'ktPortlet'
        }),
        tslib_1.__metadata("design:paramtypes", [ElementRef, LoadingBarService,
            LayoutConfigService])
    ], PortletComponent);
    return PortletComponent;
}());
export { PortletComponent };
//# sourceMappingURL=portlet.component.js.map