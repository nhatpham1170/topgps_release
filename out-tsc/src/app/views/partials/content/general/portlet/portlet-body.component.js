import * as tslib_1 from "tslib";
// Angular
import { Component, HostBinding, Input } from '@angular/core';
var PortletBodyComponent = /** @class */ (function () {
    function PortletBodyComponent() {
        // Public properties
        this.classList = 'kt-portlet__body';
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    PortletBodyComponent.prototype.ngOnInit = function () {
        if (this.class) {
            this.classList += ' ' + this.class;
        }
    };
    tslib_1.__decorate([
        HostBinding('class'),
        tslib_1.__metadata("design:type", Object)
    ], PortletBodyComponent.prototype, "classList", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], PortletBodyComponent.prototype, "class", void 0);
    PortletBodyComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-portlet-body',
            template: "\n\t\t<ng-content></ng-content>"
        })
    ], PortletBodyComponent);
    return PortletBodyComponent;
}());
export { PortletBodyComponent };
//# sourceMappingURL=portlet-body.component.js.map