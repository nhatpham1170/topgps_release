import * as tslib_1 from "tslib";
// Angular
import { Component, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
// RXJS
import { tap } from 'rxjs/operators';
import { merge } from 'rxjs';
// Crud
import { QueryParamsModel } from '../../../../../../core/_base/crud';
// Layout
import { DataTableService } from '../../../../../../core/_base/layout';
import { DataTableDataSource } from './data-table.data-source';
var DataTableComponent = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param dataTableService: DataTableService
     */
    function DataTableComponent(dataTableService) {
        this.dataTableService = dataTableService;
        this.displayedColumns = ['id', 'cManufacture', 'cModel', 'cMileage', 'cColor', 'cPrice', 'cCondition', 'cStatus', 'actions'];
        this.selection = new SelectionModel(true, []);
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    DataTableComponent.prototype.ngOnInit = function () {
        var _this = this;
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(function () { return (_this.paginator.pageIndex = 0); });
        /* Data load will be triggered in two cases:
        - when a pagination event occurs => this.paginator.page
        - when a sort event occurs => this.sort.sortChange
        **/
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(tap(function () {
            _this.loadItems();
        }))
            .subscribe();
        // Init DataSource
        this.dataSource = new DataTableDataSource(this.dataTableService);
        // First load
        this.loadItems(true);
    };
    /**
     * Load items
     *
     * @param firstLoad: boolean
     */
    DataTableComponent.prototype.loadItems = function (firstLoad) {
        if (firstLoad === void 0) { firstLoad = false; }
        var queryParams = new QueryParamsModel({}, this.sort.direction, this.sort.active, this.paginator.pageIndex, firstLoad ? 6 : this.paginator.pageSize);
        this.dataSource.loadItems(queryParams);
        this.selection.clear();
    };
    /* UI */
    /**
     * Returns item status
     *
     * @param status: number
     */
    DataTableComponent.prototype.getItemStatusString = function (status) {
        if (status === void 0) { status = 0; }
        switch (status) {
            case 0:
                return 'Selling';
            case 1:
                return 'Sold';
        }
        return '';
    };
    /**
     * Returens item CSS Class Name by status
     *
     * @param status: number
     */
    DataTableComponent.prototype.getItemCssClassByStatus = function (status) {
        if (status === void 0) { status = 0; }
        switch (status) {
            case 0:
                return 'success';
            case 1:
                return 'info';
        }
        return '';
    };
    /**
     * Returns item condition
     *
     * @param condition: number
     */
    DataTableComponent.prototype.getItemConditionString = function (condition) {
        if (condition === void 0) { condition = 0; }
        switch (condition) {
            case 0:
                return 'New';
            case 1:
                return 'Used';
        }
        return '';
    };
    /**
     * Returns CSS Class name by condition
     *
     * @param condition: number
     */
    DataTableComponent.prototype.getItemCssClassByCondition = function (condition) {
        if (condition === void 0) { condition = 0; }
        switch (condition) {
            case 0:
                return 'success';
            case 1:
                return 'info';
        }
        return '';
    };
    tslib_1.__decorate([
        ViewChild(MatPaginator, { static: true }),
        tslib_1.__metadata("design:type", MatPaginator)
    ], DataTableComponent.prototype, "paginator", void 0);
    tslib_1.__decorate([
        ViewChild(MatSort, { static: true }),
        tslib_1.__metadata("design:type", MatSort)
    ], DataTableComponent.prototype, "sort", void 0);
    DataTableComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-data-table',
            templateUrl: './data-table.component.html',
            styleUrls: ['./data-table.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [DataTableService])
    ], DataTableComponent);
    return DataTableComponent;
}());
export { DataTableComponent };
//# sourceMappingURL=data-table.component.js.map