import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
var Widget26Component = /** @class */ (function () {
    function Widget26Component() {
    }
    Widget26Component.prototype.ngOnInit = function () {
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], Widget26Component.prototype, "value", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", String)
    ], Widget26Component.prototype, "desc", void 0);
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], Widget26Component.prototype, "options", void 0);
    Widget26Component = tslib_1.__decorate([
        Component({
            selector: 'kt-widget26',
            templateUrl: './widget26.component.html',
            styleUrls: ['./widget26.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], Widget26Component);
    return Widget26Component;
}());
export { Widget26Component };
//# sourceMappingURL=widget26.component.js.map