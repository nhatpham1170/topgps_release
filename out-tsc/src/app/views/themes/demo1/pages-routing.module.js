import * as tslib_1 from "tslib";
// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
// Components
import { BaseComponent } from './base/base.component';
import { ErrorPageComponent } from './content/error-page/error-page.component';
// Auth
import { AuthGuard } from '../../../core/auth';
var routes = [
    {
        path: '',
        component: BaseComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: 'dashboard',
                loadChildren: function () { return import('app/views/pages/dashboard/dashboard.module').then(function (m) { return m.DashboardModule; }); }
            },
            {
                path: 'builder',
                loadChildren: function () { return import('app/views/themes/demo1/content/builder/builder.module').then(function (m) { return m.BuilderModule; }); }
            },
            { path: 'error/:type', component: ErrorPageComponent },
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
        ]
    },
];
var PagesRoutingModule = /** @class */ (function () {
    function PagesRoutingModule() {
    }
    PagesRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [RouterModule.forChild(routes)],
            exports: [RouterModule]
        })
    ], PagesRoutingModule);
    return PagesRoutingModule;
}());
export { PagesRoutingModule };
//# sourceMappingURL=pages-routing.module.js.map