import * as tslib_1 from "tslib";
// Angular
import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
// Layout
import { LayoutConfigService } from '../../../../../core/_base/layout';
var BuilderComponent = /** @class */ (function () {
    /**
     * Component constructor
     *
     * @param layoutConfigService: LayoutConfigService
     */
    function BuilderComponent(layoutConfigService) {
        this.layoutConfigService = layoutConfigService;
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    BuilderComponent.prototype.ngOnInit = function () {
        this.model = this.layoutConfigService.getConfig();
    };
    /**
     * Reset preview
     *
     * @param e: Event
     */
    BuilderComponent.prototype.resetPreview = function (e) {
        e.preventDefault();
        this.layoutConfigService.resetConfig();
        location.reload();
    };
    /**
     * Submit preview
     *
     * @param e: Event
     */
    BuilderComponent.prototype.submitPreview = function (e) {
        this.layoutConfigService.setConfig(this.model, true);
        location.reload();
    };
    tslib_1.__decorate([
        ViewChild('form', { static: true }),
        tslib_1.__metadata("design:type", NgForm)
    ], BuilderComponent.prototype, "form", void 0);
    BuilderComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-builder',
            templateUrl: './builder.component.html',
            styleUrls: ['./builder.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [LayoutConfigService])
    ], BuilderComponent);
    return BuilderComponent;
}());
export { BuilderComponent };
//# sourceMappingURL=builder.component.js.map