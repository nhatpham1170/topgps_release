import * as tslib_1 from "tslib";
// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
// Components
import { BaseComponent } from './base/base.component';
import { ErrorPageComponent } from './content/error-page/error-page.component';
// Auth
import { AuthGuard, ModuleGuard } from '../../../core/auth';
var routes = [
    {
        path: '',
        component: BaseComponent,
        canActivate: [ModuleGuard],
        children: [
            {
                path: 'dashboard',
                canActivate: [AuthGuard],
                loadChildren: function () { return import('app/views/pages/dashboard/dashboard.module').then(function (m) { return m.DashboardModule; }); }
            },
            {
                path: 'admin',
                canActivate: [AuthGuard],
                loadChildren: function () { return import('app/views/pages/admin/admin.module').then(function (m) { return m.AdminModule; }); }
            },
            {
                path: 'manage',
                canActivate: [AuthGuard],
                loadChildren: function () { return import('app/views/pages/manage/manage.module').then(function (m) { return m.ManageModule; }); }
            },
            {
                path: 'report',
                canActivate: [AuthGuard],
                loadChildren: function () { return import('app/views/pages/report/report.module').then(function (m) { return m.ReportModule; }); }
            },
            {
                path: 'map',
                canActivate: [AuthGuard],
                loadChildren: function () { return import('app/views/pages/map/map.module').then(function (m) { return m.MapModule; }); }
            },
            {
                path: 'builder',
                loadChildren: function () { return import('app/views/themes/default/content/builder/builder.module').then(function (m) { return m.BuilderModule; }); }
            },
            { path: 'error/:type', component: ErrorPageComponent },
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: '**', redirectTo: '/error/404', pathMatch: 'full' }
        ]
    },
];
var PagesRoutingModule = /** @class */ (function () {
    function PagesRoutingModule() {
    }
    PagesRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [RouterModule.forChild(routes)],
            providers: [ModuleGuard],
            exports: [RouterModule]
        })
    ], PagesRoutingModule);
    return PagesRoutingModule;
}());
export { PagesRoutingModule };
//# sourceMappingURL=pages-routing.module.js.map