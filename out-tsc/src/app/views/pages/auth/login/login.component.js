import * as tslib_1 from "tslib";
// Angular
import { ChangeDetectorRef, Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
// RxJS
import { Subject } from 'rxjs';
import { finalize, takeUntil, tap } from 'rxjs/operators';
// Translate
import { TranslateService } from '@ngx-translate/core';
// Store
import { Store } from '@ngrx/store';
// Auth
import { AuthNoticeService, AuthService, Login } from '../../../../core/auth';
/**
 * ! Just example => Should be removed in development
 */
var DEMO_PARAMS = {
    EMAIL: 'admin@demo.com',
    PASSWORD: 'demo'
};
var LoginComponent = /** @class */ (function () {
    // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/
    /**
     * Component constructor
     *
     * @param router: Router
     * @param auth: AuthService
     * @param authNoticeService: AuthNoticeService
     * @param translate: TranslateService
     * @param store: Store<AppState>
     * @param fb: FormBuilder
     * @param cdr
     * @param route
     */
    function LoginComponent(router, auth, authNoticeService, translate, store, fb, cdr, route) {
        this.router = router;
        this.auth = auth;
        this.authNoticeService = authNoticeService;
        this.translate = translate;
        this.store = store;
        this.fb = fb;
        this.cdr = cdr;
        this.route = route;
        this.loading = false;
        this.errors = [];
        this.unsubscribe = new Subject();
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.initLoginForm();
        // redirect back to the returnUrl before login
        this.route.queryParams.subscribe(function (params) {
            _this.returnUrl = params['returnUrl'] || '/';
        });
    };
    /**
     * On destroy
     */
    LoginComponent.prototype.ngOnDestroy = function () {
        this.authNoticeService.setNotice(null);
        this.unsubscribe.next();
        this.unsubscribe.complete();
        this.loading = false;
    };
    /**
     * Form initalization
     * Default params, validators
     */
    LoginComponent.prototype.initLoginForm = function () {
        // demo message to show
        // if (!this.authNoticeService.onNoticeChanged$.getValue()) {
        // 	const initialNotice = `Use account
        // 	<strong>${DEMO_PARAMS.EMAIL}</strong> and password
        // 	<strong>${DEMO_PARAMS.PASSWORD}</strong> to continue.`;
        // 	this.authNoticeService.setNotice(initialNotice, 'info');
        // }
        this.loginForm = this.fb.group({
            email: [DEMO_PARAMS.EMAIL, Validators.compose([
                    Validators.required,
                    // Validators.email,
                    Validators.minLength(3),
                    Validators.maxLength(320) // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
                ])
            ],
            password: [DEMO_PARAMS.PASSWORD, Validators.compose([
                    Validators.required,
                    Validators.minLength(3),
                    Validators.maxLength(100)
                ])
            ]
        });
    };
    /**
     * Form Submit
     */
    LoginComponent.prototype.submit = function () {
        var _this = this;
        var controls = this.loginForm.controls;
        /** check form */
        if (this.loginForm.invalid) {
            Object.keys(controls).forEach(function (controlName) {
                return controls[controlName].markAsTouched();
            });
            return;
        }
        this.loading = true;
        var authData = {
            email: controls['email'].value,
            password: controls['password'].value
        };
        this.auth
            .login(authData.email, authData.password)
            .pipe(tap(function (data) {
            // user.result.id;										
            // this.store.select(new Login({}))
            // localStorage.setItem('lastUserLogin',user.username);
            // console.log(user);
            // return false;
            var user = data.result;
            if (user.accessToken != null) {
                _this.store.dispatch(new Login({ authToken: user.accessToken }));
                _this.router.navigateByUrl(_this.returnUrl); // Main page
                // console.log(this.store);					
            }
            else {
                _this.authNoticeService.setNotice(_this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN'), 'danger');
            }
        }), takeUntil(this.unsubscribe), finalize(function () {
            _this.loading = false;
            _this.cdr.markForCheck();
        }))
            .subscribe();
    };
    /**
     * Checking control validation
     *
     * @param controlName: string => Equals to formControlName
     * @param validationType: string => Equals to valitors name
     */
    LoginComponent.prototype.isControlHasError = function (controlName, validationType) {
        var control = this.loginForm.controls[controlName];
        if (!control) {
            return false;
        }
        var result = control.hasError(validationType) && (control.dirty || control.touched);
        return result;
    };
    LoginComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-login',
            templateUrl: './login.component.html',
            encapsulation: ViewEncapsulation.None
        }),
        tslib_1.__metadata("design:paramtypes", [Router,
            AuthService,
            AuthNoticeService,
            TranslateService,
            Store,
            FormBuilder,
            ChangeDetectorRef,
            ActivatedRoute])
    ], LoginComponent);
    return LoginComponent;
}());
export { LoginComponent };
//# sourceMappingURL=login.component.js.map