import * as tslib_1 from "tslib";
// Angular
import { Component } from '@angular/core';
import { Router } from '@angular/router';
// Store
import { Store } from '@ngrx/store';
// Auth
import { Logout } from '../../../../core/auth';
var LogoutComponent = /** @class */ (function () {
    // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/
    /**
     * Component constructor
     * @param store: Store<AppState>
     */
    function LogoutComponent(store, router) {
        this.store = store;
        this.router = router;
    }
    /**
     * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
     */
    /**
     * On init
     */
    LogoutComponent.prototype.ngOnInit = function () {
        this.store.dispatch(new Logout());
        this.router.navigate(['/auth/login']);
    };
    LogoutComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-login',
            template: ''
        }),
        tslib_1.__metadata("design:paramtypes", [Store,
            Router])
    ], LogoutComponent);
    return LogoutComponent;
}());
export { LogoutComponent };
//# sourceMappingURL=logout.component.js.map