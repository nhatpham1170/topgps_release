import * as tslib_1 from "tslib";
// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// Partials
import { PartialsModule } from '../partials/partials.module';
// Pages
import { CoreModule } from '../../core/core.module';
var PagesModule = /** @class */ (function () {
    function PagesModule() {
    }
    PagesModule = tslib_1.__decorate([
        NgModule({
            declarations: [],
            exports: [],
            imports: [
                CommonModule,
                HttpClientModule,
                FormsModule,
                CoreModule,
                PartialsModule,
            ],
            providers: []
        })
    ], PagesModule);
    return PagesModule;
}());
export { PagesModule };
//# sourceMappingURL=pages.module.js.map