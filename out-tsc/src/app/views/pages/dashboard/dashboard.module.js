import * as tslib_1 from "tslib";
// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { DashboardComponent } from './dashboard.component';
var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                PartialsModule,
                CoreModule,
                RouterModule.forChild([
                    {
                        path: '',
                        component: DashboardComponent
                    },
                ]),
            ],
            providers: [],
            declarations: [
                DashboardComponent,
            ]
        })
    ], DashboardModule);
    return DashboardModule;
}());
export { DashboardModule };
//# sourceMappingURL=dashboard.module.js.map