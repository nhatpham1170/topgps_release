import * as tslib_1 from "tslib";
// Angular
import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../../../../src/app/core/auth';
import { Store } from '@ngrx/store';
var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(toastr, auth, store) {
        this.toastr = toastr;
        this.auth = auth;
        this.store = store;
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    DashboardComponent.prototype.testToastr = function () {
        this.auth.getUserByToken({ notifyGlobal: true }).subscribe();
        // this.toastr.success('Hello world!', 'Toastr fun!');
        // this.toastr.warning('Deleted employee',"EMP");
    };
    DashboardComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-dashboard',
            templateUrl: './dashboard.component.html',
            styleUrls: ['dashboard.component.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [ToastrService, AuthService,
            Store])
    ], DashboardComponent);
    return DashboardComponent;
}());
export { DashboardComponent };
//# sourceMappingURL=dashboard.component.js.map