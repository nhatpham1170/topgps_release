import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var DeviceTypeComponent = /** @class */ (function () {
    function DeviceTypeComponent() {
    }
    DeviceTypeComponent.prototype.ngOnInit = function () {
    };
    DeviceTypeComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-device-type',
            templateUrl: './device-type.component.html',
            styleUrls: ['./device-type.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], DeviceTypeComponent);
    return DeviceTypeComponent;
}());
export { DeviceTypeComponent };
//# sourceMappingURL=device-type.component.js.map