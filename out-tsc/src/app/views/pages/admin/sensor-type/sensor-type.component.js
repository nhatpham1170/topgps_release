import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var SensorTypeComponent = /** @class */ (function () {
    function SensorTypeComponent() {
    }
    SensorTypeComponent.prototype.ngOnInit = function () {
    };
    SensorTypeComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-sensor-type',
            templateUrl: './sensor-type.component.html',
            styleUrls: ['./sensor-type.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], SensorTypeComponent);
    return SensorTypeComponent;
}());
export { SensorTypeComponent };
//# sourceMappingURL=sensor-type.component.js.map