import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var SensorTemplateComponent = /** @class */ (function () {
    function SensorTemplateComponent() {
    }
    SensorTemplateComponent.prototype.ngOnInit = function () {
    };
    SensorTemplateComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-sensor-template',
            templateUrl: './sensor-template.component.html',
            styleUrls: ['./sensor-template.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], SensorTemplateComponent);
    return SensorTemplateComponent;
}());
export { SensorTemplateComponent };
//# sourceMappingURL=sensor-template.component.js.map