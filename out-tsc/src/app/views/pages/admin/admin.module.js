import * as tslib_1 from "tslib";
// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
// Translate
import { TranslateModule } from '@ngx-translate/core';
// Components
import { PermissionComponent } from './permission/permission.component';
import { RoleComponent } from './role/role.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { SimTypeComponent } from './sim-type/sim-type.component';
import { DeviceTypeComponent } from './device-type/device-type.component';
import { SensorTypeComponent } from './sensor-type/sensor-type.component';
import { IconTypeComponent } from './icon-type/icon-type.component';
import { AdminComponent } from './admin.component';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { PageTemplateComponent } from './page-template/page-template.component';
var routes = [
    {
        path: '',
        component: AdminComponent,
        children: [
            {
                path: 'permission',
                component: PermissionComponent,
            },
            {
                path: 'role',
                component: RoleComponent
            },
            {
                path: 'login-page',
                component: LoginPageComponent,
            },
            {
                path: 'device-type',
                component: DeviceTypeComponent,
            },
            {
                path: 'sim-type',
                component: SimTypeComponent,
            },
            {
                path: 'sensor-type',
                component: SensorTypeComponent,
            },
            {
                path: 'icon-type',
                component: IconTypeComponent,
            },
            {
                path: 'page-template',
                component: PageTemplateComponent,
            },
        ]
    }
];
var AdminModule = /** @class */ (function () {
    function AdminModule() {
    }
    AdminModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                RouterModule.forChild(routes),
                TranslateModule.forChild(),
                NgbDropdownModule,
            ],
            declarations: [
                PermissionComponent,
                RoleComponent,
                LoginPageComponent,
                SimTypeComponent,
                DeviceTypeComponent,
                SensorTypeComponent,
                IconTypeComponent,
                AdminComponent,
                PageTemplateComponent
            ],
        })
    ], AdminModule);
    return AdminModule;
}());
export { AdminModule };
//# sourceMappingURL=admin.module.js.map