import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var RoleComponent = /** @class */ (function () {
    function RoleComponent() {
    }
    RoleComponent.prototype.ngOnInit = function () {
    };
    RoleComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-role',
            templateUrl: './role.component.html',
            styleUrls: ['./role.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], RoleComponent);
    return RoleComponent;
}());
export { RoleComponent };
//# sourceMappingURL=role.component.js.map