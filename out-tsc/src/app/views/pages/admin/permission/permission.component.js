import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var PermissionComponent = /** @class */ (function () {
    function PermissionComponent() {
    }
    PermissionComponent.prototype.ngOnInit = function () {
    };
    PermissionComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-permission',
            templateUrl: './permission.component.html',
            styleUrls: ['./permission.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], PermissionComponent);
    return PermissionComponent;
}());
export { PermissionComponent };
//# sourceMappingURL=permission.component.js.map