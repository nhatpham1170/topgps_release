import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var LoginPageComponent = /** @class */ (function () {
    function LoginPageComponent() {
    }
    LoginPageComponent.prototype.ngOnInit = function () {
    };
    LoginPageComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-login-page',
            templateUrl: './login-page.component.html',
            styleUrls: ['./login-page.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], LoginPageComponent);
    return LoginPageComponent;
}());
export { LoginPageComponent };
//# sourceMappingURL=login-page.component.js.map