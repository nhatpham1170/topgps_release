import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var IconTypeComponent = /** @class */ (function () {
    function IconTypeComponent() {
    }
    IconTypeComponent.prototype.ngOnInit = function () {
    };
    IconTypeComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-icon-type',
            templateUrl: './icon-type.component.html',
            styleUrls: ['./icon-type.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], IconTypeComponent);
    return IconTypeComponent;
}());
export { IconTypeComponent };
//# sourceMappingURL=icon-type.component.js.map