import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { DataTable } from './../../../../../../src/app/core/_base/layout/models/datatable.model';
var SimTypeComponent = /** @class */ (function () {
    function SimTypeComponent() {
        this.isLoading = false;
        this.dataTable = new DataTable();
        this.tableConfig = {
            pagination: [
                10, 20, 30, 50
            ],
            columns: [
                {
                    title: '#',
                    field: 'no',
                    allowSort: false,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '40px' },
                    class: ''
                },
                {
                    title: 'Key',
                    field: 'nameKey',
                    allowSort: true,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '182px' },
                    class: ''
                },
                {
                    title: 'Name',
                    field: 'nameKey2',
                    allowSort: false,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '182px' },
                    class: ''
                },
                {
                    title: 'Hire Date',
                    field: 'createdDate',
                    allowSort: false,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '182px' },
                    class: ''
                },
                {
                    title: 'Sort Order',
                    field: 'sortOrder',
                    allowSort: true,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '182px' },
                    class: ''
                },
                {
                    title: 'Actions',
                    field: 'action',
                    allowSort: false,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '110px' },
                    class: ''
                },
            ]
        };
        this.data = {
            data: [
                {
                    "id": "9",
                    "nameKey": "sim.Mobiphone3G",
                    "createdDate": null,
                    "sortOrder": null
                },
                {
                    "id": "3",
                    "nameKey": "sim.MobiphonePaypaid",
                    "createdDate": null,
                    "sortOrder": null
                },
                {
                    "id": "6",
                    "nameKey": "sim.MobiphonePostpaid",
                    "createdDate": null,
                    "sortOrder": null
                },
                {
                    "id": "1",
                    "nameKey": "sim.Viettel3G",
                    "createdDate": null,
                    "sortOrder": null
                },
                {
                    "id": "4",
                    "nameKey": "sim.ViettelPaypaid",
                    "createdDate": null,
                    "sortOrder": null
                },
                {
                    "id": "7",
                    "nameKey": "sim.ViettelPostpaid",
                    "createdDate": null,
                    "sortOrder": null
                },
                {
                    "id": "8",
                    "nameKey": "sim.Vinaphone3G",
                    "createdDate": null,
                    "sortOrder": null
                },
                {
                    "id": "2",
                    "nameKey": "sim.VinaphonePaypaid",
                    "createdDate": "2019-08-07 00:00:00",
                    "sortOrder": null
                },
                {
                    "id": "5",
                    "nameKey": "sim.VinaphonePostpaid",
                    "createdDate": null,
                    "sortOrder": null
                },
                {
                    "id": "11",
                    "nameKey": "sim.VNETDataM1",
                    "createdDate": null,
                    "sortOrder": null
                },
                {
                    "id": "12",
                    "nameKey": "sim.VNETDataM2",
                    "createdDate": null,
                    "sortOrder": null
                },
                {
                    "id": "13",
                    "nameKey": "sim.Unknown",
                    "createdDate": null,
                    "sortOrder": "1"
                }
            ],
            meta: {
                field: "name",
                page: 1,
                pages: 5,
                perpage: 2,
                sort: "asc",
                total: 20,
            }
        };
    }
    SimTypeComponent.prototype.ngOnInit = function () {
        // this.dataTable.data=this.data.data;	
        this.dataTable.init({ data: this.data.data, totalRecod: 23, paginationSelect: [1, 2, 5, 10, 20], columns: this.tableConfig.columns });
        // this.dataTable.paginations.pageSize
        // this.dataTable.paginations.pageSize=10;	
        var _this = this;
        this.dataTable.eventUpdate.subscribe(function (option) {
            _this.getData(option);
            setTimeout(function () {
                _this.dataTable.update(option);
            }, 1000);
        });
        console.log(this.dataTable.config.paginationSelect);
        console.log(this.dataTable);
        $('.bootstrap-select').selectpicker();
    };
    SimTypeComponent.prototype.getData = function (option) {
        console.log("option");
        console.log(option);
        // console.log(this.data.data);
        return this.data.data;
    };
    SimTypeComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-sim-type',
            templateUrl: './sim-type.component.html',
            styleUrls: ['./sim-type.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], SimTypeComponent);
    return SimTypeComponent;
}());
export { SimTypeComponent };
//# sourceMappingURL=sim-type.component.js.map