import * as tslib_1 from "tslib";
// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
// import { FormsModule } from '@angular/forms';
// import { MatTabsModule } from '@angular/material';
// Builder component
import { BuilderComponent } from './builder.component';
var BuilderModule = /** @class */ (function () {
    function BuilderModule() {
    }
    BuilderModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                // PartialsModule,
                // FormsModule,
                // MatTabsModule,
                // CoreModule,
                // PerfectScrollbarModule,
                // HighlightModule,
                RouterModule.forChild([
                    {
                        path: '',
                        component: BuilderComponent
                    }
                ]),
            ],
            // providers: [],
            declarations: [BuilderComponent]
        })
    ], BuilderModule);
    return BuilderModule;
}());
export { BuilderModule };
//# sourceMappingURL=builder.module.js.map