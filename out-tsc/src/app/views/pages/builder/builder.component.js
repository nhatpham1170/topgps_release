import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var BuilderComponent = /** @class */ (function () {
    function BuilderComponent() {
    }
    BuilderComponent.prototype.ngOnInit = function () {
    };
    BuilderComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-builder',
            templateUrl: './builder.component.html',
            styleUrls: ['./builder.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], BuilderComponent);
    return BuilderComponent;
}());
export { BuilderComponent };
//# sourceMappingURL=builder.component.js.map