import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var DeviceCommandComponent = /** @class */ (function () {
    function DeviceCommandComponent() {
    }
    DeviceCommandComponent.prototype.ngOnInit = function () {
    };
    DeviceCommandComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-device-command',
            templateUrl: './device-command.component.html',
            styleUrls: ['./device-command.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], DeviceCommandComponent);
    return DeviceCommandComponent;
}());
export { DeviceCommandComponent };
//# sourceMappingURL=device-command.component.js.map