import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var ManageComponent = /** @class */ (function () {
    function ManageComponent() {
    }
    ManageComponent.prototype.ngOnInit = function () {
    };
    ManageComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-manage',
            templateUrl: './manage.component.html',
            styleUrls: ['./manage.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], ManageComponent);
    return ManageComponent;
}());
export { ManageComponent };
//# sourceMappingURL=manage.component.js.map