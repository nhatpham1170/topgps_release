import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
// Component
import { DeviceManageComponent } from './device-manage/device-manage.component';
import { DeviceCommandComponent } from './device-command/device-command.component';
import { DeviceGroupComponent } from './device-group/device-group.component';
import { UserManageComponent } from './user-manage/user-manage.component';
import { ManageComponent } from './manage.component';
import { ModuleGuard } from '../../../../../src/app/core/auth';
var routes = [{
        path: '',
        component: ManageComponent,
        children: [
            {
                path: 'devices',
                component: DeviceManageComponent,
                data: {
                    permisison: 'ROLE_device.action.command',
                }
            },
            {
                path: 'device-command',
                component: DeviceCommandComponent
            },
            {
                path: 'device-group',
                component: DeviceGroupComponent
            },
            {
                path: 'users',
                component: UserManageComponent
            },
        ]
    }];
var ManageModule = /** @class */ (function () {
    function ManageModule() {
    }
    ManageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                RouterModule.forChild(routes)
            ],
            providers: [ModuleGuard],
            declarations: [
                ManageComponent,
                DeviceManageComponent,
                DeviceCommandComponent,
                DeviceGroupComponent,
                UserManageComponent
            ]
        })
    ], ManageModule);
    return ManageModule;
}());
export { ManageModule };
//# sourceMappingURL=manage.module.js.map