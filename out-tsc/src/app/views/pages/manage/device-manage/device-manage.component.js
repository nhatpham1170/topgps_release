import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var DeviceManageComponent = /** @class */ (function () {
    function DeviceManageComponent() {
    }
    DeviceManageComponent.prototype.ngOnInit = function () {
    };
    DeviceManageComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-device-manage',
            templateUrl: './device-manage.component.html',
            styleUrls: ['./device-manage.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], DeviceManageComponent);
    return DeviceManageComponent;
}());
export { DeviceManageComponent };
//# sourceMappingURL=device-manage.component.js.map