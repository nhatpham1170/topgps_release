import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var UserManageComponent = /** @class */ (function () {
    function UserManageComponent() {
    }
    UserManageComponent.prototype.ngOnInit = function () {
    };
    UserManageComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-user-manage',
            templateUrl: './user-manage.component.html',
            styleUrls: ['./user-manage.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], UserManageComponent);
    return UserManageComponent;
}());
export { UserManageComponent };
//# sourceMappingURL=user-manage.component.js.map