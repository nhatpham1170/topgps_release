import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var DeviceGroupComponent = /** @class */ (function () {
    function DeviceGroupComponent() {
    }
    DeviceGroupComponent.prototype.ngOnInit = function () {
    };
    DeviceGroupComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-device-group',
            templateUrl: './device-group.component.html',
            styleUrls: ['./device-group.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], DeviceGroupComponent);
    return DeviceGroupComponent;
}());
export { DeviceGroupComponent };
//# sourceMappingURL=device-group.component.js.map