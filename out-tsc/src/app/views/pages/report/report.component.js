import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var ReportComponent = /** @class */ (function () {
    function ReportComponent() {
    }
    ReportComponent.prototype.ngOnInit = function () {
    };
    ReportComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-report',
            templateUrl: './report.component.html',
            styleUrls: ['./report.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], ReportComponent);
    return ReportComponent;
}());
export { ReportComponent };
//# sourceMappingURL=report.component.js.map