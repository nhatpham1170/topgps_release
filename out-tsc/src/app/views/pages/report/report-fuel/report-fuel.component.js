import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var ReportFuelComponent = /** @class */ (function () {
    function ReportFuelComponent() {
    }
    ReportFuelComponent.prototype.ngOnInit = function () {
    };
    ReportFuelComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-report-fuel',
            templateUrl: './report-fuel.component.html',
            styleUrls: ['./report-fuel.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], ReportFuelComponent);
    return ReportFuelComponent;
}());
export { ReportFuelComponent };
//# sourceMappingURL=report-fuel.component.js.map