import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReportComponent } from './report.component';
import { ReportFuelComponent } from './report-fuel/report-fuel.component';
var routes = [{
        path: '',
        component: ReportComponent,
        children: [
            {
                path: 'fuel',
                component: ReportFuelComponent
            }
        ]
    }];
var ReportModule = /** @class */ (function () {
    function ReportModule() {
    }
    ReportModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                RouterModule.forChild(routes),
            ],
            declarations: [ReportComponent, ReportFuelComponent]
        })
    ], ReportModule);
    return ReportModule;
}());
export { ReportModule };
//# sourceMappingURL=report.module.js.map