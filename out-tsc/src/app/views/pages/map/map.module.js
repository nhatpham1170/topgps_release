import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MapComponent } from './map.component';
import { AuthGuard, ModuleGuard } from '../../../../../src/app/core/auth';
var routes = [{
        path: '',
        component: MapComponent,
        canActivate: [ModuleGuard],
        data: {
            permisison: 'ROLE_device.action.command',
        },
    }];
var MapModule = /** @class */ (function () {
    function MapModule() {
    }
    MapModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                RouterModule.forChild(routes),
            ],
            providers: [AuthGuard, ModuleGuard],
            declarations: [MapComponent]
        })
    ], MapModule);
    return MapModule;
}());
export { MapModule };
//# sourceMappingURL=map.module.js.map