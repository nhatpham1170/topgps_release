import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var MapComponent = /** @class */ (function () {
    function MapComponent() {
    }
    MapComponent.prototype.ngOnInit = function () {
    };
    MapComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-map',
            templateUrl: './map.component.html',
            styleUrls: ['./map.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], MapComponent);
    return MapComponent;
}());
export { MapComponent };
//# sourceMappingURL=map.component.js.map