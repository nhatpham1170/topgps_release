import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
var MapModule = /** @class */ (function () {
    function MapModule() {
    }
    MapModule = tslib_1.__decorate([
        NgModule({
            declarations: [],
            imports: [
                CommonModule
            ]
        })
    ], MapModule);
    return MapModule;
}());
export { MapModule };
//# sourceMappingURL=map.module.js.map