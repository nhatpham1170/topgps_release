import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
var MapComponent = /** @class */ (function () {
    function MapComponent(route) {
        this.route = route;
    }
    MapComponent.prototype.ngOnInit = function () {
        this.route.data.subscribe(function (x) { return console.log(x); });
    };
    MapComponent = tslib_1.__decorate([
        Component({
            selector: 'kt-map',
            templateUrl: './map.component.html',
            styleUrls: ['./map.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [ActivatedRoute])
    ], MapComponent);
    return MapComponent;
}());
export { MapComponent };
//# sourceMappingURL=map.component.js.map