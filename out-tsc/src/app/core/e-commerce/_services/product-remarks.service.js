import * as tslib_1 from "tslib";
// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// CRUD
import { HttpUtilsService } from '../../_base/crud';
var API_PRODUCTREMARKS_URL = 'api/productRemarks';
// Real REST API
var ProductRemarksService = /** @class */ (function () {
    function ProductRemarksService(http, httpUtils) {
        this.http = http;
        this.httpUtils = httpUtils;
    }
    // CREATE =>  POST: add a new product remark to the server
    ProductRemarksService.prototype.createProductRemark = function (productRemark) {
        // Note: Add headers if needed (tokens/bearer)
        var httpHeaders = this.httpUtils.getHTTPHeaders();
        return this.http.post(API_PRODUCTREMARKS_URL, productRemark, { headers: httpHeaders });
    };
    // READ
    // Server should return filtered remarks by productId
    ProductRemarksService.prototype.getAllProductRemarksByProductId = function (productId) {
        var url = API_PRODUCTREMARKS_URL + '?productId=' + productId;
        return this.http.get(url);
    };
    ProductRemarksService.prototype.getProductRemarkById = function (productRemarkId) {
        return this.http.get(API_PRODUCTREMARKS_URL + ("/" + productRemarkId));
    };
    // Server should return sorted/filtered remarks and merge with items from state
    ProductRemarksService.prototype.findProductRemarks = function (queryParams, productId) {
        var url = API_PRODUCTREMARKS_URL + '/find?productId=' + productId;
        // Note: Add headers if needed (tokens/bearer)
        var httpHeaders = this.httpUtils.getHTTPHeaders();
        var body = {
            query: queryParams
        };
        return this.http.post(url, body, { headers: httpHeaders });
    };
    // UPDATE => PUT: update the product remark
    ProductRemarksService.prototype.updateProductRemark = function (productRemark) {
        // Note: Add headers if needed (tokens/bearer)
        var httpHeaders = this.httpUtils.getHTTPHeaders();
        return this.http.put(API_PRODUCTREMARKS_URL, productRemark, { headers: httpHeaders });
    };
    // DELETE => delete the product remark
    ProductRemarksService.prototype.deleteProductRemark = function (productRemarkId) {
        var url = API_PRODUCTREMARKS_URL + "/" + productRemarkId;
        return this.http.delete(url);
    };
    ProductRemarksService.prototype.deleteProductRemarks = function (ids) {
        if (ids === void 0) { ids = []; }
        var url = API_PRODUCTREMARKS_URL + '/deleteProductRemarks';
        var httpHeaders = this.httpUtils.getHTTPHeaders();
        var body = { productRemarkIdsForDelete: ids };
        return this.http.put(url, body, { headers: httpHeaders });
    };
    ProductRemarksService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [HttpClient, HttpUtilsService])
    ], ProductRemarksService);
    return ProductRemarksService;
}());
export { ProductRemarksService };
//# sourceMappingURL=product-remarks.service.js.map