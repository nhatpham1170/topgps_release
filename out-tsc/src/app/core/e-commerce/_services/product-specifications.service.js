import * as tslib_1 from "tslib";
// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// CRUD
import { HttpUtilsService } from '../../_base/crud';
import { SPECIFICATIONS_DICTIONARY } from '../_consts/specification.dictionary';
var API_PRODUCTSPECS_URL = 'api/productSpecs';
// Real REST API
var ProductSpecificationsService = /** @class */ (function () {
    function ProductSpecificationsService(http, httpUtils) {
        this.http = http;
        this.httpUtils = httpUtils;
    }
    // CREATE =>  POST: add a new product specification to the server
    ProductSpecificationsService.prototype.createProductSpec = function (productSpec) {
        // Note: Add headers if needed (tokens/bearer)
        var httpHeaders = this.httpUtils.getHTTPHeaders();
        return this.http.post(API_PRODUCTSPECS_URL, productSpec, { headers: httpHeaders });
    };
    // READ
    // Server should return filtered specs by productId
    ProductSpecificationsService.prototype.getAllProductSpecsByProductId = function (productId) {
        var url = API_PRODUCTSPECS_URL + '?productId=' + productId;
        return this.http.get(url);
    };
    ProductSpecificationsService.prototype.getProductSpecById = function (productSpecId) {
        return this.http.get(API_PRODUCTSPECS_URL + ("/" + productSpecId));
    };
    // Server should return sorted/filtered specs and merge with items from state
    ProductSpecificationsService.prototype.findProductSpecs = function (queryParams, productId) {
        var url = API_PRODUCTSPECS_URL + '/find?productId=' + productId;
        // Note: Add headers if needed (tokens/bearer)
        var httpHeaders = this.httpUtils.getHTTPHeaders();
        var body = {
            state: queryParams
        };
        return this.http.post(url, body, { headers: httpHeaders });
    };
    // UPDATE => PUT: update the product specification on the server
    ProductSpecificationsService.prototype.updateProductSpec = function (productSpec) {
        return this.http.put(API_PRODUCTSPECS_URL, productSpec, { headers: this.httpUtils.getHTTPHeaders() });
    };
    // DELETE => delete the product specification from the server
    ProductSpecificationsService.prototype.deleteProductSpec = function (productSpecId) {
        var url = API_PRODUCTSPECS_URL + "/" + productSpecId;
        return this.http.delete(url);
    };
    ProductSpecificationsService.prototype.deleteProductSpecifications = function (ids) {
        if (ids === void 0) { ids = []; }
        var url = API_PRODUCTSPECS_URL + '/deleteProductSpecifications';
        var httpHeaders = this.httpUtils.getHTTPHeaders();
        var body = { productSpecificationIdsForDelete: ids };
        return this.http.put(url, body, { headers: httpHeaders });
    };
    ProductSpecificationsService.prototype.getSpecs = function () {
        return SPECIFICATIONS_DICTIONARY;
    };
    ProductSpecificationsService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [HttpClient, HttpUtilsService])
    ], ProductSpecificationsService);
    return ProductSpecificationsService;
}());
export { ProductSpecificationsService };
//# sourceMappingURL=product-specifications.service.js.map