import * as tslib_1 from "tslib";
// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// CRUD
import { HttpUtilsService } from '../../_base/crud';
var API_ORDERS_URL = 'api/orders';
var OrdersService = /** @class */ (function () {
    function OrdersService(http, httpUtils) {
        this.http = http;
        this.httpUtils = httpUtils;
        this.httpOptions = this.httpUtils.getHTTPHeaders();
    }
    OrdersService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [HttpClient,
            HttpUtilsService])
    ], OrdersService);
    return OrdersService;
}());
export { OrdersService };
//# sourceMappingURL=orders.service.js.map