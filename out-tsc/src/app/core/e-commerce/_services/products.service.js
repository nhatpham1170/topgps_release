import * as tslib_1 from "tslib";
// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// RxJS
import { BehaviorSubject } from 'rxjs';
// CRUD
import { HttpUtilsService, QueryParamsModel } from '../../_base/crud';
var API_PRODUCTS_URL = 'api/products';
// Real REST API
var ProductsService = /** @class */ (function () {
    function ProductsService(http, httpUtils) {
        this.http = http;
        this.httpUtils = httpUtils;
        this.lastFilter$ = new BehaviorSubject(new QueryParamsModel({}, 'asc', '', 0, 10));
    }
    // CREATE =>  POST: add a new product to the server
    ProductsService.prototype.createProduct = function (product) {
        var httpHeaders = this.httpUtils.getHTTPHeaders();
        return this.http.post(API_PRODUCTS_URL, product, { headers: httpHeaders });
    };
    // READ
    ProductsService.prototype.getAllProducts = function () {
        return this.http.get(API_PRODUCTS_URL);
    };
    ProductsService.prototype.getProductById = function (productId) {
        return this.http.get(API_PRODUCTS_URL + ("/" + productId));
    };
    // Server should return filtered/sorted result
    ProductsService.prototype.findProducts = function (queryParams) {
        // Note: Add headers if needed (tokens/bearer)
        var httpHeaders = this.httpUtils.getHTTPHeaders();
        var httpParams = this.httpUtils.getFindHTTPParams(queryParams);
        var url = API_PRODUCTS_URL + '/find';
        return this.http.get(url, {
            headers: httpHeaders,
            params: httpParams
        });
    };
    // UPDATE => PUT: update the product on the server
    ProductsService.prototype.updateProduct = function (product) {
        // Note: Add headers if needed (tokens/bearer)
        var httpHeaders = this.httpUtils.getHTTPHeaders();
        return this.http.put(API_PRODUCTS_URL, product, { headers: httpHeaders });
    };
    // UPDATE Status
    // Comment this when you start work with real server
    // This code imitates server calls
    ProductsService.prototype.updateStatusForProduct = function (products, status) {
        var httpHeaders = this.httpUtils.getHTTPHeaders();
        var body = {
            productsForUpdate: products,
            newStatus: status
        };
        var url = API_PRODUCTS_URL + '/updateStatus';
        return this.http.put(url, body, { headers: httpHeaders });
    };
    // DELETE => delete the product from the server
    ProductsService.prototype.deleteProduct = function (productId) {
        var url = API_PRODUCTS_URL + "/" + productId;
        return this.http.delete(url);
    };
    ProductsService.prototype.deleteProducts = function (ids) {
        if (ids === void 0) { ids = []; }
        var url = API_PRODUCTS_URL + '/delete';
        var httpHeaders = this.httpUtils.getHTTPHeaders();
        var body = { prdocutIdsForDelete: ids };
        return this.http.put(url, body, { headers: httpHeaders });
    };
    ProductsService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [HttpClient,
            HttpUtilsService])
    ], ProductsService);
    return ProductsService;
}());
export { ProductsService };
//# sourceMappingURL=products.service.js.map