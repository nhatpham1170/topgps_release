import * as tslib_1 from "tslib";
// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// RxJS
import { forkJoin, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
// CRUD
import { HttpUtilsService } from '../../_base/crud';
import { SPECIFICATIONS_DICTIONARY } from '../_consts/specification.dictionary';
var API_PRODUCTSPECS_URL = 'api/productSpecs';
// Fake REST API (Mock)
// This code emulates server calls
var ProductSpecificationsService = /** @class */ (function () {
    function ProductSpecificationsService(http, httpUtils) {
        this.http = http;
        this.httpUtils = httpUtils;
    }
    // CREATE =>  POST: add a new product specification to the server
    ProductSpecificationsService.prototype.createProductSpec = function (productSpec) {
        // Note: Add headers if needed (tokens/bearer)
        var httpHeaders = this.httpUtils.getHTTPHeaders();
        return this.http.post(API_PRODUCTSPECS_URL, productSpec, { headers: httpHeaders });
    };
    // READ
    ProductSpecificationsService.prototype.getAllProductSpecsByProductId = function (productId) {
        var prodSpecs = this.http
            .get(API_PRODUCTSPECS_URL)
            .pipe(map(function (productSpecifications) {
            return productSpecifications.filter(function (ps) { return ps.carId === productId; });
        }));
        return prodSpecs.pipe(map(function (res) {
            var _prodSpecs = res;
            var result = [];
            _prodSpecs.forEach(function (item) {
                var _item = Object.assign({}, item);
                var specName = SPECIFICATIONS_DICTIONARY[_item.specId];
                if (specName) {
                    _item._specificationName = specName;
                }
                result.push(_item);
            });
            return result;
        }));
    };
    ProductSpecificationsService.prototype.getProductSpecById = function (productSpecId) {
        return this.http.get(API_PRODUCTSPECS_URL + ("/" + productSpecId));
    };
    ProductSpecificationsService.prototype.findProductSpecs = function (queryParams, productId) {
        var _this = this;
        return this.getAllProductSpecsByProductId(productId).pipe(mergeMap(function (res) {
            var result = _this.httpUtils.baseFilter(res, queryParams, []);
            return of(result);
        }));
    };
    // UPDATE => PUT: update the product specification on the server
    ProductSpecificationsService.prototype.updateProductSpec = function (productSpec) {
        return this.http.put(API_PRODUCTSPECS_URL, productSpec, {
            headers: this.httpUtils.getHTTPHeaders()
        });
    };
    // DELETE => delete the product specification from the server
    ProductSpecificationsService.prototype.deleteProductSpec = function (productSpecId) {
        var url = API_PRODUCTSPECS_URL + "/" + productSpecId;
        return this.http.delete(url);
    };
    ProductSpecificationsService.prototype.deleteProductSpecifications = function (ids) {
        if (ids === void 0) { ids = []; }
        var tasks$ = [];
        var length = ids.length;
        // tslint:disable-next-line:prefer-const
        for (var i = 0; i < length; i++) {
            tasks$.push(this.deleteProductSpec(ids[i]));
        }
        return forkJoin(tasks$);
    };
    ProductSpecificationsService.prototype.getSpecs = function () {
        return SPECIFICATIONS_DICTIONARY;
    };
    ProductSpecificationsService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [HttpClient, HttpUtilsService])
    ], ProductSpecificationsService);
    return ProductSpecificationsService;
}());
export { ProductSpecificationsService };
//# sourceMappingURL=product-specifications.service.fake.js.map