import * as tslib_1 from "tslib";
var _a;
// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { createEntityAdapter } from '@ngrx/entity';
// Actions
import { ProductRemarkActionTypes } from '../_actions/product-remark.actions';
import { QueryParamsModel } from '../../_base/crud';
export var adapter = createEntityAdapter();
export var initialProductRemarksState = adapter.getInitialState({
    loading: false,
    totalCount: 0,
    productId: undefined,
    lastCreatedProductRemarkId: undefined,
    lastQuery: new QueryParamsModel({}),
    showInitWaitingMessage: true
});
export function productRemarksReducer(state, action) {
    if (state === void 0) { state = initialProductRemarksState; }
    switch (action.type) {
        case ProductRemarkActionTypes.ProductRemarksPageToggleLoading:
            return tslib_1.__assign({}, state, { loading: action.payload.isLoading, lastCreatedProductRemarkId: undefined });
        case ProductRemarkActionTypes.ProductRemarkOnServerCreated:
            return tslib_1.__assign({}, state, { loading: true });
        case ProductRemarkActionTypes.ProductRemarkCreated:
            return adapter.addOne(action.payload.productRemark, tslib_1.__assign({}, state, { lastCreatedProductRemarkId: action.payload.productRemark.id }));
        case ProductRemarkActionTypes.ProductRemarkUpdated:
            return adapter.updateOne(action.payload.partialProductRemark, state);
        case ProductRemarkActionTypes.ProductRemarkStoreUpdated:
            return adapter.updateOne(action.payload.productRemark, state);
        case ProductRemarkActionTypes.OneProductRemarkDeleted:
            return adapter.removeOne(action.payload.id, state);
        case ProductRemarkActionTypes.ManyProductRemarksDeleted:
            return adapter.removeMany(action.payload.ids, state);
        case ProductRemarkActionTypes.ProductRemarksPageCancelled:
            return tslib_1.__assign({}, state, { totalCount: 0, loading: false, productId: undefined, lastQuery: new QueryParamsModel({}) });
        case ProductRemarkActionTypes.ProductRemarksPageRequested:
            return tslib_1.__assign({}, state, { totalCount: 0, loading: true, productId: action.payload.productId, lastQuery: action.payload.page });
        case ProductRemarkActionTypes.ProductRemarksPageLoaded:
            return adapter.addMany(action.payload.productRemarks, tslib_1.__assign({}, initialProductRemarksState, { totalCount: action.payload.totalCount, loading: false, productId: state.productId, lastQuery: state.lastQuery, showInitWaitingMessage: false }));
        default:
            return state;
    }
}
export var getProductRemarlState = createFeatureSelector('productRemarks');
export var selectAll = (_a = adapter.getSelectors(), _a.selectAll), selectEntities = _a.selectEntities, selectIds = _a.selectIds, selectTotal = _a.selectTotal;
//# sourceMappingURL=product-remark.reducers.js.map