import * as tslib_1 from "tslib";
var _a;
// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { createEntityAdapter } from '@ngrx/entity';
// Actions
import { CustomerActionTypes } from '../_actions/customer.actions';
import { QueryParamsModel } from '../../_base/crud';
export var adapter = createEntityAdapter();
export var initialCustomersState = adapter.getInitialState({
    customerForEdit: null,
    listLoading: false,
    actionsloading: false,
    totalCount: 0,
    lastCreatedCustomerId: undefined,
    lastQuery: new QueryParamsModel({}),
    showInitWaitingMessage: true
});
export function customersReducer(state, action) {
    if (state === void 0) { state = initialCustomersState; }
    switch (action.type) {
        case CustomerActionTypes.CustomersPageToggleLoading: {
            return tslib_1.__assign({}, state, { listLoading: action.payload.isLoading, lastCreatedCustomerId: undefined });
        }
        case CustomerActionTypes.CustomerActionToggleLoading: {
            return tslib_1.__assign({}, state, { actionsloading: action.payload.isLoading });
        }
        case CustomerActionTypes.CustomerOnServerCreated: return tslib_1.__assign({}, state);
        case CustomerActionTypes.CustomerCreated: return adapter.addOne(action.payload.customer, tslib_1.__assign({}, state, { lastCreatedCustomerId: action.payload.customer.id }));
        case CustomerActionTypes.CustomerUpdated: return adapter.updateOne(action.payload.partialCustomer, state);
        case CustomerActionTypes.CustomersStatusUpdated: {
            var _partialCustomers = [];
            // tslint:disable-next-line:prefer-const
            for (var i = 0; i < action.payload.customers.length; i++) {
                _partialCustomers.push({
                    id: action.payload.customers[i].id,
                    changes: {
                        status: action.payload.status
                    }
                });
            }
            return adapter.updateMany(_partialCustomers, state);
        }
        case CustomerActionTypes.OneCustomerDeleted: return adapter.removeOne(action.payload.id, state);
        case CustomerActionTypes.ManyCustomersDeleted: return adapter.removeMany(action.payload.ids, state);
        case CustomerActionTypes.CustomersPageCancelled: {
            return tslib_1.__assign({}, state, { listLoading: false, lastQuery: new QueryParamsModel({}) });
        }
        case CustomerActionTypes.CustomersPageLoaded: {
            return adapter.addMany(action.payload.customers, tslib_1.__assign({}, initialCustomersState, { totalCount: action.payload.totalCount, listLoading: false, lastQuery: action.payload.page, showInitWaitingMessage: false }));
        }
        default: return state;
    }
}
export var getCustomerState = createFeatureSelector('customers');
export var selectAll = (_a = adapter.getSelectors(), _a.selectAll), selectEntities = _a.selectEntities, selectIds = _a.selectIds, selectTotal = _a.selectTotal;
//# sourceMappingURL=customer.reducers.js.map