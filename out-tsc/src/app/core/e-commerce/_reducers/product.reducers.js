import * as tslib_1 from "tslib";
var _a;
// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { createEntityAdapter } from '@ngrx/entity';
// Actions
import { ProductActionTypes } from '../_actions/product.actions';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
export var adapter = createEntityAdapter();
export var initialProductsState = adapter.getInitialState({
    listLoading: false,
    actionsloading: false,
    totalCount: 0,
    lastQuery: new QueryParamsModel({}),
    lastCreatedProductId: undefined,
    showInitWaitingMessage: true
});
export function productsReducer(state, action) {
    if (state === void 0) { state = initialProductsState; }
    switch (action.type) {
        case ProductActionTypes.ProductsPageToggleLoading: return tslib_1.__assign({}, state, { listLoading: action.payload.isLoading, lastCreatedProductId: undefined });
        case ProductActionTypes.ProductsActionToggleLoading: return tslib_1.__assign({}, state, { actionsloading: action.payload.isLoading });
        case ProductActionTypes.ProductOnServerCreated: return tslib_1.__assign({}, state);
        case ProductActionTypes.ProductCreated: return adapter.addOne(action.payload.product, tslib_1.__assign({}, state, { lastCreatedProductId: action.payload.product.id }));
        case ProductActionTypes.ProductUpdated: return adapter.updateOne(action.payload.partialProduct, state);
        case ProductActionTypes.ProductsStatusUpdated: {
            var _partialProducts = [];
            for (var i = 0; i < action.payload.products.length; i++) {
                _partialProducts.push({
                    id: action.payload.products[i].id,
                    changes: {
                        status: action.payload.status
                    }
                });
            }
            return adapter.updateMany(_partialProducts, state);
        }
        case ProductActionTypes.OneProductDeleted: return adapter.removeOne(action.payload.id, state);
        case ProductActionTypes.ManyProductsDeleted: return adapter.removeMany(action.payload.ids, state);
        case ProductActionTypes.ProductsPageCancelled: return tslib_1.__assign({}, state, { listLoading: false, lastQuery: new QueryParamsModel({}) });
        case ProductActionTypes.ProductsPageLoaded:
            return adapter.addMany(action.payload.products, tslib_1.__assign({}, initialProductsState, { totalCount: action.payload.totalCount, listLoading: false, lastQuery: action.payload.page, showInitWaitingMessage: false }));
        default: return state;
    }
}
export var getProductState = createFeatureSelector('products');
export var selectAll = (_a = adapter.getSelectors(), _a.selectAll), selectEntities = _a.selectEntities, selectIds = _a.selectIds, selectTotal = _a.selectTotal;
//# sourceMappingURL=product.reducers.js.map