import * as tslib_1 from "tslib";
var _a;
// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { createEntityAdapter } from '@ngrx/entity';
// Actions
import { ProductSpecificationActionTypes } from '../_actions/product-specification.actions';
import { QueryParamsModel } from '../../_base/crud';
export var adapter = createEntityAdapter();
export var initialProductSpecificationsState = adapter.getInitialState({
    loading: false,
    totalCount: 0,
    productId: undefined,
    lastCreatedProductSpecificationId: undefined,
    lastQuery: new QueryParamsModel({}),
    showInitWaitingMessage: true
});
export function productSpecificationsReducer(state, action) {
    if (state === void 0) { state = initialProductSpecificationsState; }
    switch (action.type) {
        case ProductSpecificationActionTypes.ProductSpecificationsPageToggleLoading:
            return tslib_1.__assign({}, state, { loading: action.payload.isLoading, lastCreatedProductSpecificationId: undefined });
        case ProductSpecificationActionTypes.ProductSpecificationOnServerCreated:
            return tslib_1.__assign({}, state, { loading: true });
        case ProductSpecificationActionTypes.ProductSpecificationCreated:
            return adapter.addOne(action.payload.productSpecification, tslib_1.__assign({}, state, { lastCreatedProductSpecificationId: action.payload.productSpecification.id }));
        case ProductSpecificationActionTypes.ProductSpecificationUpdated:
            return adapter.updateOne(action.payload.partialProductSpecification, state);
        case ProductSpecificationActionTypes.OneProductSpecificationDeleted:
            return adapter.removeOne(action.payload.id, state);
        case ProductSpecificationActionTypes.ManyProductSpecificationsDeleted:
            return adapter.removeMany(action.payload.ids, state);
        case ProductSpecificationActionTypes.ProductSpecificationsPageCancelled:
            return tslib_1.__assign({}, state, { totalCount: 0, loading: false, productId: undefined, lastQuery: new QueryParamsModel({}) });
        case ProductSpecificationActionTypes.ProductSpecificationsPageRequested:
            return tslib_1.__assign({}, state, { totalCount: 0, loading: true, productId: action.payload.productId, lastQuery: action.payload.page });
        case ProductSpecificationActionTypes.ProductSpecificationsPageLoaded:
            return adapter.addMany(action.payload.productSpecifications, tslib_1.__assign({}, initialProductSpecificationsState, { totalCount: action.payload.totalCount, loading: false, productId: state.productId, lastQuery: state.lastQuery, showInitWaitingMessage: false }));
        default:
            return state;
    }
}
export var getProductRemarlState = createFeatureSelector('productSpecifications');
export var selectAll = (_a = adapter.getSelectors(), _a.selectAll), selectEntities = _a.selectEntities, selectIds = _a.selectIds, selectTotal = _a.selectTotal;
//# sourceMappingURL=product-specification.reducers.js.map