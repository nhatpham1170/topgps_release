export var ProductSpecificationActionTypes;
(function (ProductSpecificationActionTypes) {
    ProductSpecificationActionTypes["ProductSpecificationOnServerCreated"] = "[Edit ProductSpecification Dialog] Product Specification On Server Created";
    ProductSpecificationActionTypes["ProductSpecificationCreated"] = "[Edit ProductSpecification Dialog] Product Specification Created";
    ProductSpecificationActionTypes["ProductSpecificationUpdated"] = "[Edit SpecificationSpecification Dialog] Product Specification Updated";
    ProductSpecificationActionTypes["OneProductSpecificationDeleted"] = "[Product Specification List Page]  One Product Specification Deleted";
    ProductSpecificationActionTypes["ManyProductSpecificationsDeleted"] = "[Product Specifications List Page] Many Product Specifications Deleted";
    ProductSpecificationActionTypes["ProductSpecificationsPageRequested"] = "[Product Specifications List Page] Product Specifications Page Requested";
    ProductSpecificationActionTypes["ProductSpecificationsPageLoaded"] = "[Product Specifications API] Product Specifications Page Loaded";
    ProductSpecificationActionTypes["ProductSpecificationsPageCancelled"] = "[Product Specifications API] Product Specifications Page Cancelled";
    ProductSpecificationActionTypes["ProductSpecificationsPageToggleLoading"] = "[Product Specifications] Product Specifications Page Toggle Loading";
})(ProductSpecificationActionTypes || (ProductSpecificationActionTypes = {}));
var ProductSpecificationOnServerCreated = /** @class */ (function () {
    function ProductSpecificationOnServerCreated(payload) {
        this.payload = payload;
        this.type = ProductSpecificationActionTypes.ProductSpecificationOnServerCreated;
    }
    return ProductSpecificationOnServerCreated;
}());
export { ProductSpecificationOnServerCreated };
var ProductSpecificationCreated = /** @class */ (function () {
    function ProductSpecificationCreated(payload) {
        this.payload = payload;
        this.type = ProductSpecificationActionTypes.ProductSpecificationCreated;
    }
    return ProductSpecificationCreated;
}());
export { ProductSpecificationCreated };
var ProductSpecificationUpdated = /** @class */ (function () {
    function ProductSpecificationUpdated(payload) {
        this.payload = payload;
        this.type = ProductSpecificationActionTypes.ProductSpecificationUpdated;
    }
    return ProductSpecificationUpdated;
}());
export { ProductSpecificationUpdated };
var OneProductSpecificationDeleted = /** @class */ (function () {
    function OneProductSpecificationDeleted(payload) {
        this.payload = payload;
        this.type = ProductSpecificationActionTypes.OneProductSpecificationDeleted;
    }
    return OneProductSpecificationDeleted;
}());
export { OneProductSpecificationDeleted };
var ManyProductSpecificationsDeleted = /** @class */ (function () {
    function ManyProductSpecificationsDeleted(payload) {
        this.payload = payload;
        this.type = ProductSpecificationActionTypes.ManyProductSpecificationsDeleted;
    }
    return ManyProductSpecificationsDeleted;
}());
export { ManyProductSpecificationsDeleted };
var ProductSpecificationsPageRequested = /** @class */ (function () {
    function ProductSpecificationsPageRequested(payload) {
        this.payload = payload;
        this.type = ProductSpecificationActionTypes.ProductSpecificationsPageRequested;
    }
    return ProductSpecificationsPageRequested;
}());
export { ProductSpecificationsPageRequested };
var ProductSpecificationsPageLoaded = /** @class */ (function () {
    function ProductSpecificationsPageLoaded(payload) {
        this.payload = payload;
        this.type = ProductSpecificationActionTypes.ProductSpecificationsPageLoaded;
    }
    return ProductSpecificationsPageLoaded;
}());
export { ProductSpecificationsPageLoaded };
var ProductSpecificationsPageCancelled = /** @class */ (function () {
    function ProductSpecificationsPageCancelled() {
        this.type = ProductSpecificationActionTypes.ProductSpecificationsPageCancelled;
    }
    return ProductSpecificationsPageCancelled;
}());
export { ProductSpecificationsPageCancelled };
var ProductSpecificationsPageToggleLoading = /** @class */ (function () {
    function ProductSpecificationsPageToggleLoading(payload) {
        this.payload = payload;
        this.type = ProductSpecificationActionTypes.ProductSpecificationsPageToggleLoading;
    }
    return ProductSpecificationsPageToggleLoading;
}());
export { ProductSpecificationsPageToggleLoading };
//# sourceMappingURL=product-specification.actions.js.map