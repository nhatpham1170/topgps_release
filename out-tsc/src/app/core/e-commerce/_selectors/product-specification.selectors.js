// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
export var selectProductSpecificationsState = createFeatureSelector('productSpecifications');
export var selectProductSpecificationById = function (productSpecificationId) { return createSelector(selectProductSpecificationsState, function (productSpecificationsState) { return productSpecificationsState.entities[productSpecificationId]; }); };
export var selectProductSpecificationsPageLoading = createSelector(selectProductSpecificationsState, function (productSpecificationsState) { return productSpecificationsState.loading; });
export var selectCurrentProductIdInStoreForProductSpecs = createSelector(selectProductSpecificationsState, function (productSpecificationsState) { return productSpecificationsState.productId; });
export var selectLastCreatedProductSpecificationId = createSelector(selectProductSpecificationsState, function (productSpecificationsState) { return productSpecificationsState.lastCreatedProductSpecificationId; });
export var selectPSShowInitWaitingMessage = createSelector(selectProductSpecificationsState, function (productSpecificationsState) { return productSpecificationsState.showInitWaitingMessage; });
export var selectProductSpecificationsInStore = createSelector(selectProductSpecificationsState, function (productSpecificationsState) {
    var items = [];
    each(productSpecificationsState.entities, function (element) {
        items.push(element);
    });
    var httpExtension = new HttpExtenstionsModel();
    var result = httpExtension.sortArray(items, productSpecificationsState.lastQuery.sortField, productSpecificationsState.lastQuery.sortOrder);
    return new QueryResultsModel(result, productSpecificationsState.totalCount, '');
});
//# sourceMappingURL=product-specification.selectors.js.map