// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
export var selectProductsState = createFeatureSelector('products');
export var selectProductById = function (productId) { return createSelector(selectProductsState, function (productsState) { return productsState.entities[productId]; }); };
export var selectProductsPageLoading = createSelector(selectProductsState, function (productsState) { return productsState.listLoading; });
export var selectProductsActionLoading = createSelector(selectProductsState, function (customersState) { return customersState.actionsloading; });
export var selectProductsPageLastQuery = createSelector(selectProductsState, function (productsState) { return productsState.lastQuery; });
export var selectLastCreatedProductId = createSelector(selectProductsState, function (productsState) { return productsState.lastCreatedProductId; });
export var selectProductsInitWaitingMessage = createSelector(selectProductsState, function (productsState) { return productsState.showInitWaitingMessage; });
export var selectProductsInStore = createSelector(selectProductsState, function (productsState) {
    var items = [];
    each(productsState.entities, function (element) {
        items.push(element);
    });
    var httpExtension = new HttpExtenstionsModel();
    var result = httpExtension.sortArray(items, productsState.lastQuery.sortField, productsState.lastQuery.sortOrder);
    return new QueryResultsModel(result, productsState.totalCount, '');
});
export var selectHasProductsInStore = createSelector(selectProductsInStore, function (queryResult) {
    if (!queryResult.totalCount) {
        return false;
    }
    return true;
});
//# sourceMappingURL=product.selectors.js.map