// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
export var selectCustomersState = createFeatureSelector('customers');
export var selectCustomerById = function (customerId) { return createSelector(selectCustomersState, function (customersState) { return customersState.entities[customerId]; }); };
export var selectCustomersPageLoading = createSelector(selectCustomersState, function (customersState) { return customersState.listLoading; });
export var selectCustomersActionLoading = createSelector(selectCustomersState, function (customersState) { return customersState.actionsloading; });
export var selectLastCreatedCustomerId = createSelector(selectCustomersState, function (customersState) { return customersState.lastCreatedCustomerId; });
export var selectCustomersShowInitWaitingMessage = createSelector(selectCustomersState, function (customersState) { return customersState.showInitWaitingMessage; });
export var selectCustomersInStore = createSelector(selectCustomersState, function (customersState) {
    var items = [];
    each(customersState.entities, function (element) {
        items.push(element);
    });
    var httpExtension = new HttpExtenstionsModel();
    var result = httpExtension.sortArray(items, customersState.lastQuery.sortField, customersState.lastQuery.sortOrder);
    return new QueryResultsModel(result, customersState.totalCount, '');
});
//# sourceMappingURL=customer.selectors.js.map