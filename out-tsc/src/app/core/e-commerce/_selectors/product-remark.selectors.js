// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
export var selectProductRemarksState = createFeatureSelector('productRemarks');
export var selectProductRemarkById = function (productRemarkId) { return createSelector(selectProductRemarksState, function (productRemarksState) { return productRemarksState.entities[productRemarkId]; }); };
export var selectProductRemarksPageLoading = createSelector(selectProductRemarksState, function (productRemarksState) { return productRemarksState.loading; });
export var selectCurrentProductIdInStoreForProductRemarks = createSelector(selectProductRemarksState, function (productRemarksState) { return productRemarksState.productId; });
export var selectLastCreatedProductRemarkId = createSelector(selectProductRemarksState, function (productRemarksState) { return productRemarksState.lastCreatedProductRemarkId; });
export var selectPRShowInitWaitingMessage = createSelector(selectProductRemarksState, function (productRemarksState) { return productRemarksState.showInitWaitingMessage; });
export var selectProductRemarksInStore = createSelector(selectProductRemarksState, function (productRemarksState) {
    var items = [];
    each(productRemarksState.entities, function (element) {
        items.push(element);
    });
    var httpExtension = new HttpExtenstionsModel();
    var result = httpExtension.sortArray(items, productRemarksState.lastQuery.sortField, productRemarksState.lastQuery.sortOrder);
    return new QueryResultsModel(items, productRemarksState.totalCount, '');
});
//# sourceMappingURL=product-remark.selectors.js.map