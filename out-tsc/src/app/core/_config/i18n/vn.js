// USA
export var locale = {
    lang: 'vn',
    data: {
        TRANSLATOR: {
            SELECT: 'Chọn ngôn ngữ',
        },
        MENU: {
            NEW: 'Mới',
            ACTIONS: 'Hành động',
            CREATE_POST: 'Tạo mới',
            PAGES: 'Trang',
            FEATURES: 'Tính năng',
            APPS: 'Apps',
            DASHBOARD: 'Tổng quan',
            MAP: 'Bản đồ',
            REPORT: 'Báo cáo',
            REPORT_FUEL: 'Nạp xả nhiên liệu',
            MANAGE: 'Quản lý',
            MANAGER_USER: 'Quản lý người dùng',
            MANAGER_DEVICE: 'Quản lý thiết bị',
            GROUP_DEVICE: 'Nhóm thiết bị',
            COMMAND: 'Gửi lệnh',
            ADMIN: 'Quản trị viên',
            SETTING_SYSTEM: 'Cấu hình hệ thống',
            MANAGE_PERMISSIONS: 'Quản lý quyền',
            MANAGE_ROLES: 'Quản lý vai trò',
            LOGIN_PAGE: 'Quản lý trang đăng nhập',
            SETTING_DEVICE: 'Cấu hình thiết bị',
            SIM_TYPE: 'Loại sim',
            DEVICE_TYPE: 'Loại thiết bị',
            SENSOR_MODEL: 'Mẫu cảm biến',
            ICON: 'Biểu tượng',
        },
        AUTH: {
            GENERAL: {
                SIGNIN: 'Đăng nhập',
                OR: 'hoặc',
                SUBMIT_BUTTON: 'Gửi đi',
                NO_ACCOUNT: 'Chưa có tài khoản?',
                SIGNUP_BUTTON: 'Đăng ký',
                FORGOT_BUTTON: 'Quên mật khẩu',
                BACK_BUTTON: 'Quay lại',
                PRIVACY: 'Về chúng tôi',
                LEGAL: 'Pháp lý',
                CONTACT: 'Liên hệ',
                WELCOME: 'Chào mừng bạn đến với VNETGPS',
                DESCRIPTION: 'Công Ty Cổ Phần Công nghệ Điện Tử & Viễn Thông Việt Nam (VNETGPS) là một trong những đơn vị đi đầu trong lĩnh vực phân phối, sản xuất thiết bị định vị tại Việt Nam.',
                DONT_HAVE_ACCOUNT: 'Bạn chưa có tài khoản?',
            },
            LOGIN: {
                TITLE: 'Tài khoản đăng nhập',
                BUTTON: 'Đăng nhập',
            },
            FORGOT: {
                TITLE: 'Quên mật khẩu?',
                DESC: 'Nhập email để thiết lập lại mật khẩu',
                SUCCESS: 'Đã thiest lập lại mật khẩu thành công'
            },
            REGISTER: {
                TITLE: 'Đăng ký',
                DESC: 'Nhập email',
                SUCCESS: 'Tài khoản đã được đăng ký thành công.'
            },
            INPUT: {
                EMAIL: 'Email',
                FULLNAME: 'Tên đầy đủ',
                PASSWORD: 'Mật khẩu',
                CONFIRM_PASSWORD: 'Nhập lại mật khẩu',
                USERNAME: 'Tài khoản'
            },
            VALIDATION: {
                INVALID: '{{name}} không hợp lệ',
                REQUIRED: '{{name}} bắt buộc',
                MIN_LENGTH: '{{name}} tối thiểu là {{min}} ký tự',
                AGREEMENT_REQUIRED: 'Accepting terms & conditions are required',
                NOT_FOUND: 'The requested {{name}} is not found',
                INVALID_LOGIN: 'The login detail is incorrect',
                REQUIRED_FIELD: 'Required field',
                MIN_LENGTH_FIELD: 'Minimum field length:',
                MAX_LENGTH_FIELD: 'Maximum field length:',
                INVALID_FIELD: 'Field is not valid',
            },
            MESSAGE: {
                TEST: 'Thử nghiệm',
            }
        },
        ECOMMERCE: {
            COMMON: {
                SELECTED_RECORDS_COUNT: 'Selected records count: ',
                ALL: 'All',
                SUSPENDED: 'Suspended',
                ACTIVE: 'Active',
                FILTER: 'Filter',
                BY_STATUS: 'by Status',
                BY_TYPE: 'by Type',
                BUSINESS: 'Business',
                INDIVIDUAL: 'Individual',
                SEARCH: 'Search',
                IN_ALL_FIELDS: 'in all fields'
            },
            ECOMMERCE: 'eCommerce',
            CUSTOMERS: {
                CUSTOMERS: 'Customers',
                CUSTOMERS_LIST: 'Customers list',
                NEW_CUSTOMER: 'New Customer',
                DELETE_CUSTOMER_SIMPLE: {
                    TITLE: 'Customer Delete',
                    DESCRIPTION: 'Are you sure to permanently delete this customer?',
                    WAIT_DESCRIPTION: 'Customer is deleting...',
                    MESSAGE: 'Customer has been deleted'
                },
                DELETE_CUSTOMER_MULTY: {
                    TITLE: 'Customers Delete',
                    DESCRIPTION: 'Are you sure to permanently delete selected customers?',
                    WAIT_DESCRIPTION: 'Customers are deleting...',
                    MESSAGE: 'Selected customers have been deleted'
                },
                UPDATE_STATUS: {
                    TITLE: 'Status has been updated for selected customers',
                    MESSAGE: 'Selected customers status have successfully been updated'
                },
                EDIT: {
                    UPDATE_MESSAGE: 'Customer has been updated',
                    ADD_MESSAGE: 'Customer has been created'
                }
            },
            MESSAGE: {}
        },
    }
};
//# sourceMappingURL=vn.js.map