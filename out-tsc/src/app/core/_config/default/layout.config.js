var LayoutConfig = /** @class */ (function () {
    function LayoutConfig() {
        this.defaults = {
            'demo': 'default',
            // == Base Layout
            'self': {
                'layout': 'fluid',
                'body': {
                    'background-image': './assets/media/misc/bg-1.jpg',
                },
                'logo': {
                    'dark': './assets/media/logos/logovnet.png',
                    'light': './assets/media/logos/logovnet.png',
                    'brand': './assets/media/logos/logovnet.png',
                    'green': './assets/media/logos/logovnet.png',
                }
            },
            // == Page Splash Screen loading
            'loader': {
                'enabled': true,
                'type': 'spinner-logo',
                'logo': './assets/media/logos/icon-loader.png',
                'message': 'Please wait...'
            },
            // == Colors for javascript
            'colors': {
                'state': {
                    'brand': '#5d78ff',
                    'dark': '#282a3c',
                    'light': '#ffffff',
                    'primary': '#5867dd',
                    'success': '#34bfa3',
                    'info': '#36a3f7',
                    'warning': '#ffb822',
                    'danger': '#fd3995'
                },
                'base': {
                    'label': [
                        '#c5cbe3',
                        '#a1a8c3',
                        '#3d4465',
                        '#3e4466'
                    ],
                    'shape': [
                        '#f0f3ff',
                        '#d9dffa',
                        '#afb4d4',
                        '#646c9a'
                    ]
                }
            },
            'header': {
                'self': {
                    'skin': 'light',
                    'fixed': {
                        'desktop': true,
                        'mobile': true
                    }
                },
                'menu': {
                    'self': {
                        'display': true,
                        'layout': 'default',
                        'root-arrow': false,
                    },
                    'desktop': {
                        'arrow': true,
                        'toggle': 'click',
                        'submenu': {
                            'skin': 'light',
                            'arrow': true
                        }
                    },
                    'mobile': {
                        'submenu': {
                            'skin': 'dark',
                            'accordion': true
                        }
                    }
                }
            },
            'subheader': {
                'display': false,
                'layout': 'subheader-v1',
                'fixed': false,
                'width': 'fluid',
                'style': 'solid'
            },
            'content': {
                'width': 'fluid'
            },
            'brand': {
                'self': {
                    'skin': 'light'
                }
            },
            'aside': {
                'self': {
                    'skin': 'light',
                    'display': true,
                    'fixed': true,
                    'minimize': {
                        'toggle': true,
                        'default': true
                    }
                },
                'footer': {
                    'self': {
                        'display': true
                    }
                },
                'menu': {
                    'dropdown': false,
                    'scroll': false,
                    'submenu': {
                        'accordion': true,
                        'dropdown': {
                            'arrow': true,
                            'hover-timeout': 500
                        }
                    }
                }
            },
            'footer': {
                'self': {
                    'width': 'fluid'
                }
            }
        };
    }
    Object.defineProperty(LayoutConfig.prototype, "configs", {
        /**
         * Good place for getting the remote config
         */
        get: function () {
            return this.defaults;
        },
        enumerable: true,
        configurable: true
    });
    return LayoutConfig;
}());
export { LayoutConfig };
//# sourceMappingURL=layout.config.js.map