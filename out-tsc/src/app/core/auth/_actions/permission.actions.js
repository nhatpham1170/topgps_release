export var PermissionActionTypes;
(function (PermissionActionTypes) {
    PermissionActionTypes["AllPermissionsRequested"] = "[Permission Requested ] Action";
    PermissionActionTypes["AllPermissionsLoaded"] = "[Permission Loaded] Auth API";
    PermissionActionTypes["PermissionsInit"] = "[Permisison Init] Permissions ";
})(PermissionActionTypes || (PermissionActionTypes = {}));
var AllPermissionsRequested = /** @class */ (function () {
    function AllPermissionsRequested() {
        this.type = PermissionActionTypes.AllPermissionsRequested;
    }
    return AllPermissionsRequested;
}());
export { AllPermissionsRequested };
var PermissionInit = /** @class */ (function () {
    function PermissionInit() {
        this.type = PermissionActionTypes.PermissionsInit;
    }
    return PermissionInit;
}());
export { PermissionInit };
var AllPermissionsLoaded = /** @class */ (function () {
    function AllPermissionsLoaded(payload) {
        this.payload = payload;
        this.type = PermissionActionTypes.AllPermissionsLoaded;
    }
    return AllPermissionsLoaded;
}());
export { AllPermissionsLoaded };
//# sourceMappingURL=permission.actions.js.map