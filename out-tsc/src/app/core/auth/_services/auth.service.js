import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import { User } from '../_models/user.model';
import { Permission } from '../_models/permission.model';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { currentUser } from '../_selectors/auth.selectors';
import { Logout } from '../_actions/auth.actions';
import { AuthRequest } from '../utils/auth.request.service';
// import { RSA_X931_PADDING } from 'constants';
var API_USERS_URL = 'api/users';
var API_PERMISSION_URL = 'api/permissions';
var API_ROLES_URL = 'api/roles';
var AuthService = /** @class */ (function () {
    function AuthService(http, store, router, authRequest) {
        this.http = http;
        this.store = store;
        this.router = router;
        this.authRequest = authRequest;
    }
    // Authentication/Authorization
    AuthService.prototype.login = function (email, password) {
        var reqHeader = new HttpHeaders()
            .set('Auth', 'False');
        return this.http.post(environment.api.host + '/auth/login', { 'username': email, 'password': password }, { headers: reqHeader })
            .pipe(tap(function (body) {
            // user.result.id;										
            body.result = new User().deserialize(body.result, {
                tranform: {
                    'accessToken': 'token',
                    'email': 'username'
                }
            });
        }));
    };
    AuthService.prototype.getUserByToken = function (option) {
        var _this = this;
        var userToken = localStorage.getItem(environment.authTokenKey);
        var httpHeaders = new HttpHeaders();
        if (option && option.notifyGlobal) {
            httpHeaders = httpHeaders.set('Notify-Global', 'True');
        }
        // httpHeaders.append('Notify-Global', 'True');
        // if(option){
        // const httpHeaders = new HttpHeaders();
        // }           
        return this.http.get(environment.api.host + "/users/profile", { headers: httpHeaders })
            .pipe(tap(function (body) {
            if (body.status == 401) {
                _this.store.dispatch(new Logout());
                _this.router.navigate(['/auth/login']);
            }
            body.result = new User().deserialize(body.result, {
                tranform: {
                    'fullname': 'name'
                }
            });
        }));
    };
    AuthService.prototype.register = function (user) {
        return this.http.post(API_USERS_URL, user)
            .pipe(map(function (res) {
            return res;
        }), catchError(function (err) {
            return null;
        }));
    };
    /*
     * Submit forgot password request
     *
     * @param {string} email
     * @returns {Observable<any>}
     */
    AuthService.prototype.requestPassword = function (email) {
        return this.http.get(API_USERS_URL + '/forgot?=' + email)
            .pipe(catchError(this.handleError('forgot-password', [])));
    };
    AuthService.prototype.getAllUsers = function () {
        return this.http.get(API_USERS_URL);
    };
    AuthService.prototype.getUserById = function (userId) {
        return this.http.get(API_USERS_URL + ("/" + userId));
    };
    // DELETE => delete the user from the server
    AuthService.prototype.deleteUser = function (userId) {
        var url = API_USERS_URL + "/" + userId;
        return this.http.delete(url);
    };
    // UPDATE => PUT: update the user on the server
    AuthService.prototype.updateUser = function (_user) {
        var httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.put(API_USERS_URL, _user, { headers: httpHeaders });
    };
    // CREATE =>  POST: add a new user to the server
    AuthService.prototype.createUser = function (user) {
        var httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post(API_USERS_URL, user, { headers: httpHeaders });
    };
    // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
    // items => filtered/sorted result
    AuthService.prototype.findUsers = function (queryParams) {
        var httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post(API_USERS_URL + '/findUsers', queryParams, { headers: httpHeaders });
    };
    // getAllPermissions(): Observable<Permission[]> {
    //     let permissionResult= this.http.get<Permission[]>(API_USERS_URL+'/permisisons');        
    //     permissionResult.subscribe(x=>console.log(x));
    //     return permissionResult;
    // }
    // Permission
    AuthService.prototype.getAllPermissions = function () {
        var user = this.store.pipe(select(currentUser));
        var ObPermissions = of([]);
        var permissionStr = '';
        if (user) {
            select(user.subscribe(function (data) {
                if (data)
                    permissionStr = data.permission;
            }).unsubscribe());
            var permissions = permissionStr.split(',').map(function (value, key) {
                var obj = {
                    id: key + 1,
                    name: value,
                    title: value,
                };
                return new Permission().deserialize(obj, { tranform: {} });
            });
            ObPermissions = of(permissions);
        }
        return ObPermissions;
        // return this.http.get<Permission[]>(API_PERMISSION_URL);
    };
    AuthService.prototype.getRolePermissions = function (roleId) {
        return this.http.get(API_PERMISSION_URL + '/getRolePermission?=' + roleId);
    };
    // Roles
    AuthService.prototype.getAllRoles = function () {
        return this.http.get(API_ROLES_URL);
    };
    AuthService.prototype.getRoleById = function (roleId) {
        return this.http.get(API_ROLES_URL + ("/" + roleId));
    };
    // CREATE =>  POST: add a new role to the server
    AuthService.prototype.createRole = function (role) {
        // Note: Add headers if needed (tokens/bearer)
        var httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post(API_ROLES_URL, role, { headers: httpHeaders });
    };
    // UPDATE => PUT: update the role on the server
    AuthService.prototype.updateRole = function (role) {
        var httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.put(API_ROLES_URL, role, { headers: httpHeaders });
    };
    // DELETE => delete the role from the server
    AuthService.prototype.deleteRole = function (roleId) {
        var url = API_ROLES_URL + "/" + roleId;
        return this.http.delete(url);
    };
    // Check Role Before deletion
    AuthService.prototype.isRoleAssignedToUsers = function (roleId) {
        return this.http.get(API_ROLES_URL + '/checkIsRollAssignedToUser?roleId=' + roleId);
    };
    AuthService.prototype.findRoles = function (queryParams) {
        // This code imitates server calls
        var httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        return this.http.post(API_ROLES_URL + '/findRoles', queryParams, { headers: httpHeaders });
    };
    /*
     * Handle Http operation that failed.
     * Let the app continue.
   *
   * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    AuthService.prototype.handleError = function (operation, result) {
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // Let the app keep running by returning an empty result.
            return of(result);
        };
    };
    AuthService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [HttpClient, Store, Router, AuthRequest])
    ], AuthService);
    return AuthService;
}());
export { AuthService };
//# sourceMappingURL=auth.service.js.map