import * as tslib_1 from "tslib";
import { BaseModelCT } from '../../_base/crud/models/_base.model.ct';
var User = /** @class */ (function (_super) {
    tslib_1.__extends(User, _super);
    function User() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    User.prototype.clear = function () {
        // this.id = undefined;
        // this.username = '';
        // this.password = '';
        // this.email = '';
        // this.roles = [];
        // this.fullname = '';
        // this.accessToken = null;
        // // this.accessToken = 'access-token-' + Math.random();
        // this.refreshToken = 'access-token-' + Math.random();
        // this.pic = './assets/media/users/default.jpg';
        // this.occupation = '';
        // this.companyName = '';
        // this.phone = '';
        // this.address = new Address();
        // this.address.clear();
        // this.socialNetworks = new SocialNetworks();
        // this.socialNetworks.clear();
        this.id = undefined;
        this.username = 'admin';
        this.password = 'demo';
        this.email = 'admin@demo.com';
        this.accessToken = null;
        this.refreshToken = 'access-token-f8c137a2c98743f48b643e71161d90aa';
        this.roles = [1]; // Administrator
        this.pic = './assets/media/users/300_25.jpg';
        this.fullname = 'Sean';
        this.occupation = 'CEO';
        this.companyName = 'Keenthemes';
        this.phone = '456669067890';
        this.address = {
            addressLine: 'L-12-20 Vertex, Cybersquare',
            city: 'San Francisco',
            state: 'California',
            postCode: '45000'
        };
        this.socialNetworks = {
            linkedIn: 'https://linkedin.com/admin',
            facebook: 'https://facebook.com/admin',
            twitter: 'https://twitter.com/admin',
            instagram: 'https://instagram.com/admin'
        };
    };
    return User;
}(BaseModelCT));
export { User };
//# sourceMappingURL=user.model.js.map