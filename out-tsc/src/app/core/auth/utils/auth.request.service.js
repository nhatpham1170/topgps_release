import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
var AuthRequest = /** @class */ (function () {
    function AuthRequest(http, store, router) {
        this.http = http;
        this.store = store;
        this.router = router;
    }
    // request(response:Observable<ResponseBody>,model,tranform:Object={}){
    //     response.pipe(
    //         tap(body=>{
    //             body.result = model.deserialize(body.result,{
    //                 tranform: tranform
    //             })
    //         })
    //     );
    //     return response;
    // }
    AuthRequest.prototype.request = function (response) {
        // let status = status.SUCCESS;
        return response.pipe(tap(function (body) {
        }));
        ;
    };
    AuthRequest = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient,
            Store,
            Router])
    ], AuthRequest);
    return AuthRequest;
}());
export { AuthRequest };
//# sourceMappingURL=auth.request.service.js.map