import * as tslib_1 from "tslib";
// Angular
import { Injectable } from '@angular/core';
// Auth
import { AuthDataContext } from '../../../../auth';
// ECommerce
import { ECommerceDataContext } from '../../../../e-commerce';
// Models
import { CarsDb } from './fake-db/cars';
var FakeApiService = /** @class */ (function () {
    /**
     * Service Constructore
     */
    function FakeApiService() {
    }
    /**
     * Create Fake DB and API
     */
    FakeApiService.prototype.createDb = function () {
        // tslint:disable-next-line:class-name
        var db = {
            // auth module
            users: AuthDataContext.users,
            roles: AuthDataContext.roles,
            permissions: AuthDataContext.permissions,
            // e-commerce
            // customers
            customers: ECommerceDataContext.customers,
            // products
            products: ECommerceDataContext.cars,
            productRemarks: ECommerceDataContext.remarks,
            productSpecs: ECommerceDataContext.carSpecs,
            // orders
            orders: ECommerceDataContext.orders,
            // data-table
            cars: CarsDb.cars
        };
        return db;
    };
    FakeApiService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [])
    ], FakeApiService);
    return FakeApiService;
}());
export { FakeApiService };
//# sourceMappingURL=fake-api.service.js.map