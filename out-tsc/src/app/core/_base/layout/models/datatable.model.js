import { EventEmitter } from '@angular/core';
var DataTable = /** @class */ (function () {
    function DataTable() {
        this.callback = function (option) {
        };
        this.paginations = {
            total: 0,
            pageSize: 10,
            currentPage: 1,
            maxPageDisplay: 5,
            pageDisplays: [1, 2, 3, 4, 5],
            firstPage: 1,
            lastPage: 5,
            form: 0,
            to: 0,
        };
        // column = {
        //     field: '',
        //     title: '',
        //     sortable: 'asc',
        //     width: 40,
        //     type: '',
        //     selector: false,
        //     textAlign: 'center',
        //     format: '',
        //     class: '',
        //     style: '',
        //     template: '',
        // };
        this.api = {
            pagination: {
                currentPage: 1,
                pageSize: 10,
            },
            querys: [{
                    key: "",
                    value: ""
                }],
            sorts: {
                sort: "",
                field: ""
            }
        };
        this.columnLastSort = '';
        this.config = {
            paginationSelect: [10, 20, 30, 50, 100]
        };
        this.isLoading = false;
        this.eventUpdate = new EventEmitter();
    }
    DataTable.prototype.setPageSize = function (pageSize) {
        if (pageSize === void 0) { pageSize = 10; }
        if (pageSize > 0) {
            // this.paginations.pageSize = pageSize;            
            this.reload({ pageSize: pageSize, currentPage: 1 });
        }
    };
    DataTable.prototype.getPageDisplay = function () {
        return this.paginations.pageDisplays;
    };
    DataTable.prototype.getFirstPage = function () {
        return this.paginations.firstPage;
    };
    DataTable.prototype.getLastPage = function () {
        return this.paginations.lastPage;
    };
    DataTable.prototype.getCurrentPage = function () {
        return this.paginations.currentPage;
    };
    DataTable.prototype.gotoPage = function (page) {
        if (page >= this.paginations.firstPage && page <= this.paginations.lastPage) {
            console.log(page);
            this.paginations.currentPage = page;
            this.reload({ currentPage: this.paginations.currentPage });
        }
    };
    DataTable.prototype.sort = function (columnName) {
        var lastColumn = this.columnLastSort;
        if (columnName == lastColumn) {
            var index = this.columns.findIndex(function (x) { return x.field == lastColumn; });
            if (index != -1) {
                if (this.columns[index].allowSort) {
                    switch (this.columns[index].sortable) {
                        case 'asc':
                            this.columns[index].sortable = 'desc';
                            break;
                        case 'desc':
                            this.columns[index].sortable = 'asc';
                            break;
                        default:
                            this.columns[index].sortable = 'asc';
                            break;
                    }
                    this.api.sorts.field = this.columns[index].field;
                    this.api.sorts.sort = this.columns[index].sortable;
                    this.reload({});
                }
            }
        }
        else {
            var indexNew = this.columns.findIndex(function (x) { return x.field == columnName; });
            if (indexNew != -1) {
                if (this.columns[indexNew].allowSort) {
                    this.columns[indexNew].sortable = 'asc';
                    this.columns[indexNew].isSort = true;
                    if (lastColumn.length > 0) {
                        var index = this.columns.findIndex(function (x) { return x.field == lastColumn; });
                        if (index != -1) {
                            this.columns[index].sortable = '';
                            this.columns[index].isSort = false;
                        }
                    }
                    this.columnLastSort = columnName;
                    this.api.sorts.field = this.columns[indexNew].field;
                    this.api.sorts.sort = this.columns[indexNew].sortable;
                    this.reload({});
                }
            }
        }
    };
    DataTable.prototype.getColumns = function () {
        return this.columns;
    };
    DataTable.prototype.checkLoading = function () {
        return this.isLoading;
    };
    DataTable.prototype.init = function (option) {
        if (option.data) {
            this.data = option.data;
        }
        if (option.currentPage) {
            this.paginations.currentPage = option.currentPage;
        }
        if (option.totalRecod) {
            this.paginations.total = option.totalRecod;
        }
        if (option.paginationSelect) {
            this.config.paginationSelect = option.paginationSelect;
        }
        if (option.pageSize) {
            this.paginations.pageSize = option.pageSize;
        }
        if (option.columns) {
            this.columns = option.columns;
        }
        this.update(option);
    };
    ;
    DataTable.prototype.update = function (option) {
        console.log("table updated");
        if (option.data) {
            this.data = option.data;
        }
        if (option.currentPage) {
            this.paginations.currentPage = option.currentPage;
        }
        if (option.totalRecod) {
            this.paginations.total = option.totalRecod;
        }
        if (option.paginationSelect) {
            this.config.paginationSelect = option.paginationSelect;
        }
        if (option.pageSize) {
            this.paginations.pageSize = option.pageSize;
        }
        if (option.maxPageDisplay) {
            if (option.maxPageDisplay > 0 && option.maxPageDisplay < 10)
                this.paginations.maxPageDisplay = option.maxPageDisplay;
        }
        // this.data =this.callback(this.api);
        // set from to.
        this.paginations.form = 1 + (this.paginations.currentPage - 1) * this.paginations.pageSize;
        var to = this.paginations.pageSize * this.paginations.currentPage;
        if (this.paginations.total < to) {
            this.paginations.to = this.paginations.total;
        }
        else {
            this.paginations.to = to;
        }
        // splipt array if data > page size.
        if (this.data.length > this.paginations.pageSize) {
            this.data = this.data.slice(0, this.paginations.pageSize);
        }
        // else{
        //     let checkSize = this.paginations.form-this.paginations.to;
        //     if(this.data.length>checkSize)
        //     {
        //         this.data = this.data.slice(0,checkSize+1); 
        //     }
        // }
        // set firstPage and lastpage
        if (this.data.length > 0) {
            this.paginations.firstPage = 1;
            if (this.paginations.total % this.paginations.pageSize == 0) {
                this.paginations.lastPage = Math.trunc(this.paginations.total / this.paginations.pageSize);
            }
            else {
                this.paginations.lastPage = Math.trunc(this.paginations.total / this.paginations.pageSize) + 1;
            }
            // set pagination display
            this.paginations.pageDisplays = [1];
            if (this.paginations.lastPage < this.paginations.maxPageDisplay) {
                this.paginations.pageDisplays = [];
                for (var i = 1; i <= this.paginations.lastPage; i++) {
                    this.paginations.pageDisplays.push(i);
                }
            }
            else {
                var pageStart = 1;
                this.paginations.pageDisplays = [];
                if (this.paginations.currentPage % this.paginations.maxPageDisplay == 0) {
                    pageStart = (Math.trunc(this.paginations.currentPage / this.paginations.maxPageDisplay) - 1) * this.paginations.maxPageDisplay + 1;
                }
                else {
                    pageStart = (Math.trunc(this.paginations.currentPage / this.paginations.maxPageDisplay)) * this.paginations.maxPageDisplay + 1;
                }
                for (var i = 0; i < this.paginations.maxPageDisplay; i++) {
                    if ((pageStart + i) <= this.paginations.lastPage) {
                        this.paginations.pageDisplays.push(pageStart + i);
                    }
                }
            }
        }
        this.isLoading = false;
    };
    ;
    /**
     * Reload data table after change page
     * @param option setting
     */
    DataTable.prototype.reload = function (option) {
        this.isLoading = true;
        this.eventUpdate.emit(this.api);
        // this.update(option);
    };
    return DataTable;
}());
export { DataTable };
//# sourceMappingURL=datatable.model.js.map