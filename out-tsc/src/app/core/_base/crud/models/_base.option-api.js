var BaseOptionAPI = /** @class */ (function () {
    function BaseOptionAPI() {
        this.notifyGlobal = false;
        this.module = 'default';
        this.message = '';
    }
    return BaseOptionAPI;
}());
export { BaseOptionAPI };
//# sourceMappingURL=_base.option-api.js.map