var ResponseBody = /** @class */ (function () {
    function ResponseBody() {
        this.message = '';
        this.status = 0;
    }
    return ResponseBody;
}());
export { ResponseBody };
//# sourceMappingURL=_response-body.model.js.map