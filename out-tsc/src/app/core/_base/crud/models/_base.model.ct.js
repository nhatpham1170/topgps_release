var BaseModelCT = /** @class */ (function () {
    function BaseModelCT() {
    }
    BaseModelCT.prototype.clear = function () {
    };
    ;
    BaseModelCT.prototype.deserialize = function (input, option) {
        this.clear();
        if (input) {
            if (option.tranform) {
                Object.keys(option.tranform).forEach(function (key) {
                    input[key] = input[option.tranform[key]];
                });
            }
            Object.assign(this, input);
        }
        return this;
    };
    ;
    return BaseModelCT;
}());
export { BaseModelCT };
//# sourceMappingURL=_base.model.ct.js.map