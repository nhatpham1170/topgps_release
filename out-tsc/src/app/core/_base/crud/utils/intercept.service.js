import * as tslib_1 from "tslib";
// Angular
import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// RxJS
import { EMPTY } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ResponseUtilsService } from './response-utils.service';
import { environment } from '../../../../../environments/environment.prod';
// import { debug } from 'util';
/**
 * More information there => https://medium.com/@MetonymyQT/angular-http-interceptors-what-are-they-and-how-to-use-them-52e060321088
 */
var InterceptService = /** @class */ (function () {
    function InterceptService(responseUtils) {
        this.responseUtils = responseUtils;
        this.notifyGlobal = false;
    }
    InterceptService.prototype.intercept = function (request, next) {
        // tslint:disable-next-line:no-debugger
        // modify request
        // request = request.clone({
        // 	setHeaders: {
        // 		Authorization: ${localStorage.getItem('accessToken')}
        // 	}
        // }
        // });
        // console.log('----request----');
        // console.log(request);
        // console.log('--- end of request---');
        var _this = this;
        //#region check prefix send request	
        if (!this.checkHost(request.url)) {
            return EMPTY;
        }
        if (request.headers.get('Notify-Global') == 'True') {
            this.notifyGlobal = true;
            request = request.clone({
                headers: request.headers
                    .delete('Notify-Global')
            });
        }
        request = this.modifyHeader(request);
        //#endregion
        //#region  response
        return next.handle(request).pipe(tap(function (event) {
            if (event instanceof HttpResponse) {
                _this.responseUtils.processSuccess(event, _this.notifyGlobal);
                // console.log('all looks good');
                // http response status code
                // console.log(event.status);						
            }
        }, function (error) {
            _this.responseUtils.processError(error);
            // http response status code
            // console.log('----response----');
            // console.error('status code:');
            // tslint:disable-next-line:no-debugger					
            // console.error(error.status); 					
            // console.error(error.message);
            // console.log('--- end of response---');
        }));
        //#endregion
    };
    /**
     * Check url request in list host allow config
     * @param url Url request
     */
    InterceptService.prototype.checkHost = function (url) {
        var checkUrl = environment.hostAllow.findIndex(function (h) {
            if (url.match(h))
                return true;
        });
        if (checkUrl === -1) {
            return false;
        }
        return true;
    };
    /**
     * Add header default
     * @param request HttpRequest<any>
     */
    InterceptService.prototype.modifyHeader = function (request) {
        if (request.headers.get('Auth') == "False") {
            return request.clone({
                headers: request.headers
                    // .set('AppID', environment.api.AppID)
                    .set('Accept', 'application/json')
                    .set('Content-Type', 'application/json')
                    .delete('Auth')
            });
        }
        else {
            var userToken = localStorage.getItem(environment.authTokenKey);
            return request.clone({
                headers: request.headers
                    // .set('Appid', environment.api.AppID)
                    .set('Authorization', userToken)
                    .set('Accept', 'application/json')
                    .set('Content-Type', 'application/json')
            });
        }
    };
    InterceptService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [ResponseUtilsService])
    ], InterceptService);
    return InterceptService;
}());
export { InterceptService };
//# sourceMappingURL=intercept.service.js.map