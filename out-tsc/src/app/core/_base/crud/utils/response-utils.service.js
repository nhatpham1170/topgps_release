import * as tslib_1 from "tslib";
// Angular
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
/**
 * List status code for api response.
 */
export var StatusCode;
(function (StatusCode) {
    // UNAUTHORIZED = 401,
    // FORBIDDEN = 403,
    // NOT_FOUND = 404,
    // SUCCESS = 200,
    // CREATED = 201,
    StatusCode[StatusCode["CONTINUE"] = 100] = "CONTINUE";
    StatusCode[StatusCode["SWITCHING_PROTOCOLS"] = 101] = "SWITCHING_PROTOCOLS";
    StatusCode[StatusCode["OK"] = 200] = "OK";
    StatusCode[StatusCode["CREATED"] = 201] = "CREATED";
    StatusCode[StatusCode["ACCEPTED"] = 202] = "ACCEPTED";
    StatusCode[StatusCode["NON_AUTHORITATIVE_INFORMATION"] = 203] = "NON_AUTHORITATIVE_INFORMATION";
    StatusCode[StatusCode["NO_CONTENT"] = 204] = "NO_CONTENT";
    StatusCode[StatusCode["RESET_CONTENT"] = 205] = "RESET_CONTENT";
    StatusCode[StatusCode["PARTIAL_CONTENT"] = 206] = "PARTIAL_CONTENT";
    StatusCode[StatusCode["MULTIPLE_CHOICES"] = 300] = "MULTIPLE_CHOICES";
    StatusCode[StatusCode["MOVED_PERMANENTLY"] = 301] = "MOVED_PERMANENTLY";
    StatusCode[StatusCode["FOUND"] = 302] = "FOUND";
    StatusCode[StatusCode["SEE_OTHER"] = 303] = "SEE_OTHER";
    StatusCode[StatusCode["NOT_MODIFIED"] = 304] = "NOT_MODIFIED";
    StatusCode[StatusCode["USE_PROXY"] = 305] = "USE_PROXY";
    StatusCode[StatusCode["TEMPORARY_REDIRECT"] = 307] = "TEMPORARY_REDIRECT";
    StatusCode[StatusCode["BAD_REQUEST"] = 400] = "BAD_REQUEST";
    StatusCode[StatusCode["UNAUTHORIZED"] = 401] = "UNAUTHORIZED";
    StatusCode[StatusCode["PAYMENT_REQUIRED"] = 402] = "PAYMENT_REQUIRED";
    StatusCode[StatusCode["FORBIDDEN"] = 403] = "FORBIDDEN";
    StatusCode[StatusCode["NOT_FOUND"] = 404] = "NOT_FOUND";
    StatusCode[StatusCode["METHOD_NOT_ALLOWED"] = 405] = "METHOD_NOT_ALLOWED";
    StatusCode[StatusCode["NOT_ACCEPTABLE"] = 406] = "NOT_ACCEPTABLE";
    StatusCode[StatusCode["PROXY_AUTHENTICATION_REQUIRED"] = 407] = "PROXY_AUTHENTICATION_REQUIRED";
    StatusCode[StatusCode["REQUEST_TIME_OUT"] = 408] = "REQUEST_TIME_OUT";
    StatusCode[StatusCode["CONFLICT"] = 409] = "CONFLICT";
    StatusCode[StatusCode["GONE"] = 410] = "GONE";
    StatusCode[StatusCode["LENGTH_REQUIRED"] = 411] = "LENGTH_REQUIRED";
    StatusCode[StatusCode["PRECONDITION_FAILED"] = 412] = "PRECONDITION_FAILED";
    StatusCode[StatusCode["REQUEST_ENTITY_TOO_LARGE"] = 413] = "REQUEST_ENTITY_TOO_LARGE";
    StatusCode[StatusCode["REQUEST_URI_TOO_LARGE"] = 414] = "REQUEST_URI_TOO_LARGE";
    StatusCode[StatusCode["UNSUPPORTED_MEDIA_TYPE"] = 415] = "UNSUPPORTED_MEDIA_TYPE";
    StatusCode[StatusCode["REQUESTED_RANGE_NOT_SATISFIABLE"] = 416] = "REQUESTED_RANGE_NOT_SATISFIABLE";
    StatusCode[StatusCode["EXPECTATION_FAILED"] = 417] = "EXPECTATION_FAILED";
    StatusCode[StatusCode["INTERNAL_SERVER_ERROR"] = 500] = "INTERNAL_SERVER_ERROR";
    StatusCode[StatusCode["NOT_IMPLEMENTED"] = 501] = "NOT_IMPLEMENTED";
    StatusCode[StatusCode["BAD_GATEWAY"] = 502] = "BAD_GATEWAY";
    StatusCode[StatusCode["SERVICE_UNAVAILABLE"] = 503] = "SERVICE_UNAVAILABLE";
    StatusCode[StatusCode["GATEWAY_TIME_OUT"] = 504] = "GATEWAY_TIME_OUT";
    StatusCode[StatusCode["HTTP_VERSION_NOT_SUPPORTED"] = 505] = "HTTP_VERSION_NOT_SUPPORTED";
})(StatusCode || (StatusCode = {}));
var ResponseUtilsService = /** @class */ (function () {
    function ResponseUtilsService(router, toastr, translate) {
        this.router = router;
        this.toastr = toastr;
        this.translate = translate;
        this.toastConfig = {
            autoDismiss: false,
            closeButton: false,
            countDuplicates: false,
            disableTimeOut: false,
            easeTime: 300,
            easing: "ease-in",
            enableHtml: false,
            extendedTimeOut: 1000,
            maxOpened: 0,
            messageClass: "toast-message",
            newestOnTop: true,
            onActivateTick: false,
            positionClass: "toast-bottom-right",
            preventDuplicates: false,
            progressAnimation: "decreasing",
            progressBar: false,
            resetTimeoutOnDuplicate: false,
            tapToDismiss: true,
            timeOut: 5000,
            titleClass: "toast-title",
            toastClass: "ngx-toastr"
        };
        this.toastr.toastrConfig = Object.assign(this.toastr.toastrConfig, this.toastConfig);
    }
    /**
     * Process `event` response
     * @param event Event response
     * @param notifyGlobal Show notify global
     *
     * ### Example
     * ```
     * responseUtilsService.processSuccess(event);
     * ```
     */
    ResponseUtilsService.prototype.processSuccess = function (event, notifyGlobal) {
        if (notifyGlobal && notifyGlobal === true) {
            this.toastr.success(this.translate.instant('AUTH.MESSAGE.TEST'));
        }
    };
    /**
     * Process error and redirect
     *
     * @param error `error` Error Response
     *
     * ### Example
     *
     * ```
     * responseUtilsService.processError(error);
     * ```
     */
    ResponseUtilsService.prototype.processError = function (error) {
        var status = 0;
        if (error.status) {
            switch (error.status) {
                case StatusCode.UNAUTHORIZED:
                    // redirect to logout - reset sate
                    // after redirect to login 
                    this.router.navigateByUrl("/auth/logout");
                    break;
            }
        }
    };
    ResponseUtilsService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__metadata("design:paramtypes", [Router,
            ToastrService,
            TranslateService])
    ], ResponseUtilsService);
    return ResponseUtilsService;
}());
export { ResponseUtilsService };
//# sourceMappingURL=response-utils.service.js.map