import * as tslib_1 from "tslib";
// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
// const value:string = 'app/views/themes/demo2/theme.module';
var routes = [
    { path: 'auth', loadChildren: function () { return import('app/views/pages/auth/auth.module').then(function (m) { return m.AuthModule; }); } },
    // enable this router to set which demo theme to load,
    { path: '', loadChildren: function () { return import('app/views/themes/default/theme.module').then(function (m) { return m.ThemeModule; }); } },
    { path: '**', redirectTo: 'error/403', pathMatch: 'full' },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [
                RouterModule.forRoot(routes)
            ],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map