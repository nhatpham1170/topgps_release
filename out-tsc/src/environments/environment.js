// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export var environment = {
    production: false,
    isMockEnabled: true,
    authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',
    api: {
        host: 'http://192.168.1.67:8080',
        // host:'http://localhost:1000',
        AppID: 'Mr2n4FaCO8XdS7K6x4sHbIT6L+Gumltq7dy/EW0eIXQ=',
    },
    googleMapKey: 'AIzaSyDZbYx6ZOJH2e7uAbVwlC0dwDrsnq_NdTs',
    osrm: {
        host: 'http://125.212.203.173:5000'
    },
    hostAllow: [
        'http://admin.haduwaco.com',
        'http://192.168.1.67:8080'
    ],
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
//# sourceMappingURL=environment.js.map