export var environment = {
    production: true,
    isMockEnabled: true,
    authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',
    api: {
        host: 'http://192.168.1.67:8080',
        // host:'http://localhost:1000',
        AppID: 'Mr2n4FaCO8XdS7K6x4sHbIT6L+Gumltq7dy/EW0eIXQ=',
    },
    googleMapKey: 'AIzaSyDZbYx6ZOJH2e7uAbVwlC0dwDrsnq_NdTs',
    osrm: {
        host: 'http://125.212.203.173:5000'
    },
    hostAllow: [
        'http://admin.haduwaco.com',
        'http://192.168.1.67:8080'
    ],
};
//# sourceMappingURL=environment.prod.js.map