(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-views-pages-report-report-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/views/pages/report/report-fuel/report-fuel.component.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/pages/report/report-fuel/report-fuel.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div resizeObserver (resize)=\"onResize($event.contentRect)\" style=\"width:100%;height:100%\"><p>report-fuel works!</p></div> \r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/pages/report/report.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/pages/report/report.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/views/pages/report/report-fuel/report-fuel.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/views/pages/report/report-fuel/report-fuel.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL3JlcG9ydC9yZXBvcnQtZnVlbC9yZXBvcnQtZnVlbC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/views/pages/report/report-fuel/report-fuel.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/views/pages/report/report-fuel/report-fuel.component.ts ***!
  \*************************************************************************/
/*! exports provided: ReportFuelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportFuelComponent", function() { return ReportFuelComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ReportFuelComponent = /** @class */ (function () {
    function ReportFuelComponent() {
    }
    ReportFuelComponent.prototype.ngOnInit = function () {
    };
    ReportFuelComponent.prototype.onResize = function (event) {
        console.log(event);
    };
    ReportFuelComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-report-fuel',
            template: __webpack_require__(/*! raw-loader!./report-fuel.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/pages/report/report-fuel/report-fuel.component.html"),
            styles: [__webpack_require__(/*! ./report-fuel.component.scss */ "./src/app/views/pages/report/report-fuel/report-fuel.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ReportFuelComponent);
    return ReportFuelComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/report/report.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/views/pages/report/report.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL3JlcG9ydC9yZXBvcnQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/views/pages/report/report.component.ts":
/*!********************************************************!*\
  !*** ./src/app/views/pages/report/report.component.ts ***!
  \********************************************************/
/*! exports provided: ReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportComponent", function() { return ReportComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ReportComponent = /** @class */ (function () {
    function ReportComponent() {
    }
    ReportComponent.prototype.ngOnInit = function () {
    };
    ReportComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-report',
            template: __webpack_require__(/*! raw-loader!./report.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/pages/report/report.component.html"),
            styles: [__webpack_require__(/*! ./report.component.scss */ "./src/app/views/pages/report/report.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ReportComponent);
    return ReportComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/report/report.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/views/pages/report/report.module.ts ***!
  \*****************************************************/
/*! exports provided: ReportModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportModule", function() { return ReportModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _report_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./report.component */ "./src/app/views/pages/report/report.component.ts");
/* harmony import */ var _report_fuel_report_fuel_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./report-fuel/report-fuel.component */ "./src/app/views/pages/report/report-fuel/report-fuel.component.ts");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @core/auth */ "./src/app/core/auth/index.ts");







var routes = [{
        path: '',
        component: _report_component__WEBPACK_IMPORTED_MODULE_4__["ReportComponent"],
        children: [
            {
                path: 'fuel',
                component: _report_fuel_report_fuel_component__WEBPACK_IMPORTED_MODULE_5__["ReportFuelComponent"],
                canActivate: [_core_auth__WEBPACK_IMPORTED_MODULE_6__["ModuleGuard"], _core_auth__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
                data: {
                    permisison: 'ROLE_report.fuel',
                },
            }
        ]
    }];
var ReportModule = /** @class */ (function () {
    function ReportModule() {
    }
    ReportModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
            ],
            declarations: [_report_component__WEBPACK_IMPORTED_MODULE_4__["ReportComponent"], _report_fuel_report_fuel_component__WEBPACK_IMPORTED_MODULE_5__["ReportFuelComponent"]]
        })
    ], ReportModule);
    return ReportModule;
}());



/***/ })

}]);
//# sourceMappingURL=app-views-pages-report-report-module.js.map