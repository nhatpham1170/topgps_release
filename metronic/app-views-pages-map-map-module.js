(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-views-pages-map-map-module"],{

/***/ "./node_modules/@asymmetrik/ngx-leaflet/dist/index.js":
/*!************************************************************!*\
  !*** ./node_modules/@asymmetrik/ngx-leaflet/dist/index.js ***!
  \************************************************************/
/*! exports provided: LeafletModule, LeafletDirective, LeafletDirectiveWrapper, LeafletUtil, LeafletLayerDirective, LeafletLayersDirective, LeafletTileLayerDefinition, LeafletBaseLayersDirective, LeafletLayersControlDirective, LeafletControlLayersWrapper, LeafletControlLayersConfig, LeafletControlLayersChanges */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _leaflet_leaflet_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./leaflet/leaflet.module */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/leaflet.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LeafletModule", function() { return _leaflet_leaflet_module__WEBPACK_IMPORTED_MODULE_0__["LeafletModule"]; });

/* harmony import */ var _leaflet_core_leaflet_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./leaflet/core/leaflet.directive */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LeafletDirective", function() { return _leaflet_core_leaflet_directive__WEBPACK_IMPORTED_MODULE_1__["LeafletDirective"]; });

/* harmony import */ var _leaflet_core_leaflet_directive_wrapper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./leaflet/core/leaflet.directive.wrapper */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.directive.wrapper.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LeafletDirectiveWrapper", function() { return _leaflet_core_leaflet_directive_wrapper__WEBPACK_IMPORTED_MODULE_2__["LeafletDirectiveWrapper"]; });

/* harmony import */ var _leaflet_core_leaflet_util__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./leaflet/core/leaflet.util */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.util.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LeafletUtil", function() { return _leaflet_core_leaflet_util__WEBPACK_IMPORTED_MODULE_3__["LeafletUtil"]; });

/* harmony import */ var _leaflet_layers_leaflet_layer_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./leaflet/layers/leaflet-layer.directive */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/leaflet-layer.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LeafletLayerDirective", function() { return _leaflet_layers_leaflet_layer_directive__WEBPACK_IMPORTED_MODULE_4__["LeafletLayerDirective"]; });

/* harmony import */ var _leaflet_layers_leaflet_layers_directive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./leaflet/layers/leaflet-layers.directive */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/leaflet-layers.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LeafletLayersDirective", function() { return _leaflet_layers_leaflet_layers_directive__WEBPACK_IMPORTED_MODULE_5__["LeafletLayersDirective"]; });

/* harmony import */ var _leaflet_layers_leaflet_tile_layer_definition_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./leaflet/layers/leaflet-tile-layer-definition.model */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/leaflet-tile-layer-definition.model.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LeafletTileLayerDefinition", function() { return _leaflet_layers_leaflet_tile_layer_definition_model__WEBPACK_IMPORTED_MODULE_6__["LeafletTileLayerDefinition"]; });

/* harmony import */ var _leaflet_layers_base_leaflet_baselayers_directive__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./leaflet/layers/base/leaflet-baselayers.directive */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/base/leaflet-baselayers.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LeafletBaseLayersDirective", function() { return _leaflet_layers_base_leaflet_baselayers_directive__WEBPACK_IMPORTED_MODULE_7__["LeafletBaseLayersDirective"]; });

/* harmony import */ var _leaflet_layers_control_leaflet_control_layers_directive__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./leaflet/layers/control/leaflet-control-layers.directive */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers.directive.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LeafletLayersControlDirective", function() { return _leaflet_layers_control_leaflet_control_layers_directive__WEBPACK_IMPORTED_MODULE_8__["LeafletLayersControlDirective"]; });

/* harmony import */ var _leaflet_layers_control_leaflet_control_layers_wrapper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./leaflet/layers/control/leaflet-control-layers.wrapper */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers.wrapper.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LeafletControlLayersWrapper", function() { return _leaflet_layers_control_leaflet_control_layers_wrapper__WEBPACK_IMPORTED_MODULE_9__["LeafletControlLayersWrapper"]; });

/* harmony import */ var _leaflet_layers_control_leaflet_control_layers_config_model__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./leaflet/layers/control/leaflet-control-layers-config.model */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers-config.model.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LeafletControlLayersConfig", function() { return _leaflet_layers_control_leaflet_control_layers_config_model__WEBPACK_IMPORTED_MODULE_10__["LeafletControlLayersConfig"]; });

/* harmony import */ var _leaflet_layers_control_leaflet_control_layers_changes_model__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./leaflet/layers/control/leaflet-control-layers-changes.model */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers-changes.model.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LeafletControlLayersChanges", function() { return _leaflet_layers_control_leaflet_control_layers_changes_model__WEBPACK_IMPORTED_MODULE_11__["LeafletControlLayersChanges"]; });













//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.directive.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.directive.js ***!
  \*************************************************************************************/
/*! exports provided: LeafletDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeafletDirective", function() { return LeafletDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _leaflet_util__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./leaflet.util */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.util.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LeafletDirective = /** @class */ (function () {
    function LeafletDirective(element, zone) {
        this.element = element;
        this.zone = zone;
        this.DEFAULT_ZOOM = 1;
        this.DEFAULT_CENTER = Object(leaflet__WEBPACK_IMPORTED_MODULE_1__["latLng"])(38.907192, -77.036871);
        this.DEFAULT_FPZ_OPTIONS = {};
        this.fitBoundsOptions = this.DEFAULT_FPZ_OPTIONS;
        this.panOptions = this.DEFAULT_FPZ_OPTIONS;
        this.zoomOptions = this.DEFAULT_FPZ_OPTIONS;
        this.zoomPanOptions = this.DEFAULT_FPZ_OPTIONS;
        // Default configuration
        this.options = {};
        // Configure callback function for the map
        this.mapReady = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.zoomChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.centerChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        // Mouse Map Events
        this.onClick = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onDoubleClick = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onMouseDown = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onMouseUp = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onMouseMove = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onMouseOver = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onMouseOut = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        // Map Move Events
        this.onMapMove = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onMapMoveStart = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onMapMoveEnd = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        // Map Zoom Events
        this.onMapZoom = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onMapZoomStart = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onMapZoomEnd = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        // Nothing here
    }
    LeafletDirective.prototype.ngOnInit = function () {
        var _this = this;
        // Create the map outside of angular so the various map events don't trigger change detection
        this.zone.runOutsideAngular(function () {
            // Create the map with some reasonable defaults
            _this.map = Object(leaflet__WEBPACK_IMPORTED_MODULE_1__["map"])(_this.element.nativeElement, _this.options);
            _this.addMapEventListeners();
        });
        // Only setView if there is a center/zoom
        if (null != this.center && null != this.zoom) {
            this.setView(this.center, this.zoom);
        }
        // Set up all the initial settings
        if (null != this.fitBounds) {
            this.setFitBounds(this.fitBounds);
        }
        if (null != this.maxBounds) {
            this.setMaxBounds(this.maxBounds);
        }
        if (null != this.minZoom) {
            this.setMinZoom(this.minZoom);
        }
        if (null != this.maxZoom) {
            this.setMaxZoom(this.maxZoom);
        }
        this.doResize();
        // Fire map ready event
        this.mapReady.emit(this.map);
    };
    LeafletDirective.prototype.ngOnChanges = function (changes) {
        /*
         * The following code is to address an issue with our (basic) implementation of
         * zooming and panning. From our testing, it seems that a pan operation followed
         * by a zoom operation in the same thread will interfere with eachother. The zoom
         * operation interrupts/cancels the pan, resulting in a final center point that is
         * inaccurate. The solution seems to be to either separate them with a timeout or
          * to collapse them into a setView call.
         */
        // Zooming and Panning
        if (changes['zoom'] && changes['center'] && null != this.zoom && null != this.center) {
            this.setView(changes['center'].currentValue, changes['zoom'].currentValue);
        }
        // Set the zoom level
        else if (changes['zoom']) {
            this.setZoom(changes['zoom'].currentValue);
        }
        // Set the map center
        else if (changes['center']) {
            this.setCenter(changes['center'].currentValue);
        }
        // Other options
        if (changes['fitBounds']) {
            this.setFitBounds(changes['fitBounds'].currentValue);
        }
        if (changes['maxBounds']) {
            this.setMaxBounds(changes['maxBounds'].currentValue);
        }
        if (changes['minZoom']) {
            this.setMinZoom(changes['minZoom'].currentValue);
        }
        if (changes['maxZoom']) {
            this.setMaxZoom(changes['maxZoom'].currentValue);
        }
    };
    LeafletDirective.prototype.getMap = function () {
        return this.map;
    };
    LeafletDirective.prototype.onResize = function () {
        this.delayResize();
    };
    LeafletDirective.prototype.addMapEventListeners = function () {
        var _this = this;
        // Add all the pass-through mouse event handlers
        this.map.on('click', function (e) { return _leaflet_util__WEBPACK_IMPORTED_MODULE_2__["LeafletUtil"].handleEvent(_this.zone, _this.onClick, e); });
        this.map.on('dblclick', function (e) { return _leaflet_util__WEBPACK_IMPORTED_MODULE_2__["LeafletUtil"].handleEvent(_this.zone, _this.onDoubleClick, e); });
        this.map.on('mousedown', function (e) { return _leaflet_util__WEBPACK_IMPORTED_MODULE_2__["LeafletUtil"].handleEvent(_this.zone, _this.onMouseDown, e); });
        this.map.on('mouseup', function (e) { return _leaflet_util__WEBPACK_IMPORTED_MODULE_2__["LeafletUtil"].handleEvent(_this.zone, _this.onMouseUp, e); });
        this.map.on('mouseover', function (e) { return _leaflet_util__WEBPACK_IMPORTED_MODULE_2__["LeafletUtil"].handleEvent(_this.zone, _this.onMouseOver, e); });
        this.map.on('mouseout', function (e) { return _leaflet_util__WEBPACK_IMPORTED_MODULE_2__["LeafletUtil"].handleEvent(_this.zone, _this.onMouseOut, e); });
        this.map.on('mousemove', function (e) { return _leaflet_util__WEBPACK_IMPORTED_MODULE_2__["LeafletUtil"].handleEvent(_this.zone, _this.onMouseMove, e); });
        this.map.on('zoomstart', function (e) { return _leaflet_util__WEBPACK_IMPORTED_MODULE_2__["LeafletUtil"].handleEvent(_this.zone, _this.onMapZoomStart, e); });
        this.map.on('zoom', function (e) { return _leaflet_util__WEBPACK_IMPORTED_MODULE_2__["LeafletUtil"].handleEvent(_this.zone, _this.onMapZoom, e); });
        this.map.on('zoomend', function (e) { return _leaflet_util__WEBPACK_IMPORTED_MODULE_2__["LeafletUtil"].handleEvent(_this.zone, _this.onMapZoomEnd, e); });
        this.map.on('movestart', function (e) { return _leaflet_util__WEBPACK_IMPORTED_MODULE_2__["LeafletUtil"].handleEvent(_this.zone, _this.onMapMoveStart, e); });
        this.map.on('move', function (e) { return _leaflet_util__WEBPACK_IMPORTED_MODULE_2__["LeafletUtil"].handleEvent(_this.zone, _this.onMapMove, e); });
        this.map.on('moveend', function (e) { return _leaflet_util__WEBPACK_IMPORTED_MODULE_2__["LeafletUtil"].handleEvent(_this.zone, _this.onMapMoveEnd, e); });
        // Update any things for which we provide output bindings
        this.map.on('zoomend moveend', function () {
            var zoom = _this.map.getZoom();
            if (zoom !== _this.zoom) {
                _this.zoom = zoom;
                _leaflet_util__WEBPACK_IMPORTED_MODULE_2__["LeafletUtil"].handleEvent(_this.zone, _this.zoomChange, zoom);
            }
            var center = _this.map.getCenter();
            if (null != center || null != _this.center) {
                if (((null == center || null == _this.center) && center !== _this.center)
                    || (center.lat !== _this.center.lat || center.lng !== _this.center.lng)) {
                    _this.center = center;
                    _leaflet_util__WEBPACK_IMPORTED_MODULE_2__["LeafletUtil"].handleEvent(_this.zone, _this.centerChange, center);
                }
            }
        });
    };
    /**
     * Resize the map to fit it's parent container
     */
    LeafletDirective.prototype.doResize = function () {
        var _this = this;
        // Run this outside of angular so the map events stay outside of angular
        this.zone.runOutsideAngular(function () {
            // Invalidate the map size to trigger it to update itself
            _this.map.invalidateSize({});
        });
    };
    /**
     * Manage a delayed resize of the component
     */
    LeafletDirective.prototype.delayResize = function () {
        if (null != this.resizeTimer) {
            clearTimeout(this.resizeTimer);
        }
        this.resizeTimer = setTimeout(this.doResize.bind(this), 200);
    };
    /**
     * Set the view (center/zoom) all at once
     * @param center The new center
     * @param zoom The new zoom level
     */
    LeafletDirective.prototype.setView = function (center, zoom) {
        if (this.map && null != center && null != zoom) {
            this.map.setView(center, zoom, this.zoomPanOptions);
        }
    };
    /**
     * Set the map zoom level
     * @param zoom the new zoom level for the map
     */
    LeafletDirective.prototype.setZoom = function (zoom) {
        if (this.map && null != zoom) {
            this.map.setZoom(zoom, this.zoomOptions);
        }
    };
    /**
     * Set the center of the map
     * @param center the center point
     */
    LeafletDirective.prototype.setCenter = function (center) {
        if (this.map && null != center) {
            this.map.panTo(center, this.panOptions);
        }
    };
    /**
     * Fit the map to the bounds
     * @param latLngBounds the boundary to set
     */
    LeafletDirective.prototype.setFitBounds = function (latLngBounds) {
        if (this.map && null != latLngBounds) {
            this.map.fitBounds(latLngBounds, this.fitBoundsOptions);
        }
    };
    /**
     * Set the map's max bounds
     * @param latLngBounds the boundary to set
     */
    LeafletDirective.prototype.setMaxBounds = function (latLngBounds) {
        if (this.map && null != latLngBounds) {
            this.map.setMaxBounds(latLngBounds);
        }
    };
    /**
     * Set the map's min zoom
     * @param number the new min zoom
     */
    LeafletDirective.prototype.setMinZoom = function (zoom) {
        if (this.map && null != zoom) {
            this.map.setMinZoom(zoom);
        }
    };
    /**
     * Set the map's min zoom
     * @param number the new min zoom
     */
    LeafletDirective.prototype.setMaxZoom = function (zoom) {
        if (this.map && null != zoom) {
            this.map.setMaxZoom(zoom);
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletFitBoundsOptions'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "fitBoundsOptions", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletPanOptions'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "panOptions", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletZoomOptions'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "zoomOptions", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletZoomPanOptions'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "zoomPanOptions", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletOptions'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "options", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletMapReady'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "mapReady", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletZoom'),
        __metadata("design:type", Number)
    ], LeafletDirective.prototype, "zoom", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletZoomChange'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "zoomChange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletCenter'),
        __metadata("design:type", leaflet__WEBPACK_IMPORTED_MODULE_1__["LatLng"])
    ], LeafletDirective.prototype, "center", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletCenterChange'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "centerChange", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletFitBounds'),
        __metadata("design:type", leaflet__WEBPACK_IMPORTED_MODULE_1__["LatLngBounds"])
    ], LeafletDirective.prototype, "fitBounds", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletMaxBounds'),
        __metadata("design:type", leaflet__WEBPACK_IMPORTED_MODULE_1__["LatLngBounds"])
    ], LeafletDirective.prototype, "maxBounds", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletMinZoom'),
        __metadata("design:type", Number)
    ], LeafletDirective.prototype, "minZoom", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletMaxZoom'),
        __metadata("design:type", Number)
    ], LeafletDirective.prototype, "maxZoom", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletClick'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "onClick", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletDoubleClick'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "onDoubleClick", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletMouseDown'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "onMouseDown", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletMouseUp'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "onMouseUp", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletMouseMove'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "onMouseMove", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletMouseOver'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "onMouseOver", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletMouseOut'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "onMouseOut", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletMapMove'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "onMapMove", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletMapMoveStart'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "onMapMoveStart", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletMapMoveEnd'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "onMapMoveEnd", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletMapZoom'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "onMapZoom", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletMapZoomStart'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "onMapZoomStart", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletMapZoomEnd'),
        __metadata("design:type", Object)
    ], LeafletDirective.prototype, "onMapZoomEnd", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:resize', []),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], LeafletDirective.prototype, "onResize", null);
    LeafletDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[leaflet]'
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]])
    ], LeafletDirective);
    return LeafletDirective;
}());

//# sourceMappingURL=leaflet.directive.js.map

/***/ }),

/***/ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.directive.wrapper.js":
/*!*********************************************************************************************!*\
  !*** ./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.directive.wrapper.js ***!
  \*********************************************************************************************/
/*! exports provided: LeafletDirectiveWrapper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeafletDirectiveWrapper", function() { return LeafletDirectiveWrapper; });
var LeafletDirectiveWrapper = /** @class */ (function () {
    function LeafletDirectiveWrapper(leafletDirective) {
        this.leafletDirective = leafletDirective;
    }
    LeafletDirectiveWrapper.prototype.init = function () {
        // Nothing for now
    };
    LeafletDirectiveWrapper.prototype.getMap = function () {
        return this.leafletDirective.getMap();
    };
    return LeafletDirectiveWrapper;
}());

//# sourceMappingURL=leaflet.directive.wrapper.js.map

/***/ }),

/***/ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.util.js":
/*!********************************************************************************!*\
  !*** ./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.util.js ***!
  \********************************************************************************/
/*! exports provided: LeafletUtil */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeafletUtil", function() { return LeafletUtil; });
var LeafletUtil = /** @class */ (function () {
    function LeafletUtil() {
    }
    LeafletUtil.mapToArray = function (map) {
        var toReturn = [];
        for (var k in map) {
            if (map.hasOwnProperty(k)) {
                toReturn.push(map[k]);
            }
        }
        return toReturn;
    };
    LeafletUtil.handleEvent = function (zone, eventEmitter, event) {
        // Don't want to emit if there are no observers
        if (0 < eventEmitter.observers.length) {
            zone.run(function () {
                eventEmitter.emit(event);
            });
        }
    };
    return LeafletUtil;
}());

//# sourceMappingURL=leaflet.util.js.map

/***/ }),

/***/ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/base/leaflet-baselayers.directive.js":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/base/leaflet-baselayers.directive.js ***!
  \*******************************************************************************************************/
/*! exports provided: LeafletBaseLayersDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeafletBaseLayersDirective", function() { return LeafletBaseLayersDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _core_leaflet_util__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/leaflet.util */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.util.js");
/* harmony import */ var _core_leaflet_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/leaflet.directive */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.directive.js");
/* harmony import */ var _core_leaflet_directive_wrapper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../core/leaflet.directive.wrapper */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.directive.wrapper.js");
/* harmony import */ var _control_leaflet_control_layers_wrapper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../control/leaflet-control-layers.wrapper */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers.wrapper.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Baselayers directive
 *
 * This directive is provided as a convenient way to add baselayers to the map. The input accepts
 * a key-value map of layer name -> layer. Mutable changed are detected. On changes, a differ is
 * used to determine what changed so that layers are appropriately added or removed. This directive
 * will also add the layers control so users can switch between available base layers.
 *
 * To specify which layer to show as the 'active' baselayer, you will want to add it to the map
 * using the layers directive. Otherwise, the plugin will use the last one it sees.
 */
var LeafletBaseLayersDirective = /** @class */ (function () {
    function LeafletBaseLayersDirective(leafletDirective, differs, zone) {
        this.differs = differs;
        this.zone = zone;
        // Output for once the layers control is ready
        this.layersControlReady = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.leafletDirective = new _core_leaflet_directive_wrapper__WEBPACK_IMPORTED_MODULE_4__["LeafletDirectiveWrapper"](leafletDirective);
        this.controlLayers = new _control_leaflet_control_layers_wrapper__WEBPACK_IMPORTED_MODULE_5__["LeafletControlLayersWrapper"](this.zone, this.layersControlReady);
        this.baseLayersDiffer = this.differs.find({}).create();
    }
    Object.defineProperty(LeafletBaseLayersDirective.prototype, "baseLayers", {
        get: function () {
            return this.baseLayersValue;
        },
        // Set/get baseLayers
        set: function (v) {
            this.baseLayersValue = v;
            this.updateBaseLayers();
        },
        enumerable: true,
        configurable: true
    });
    LeafletBaseLayersDirective.prototype.ngOnDestroy = function () {
        this.baseLayers = {};
        this.controlLayers.getLayersControl().remove();
    };
    LeafletBaseLayersDirective.prototype.ngOnInit = function () {
        var _this = this;
        // Init the map
        this.leafletDirective.init();
        // Create the control outside angular to prevent events from triggering chnage detection
        this.zone.runOutsideAngular(function () {
            // Initially configure the controlLayers
            _this.controlLayers
                .init({}, _this.layersControlOptions)
                .addTo(_this.leafletDirective.getMap());
        });
        this.updateBaseLayers();
    };
    LeafletBaseLayersDirective.prototype.ngDoCheck = function () {
        this.updateBaseLayers();
    };
    LeafletBaseLayersDirective.prototype.updateBaseLayers = function () {
        var map = this.leafletDirective.getMap();
        var layersControl = this.controlLayers.getLayersControl();
        if (null != map && null != layersControl && null != this.baseLayersDiffer) {
            var changes = this.baseLayersDiffer.diff(this.baseLayersValue);
            var results = this.controlLayers.applyBaseLayerChanges(changes);
            if (results.changed()) {
                this.syncBaseLayer();
            }
        }
    };
    /**
     * Check the current base layer and change it to the new one if necessary
     */
    LeafletBaseLayersDirective.prototype.syncBaseLayer = function () {
        var _this = this;
        var map = this.leafletDirective.getMap();
        var layers = _core_leaflet_util__WEBPACK_IMPORTED_MODULE_2__["LeafletUtil"].mapToArray(this.baseLayers);
        var foundLayer;
        // Search all the layers in the map to see if we can find them in the baselayer array
        map.eachLayer(function (l) {
            foundLayer = layers.find(function (bl) { return (l === bl); });
        });
        // Did we find the layer?
        if (null != foundLayer) {
            // Yes - set the baselayer to the one we found
            this.baseLayer = foundLayer;
        }
        else {
            // No - set the baselayer to the first in the array and add it to the map
            if (layers.length > 0) {
                this.baseLayer = layers[0];
                // Add layers outside of angular to prevent events from triggering change detection
                this.zone.runOutsideAngular(function () {
                    _this.baseLayer.addTo(map);
                });
            }
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletBaseLayers'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], LeafletBaseLayersDirective.prototype, "baseLayers", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletLayersControlOptions'),
        __metadata("design:type", Object)
    ], LeafletBaseLayersDirective.prototype, "layersControlOptions", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletLayersControlReady'),
        __metadata("design:type", Object)
    ], LeafletBaseLayersDirective.prototype, "layersControlReady", void 0);
    LeafletBaseLayersDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[leafletBaseLayers]'
        }),
        __metadata("design:paramtypes", [_core_leaflet_directive__WEBPACK_IMPORTED_MODULE_3__["LeafletDirective"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["KeyValueDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]])
    ], LeafletBaseLayersDirective);
    return LeafletBaseLayersDirective;
}());

//# sourceMappingURL=leaflet-baselayers.directive.js.map

/***/ }),

/***/ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers-changes.model.js":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers-changes.model.js ***!
  \******************************************************************************************************************/
/*! exports provided: LeafletControlLayersChanges */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeafletControlLayersChanges", function() { return LeafletControlLayersChanges; });
var LeafletControlLayersChanges = /** @class */ (function () {
    function LeafletControlLayersChanges() {
        this.layersRemoved = 0;
        this.layersChanged = 0;
        this.layersAdded = 0;
    }
    LeafletControlLayersChanges.prototype.changed = function () {
        return !(this.layersRemoved === 0 && this.layersChanged === 0 && this.layersAdded === 0);
    };
    return LeafletControlLayersChanges;
}());

//# sourceMappingURL=leaflet-control-layers-changes.model.js.map

/***/ }),

/***/ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers-config.model.js":
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers-config.model.js ***!
  \*****************************************************************************************************************/
/*! exports provided: LeafletControlLayersConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeafletControlLayersConfig", function() { return LeafletControlLayersConfig; });
var LeafletControlLayersConfig = /** @class */ (function () {
    function LeafletControlLayersConfig() {
        this.baseLayers = {};
        this.overlays = {};
    }
    return LeafletControlLayersConfig;
}());

//# sourceMappingURL=leaflet-control-layers-config.model.js.map

/***/ }),

/***/ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers.directive.js":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers.directive.js ***!
  \**************************************************************************************************************/
/*! exports provided: LeafletLayersControlDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeafletLayersControlDirective", function() { return LeafletLayersControlDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_leaflet_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core/leaflet.directive */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.directive.js");
/* harmony import */ var _core_leaflet_directive_wrapper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/leaflet.directive.wrapper */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.directive.wrapper.js");
/* harmony import */ var _leaflet_control_layers_wrapper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./leaflet-control-layers.wrapper */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers.wrapper.js");
/* harmony import */ var _leaflet_control_layers_config_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./leaflet-control-layers-config.model */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers-config.model.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Layers Control
 *
 * This directive is used to configure the layers control. The input accepts an object with two
 * key-value maps of layer name -> layer. Mutable changes are detected. On changes, a differ is
 * used to determine what changed so that layers are appropriately added or removed.
 *
 * To specify which layer to show as the 'active' baselayer, you will want to add it to the map
 * using the layers directive. Otherwise, the last one it sees will be used.
 */
var LeafletLayersControlDirective = /** @class */ (function () {
    function LeafletLayersControlDirective(leafletDirective, differs, zone) {
        this.differs = differs;
        this.zone = zone;
        this.layersControlReady = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.leafletDirective = new _core_leaflet_directive_wrapper__WEBPACK_IMPORTED_MODULE_2__["LeafletDirectiveWrapper"](leafletDirective);
        this.controlLayers = new _leaflet_control_layers_wrapper__WEBPACK_IMPORTED_MODULE_3__["LeafletControlLayersWrapper"](this.zone, this.layersControlReady);
        // Generate differs
        this.baseLayersDiffer = this.differs.find({}).create();
        this.overlaysDiffer = this.differs.find({}).create();
    }
    Object.defineProperty(LeafletLayersControlDirective.prototype, "layersControlConfig", {
        get: function () {
            return this.layersControlConfigValue;
        },
        set: function (v) {
            // Validation/init stuff
            if (null == v) {
                v = new _leaflet_control_layers_config_model__WEBPACK_IMPORTED_MODULE_4__["LeafletControlLayersConfig"]();
            }
            if (null == v.baseLayers) {
                v.baseLayers = {};
            }
            if (null == v.overlays) {
                v.overlays = {};
            }
            // Store the value
            this.layersControlConfigValue = v;
            // Update the map
            this.updateLayers();
        },
        enumerable: true,
        configurable: true
    });
    LeafletLayersControlDirective.prototype.ngOnInit = function () {
        var _this = this;
        // Init the map
        this.leafletDirective.init();
        // Set up control outside of angular to avoid change detection when using the control
        this.zone.runOutsideAngular(function () {
            // Set up all the initial settings
            _this.controlLayers
                .init({}, _this.layersControlOptions)
                .addTo(_this.leafletDirective.getMap());
        });
        this.updateLayers();
    };
    LeafletLayersControlDirective.prototype.ngOnDestroy = function () {
        this.layersControlConfig = { baseLayers: {}, overlays: {} };
        this.controlLayers.getLayersControl().remove();
    };
    LeafletLayersControlDirective.prototype.ngDoCheck = function () {
        this.updateLayers();
    };
    LeafletLayersControlDirective.prototype.updateLayers = function () {
        var map = this.leafletDirective.getMap();
        var layersControl = this.controlLayers.getLayersControl();
        if (null != map && null != layersControl) {
            // Run the baselayers differ
            if (null != this.baseLayersDiffer && null != this.layersControlConfigValue.baseLayers) {
                var changes = this.baseLayersDiffer.diff(this.layersControlConfigValue.baseLayers);
                this.controlLayers.applyBaseLayerChanges(changes);
            }
            // Run the overlays differ
            if (null != this.overlaysDiffer && null != this.layersControlConfigValue.overlays) {
                var changes = this.overlaysDiffer.diff(this.layersControlConfigValue.overlays);
                this.controlLayers.applyOverlayChanges(changes);
            }
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletLayersControl'),
        __metadata("design:type", _leaflet_control_layers_config_model__WEBPACK_IMPORTED_MODULE_4__["LeafletControlLayersConfig"]),
        __metadata("design:paramtypes", [_leaflet_control_layers_config_model__WEBPACK_IMPORTED_MODULE_4__["LeafletControlLayersConfig"]])
    ], LeafletLayersControlDirective.prototype, "layersControlConfig", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletLayersControlOptions'),
        __metadata("design:type", Object)
    ], LeafletLayersControlDirective.prototype, "layersControlOptions", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletLayersControlReady'),
        __metadata("design:type", Object)
    ], LeafletLayersControlDirective.prototype, "layersControlReady", void 0);
    LeafletLayersControlDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[leafletLayersControl]'
        }),
        __metadata("design:paramtypes", [_core_leaflet_directive__WEBPACK_IMPORTED_MODULE_1__["LeafletDirective"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["KeyValueDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]])
    ], LeafletLayersControlDirective);
    return LeafletLayersControlDirective;
}());

//# sourceMappingURL=leaflet-control-layers.directive.js.map

/***/ }),

/***/ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers.wrapper.js":
/*!************************************************************************************************************!*\
  !*** ./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers.wrapper.js ***!
  \************************************************************************************************************/
/*! exports provided: LeafletControlLayersWrapper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeafletControlLayersWrapper", function() { return LeafletControlLayersWrapper; });
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _leaflet_control_layers_changes_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./leaflet-control-layers-changes.model */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers-changes.model.js");


var LeafletControlLayersWrapper = /** @class */ (function () {
    function LeafletControlLayersWrapper(zone, layersControlReady) {
        this.zone = zone;
        this.layersControlReady = layersControlReady;
    }
    LeafletControlLayersWrapper.prototype.getLayersControl = function () {
        return this.layersControl;
    };
    LeafletControlLayersWrapper.prototype.init = function (controlConfig, controlOptions) {
        var _this = this;
        var baseLayers = controlConfig.baseLayers || {};
        var overlays = controlConfig.overlays || {};
        // Create the control outside of angular to ensure events don't trigger change detection
        this.zone.runOutsideAngular(function () {
            _this.layersControl = leaflet__WEBPACK_IMPORTED_MODULE_0__["control"].layers(baseLayers, overlays, controlOptions);
        });
        this.layersControlReady.emit(this.layersControl);
        return this.layersControl;
    };
    LeafletControlLayersWrapper.prototype.applyBaseLayerChanges = function (changes) {
        var results = new _leaflet_control_layers_changes_model__WEBPACK_IMPORTED_MODULE_1__["LeafletControlLayersChanges"]();
        if (null != this.layersControl) {
            results = this.applyChanges(changes, this.layersControl.addBaseLayer);
        }
        return results;
    };
    LeafletControlLayersWrapper.prototype.applyOverlayChanges = function (changes) {
        var results = new _leaflet_control_layers_changes_model__WEBPACK_IMPORTED_MODULE_1__["LeafletControlLayersChanges"]();
        if (null != this.layersControl) {
            results = this.applyChanges(changes, this.layersControl.addOverlay);
        }
        return results;
    };
    LeafletControlLayersWrapper.prototype.applyChanges = function (changes, addFn) {
        var _this = this;
        var results = new _leaflet_control_layers_changes_model__WEBPACK_IMPORTED_MODULE_1__["LeafletControlLayersChanges"]();
        if (null != changes) {
            // All layer management is outside angular to avoid layer events from triggering change detection
            this.zone.runOutsideAngular(function () {
                changes.forEachChangedItem(function (c) {
                    _this.layersControl.removeLayer(c.previousValue);
                    addFn.call(_this.layersControl, c.currentValue, c.key);
                    results.layersChanged++;
                });
                changes.forEachRemovedItem(function (c) {
                    _this.layersControl.removeLayer(c.previousValue);
                    results.layersRemoved++;
                });
                changes.forEachAddedItem(function (c) {
                    addFn.call(_this.layersControl, c.currentValue, c.key);
                    results.layersAdded++;
                });
            });
        }
        return results;
    };
    return LeafletControlLayersWrapper;
}());

//# sourceMappingURL=leaflet-control-layers.wrapper.js.map

/***/ }),

/***/ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/leaflet-layer.directive.js":
/*!*********************************************************************************************!*\
  !*** ./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/leaflet-layer.directive.js ***!
  \*********************************************************************************************/
/*! exports provided: LeafletLayerDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeafletLayerDirective", function() { return LeafletLayerDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _core_leaflet_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../core/leaflet.directive */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.directive.js");
/* harmony import */ var _core_leaflet_directive_wrapper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/leaflet.directive.wrapper */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.directive.wrapper.js");
/* harmony import */ var _core_leaflet_util__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../core/leaflet.util */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.util.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Layer directive
 *
 * This directive is used to directly control a single map layer. The purpose of this directive is to
 * be used as part of a child structural directive of the map element.
 *
 */
var LeafletLayerDirective = /** @class */ (function () {
    function LeafletLayerDirective(leafletDirective, zone) {
        this.zone = zone;
        // Layer Events
        this.onAdd = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onRemove = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.leafletDirective = new _core_leaflet_directive_wrapper__WEBPACK_IMPORTED_MODULE_3__["LeafletDirectiveWrapper"](leafletDirective);
    }
    LeafletLayerDirective.prototype.ngOnInit = function () {
        // Init the map
        this.leafletDirective.init();
    };
    LeafletLayerDirective.prototype.ngOnDestroy = function () {
        this.layer.remove();
    };
    LeafletLayerDirective.prototype.ngOnChanges = function (changes) {
        var _this = this;
        if (changes['layer']) {
            // Update the layer
            var p_1 = changes['layer'].previousValue;
            var n_1 = changes['layer'].currentValue;
            this.zone.runOutsideAngular(function () {
                if (null != p_1) {
                    p_1.remove();
                }
                if (null != n_1) {
                    _this.addLayerEventListeners(n_1);
                    _this.leafletDirective.getMap().addLayer(n_1);
                }
            });
        }
    };
    LeafletLayerDirective.prototype.addLayerEventListeners = function (l) {
        var _this = this;
        l.on('add', function (e) { return _core_leaflet_util__WEBPACK_IMPORTED_MODULE_4__["LeafletUtil"].handleEvent(_this.zone, _this.onAdd, e); });
        l.on('remove', function (e) { return _core_leaflet_util__WEBPACK_IMPORTED_MODULE_4__["LeafletUtil"].handleEvent(_this.zone, _this.onRemove, e); });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletLayer'),
        __metadata("design:type", leaflet__WEBPACK_IMPORTED_MODULE_1__["Layer"])
    ], LeafletLayerDirective.prototype, "layer", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletLayerAdd'),
        __metadata("design:type", Object)
    ], LeafletLayerDirective.prototype, "onAdd", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])('leafletLayerRemove'),
        __metadata("design:type", Object)
    ], LeafletLayerDirective.prototype, "onRemove", void 0);
    LeafletLayerDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[leafletLayer]'
        }),
        __metadata("design:paramtypes", [_core_leaflet_directive__WEBPACK_IMPORTED_MODULE_2__["LeafletDirective"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]])
    ], LeafletLayerDirective);
    return LeafletLayerDirective;
}());

//# sourceMappingURL=leaflet-layer.directive.js.map

/***/ }),

/***/ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/leaflet-layers.directive.js":
/*!**********************************************************************************************!*\
  !*** ./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/leaflet-layers.directive.js ***!
  \**********************************************************************************************/
/*! exports provided: LeafletLayersDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeafletLayersDirective", function() { return LeafletLayersDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_leaflet_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../core/leaflet.directive */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.directive.js");
/* harmony import */ var _core_leaflet_directive_wrapper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../core/leaflet.directive.wrapper */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.directive.wrapper.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Layers directive
 *
 * This directive is used to directly control map layers. As changes are made to the input array of
 * layers, the map is synched to the array. As layers are added or removed from the input array, they
 * are also added or removed from the map. The input array is treated as immutable. To detect changes,
 * you must change the array instance.
 *
 * Important Note: The input layers array is assumed to be immutable. This means you need to use an
 * immutable array implementation or create a new copy of your array when you make changes, otherwise
 * this directive won't detect the change. This is by design. It's for performance reasons. Change
 * detection of mutable arrays requires diffing the state of the array on every DoCheck cycle, which
 * is extremely expensive from a time complexity perspective.
 *
 */
var LeafletLayersDirective = /** @class */ (function () {
    function LeafletLayersDirective(leafletDirective, differs, zone) {
        this.differs = differs;
        this.zone = zone;
        this.leafletDirective = new _core_leaflet_directive_wrapper__WEBPACK_IMPORTED_MODULE_2__["LeafletDirectiveWrapper"](leafletDirective);
        this.layersDiffer = this.differs.find([]).create();
    }
    Object.defineProperty(LeafletLayersDirective.prototype, "layers", {
        get: function () {
            return this.layersValue;
        },
        // Set/get the layers
        set: function (v) {
            this.layersValue = v;
            // Now that we have a differ, do an immediate layer update
            this.updateLayers();
        },
        enumerable: true,
        configurable: true
    });
    LeafletLayersDirective.prototype.ngDoCheck = function () {
        this.updateLayers();
    };
    LeafletLayersDirective.prototype.ngOnInit = function () {
        // Init the map
        this.leafletDirective.init();
        // Update layers once the map is ready
        this.updateLayers();
    };
    LeafletLayersDirective.prototype.ngOnDestroy = function () {
        this.layers = [];
    };
    /**
     * Update the state of the layers.
     * We use an iterable differ to synchronize the map layers with the state of the bound layers array.
     * This is important because it allows us to react to changes to the contents of the array as well
     * as changes to the actual array instance.
     */
    LeafletLayersDirective.prototype.updateLayers = function () {
        var map = this.leafletDirective.getMap();
        if (null != map && null != this.layersDiffer) {
            var changes_1 = this.layersDiffer.diff(this.layersValue);
            if (null != changes_1) {
                // Run outside angular to ensure layer events don't trigger change detection
                this.zone.runOutsideAngular(function () {
                    changes_1.forEachRemovedItem(function (c) {
                        map.removeLayer(c.item);
                    });
                    changes_1.forEachAddedItem(function (c) {
                        map.addLayer(c.item);
                    });
                });
            }
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('leafletLayers'),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], LeafletLayersDirective.prototype, "layers", null);
    LeafletLayersDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[leafletLayers]'
        }),
        __metadata("design:paramtypes", [_core_leaflet_directive__WEBPACK_IMPORTED_MODULE_1__["LeafletDirective"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["IterableDiffers"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"]])
    ], LeafletLayersDirective);
    return LeafletLayersDirective;
}());

//# sourceMappingURL=leaflet-layers.directive.js.map

/***/ }),

/***/ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/leaflet-tile-layer-definition.model.js":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/leaflet-tile-layer-definition.model.js ***!
  \*********************************************************************************************************/
/*! exports provided: LeafletTileLayerDefinition */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeafletTileLayerDefinition", function() { return LeafletTileLayerDefinition; });
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_0__);

var LeafletTileLayerDefinition = /** @class */ (function () {
    function LeafletTileLayerDefinition(type, url, options) {
        this.type = type;
        this.url = url;
        this.options = options;
    }
    /**
     * Creates a TileLayer from the provided definition. This is a convenience function
     * to help with generating layers from objects.
     *
     * @param layerDef The layer to create
     * @returns {TileLayer} The TileLayer that has been created
     */
    LeafletTileLayerDefinition.createTileLayer = function (layerDef) {
        var layer;
        switch (layerDef.type) {
            case 'xyz':
                layer = Object(leaflet__WEBPACK_IMPORTED_MODULE_0__["tileLayer"])(layerDef.url, layerDef.options);
                break;
            case 'wms':
            default:
                layer = leaflet__WEBPACK_IMPORTED_MODULE_0__["tileLayer"].wms(layerDef.url, layerDef.options);
                break;
        }
        return layer;
    };
    /**
     * Creates a TileLayer for each key in the incoming map. This is a convenience function
     * for generating an associative array of layers from an associative array of objects
     *
     * @param layerDefs A map of key to tile layer definition
     * @returns {{[p: string]: TileLayer}} A new map of key to TileLayer
     */
    LeafletTileLayerDefinition.createTileLayers = function (layerDefs) {
        var layers = {};
        for (var k in layerDefs) {
            if (layerDefs.hasOwnProperty(k)) {
                layers[k] = (LeafletTileLayerDefinition.createTileLayer(layerDefs[k]));
            }
        }
        return layers;
    };
    /**
     * Create a Tile Layer from the current state of this object
     *
     * @returns {TileLayer} A new TileLayer
     */
    LeafletTileLayerDefinition.prototype.createTileLayer = function () {
        return LeafletTileLayerDefinition.createTileLayer(this);
    };
    return LeafletTileLayerDefinition;
}());

//# sourceMappingURL=leaflet-tile-layer-definition.model.js.map

/***/ }),

/***/ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/leaflet.module.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/leaflet.module.js ***!
  \*****************************************************************************/
/*! exports provided: LeafletModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LeafletModule", function() { return LeafletModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_leaflet_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./core/leaflet.directive */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/core/leaflet.directive.js");
/* harmony import */ var _layers_leaflet_layer_directive__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./layers/leaflet-layer.directive */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/leaflet-layer.directive.js");
/* harmony import */ var _layers_leaflet_layers_directive__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./layers/leaflet-layers.directive */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/leaflet-layers.directive.js");
/* harmony import */ var _layers_control_leaflet_control_layers_directive__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./layers/control/leaflet-control-layers.directive */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/control/leaflet-control-layers.directive.js");
/* harmony import */ var _layers_base_leaflet_baselayers_directive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./layers/base/leaflet-baselayers.directive */ "./node_modules/@asymmetrik/ngx-leaflet/dist/leaflet/layers/base/leaflet-baselayers.directive.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var LeafletModule = /** @class */ (function () {
    function LeafletModule() {
    }
    LeafletModule_1 = LeafletModule;
    LeafletModule.forRoot = function () {
        return { ngModule: LeafletModule_1, providers: [] };
    };
    var LeafletModule_1;
    LeafletModule = LeafletModule_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            exports: [
                _core_leaflet_directive__WEBPACK_IMPORTED_MODULE_1__["LeafletDirective"],
                _layers_leaflet_layer_directive__WEBPACK_IMPORTED_MODULE_2__["LeafletLayerDirective"],
                _layers_leaflet_layers_directive__WEBPACK_IMPORTED_MODULE_3__["LeafletLayersDirective"],
                _layers_control_leaflet_control_layers_directive__WEBPACK_IMPORTED_MODULE_4__["LeafletLayersControlDirective"],
                _layers_base_leaflet_baselayers_directive__WEBPACK_IMPORTED_MODULE_5__["LeafletBaseLayersDirective"]
            ],
            declarations: [
                _core_leaflet_directive__WEBPACK_IMPORTED_MODULE_1__["LeafletDirective"],
                _layers_leaflet_layer_directive__WEBPACK_IMPORTED_MODULE_2__["LeafletLayerDirective"],
                _layers_leaflet_layers_directive__WEBPACK_IMPORTED_MODULE_3__["LeafletLayersDirective"],
                _layers_control_leaflet_control_layers_directive__WEBPACK_IMPORTED_MODULE_4__["LeafletLayersControlDirective"],
                _layers_base_leaflet_baselayers_directive__WEBPACK_IMPORTED_MODULE_5__["LeafletBaseLayersDirective"]
            ]
        })
    ], LeafletModule);
    return LeafletModule;
}());

//# sourceMappingURL=leaflet.module.js.map

/***/ }),

/***/ "./node_modules/leaflet.markercluster/dist/leaflet.markercluster-src.js":
/*!******************************************************************************!*\
  !*** ./node_modules/leaflet.markercluster/dist/leaflet.markercluster-src.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
 * Leaflet.markercluster 1.4.1+master.94f9815,
 * Provides Beautiful Animated Marker Clustering functionality for Leaflet, a JS library for interactive maps.
 * https://github.com/Leaflet/Leaflet.markercluster
 * (c) 2012-2017, Dave Leaver, smartrak
 */
(function (global, factory) {
	 true ? factory(exports) :
	undefined;
}(this, (function (exports) { 'use strict';

/*
 * L.MarkerClusterGroup extends L.FeatureGroup by clustering the markers contained within
 */

var MarkerClusterGroup = L.MarkerClusterGroup = L.FeatureGroup.extend({

	options: {
		maxClusterRadius: 80, //A cluster will cover at most this many pixels from its center
		iconCreateFunction: null,
		clusterPane: L.Marker.prototype.options.pane,

		spiderfyOnMaxZoom: true,
		showCoverageOnHover: true,
		zoomToBoundsOnClick: true,
		singleMarkerMode: false,

		disableClusteringAtZoom: null,

		// Setting this to false prevents the removal of any clusters outside of the viewpoint, which
		// is the default behaviour for performance reasons.
		removeOutsideVisibleBounds: true,

		// Set to false to disable all animations (zoom and spiderfy).
		// If false, option animateAddingMarkers below has no effect.
		// If L.DomUtil.TRANSITION is falsy, this option has no effect.
		animate: true,

		//Whether to animate adding markers after adding the MarkerClusterGroup to the map
		// If you are adding individual markers set to true, if adding bulk markers leave false for massive performance gains.
		animateAddingMarkers: false,

		//Increase to increase the distance away that spiderfied markers appear from the center
		spiderfyDistanceMultiplier: 1,

		// Make it possible to specify a polyline options on a spider leg
		spiderLegPolylineOptions: { weight: 1.5, color: '#222', opacity: 0.5 },

		// When bulk adding layers, adds markers in chunks. Means addLayers may not add all the layers in the call, others will be loaded during setTimeouts
		chunkedLoading: false,
		chunkInterval: 200, // process markers for a maximum of ~ n milliseconds (then trigger the chunkProgress callback)
		chunkDelay: 50, // at the end of each interval, give n milliseconds back to system/browser
		chunkProgress: null, // progress callback: function(processed, total, elapsed) (e.g. for a progress indicator)

		//Options to pass to the L.Polygon constructor
		polygonOptions: {}
	},

	initialize: function (options) {
		L.Util.setOptions(this, options);
		if (!this.options.iconCreateFunction) {
			this.options.iconCreateFunction = this._defaultIconCreateFunction;
		}

		this._featureGroup = L.featureGroup();
		this._featureGroup.addEventParent(this);

		this._nonPointGroup = L.featureGroup();
		this._nonPointGroup.addEventParent(this);

		this._inZoomAnimation = 0;
		this._needsClustering = [];
		this._needsRemoving = []; //Markers removed while we aren't on the map need to be kept track of
		//The bounds of the currently shown area (from _getExpandedVisibleBounds) Updated on zoom/move
		this._currentShownBounds = null;

		this._queue = [];

		this._childMarkerEventHandlers = {
			'dragstart': this._childMarkerDragStart,
			'move': this._childMarkerMoved,
			'dragend': this._childMarkerDragEnd,
		};

		// Hook the appropriate animation methods.
		var animate = L.DomUtil.TRANSITION && this.options.animate;
		L.extend(this, animate ? this._withAnimation : this._noAnimation);
		// Remember which MarkerCluster class to instantiate (animated or not).
		this._markerCluster = animate ? L.MarkerCluster : L.MarkerClusterNonAnimated;
	},

	addLayer: function (layer) {

		if (layer instanceof L.LayerGroup) {
			return this.addLayers([layer]);
		}

		//Don't cluster non point data
		if (!layer.getLatLng) {
			this._nonPointGroup.addLayer(layer);
			this.fire('layeradd', { layer: layer });
			return this;
		}

		if (!this._map) {
			this._needsClustering.push(layer);
			this.fire('layeradd', { layer: layer });
			return this;
		}

		if (this.hasLayer(layer)) {
			return this;
		}


		//If we have already clustered we'll need to add this one to a cluster

		if (this._unspiderfy) {
			this._unspiderfy();
		}

		this._addLayer(layer, this._maxZoom);
		this.fire('layeradd', { layer: layer });

		// Refresh bounds and weighted positions.
		this._topClusterLevel._recalculateBounds();

		this._refreshClustersIcons();

		//Work out what is visible
		var visibleLayer = layer,
		    currentZoom = this._zoom;
		if (layer.__parent) {
			while (visibleLayer.__parent._zoom >= currentZoom) {
				visibleLayer = visibleLayer.__parent;
			}
		}

		if (this._currentShownBounds.contains(visibleLayer.getLatLng())) {
			if (this.options.animateAddingMarkers) {
				this._animationAddLayer(layer, visibleLayer);
			} else {
				this._animationAddLayerNonAnimated(layer, visibleLayer);
			}
		}
		return this;
	},

	removeLayer: function (layer) {

		if (layer instanceof L.LayerGroup) {
			return this.removeLayers([layer]);
		}

		//Non point layers
		if (!layer.getLatLng) {
			this._nonPointGroup.removeLayer(layer);
			this.fire('layerremove', { layer: layer });
			return this;
		}

		if (!this._map) {
			if (!this._arraySplice(this._needsClustering, layer) && this.hasLayer(layer)) {
				this._needsRemoving.push({ layer: layer, latlng: layer._latlng });
			}
			this.fire('layerremove', { layer: layer });
			return this;
		}

		if (!layer.__parent) {
			return this;
		}

		if (this._unspiderfy) {
			this._unspiderfy();
			this._unspiderfyLayer(layer);
		}

		//Remove the marker from clusters
		this._removeLayer(layer, true);
		this.fire('layerremove', { layer: layer });

		// Refresh bounds and weighted positions.
		this._topClusterLevel._recalculateBounds();

		this._refreshClustersIcons();

		layer.off(this._childMarkerEventHandlers, this);

		if (this._featureGroup.hasLayer(layer)) {
			this._featureGroup.removeLayer(layer);
			if (layer.clusterShow) {
				layer.clusterShow();
			}
		}

		return this;
	},

	//Takes an array of markers and adds them in bulk
	addLayers: function (layersArray, skipLayerAddEvent) {
		if (!L.Util.isArray(layersArray)) {
			return this.addLayer(layersArray);
		}

		var fg = this._featureGroup,
		    npg = this._nonPointGroup,
		    chunked = this.options.chunkedLoading,
		    chunkInterval = this.options.chunkInterval,
		    chunkProgress = this.options.chunkProgress,
		    l = layersArray.length,
		    offset = 0,
		    originalArray = true,
		    m;

		if (this._map) {
			var started = (new Date()).getTime();
			var process = L.bind(function () {
				var start = (new Date()).getTime();
				for (; offset < l; offset++) {
					if (chunked && offset % 200 === 0) {
						// every couple hundred markers, instrument the time elapsed since processing started:
						var elapsed = (new Date()).getTime() - start;
						if (elapsed > chunkInterval) {
							break; // been working too hard, time to take a break :-)
						}
					}

					m = layersArray[offset];

					// Group of layers, append children to layersArray and skip.
					// Side effects:
					// - Total increases, so chunkProgress ratio jumps backward.
					// - Groups are not included in this group, only their non-group child layers (hasLayer).
					// Changing array length while looping does not affect performance in current browsers:
					// http://jsperf.com/for-loop-changing-length/6
					if (m instanceof L.LayerGroup) {
						if (originalArray) {
							layersArray = layersArray.slice();
							originalArray = false;
						}
						this._extractNonGroupLayers(m, layersArray);
						l = layersArray.length;
						continue;
					}

					//Not point data, can't be clustered
					if (!m.getLatLng) {
						npg.addLayer(m);
						if (!skipLayerAddEvent) {
							this.fire('layeradd', { layer: m });
						}
						continue;
					}

					if (this.hasLayer(m)) {
						continue;
					}

					this._addLayer(m, this._maxZoom);
					if (!skipLayerAddEvent) {
						this.fire('layeradd', { layer: m });
					}

					//If we just made a cluster of size 2 then we need to remove the other marker from the map (if it is) or we never will
					if (m.__parent) {
						if (m.__parent.getChildCount() === 2) {
							var markers = m.__parent.getAllChildMarkers(),
							    otherMarker = markers[0] === m ? markers[1] : markers[0];
							fg.removeLayer(otherMarker);
						}
					}
				}

				if (chunkProgress) {
					// report progress and time elapsed:
					chunkProgress(offset, l, (new Date()).getTime() - started);
				}

				// Completed processing all markers.
				if (offset === l) {

					// Refresh bounds and weighted positions.
					this._topClusterLevel._recalculateBounds();

					this._refreshClustersIcons();

					this._topClusterLevel._recursivelyAddChildrenToMap(null, this._zoom, this._currentShownBounds);
				} else {
					setTimeout(process, this.options.chunkDelay);
				}
			}, this);

			process();
		} else {
			var needsClustering = this._needsClustering;

			for (; offset < l; offset++) {
				m = layersArray[offset];

				// Group of layers, append children to layersArray and skip.
				if (m instanceof L.LayerGroup) {
					if (originalArray) {
						layersArray = layersArray.slice();
						originalArray = false;
					}
					this._extractNonGroupLayers(m, layersArray);
					l = layersArray.length;
					continue;
				}

				//Not point data, can't be clustered
				if (!m.getLatLng) {
					npg.addLayer(m);
					continue;
				}

				if (this.hasLayer(m)) {
					continue;
				}

				needsClustering.push(m);
			}
		}
		return this;
	},

	//Takes an array of markers and removes them in bulk
	removeLayers: function (layersArray) {
		var i, m,
		    l = layersArray.length,
		    fg = this._featureGroup,
		    npg = this._nonPointGroup,
		    originalArray = true;

		if (!this._map) {
			for (i = 0; i < l; i++) {
				m = layersArray[i];

				// Group of layers, append children to layersArray and skip.
				if (m instanceof L.LayerGroup) {
					if (originalArray) {
						layersArray = layersArray.slice();
						originalArray = false;
					}
					this._extractNonGroupLayers(m, layersArray);
					l = layersArray.length;
					continue;
				}

				this._arraySplice(this._needsClustering, m);
				npg.removeLayer(m);
				if (this.hasLayer(m)) {
					this._needsRemoving.push({ layer: m, latlng: m._latlng });
				}
				this.fire('layerremove', { layer: m });
			}
			return this;
		}

		if (this._unspiderfy) {
			this._unspiderfy();

			// Work on a copy of the array, so that next loop is not affected.
			var layersArray2 = layersArray.slice(),
			    l2 = l;
			for (i = 0; i < l2; i++) {
				m = layersArray2[i];

				// Group of layers, append children to layersArray and skip.
				if (m instanceof L.LayerGroup) {
					this._extractNonGroupLayers(m, layersArray2);
					l2 = layersArray2.length;
					continue;
				}

				this._unspiderfyLayer(m);
			}
		}

		for (i = 0; i < l; i++) {
			m = layersArray[i];

			// Group of layers, append children to layersArray and skip.
			if (m instanceof L.LayerGroup) {
				if (originalArray) {
					layersArray = layersArray.slice();
					originalArray = false;
				}
				this._extractNonGroupLayers(m, layersArray);
				l = layersArray.length;
				continue;
			}

			if (!m.__parent) {
				npg.removeLayer(m);
				this.fire('layerremove', { layer: m });
				continue;
			}

			this._removeLayer(m, true, true);
			this.fire('layerremove', { layer: m });

			if (fg.hasLayer(m)) {
				fg.removeLayer(m);
				if (m.clusterShow) {
					m.clusterShow();
				}
			}
		}

		// Refresh bounds and weighted positions.
		this._topClusterLevel._recalculateBounds();

		this._refreshClustersIcons();

		//Fix up the clusters and markers on the map
		this._topClusterLevel._recursivelyAddChildrenToMap(null, this._zoom, this._currentShownBounds);

		return this;
	},

	//Removes all layers from the MarkerClusterGroup
	clearLayers: function () {
		//Need our own special implementation as the LayerGroup one doesn't work for us

		//If we aren't on the map (yet), blow away the markers we know of
		if (!this._map) {
			this._needsClustering = [];
			this._needsRemoving = [];
			delete this._gridClusters;
			delete this._gridUnclustered;
		}

		if (this._noanimationUnspiderfy) {
			this._noanimationUnspiderfy();
		}

		//Remove all the visible layers
		this._featureGroup.clearLayers();
		this._nonPointGroup.clearLayers();

		this.eachLayer(function (marker) {
			marker.off(this._childMarkerEventHandlers, this);
			delete marker.__parent;
		}, this);

		if (this._map) {
			//Reset _topClusterLevel and the DistanceGrids
			this._generateInitialClusters();
		}

		return this;
	},

	//Override FeatureGroup.getBounds as it doesn't work
	getBounds: function () {
		var bounds = new L.LatLngBounds();

		if (this._topClusterLevel) {
			bounds.extend(this._topClusterLevel._bounds);
		}

		for (var i = this._needsClustering.length - 1; i >= 0; i--) {
			bounds.extend(this._needsClustering[i].getLatLng());
		}

		bounds.extend(this._nonPointGroup.getBounds());

		return bounds;
	},

	//Overrides LayerGroup.eachLayer
	eachLayer: function (method, context) {
		var markers = this._needsClustering.slice(),
			needsRemoving = this._needsRemoving,
			thisNeedsRemoving, i, j;

		if (this._topClusterLevel) {
			this._topClusterLevel.getAllChildMarkers(markers);
		}

		for (i = markers.length - 1; i >= 0; i--) {
			thisNeedsRemoving = true;

			for (j = needsRemoving.length - 1; j >= 0; j--) {
				if (needsRemoving[j].layer === markers[i]) {
					thisNeedsRemoving = false;
					break;
				}
			}

			if (thisNeedsRemoving) {
				method.call(context, markers[i]);
			}
		}

		this._nonPointGroup.eachLayer(method, context);
	},

	//Overrides LayerGroup.getLayers
	getLayers: function () {
		var layers = [];
		this.eachLayer(function (l) {
			layers.push(l);
		});
		return layers;
	},

	//Overrides LayerGroup.getLayer, WARNING: Really bad performance
	getLayer: function (id) {
		var result = null;

		id = parseInt(id, 10);

		this.eachLayer(function (l) {
			if (L.stamp(l) === id) {
				result = l;
			}
		});

		return result;
	},

	//Returns true if the given layer is in this MarkerClusterGroup
	hasLayer: function (layer) {
		if (!layer) {
			return false;
		}

		var i, anArray = this._needsClustering;

		for (i = anArray.length - 1; i >= 0; i--) {
			if (anArray[i] === layer) {
				return true;
			}
		}

		anArray = this._needsRemoving;
		for (i = anArray.length - 1; i >= 0; i--) {
			if (anArray[i].layer === layer) {
				return false;
			}
		}

		return !!(layer.__parent && layer.__parent._group === this) || this._nonPointGroup.hasLayer(layer);
	},

	//Zoom down to show the given layer (spiderfying if necessary) then calls the callback
	zoomToShowLayer: function (layer, callback) {

		if (typeof callback !== 'function') {
			callback = function () {};
		}

		var showMarker = function () {
			if ((layer._icon || layer.__parent._icon) && !this._inZoomAnimation) {
				this._map.off('moveend', showMarker, this);
				this.off('animationend', showMarker, this);

				if (layer._icon) {
					callback();
				} else if (layer.__parent._icon) {
					this.once('spiderfied', callback, this);
					layer.__parent.spiderfy();
				}
			}
		};

		if (layer._icon && this._map.getBounds().contains(layer.getLatLng())) {
			//Layer is visible ond on screen, immediate return
			callback();
		} else if (layer.__parent._zoom < Math.round(this._map._zoom)) {
			//Layer should be visible at this zoom level. It must not be on screen so just pan over to it
			this._map.on('moveend', showMarker, this);
			this._map.panTo(layer.getLatLng());
		} else {
			this._map.on('moveend', showMarker, this);
			this.on('animationend', showMarker, this);
			layer.__parent.zoomToBounds();
		}
	},

	//Overrides FeatureGroup.onAdd
	onAdd: function (map) {
		this._map = map;
		var i, l, layer;

		if (!isFinite(this._map.getMaxZoom())) {
			throw "Map has no maxZoom specified";
		}

		this._featureGroup.addTo(map);
		this._nonPointGroup.addTo(map);

		if (!this._gridClusters) {
			this._generateInitialClusters();
		}

		this._maxLat = map.options.crs.projection.MAX_LATITUDE;

		//Restore all the positions as they are in the MCG before removing them
		for (i = 0, l = this._needsRemoving.length; i < l; i++) {
			layer = this._needsRemoving[i];
			layer.newlatlng = layer.layer._latlng;
			layer.layer._latlng = layer.latlng;
		}
		//Remove them, then restore their new positions
		for (i = 0, l = this._needsRemoving.length; i < l; i++) {
			layer = this._needsRemoving[i];
			this._removeLayer(layer.layer, true);
			layer.layer._latlng = layer.newlatlng;
		}
		this._needsRemoving = [];

		//Remember the current zoom level and bounds
		this._zoom = Math.round(this._map._zoom);
		this._currentShownBounds = this._getExpandedVisibleBounds();

		this._map.on('zoomend', this._zoomEnd, this);
		this._map.on('moveend', this._moveEnd, this);

		if (this._spiderfierOnAdd) { //TODO FIXME: Not sure how to have spiderfier add something on here nicely
			this._spiderfierOnAdd();
		}

		this._bindEvents();

		//Actually add our markers to the map:
		l = this._needsClustering;
		this._needsClustering = [];
		this.addLayers(l, true);
	},

	//Overrides FeatureGroup.onRemove
	onRemove: function (map) {
		map.off('zoomend', this._zoomEnd, this);
		map.off('moveend', this._moveEnd, this);

		this._unbindEvents();

		//In case we are in a cluster animation
		this._map._mapPane.className = this._map._mapPane.className.replace(' leaflet-cluster-anim', '');

		if (this._spiderfierOnRemove) { //TODO FIXME: Not sure how to have spiderfier add something on here nicely
			this._spiderfierOnRemove();
		}

		delete this._maxLat;

		//Clean up all the layers we added to the map
		this._hideCoverage();
		this._featureGroup.remove();
		this._nonPointGroup.remove();

		this._featureGroup.clearLayers();

		this._map = null;
	},

	getVisibleParent: function (marker) {
		var vMarker = marker;
		while (vMarker && !vMarker._icon) {
			vMarker = vMarker.__parent;
		}
		return vMarker || null;
	},

	//Remove the given object from the given array
	_arraySplice: function (anArray, obj) {
		for (var i = anArray.length - 1; i >= 0; i--) {
			if (anArray[i] === obj) {
				anArray.splice(i, 1);
				return true;
			}
		}
	},

	/**
	 * Removes a marker from all _gridUnclustered zoom levels, starting at the supplied zoom.
	 * @param marker to be removed from _gridUnclustered.
	 * @param z integer bottom start zoom level (included)
	 * @private
	 */
	_removeFromGridUnclustered: function (marker, z) {
		var map = this._map,
		    gridUnclustered = this._gridUnclustered,
			minZoom = Math.floor(this._map.getMinZoom());

		for (; z >= minZoom; z--) {
			if (!gridUnclustered[z].removeObject(marker, map.project(marker.getLatLng(), z))) {
				break;
			}
		}
	},

	_childMarkerDragStart: function (e) {
		e.target.__dragStart = e.target._latlng;
	},

	_childMarkerMoved: function (e) {
		if (!this._ignoreMove && !e.target.__dragStart) {
			var isPopupOpen = e.target._popup && e.target._popup.isOpen();

			this._moveChild(e.target, e.oldLatLng, e.latlng);

			if (isPopupOpen) {
				e.target.openPopup();
			}
		}
	},

	_moveChild: function (layer, from, to) {
		layer._latlng = from;
		this.removeLayer(layer);

		layer._latlng = to;
		this.addLayer(layer);
	},

	_childMarkerDragEnd: function (e) {
		var dragStart = e.target.__dragStart;
		delete e.target.__dragStart;
		if (dragStart) {
			this._moveChild(e.target, dragStart, e.target._latlng);
		}		
	},


	//Internal function for removing a marker from everything.
	//dontUpdateMap: set to true if you will handle updating the map manually (for bulk functions)
	_removeLayer: function (marker, removeFromDistanceGrid, dontUpdateMap) {
		var gridClusters = this._gridClusters,
			gridUnclustered = this._gridUnclustered,
			fg = this._featureGroup,
			map = this._map,
			minZoom = Math.floor(this._map.getMinZoom());

		//Remove the marker from distance clusters it might be in
		if (removeFromDistanceGrid) {
			this._removeFromGridUnclustered(marker, this._maxZoom);
		}

		//Work our way up the clusters removing them as we go if required
		var cluster = marker.__parent,
			markers = cluster._markers,
			otherMarker;

		//Remove the marker from the immediate parents marker list
		this._arraySplice(markers, marker);

		while (cluster) {
			cluster._childCount--;
			cluster._boundsNeedUpdate = true;

			if (cluster._zoom < minZoom) {
				//Top level, do nothing
				break;
			} else if (removeFromDistanceGrid && cluster._childCount <= 1) { //Cluster no longer required
				//We need to push the other marker up to the parent
				otherMarker = cluster._markers[0] === marker ? cluster._markers[1] : cluster._markers[0];

				//Update distance grid
				gridClusters[cluster._zoom].removeObject(cluster, map.project(cluster._cLatLng, cluster._zoom));
				gridUnclustered[cluster._zoom].addObject(otherMarker, map.project(otherMarker.getLatLng(), cluster._zoom));

				//Move otherMarker up to parent
				this._arraySplice(cluster.__parent._childClusters, cluster);
				cluster.__parent._markers.push(otherMarker);
				otherMarker.__parent = cluster.__parent;

				if (cluster._icon) {
					//Cluster is currently on the map, need to put the marker on the map instead
					fg.removeLayer(cluster);
					if (!dontUpdateMap) {
						fg.addLayer(otherMarker);
					}
				}
			} else {
				cluster._iconNeedsUpdate = true;
			}

			cluster = cluster.__parent;
		}

		delete marker.__parent;
	},

	_isOrIsParent: function (el, oel) {
		while (oel) {
			if (el === oel) {
				return true;
			}
			oel = oel.parentNode;
		}
		return false;
	},

	//Override L.Evented.fire
	fire: function (type, data, propagate) {
		if (data && data.layer instanceof L.MarkerCluster) {
			//Prevent multiple clustermouseover/off events if the icon is made up of stacked divs (Doesn't work in ie <= 8, no relatedTarget)
			if (data.originalEvent && this._isOrIsParent(data.layer._icon, data.originalEvent.relatedTarget)) {
				return;
			}
			type = 'cluster' + type;
		}

		L.FeatureGroup.prototype.fire.call(this, type, data, propagate);
	},

	//Override L.Evented.listens
	listens: function (type, propagate) {
		return L.FeatureGroup.prototype.listens.call(this, type, propagate) || L.FeatureGroup.prototype.listens.call(this, 'cluster' + type, propagate);
	},

	//Default functionality
	_defaultIconCreateFunction: function (cluster) {
		var childCount = cluster.getChildCount();

		var c = ' marker-cluster-';
		if (childCount < 10) {
			c += 'small';
		} else if (childCount < 100) {
			c += 'medium';
		} else {
			c += 'large';
		}

		return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
	},

	_bindEvents: function () {
		var map = this._map,
		    spiderfyOnMaxZoom = this.options.spiderfyOnMaxZoom,
		    showCoverageOnHover = this.options.showCoverageOnHover,
		    zoomToBoundsOnClick = this.options.zoomToBoundsOnClick;

		//Zoom on cluster click or spiderfy if we are at the lowest level
		if (spiderfyOnMaxZoom || zoomToBoundsOnClick) {
			this.on('clusterclick', this._zoomOrSpiderfy, this);
		}

		//Show convex hull (boundary) polygon on mouse over
		if (showCoverageOnHover) {
			this.on('clustermouseover', this._showCoverage, this);
			this.on('clustermouseout', this._hideCoverage, this);
			map.on('zoomend', this._hideCoverage, this);
		}
	},

	_zoomOrSpiderfy: function (e) {
		var cluster = e.layer,
		    bottomCluster = cluster;

		while (bottomCluster._childClusters.length === 1) {
			bottomCluster = bottomCluster._childClusters[0];
		}

		if (bottomCluster._zoom === this._maxZoom &&
			bottomCluster._childCount === cluster._childCount &&
			this.options.spiderfyOnMaxZoom) {

			// All child markers are contained in a single cluster from this._maxZoom to this cluster.
			cluster.spiderfy();
		} else if (this.options.zoomToBoundsOnClick) {
			cluster.zoomToBounds();
		}

		// Focus the map again for keyboard users.
		if (e.originalEvent && e.originalEvent.keyCode === 13) {
			this._map._container.focus();
		}
	},

	_showCoverage: function (e) {
		var map = this._map;
		if (this._inZoomAnimation) {
			return;
		}
		if (this._shownPolygon) {
			map.removeLayer(this._shownPolygon);
		}
		if (e.layer.getChildCount() > 2 && e.layer !== this._spiderfied) {
			this._shownPolygon = new L.Polygon(e.layer.getConvexHull(), this.options.polygonOptions);
			map.addLayer(this._shownPolygon);
		}
	},

	_hideCoverage: function () {
		if (this._shownPolygon) {
			this._map.removeLayer(this._shownPolygon);
			this._shownPolygon = null;
		}
	},

	_unbindEvents: function () {
		var spiderfyOnMaxZoom = this.options.spiderfyOnMaxZoom,
			showCoverageOnHover = this.options.showCoverageOnHover,
			zoomToBoundsOnClick = this.options.zoomToBoundsOnClick,
			map = this._map;

		if (spiderfyOnMaxZoom || zoomToBoundsOnClick) {
			this.off('clusterclick', this._zoomOrSpiderfy, this);
		}
		if (showCoverageOnHover) {
			this.off('clustermouseover', this._showCoverage, this);
			this.off('clustermouseout', this._hideCoverage, this);
			map.off('zoomend', this._hideCoverage, this);
		}
	},

	_zoomEnd: function () {
		if (!this._map) { //May have been removed from the map by a zoomEnd handler
			return;
		}
		this._mergeSplitClusters();

		this._zoom = Math.round(this._map._zoom);
		this._currentShownBounds = this._getExpandedVisibleBounds();
	},

	_moveEnd: function () {
		if (this._inZoomAnimation) {
			return;
		}

		var newBounds = this._getExpandedVisibleBounds();

		this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, Math.floor(this._map.getMinZoom()), this._zoom, newBounds);
		this._topClusterLevel._recursivelyAddChildrenToMap(null, Math.round(this._map._zoom), newBounds);

		this._currentShownBounds = newBounds;
		return;
	},

	_generateInitialClusters: function () {
		var maxZoom = Math.ceil(this._map.getMaxZoom()),
			minZoom = Math.floor(this._map.getMinZoom()),
			radius = this.options.maxClusterRadius,
			radiusFn = radius;

		//If we just set maxClusterRadius to a single number, we need to create
		//a simple function to return that number. Otherwise, we just have to
		//use the function we've passed in.
		if (typeof radius !== "function") {
			radiusFn = function () { return radius; };
		}

		if (this.options.disableClusteringAtZoom !== null) {
			maxZoom = this.options.disableClusteringAtZoom - 1;
		}
		this._maxZoom = maxZoom;
		this._gridClusters = {};
		this._gridUnclustered = {};

		//Set up DistanceGrids for each zoom
		for (var zoom = maxZoom; zoom >= minZoom; zoom--) {
			this._gridClusters[zoom] = new L.DistanceGrid(radiusFn(zoom));
			this._gridUnclustered[zoom] = new L.DistanceGrid(radiusFn(zoom));
		}

		// Instantiate the appropriate L.MarkerCluster class (animated or not).
		this._topClusterLevel = new this._markerCluster(this, minZoom - 1);
	},

	//Zoom: Zoom to start adding at (Pass this._maxZoom to start at the bottom)
	_addLayer: function (layer, zoom) {
		var gridClusters = this._gridClusters,
		    gridUnclustered = this._gridUnclustered,
			minZoom = Math.floor(this._map.getMinZoom()),
		    markerPoint, z;

		if (this.options.singleMarkerMode) {
			this._overrideMarkerIcon(layer);
		}

		layer.on(this._childMarkerEventHandlers, this);

		//Find the lowest zoom level to slot this one in
		for (; zoom >= minZoom; zoom--) {
			markerPoint = this._map.project(layer.getLatLng(), zoom); // calculate pixel position

			//Try find a cluster close by
			var closest = gridClusters[zoom].getNearObject(markerPoint);
			if (closest) {
				closest._addChild(layer);
				layer.__parent = closest;
				return;
			}

			//Try find a marker close by to form a new cluster with
			closest = gridUnclustered[zoom].getNearObject(markerPoint);
			if (closest) {
				var parent = closest.__parent;
				if (parent) {
					this._removeLayer(closest, false);
				}

				//Create new cluster with these 2 in it

				var newCluster = new this._markerCluster(this, zoom, closest, layer);
				gridClusters[zoom].addObject(newCluster, this._map.project(newCluster._cLatLng, zoom));
				closest.__parent = newCluster;
				layer.__parent = newCluster;

				//First create any new intermediate parent clusters that don't exist
				var lastParent = newCluster;
				for (z = zoom - 1; z > parent._zoom; z--) {
					lastParent = new this._markerCluster(this, z, lastParent);
					gridClusters[z].addObject(lastParent, this._map.project(closest.getLatLng(), z));
				}
				parent._addChild(lastParent);

				//Remove closest from this zoom level and any above that it is in, replace with newCluster
				this._removeFromGridUnclustered(closest, zoom);

				return;
			}

			//Didn't manage to cluster in at this zoom, record us as a marker here and continue upwards
			gridUnclustered[zoom].addObject(layer, markerPoint);
		}

		//Didn't get in anything, add us to the top
		this._topClusterLevel._addChild(layer);
		layer.__parent = this._topClusterLevel;
		return;
	},

	/**
	 * Refreshes the icon of all "dirty" visible clusters.
	 * Non-visible "dirty" clusters will be updated when they are added to the map.
	 * @private
	 */
	_refreshClustersIcons: function () {
		this._featureGroup.eachLayer(function (c) {
			if (c instanceof L.MarkerCluster && c._iconNeedsUpdate) {
				c._updateIcon();
			}
		});
	},

	//Enqueue code to fire after the marker expand/contract has happened
	_enqueue: function (fn) {
		this._queue.push(fn);
		if (!this._queueTimeout) {
			this._queueTimeout = setTimeout(L.bind(this._processQueue, this), 300);
		}
	},
	_processQueue: function () {
		for (var i = 0; i < this._queue.length; i++) {
			this._queue[i].call(this);
		}
		this._queue.length = 0;
		clearTimeout(this._queueTimeout);
		this._queueTimeout = null;
	},

	//Merge and split any existing clusters that are too big or small
	_mergeSplitClusters: function () {
		var mapZoom = Math.round(this._map._zoom);

		//In case we are starting to split before the animation finished
		this._processQueue();

		if (this._zoom < mapZoom && this._currentShownBounds.intersects(this._getExpandedVisibleBounds())) { //Zoom in, split
			this._animationStart();
			//Remove clusters now off screen
			this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, Math.floor(this._map.getMinZoom()), this._zoom, this._getExpandedVisibleBounds());

			this._animationZoomIn(this._zoom, mapZoom);

		} else if (this._zoom > mapZoom) { //Zoom out, merge
			this._animationStart();

			this._animationZoomOut(this._zoom, mapZoom);
		} else {
			this._moveEnd();
		}
	},

	//Gets the maps visible bounds expanded in each direction by the size of the screen (so the user cannot see an area we do not cover in one pan)
	_getExpandedVisibleBounds: function () {
		if (!this.options.removeOutsideVisibleBounds) {
			return this._mapBoundsInfinite;
		} else if (L.Browser.mobile) {
			return this._checkBoundsMaxLat(this._map.getBounds());
		}

		return this._checkBoundsMaxLat(this._map.getBounds().pad(1)); // Padding expands the bounds by its own dimensions but scaled with the given factor.
	},

	/**
	 * Expands the latitude to Infinity (or -Infinity) if the input bounds reach the map projection maximum defined latitude
	 * (in the case of Web/Spherical Mercator, it is 85.0511287798 / see https://en.wikipedia.org/wiki/Web_Mercator#Formulas).
	 * Otherwise, the removeOutsideVisibleBounds option will remove markers beyond that limit, whereas the same markers without
	 * this option (or outside MCG) will have their position floored (ceiled) by the projection and rendered at that limit,
	 * making the user think that MCG "eats" them and never displays them again.
	 * @param bounds L.LatLngBounds
	 * @returns {L.LatLngBounds}
	 * @private
	 */
	_checkBoundsMaxLat: function (bounds) {
		var maxLat = this._maxLat;

		if (maxLat !== undefined) {
			if (bounds.getNorth() >= maxLat) {
				bounds._northEast.lat = Infinity;
			}
			if (bounds.getSouth() <= -maxLat) {
				bounds._southWest.lat = -Infinity;
			}
		}

		return bounds;
	},

	//Shared animation code
	_animationAddLayerNonAnimated: function (layer, newCluster) {
		if (newCluster === layer) {
			this._featureGroup.addLayer(layer);
		} else if (newCluster._childCount === 2) {
			newCluster._addToMap();

			var markers = newCluster.getAllChildMarkers();
			this._featureGroup.removeLayer(markers[0]);
			this._featureGroup.removeLayer(markers[1]);
		} else {
			newCluster._updateIcon();
		}
	},

	/**
	 * Extracts individual (i.e. non-group) layers from a Layer Group.
	 * @param group to extract layers from.
	 * @param output {Array} in which to store the extracted layers.
	 * @returns {*|Array}
	 * @private
	 */
	_extractNonGroupLayers: function (group, output) {
		var layers = group.getLayers(),
		    i = 0,
		    layer;

		output = output || [];

		for (; i < layers.length; i++) {
			layer = layers[i];

			if (layer instanceof L.LayerGroup) {
				this._extractNonGroupLayers(layer, output);
				continue;
			}

			output.push(layer);
		}

		return output;
	},

	/**
	 * Implements the singleMarkerMode option.
	 * @param layer Marker to re-style using the Clusters iconCreateFunction.
	 * @returns {L.Icon} The newly created icon.
	 * @private
	 */
	_overrideMarkerIcon: function (layer) {
		var icon = layer.options.icon = this.options.iconCreateFunction({
			getChildCount: function () {
				return 1;
			},
			getAllChildMarkers: function () {
				return [layer];
			}
		});

		return icon;
	}
});

// Constant bounds used in case option "removeOutsideVisibleBounds" is set to false.
L.MarkerClusterGroup.include({
	_mapBoundsInfinite: new L.LatLngBounds(new L.LatLng(-Infinity, -Infinity), new L.LatLng(Infinity, Infinity))
});

L.MarkerClusterGroup.include({
	_noAnimation: {
		//Non Animated versions of everything
		_animationStart: function () {
			//Do nothing...
		},
		_animationZoomIn: function (previousZoomLevel, newZoomLevel) {
			this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, Math.floor(this._map.getMinZoom()), previousZoomLevel);
			this._topClusterLevel._recursivelyAddChildrenToMap(null, newZoomLevel, this._getExpandedVisibleBounds());

			//We didn't actually animate, but we use this event to mean "clustering animations have finished"
			this.fire('animationend');
		},
		_animationZoomOut: function (previousZoomLevel, newZoomLevel) {
			this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, Math.floor(this._map.getMinZoom()), previousZoomLevel);
			this._topClusterLevel._recursivelyAddChildrenToMap(null, newZoomLevel, this._getExpandedVisibleBounds());

			//We didn't actually animate, but we use this event to mean "clustering animations have finished"
			this.fire('animationend');
		},
		_animationAddLayer: function (layer, newCluster) {
			this._animationAddLayerNonAnimated(layer, newCluster);
		}
	},

	_withAnimation: {
		//Animated versions here
		_animationStart: function () {
			this._map._mapPane.className += ' leaflet-cluster-anim';
			this._inZoomAnimation++;
		},

		_animationZoomIn: function (previousZoomLevel, newZoomLevel) {
			var bounds = this._getExpandedVisibleBounds(),
			    fg = this._featureGroup,
				minZoom = Math.floor(this._map.getMinZoom()),
			    i;

			this._ignoreMove = true;

			//Add all children of current clusters to map and remove those clusters from map
			this._topClusterLevel._recursively(bounds, previousZoomLevel, minZoom, function (c) {
				var startPos = c._latlng,
				    markers  = c._markers,
				    m;

				if (!bounds.contains(startPos)) {
					startPos = null;
				}

				if (c._isSingleParent() && previousZoomLevel + 1 === newZoomLevel) { //Immediately add the new child and remove us
					fg.removeLayer(c);
					c._recursivelyAddChildrenToMap(null, newZoomLevel, bounds);
				} else {
					//Fade out old cluster
					c.clusterHide();
					c._recursivelyAddChildrenToMap(startPos, newZoomLevel, bounds);
				}

				//Remove all markers that aren't visible any more
				//TODO: Do we actually need to do this on the higher levels too?
				for (i = markers.length - 1; i >= 0; i--) {
					m = markers[i];
					if (!bounds.contains(m._latlng)) {
						fg.removeLayer(m);
					}
				}

			});

			this._forceLayout();

			//Update opacities
			this._topClusterLevel._recursivelyBecomeVisible(bounds, newZoomLevel);
			//TODO Maybe? Update markers in _recursivelyBecomeVisible
			fg.eachLayer(function (n) {
				if (!(n instanceof L.MarkerCluster) && n._icon) {
					n.clusterShow();
				}
			});

			//update the positions of the just added clusters/markers
			this._topClusterLevel._recursively(bounds, previousZoomLevel, newZoomLevel, function (c) {
				c._recursivelyRestoreChildPositions(newZoomLevel);
			});

			this._ignoreMove = false;

			//Remove the old clusters and close the zoom animation
			this._enqueue(function () {
				//update the positions of the just added clusters/markers
				this._topClusterLevel._recursively(bounds, previousZoomLevel, minZoom, function (c) {
					fg.removeLayer(c);
					c.clusterShow();
				});

				this._animationEnd();
			});
		},

		_animationZoomOut: function (previousZoomLevel, newZoomLevel) {
			this._animationZoomOutSingle(this._topClusterLevel, previousZoomLevel - 1, newZoomLevel);

			//Need to add markers for those that weren't on the map before but are now
			this._topClusterLevel._recursivelyAddChildrenToMap(null, newZoomLevel, this._getExpandedVisibleBounds());
			//Remove markers that were on the map before but won't be now
			this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, Math.floor(this._map.getMinZoom()), previousZoomLevel, this._getExpandedVisibleBounds());
		},

		_animationAddLayer: function (layer, newCluster) {
			var me = this,
			    fg = this._featureGroup;

			fg.addLayer(layer);
			if (newCluster !== layer) {
				if (newCluster._childCount > 2) { //Was already a cluster

					newCluster._updateIcon();
					this._forceLayout();
					this._animationStart();

					layer._setPos(this._map.latLngToLayerPoint(newCluster.getLatLng()));
					layer.clusterHide();

					this._enqueue(function () {
						fg.removeLayer(layer);
						layer.clusterShow();

						me._animationEnd();
					});

				} else { //Just became a cluster
					this._forceLayout();

					me._animationStart();
					me._animationZoomOutSingle(newCluster, this._map.getMaxZoom(), this._zoom);
				}
			}
		}
	},

	// Private methods for animated versions.
	_animationZoomOutSingle: function (cluster, previousZoomLevel, newZoomLevel) {
		var bounds = this._getExpandedVisibleBounds(),
			minZoom = Math.floor(this._map.getMinZoom());

		//Animate all of the markers in the clusters to move to their cluster center point
		cluster._recursivelyAnimateChildrenInAndAddSelfToMap(bounds, minZoom, previousZoomLevel + 1, newZoomLevel);

		var me = this;

		//Update the opacity (If we immediately set it they won't animate)
		this._forceLayout();
		cluster._recursivelyBecomeVisible(bounds, newZoomLevel);

		//TODO: Maybe use the transition timing stuff to make this more reliable
		//When the animations are done, tidy up
		this._enqueue(function () {

			//This cluster stopped being a cluster before the timeout fired
			if (cluster._childCount === 1) {
				var m = cluster._markers[0];
				//If we were in a cluster animation at the time then the opacity and position of our child could be wrong now, so fix it
				this._ignoreMove = true;
				m.setLatLng(m.getLatLng());
				this._ignoreMove = false;
				if (m.clusterShow) {
					m.clusterShow();
				}
			} else {
				cluster._recursively(bounds, newZoomLevel, minZoom, function (c) {
					c._recursivelyRemoveChildrenFromMap(bounds, minZoom, previousZoomLevel + 1);
				});
			}
			me._animationEnd();
		});
	},

	_animationEnd: function () {
		if (this._map) {
			this._map._mapPane.className = this._map._mapPane.className.replace(' leaflet-cluster-anim', '');
		}
		this._inZoomAnimation--;
		this.fire('animationend');
	},

	//Force a browser layout of stuff in the map
	// Should apply the current opacity and location to all elements so we can update them again for an animation
	_forceLayout: function () {
		//In my testing this works, infact offsetWidth of any element seems to work.
		//Could loop all this._layers and do this for each _icon if it stops working

		L.Util.falseFn(document.body.offsetWidth);
	}
});

L.markerClusterGroup = function (options) {
	return new L.MarkerClusterGroup(options);
};

var MarkerCluster = L.MarkerCluster = L.Marker.extend({
	options: L.Icon.prototype.options,

	initialize: function (group, zoom, a, b) {

		L.Marker.prototype.initialize.call(this, a ? (a._cLatLng || a.getLatLng()) : new L.LatLng(0, 0),
            { icon: this, pane: group.options.clusterPane });

		this._group = group;
		this._zoom = zoom;

		this._markers = [];
		this._childClusters = [];
		this._childCount = 0;
		this._iconNeedsUpdate = true;
		this._boundsNeedUpdate = true;

		this._bounds = new L.LatLngBounds();

		if (a) {
			this._addChild(a);
		}
		if (b) {
			this._addChild(b);
		}
	},

	//Recursively retrieve all child markers of this cluster
	getAllChildMarkers: function (storageArray, ignoreDraggedMarker) {
		storageArray = storageArray || [];

		for (var i = this._childClusters.length - 1; i >= 0; i--) {
			this._childClusters[i].getAllChildMarkers(storageArray);
		}

		for (var j = this._markers.length - 1; j >= 0; j--) {
			if (ignoreDraggedMarker && this._markers[j].__dragStart) {
				continue;
			}
			storageArray.push(this._markers[j]);
		}

		return storageArray;
	},

	//Returns the count of how many child markers we have
	getChildCount: function () {
		return this._childCount;
	},

	//Zoom to the minimum of showing all of the child markers, or the extents of this cluster
	zoomToBounds: function (fitBoundsOptions) {
		var childClusters = this._childClusters.slice(),
			map = this._group._map,
			boundsZoom = map.getBoundsZoom(this._bounds),
			zoom = this._zoom + 1,
			mapZoom = map.getZoom(),
			i;

		//calculate how far we need to zoom down to see all of the markers
		while (childClusters.length > 0 && boundsZoom > zoom) {
			zoom++;
			var newClusters = [];
			for (i = 0; i < childClusters.length; i++) {
				newClusters = newClusters.concat(childClusters[i]._childClusters);
			}
			childClusters = newClusters;
		}

		if (boundsZoom > zoom) {
			this._group._map.setView(this._latlng, zoom);
		} else if (boundsZoom <= mapZoom) { //If fitBounds wouldn't zoom us down, zoom us down instead
			this._group._map.setView(this._latlng, mapZoom + 1);
		} else {
			this._group._map.fitBounds(this._bounds, fitBoundsOptions);
		}
	},

	getBounds: function () {
		var bounds = new L.LatLngBounds();
		bounds.extend(this._bounds);
		return bounds;
	},

	_updateIcon: function () {
		this._iconNeedsUpdate = true;
		if (this._icon) {
			this.setIcon(this);
		}
	},

	//Cludge for Icon, we pretend to be an icon for performance
	createIcon: function () {
		if (this._iconNeedsUpdate) {
			this._iconObj = this._group.options.iconCreateFunction(this);
			this._iconNeedsUpdate = false;
		}
		return this._iconObj.createIcon();
	},
	createShadow: function () {
		return this._iconObj.createShadow();
	},


	_addChild: function (new1, isNotificationFromChild) {

		this._iconNeedsUpdate = true;

		this._boundsNeedUpdate = true;
		this._setClusterCenter(new1);

		if (new1 instanceof L.MarkerCluster) {
			if (!isNotificationFromChild) {
				this._childClusters.push(new1);
				new1.__parent = this;
			}
			this._childCount += new1._childCount;
		} else {
			if (!isNotificationFromChild) {
				this._markers.push(new1);
			}
			this._childCount++;
		}

		if (this.__parent) {
			this.__parent._addChild(new1, true);
		}
	},

	/**
	 * Makes sure the cluster center is set. If not, uses the child center if it is a cluster, or the marker position.
	 * @param child L.MarkerCluster|L.Marker that will be used as cluster center if not defined yet.
	 * @private
	 */
	_setClusterCenter: function (child) {
		if (!this._cLatLng) {
			// when clustering, take position of the first point as the cluster center
			this._cLatLng = child._cLatLng || child._latlng;
		}
	},

	/**
	 * Assigns impossible bounding values so that the next extend entirely determines the new bounds.
	 * This method avoids having to trash the previous L.LatLngBounds object and to create a new one, which is much slower for this class.
	 * As long as the bounds are not extended, most other methods would probably fail, as they would with bounds initialized but not extended.
	 * @private
	 */
	_resetBounds: function () {
		var bounds = this._bounds;

		if (bounds._southWest) {
			bounds._southWest.lat = Infinity;
			bounds._southWest.lng = Infinity;
		}
		if (bounds._northEast) {
			bounds._northEast.lat = -Infinity;
			bounds._northEast.lng = -Infinity;
		}
	},

	_recalculateBounds: function () {
		var markers = this._markers,
		    childClusters = this._childClusters,
		    latSum = 0,
		    lngSum = 0,
		    totalCount = this._childCount,
		    i, child, childLatLng, childCount;

		// Case where all markers are removed from the map and we are left with just an empty _topClusterLevel.
		if (totalCount === 0) {
			return;
		}

		// Reset rather than creating a new object, for performance.
		this._resetBounds();

		// Child markers.
		for (i = 0; i < markers.length; i++) {
			childLatLng = markers[i]._latlng;

			this._bounds.extend(childLatLng);

			latSum += childLatLng.lat;
			lngSum += childLatLng.lng;
		}

		// Child clusters.
		for (i = 0; i < childClusters.length; i++) {
			child = childClusters[i];

			// Re-compute child bounds and weighted position first if necessary.
			if (child._boundsNeedUpdate) {
				child._recalculateBounds();
			}

			this._bounds.extend(child._bounds);

			childLatLng = child._wLatLng;
			childCount = child._childCount;

			latSum += childLatLng.lat * childCount;
			lngSum += childLatLng.lng * childCount;
		}

		this._latlng = this._wLatLng = new L.LatLng(latSum / totalCount, lngSum / totalCount);

		// Reset dirty flag.
		this._boundsNeedUpdate = false;
	},

	//Set our markers position as given and add it to the map
	_addToMap: function (startPos) {
		if (startPos) {
			this._backupLatlng = this._latlng;
			this.setLatLng(startPos);
		}
		this._group._featureGroup.addLayer(this);
	},

	_recursivelyAnimateChildrenIn: function (bounds, center, maxZoom) {
		this._recursively(bounds, this._group._map.getMinZoom(), maxZoom - 1,
			function (c) {
				var markers = c._markers,
					i, m;
				for (i = markers.length - 1; i >= 0; i--) {
					m = markers[i];

					//Only do it if the icon is still on the map
					if (m._icon) {
						m._setPos(center);
						m.clusterHide();
					}
				}
			},
			function (c) {
				var childClusters = c._childClusters,
					j, cm;
				for (j = childClusters.length - 1; j >= 0; j--) {
					cm = childClusters[j];
					if (cm._icon) {
						cm._setPos(center);
						cm.clusterHide();
					}
				}
			}
		);
	},

	_recursivelyAnimateChildrenInAndAddSelfToMap: function (bounds, mapMinZoom, previousZoomLevel, newZoomLevel) {
		this._recursively(bounds, newZoomLevel, mapMinZoom,
			function (c) {
				c._recursivelyAnimateChildrenIn(bounds, c._group._map.latLngToLayerPoint(c.getLatLng()).round(), previousZoomLevel);

				//TODO: depthToAnimateIn affects _isSingleParent, if there is a multizoom we may/may not be.
				//As a hack we only do a animation free zoom on a single level zoom, if someone does multiple levels then we always animate
				if (c._isSingleParent() && previousZoomLevel - 1 === newZoomLevel) {
					c.clusterShow();
					c._recursivelyRemoveChildrenFromMap(bounds, mapMinZoom, previousZoomLevel); //Immediately remove our children as we are replacing them. TODO previousBounds not bounds
				} else {
					c.clusterHide();
				}

				c._addToMap();
			}
		);
	},

	_recursivelyBecomeVisible: function (bounds, zoomLevel) {
		this._recursively(bounds, this._group._map.getMinZoom(), zoomLevel, null, function (c) {
			c.clusterShow();
		});
	},

	_recursivelyAddChildrenToMap: function (startPos, zoomLevel, bounds) {
		this._recursively(bounds, this._group._map.getMinZoom() - 1, zoomLevel,
			function (c) {
				if (zoomLevel === c._zoom) {
					return;
				}

				//Add our child markers at startPos (so they can be animated out)
				for (var i = c._markers.length - 1; i >= 0; i--) {
					var nm = c._markers[i];

					if (!bounds.contains(nm._latlng)) {
						continue;
					}

					if (startPos) {
						nm._backupLatlng = nm.getLatLng();

						nm.setLatLng(startPos);
						if (nm.clusterHide) {
							nm.clusterHide();
						}
					}

					c._group._featureGroup.addLayer(nm);
				}
			},
			function (c) {
				c._addToMap(startPos);
			}
		);
	},

	_recursivelyRestoreChildPositions: function (zoomLevel) {
		//Fix positions of child markers
		for (var i = this._markers.length - 1; i >= 0; i--) {
			var nm = this._markers[i];
			if (nm._backupLatlng) {
				nm.setLatLng(nm._backupLatlng);
				delete nm._backupLatlng;
			}
		}

		if (zoomLevel - 1 === this._zoom) {
			//Reposition child clusters
			for (var j = this._childClusters.length - 1; j >= 0; j--) {
				this._childClusters[j]._restorePosition();
			}
		} else {
			for (var k = this._childClusters.length - 1; k >= 0; k--) {
				this._childClusters[k]._recursivelyRestoreChildPositions(zoomLevel);
			}
		}
	},

	_restorePosition: function () {
		if (this._backupLatlng) {
			this.setLatLng(this._backupLatlng);
			delete this._backupLatlng;
		}
	},

	//exceptBounds: If set, don't remove any markers/clusters in it
	_recursivelyRemoveChildrenFromMap: function (previousBounds, mapMinZoom, zoomLevel, exceptBounds) {
		var m, i;
		this._recursively(previousBounds, mapMinZoom - 1, zoomLevel - 1,
			function (c) {
				//Remove markers at every level
				for (i = c._markers.length - 1; i >= 0; i--) {
					m = c._markers[i];
					if (!exceptBounds || !exceptBounds.contains(m._latlng)) {
						c._group._featureGroup.removeLayer(m);
						if (m.clusterShow) {
							m.clusterShow();
						}
					}
				}
			},
			function (c) {
				//Remove child clusters at just the bottom level
				for (i = c._childClusters.length - 1; i >= 0; i--) {
					m = c._childClusters[i];
					if (!exceptBounds || !exceptBounds.contains(m._latlng)) {
						c._group._featureGroup.removeLayer(m);
						if (m.clusterShow) {
							m.clusterShow();
						}
					}
				}
			}
		);
	},

	//Run the given functions recursively to this and child clusters
	// boundsToApplyTo: a L.LatLngBounds representing the bounds of what clusters to recurse in to
	// zoomLevelToStart: zoom level to start running functions (inclusive)
	// zoomLevelToStop: zoom level to stop running functions (inclusive)
	// runAtEveryLevel: function that takes an L.MarkerCluster as an argument that should be applied on every level
	// runAtBottomLevel: function that takes an L.MarkerCluster as an argument that should be applied at only the bottom level
	_recursively: function (boundsToApplyTo, zoomLevelToStart, zoomLevelToStop, runAtEveryLevel, runAtBottomLevel) {
		var childClusters = this._childClusters,
		    zoom = this._zoom,
		    i, c;

		if (zoomLevelToStart <= zoom) {
			if (runAtEveryLevel) {
				runAtEveryLevel(this);
			}
			if (runAtBottomLevel && zoom === zoomLevelToStop) {
				runAtBottomLevel(this);
			}
		}

		if (zoom < zoomLevelToStart || zoom < zoomLevelToStop) {
			for (i = childClusters.length - 1; i >= 0; i--) {
				c = childClusters[i];
				if (c._boundsNeedUpdate) {
					c._recalculateBounds();
				}
				if (boundsToApplyTo.intersects(c._bounds)) {
					c._recursively(boundsToApplyTo, zoomLevelToStart, zoomLevelToStop, runAtEveryLevel, runAtBottomLevel);
				}
			}
		}
	},

	//Returns true if we are the parent of only one cluster and that cluster is the same as us
	_isSingleParent: function () {
		//Don't need to check this._markers as the rest won't work if there are any
		return this._childClusters.length > 0 && this._childClusters[0]._childCount === this._childCount;
	}
});

/*
* Extends L.Marker to include two extra methods: clusterHide and clusterShow.
* 
* They work as setOpacity(0) and setOpacity(1) respectively, but
* don't overwrite the options.opacity
* 
*/

L.Marker.include({
	clusterHide: function () {
		var backup = this.options.opacity;
		this.setOpacity(0);
		this.options.opacity = backup;
		return this;
	},
	
	clusterShow: function () {
		return this.setOpacity(this.options.opacity);
	}
});

L.DistanceGrid = function (cellSize) {
	this._cellSize = cellSize;
	this._sqCellSize = cellSize * cellSize;
	this._grid = {};
	this._objectPoint = { };
};

L.DistanceGrid.prototype = {

	addObject: function (obj, point) {
		var x = this._getCoord(point.x),
		    y = this._getCoord(point.y),
		    grid = this._grid,
		    row = grid[y] = grid[y] || {},
		    cell = row[x] = row[x] || [],
		    stamp = L.Util.stamp(obj);

		this._objectPoint[stamp] = point;

		cell.push(obj);
	},

	updateObject: function (obj, point) {
		this.removeObject(obj);
		this.addObject(obj, point);
	},

	//Returns true if the object was found
	removeObject: function (obj, point) {
		var x = this._getCoord(point.x),
		    y = this._getCoord(point.y),
		    grid = this._grid,
		    row = grid[y] = grid[y] || {},
		    cell = row[x] = row[x] || [],
		    i, len;

		delete this._objectPoint[L.Util.stamp(obj)];

		for (i = 0, len = cell.length; i < len; i++) {
			if (cell[i] === obj) {

				cell.splice(i, 1);

				if (len === 1) {
					delete row[x];
				}

				return true;
			}
		}

	},

	eachObject: function (fn, context) {
		var i, j, k, len, row, cell, removed,
		    grid = this._grid;

		for (i in grid) {
			row = grid[i];

			for (j in row) {
				cell = row[j];

				for (k = 0, len = cell.length; k < len; k++) {
					removed = fn.call(context, cell[k]);
					if (removed) {
						k--;
						len--;
					}
				}
			}
		}
	},

	getNearObject: function (point) {
		var x = this._getCoord(point.x),
		    y = this._getCoord(point.y),
		    i, j, k, row, cell, len, obj, dist,
		    objectPoint = this._objectPoint,
		    closestDistSq = this._sqCellSize,
		    closest = null;

		for (i = y - 1; i <= y + 1; i++) {
			row = this._grid[i];
			if (row) {

				for (j = x - 1; j <= x + 1; j++) {
					cell = row[j];
					if (cell) {

						for (k = 0, len = cell.length; k < len; k++) {
							obj = cell[k];
							dist = this._sqDist(objectPoint[L.Util.stamp(obj)], point);
							if (dist < closestDistSq ||
								dist <= closestDistSq && closest === null) {
								closestDistSq = dist;
								closest = obj;
							}
						}
					}
				}
			}
		}
		return closest;
	},

	_getCoord: function (x) {
		var coord = Math.floor(x / this._cellSize);
		return isFinite(coord) ? coord : x;
	},

	_sqDist: function (p, p2) {
		var dx = p2.x - p.x,
		    dy = p2.y - p.y;
		return dx * dx + dy * dy;
	}
};

/* Copyright (c) 2012 the authors listed at the following URL, and/or
the authors of referenced articles or incorporated external code:
http://en.literateprograms.org/Quickhull_(Javascript)?action=history&offset=20120410175256

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Retrieved from: http://en.literateprograms.org/Quickhull_(Javascript)?oldid=18434
*/

(function () {
	L.QuickHull = {

		/*
		 * @param {Object} cpt a point to be measured from the baseline
		 * @param {Array} bl the baseline, as represented by a two-element
		 *   array of latlng objects.
		 * @returns {Number} an approximate distance measure
		 */
		getDistant: function (cpt, bl) {
			var vY = bl[1].lat - bl[0].lat,
				vX = bl[0].lng - bl[1].lng;
			return (vX * (cpt.lat - bl[0].lat) + vY * (cpt.lng - bl[0].lng));
		},

		/*
		 * @param {Array} baseLine a two-element array of latlng objects
		 *   representing the baseline to project from
		 * @param {Array} latLngs an array of latlng objects
		 * @returns {Object} the maximum point and all new points to stay
		 *   in consideration for the hull.
		 */
		findMostDistantPointFromBaseLine: function (baseLine, latLngs) {
			var maxD = 0,
				maxPt = null,
				newPoints = [],
				i, pt, d;

			for (i = latLngs.length - 1; i >= 0; i--) {
				pt = latLngs[i];
				d = this.getDistant(pt, baseLine);

				if (d > 0) {
					newPoints.push(pt);
				} else {
					continue;
				}

				if (d > maxD) {
					maxD = d;
					maxPt = pt;
				}
			}

			return { maxPoint: maxPt, newPoints: newPoints };
		},


		/*
		 * Given a baseline, compute the convex hull of latLngs as an array
		 * of latLngs.
		 *
		 * @param {Array} latLngs
		 * @returns {Array}
		 */
		buildConvexHull: function (baseLine, latLngs) {
			var convexHullBaseLines = [],
				t = this.findMostDistantPointFromBaseLine(baseLine, latLngs);

			if (t.maxPoint) { // if there is still a point "outside" the base line
				convexHullBaseLines =
					convexHullBaseLines.concat(
						this.buildConvexHull([baseLine[0], t.maxPoint], t.newPoints)
					);
				convexHullBaseLines =
					convexHullBaseLines.concat(
						this.buildConvexHull([t.maxPoint, baseLine[1]], t.newPoints)
					);
				return convexHullBaseLines;
			} else {  // if there is no more point "outside" the base line, the current base line is part of the convex hull
				return [baseLine[0]];
			}
		},

		/*
		 * Given an array of latlngs, compute a convex hull as an array
		 * of latlngs
		 *
		 * @param {Array} latLngs
		 * @returns {Array}
		 */
		getConvexHull: function (latLngs) {
			// find first baseline
			var maxLat = false, minLat = false,
				maxLng = false, minLng = false,
				maxLatPt = null, minLatPt = null,
				maxLngPt = null, minLngPt = null,
				maxPt = null, minPt = null,
				i;

			for (i = latLngs.length - 1; i >= 0; i--) {
				var pt = latLngs[i];
				if (maxLat === false || pt.lat > maxLat) {
					maxLatPt = pt;
					maxLat = pt.lat;
				}
				if (minLat === false || pt.lat < minLat) {
					minLatPt = pt;
					minLat = pt.lat;
				}
				if (maxLng === false || pt.lng > maxLng) {
					maxLngPt = pt;
					maxLng = pt.lng;
				}
				if (minLng === false || pt.lng < minLng) {
					minLngPt = pt;
					minLng = pt.lng;
				}
			}
			
			if (minLat !== maxLat) {
				minPt = minLatPt;
				maxPt = maxLatPt;
			} else {
				minPt = minLngPt;
				maxPt = maxLngPt;
			}

			var ch = [].concat(this.buildConvexHull([minPt, maxPt], latLngs),
								this.buildConvexHull([maxPt, minPt], latLngs));
			return ch;
		}
	};
}());

L.MarkerCluster.include({
	getConvexHull: function () {
		var childMarkers = this.getAllChildMarkers(),
			points = [],
			p, i;

		for (i = childMarkers.length - 1; i >= 0; i--) {
			p = childMarkers[i].getLatLng();
			points.push(p);
		}

		return L.QuickHull.getConvexHull(points);
	}
});

//This code is 100% based on https://github.com/jawj/OverlappingMarkerSpiderfier-Leaflet
//Huge thanks to jawj for implementing it first to make my job easy :-)

L.MarkerCluster.include({

	_2PI: Math.PI * 2,
	_circleFootSeparation: 25, //related to circumference of circle
	_circleStartAngle: 0,

	_spiralFootSeparation:  28, //related to size of spiral (experiment!)
	_spiralLengthStart: 11,
	_spiralLengthFactor: 5,

	_circleSpiralSwitchover: 9, //show spiral instead of circle from this marker count upwards.
								// 0 -> always spiral; Infinity -> always circle

	spiderfy: function () {
		if (this._group._spiderfied === this || this._group._inZoomAnimation) {
			return;
		}

		var childMarkers = this.getAllChildMarkers(null, true),
			group = this._group,
			map = group._map,
			center = map.latLngToLayerPoint(this._latlng),
			positions;

		this._group._unspiderfy();
		this._group._spiderfied = this;

		//TODO Maybe: childMarkers order by distance to center

		if (childMarkers.length >= this._circleSpiralSwitchover) {
			positions = this._generatePointsSpiral(childMarkers.length, center);
		} else {
			center.y += 10; // Otherwise circles look wrong => hack for standard blue icon, renders differently for other icons.
			positions = this._generatePointsCircle(childMarkers.length, center);
		}

		this._animationSpiderfy(childMarkers, positions);
	},

	unspiderfy: function (zoomDetails) {
		/// <param Name="zoomDetails">Argument from zoomanim if being called in a zoom animation or null otherwise</param>
		if (this._group._inZoomAnimation) {
			return;
		}
		this._animationUnspiderfy(zoomDetails);

		this._group._spiderfied = null;
	},

	_generatePointsCircle: function (count, centerPt) {
		var circumference = this._group.options.spiderfyDistanceMultiplier * this._circleFootSeparation * (2 + count),
			legLength = circumference / this._2PI,  //radius from circumference
			angleStep = this._2PI / count,
			res = [],
			i, angle;

		legLength = Math.max(legLength, 35); // Minimum distance to get outside the cluster icon.

		res.length = count;

		for (i = 0; i < count; i++) { // Clockwise, like spiral.
			angle = this._circleStartAngle + i * angleStep;
			res[i] = new L.Point(centerPt.x + legLength * Math.cos(angle), centerPt.y + legLength * Math.sin(angle))._round();
		}

		return res;
	},

	_generatePointsSpiral: function (count, centerPt) {
		var spiderfyDistanceMultiplier = this._group.options.spiderfyDistanceMultiplier,
			legLength = spiderfyDistanceMultiplier * this._spiralLengthStart,
			separation = spiderfyDistanceMultiplier * this._spiralFootSeparation,
			lengthFactor = spiderfyDistanceMultiplier * this._spiralLengthFactor * this._2PI,
			angle = 0,
			res = [],
			i;

		res.length = count;

		// Higher index, closer position to cluster center.
		for (i = count; i >= 0; i--) {
			// Skip the first position, so that we are already farther from center and we avoid
			// being under the default cluster icon (especially important for Circle Markers).
			if (i < count) {
				res[i] = new L.Point(centerPt.x + legLength * Math.cos(angle), centerPt.y + legLength * Math.sin(angle))._round();
			}
			angle += separation / legLength + i * 0.0005;
			legLength += lengthFactor / angle;
		}
		return res;
	},

	_noanimationUnspiderfy: function () {
		var group = this._group,
			map = group._map,
			fg = group._featureGroup,
			childMarkers = this.getAllChildMarkers(null, true),
			m, i;

		group._ignoreMove = true;

		this.setOpacity(1);
		for (i = childMarkers.length - 1; i >= 0; i--) {
			m = childMarkers[i];

			fg.removeLayer(m);

			if (m._preSpiderfyLatlng) {
				m.setLatLng(m._preSpiderfyLatlng);
				delete m._preSpiderfyLatlng;
			}
			if (m.setZIndexOffset) {
				m.setZIndexOffset(0);
			}

			if (m._spiderLeg) {
				map.removeLayer(m._spiderLeg);
				delete m._spiderLeg;
			}
		}

		group.fire('unspiderfied', {
			cluster: this,
			markers: childMarkers
		});
		group._ignoreMove = false;
		group._spiderfied = null;
	}
});

//Non Animated versions of everything
L.MarkerClusterNonAnimated = L.MarkerCluster.extend({
	_animationSpiderfy: function (childMarkers, positions) {
		var group = this._group,
			map = group._map,
			fg = group._featureGroup,
			legOptions = this._group.options.spiderLegPolylineOptions,
			i, m, leg, newPos;

		group._ignoreMove = true;

		// Traverse in ascending order to make sure that inner circleMarkers are on top of further legs. Normal markers are re-ordered by newPosition.
		// The reverse order trick no longer improves performance on modern browsers.
		for (i = 0; i < childMarkers.length; i++) {
			newPos = map.layerPointToLatLng(positions[i]);
			m = childMarkers[i];

			// Add the leg before the marker, so that in case the latter is a circleMarker, the leg is behind it.
			leg = new L.Polyline([this._latlng, newPos], legOptions);
			map.addLayer(leg);
			m._spiderLeg = leg;

			// Now add the marker.
			m._preSpiderfyLatlng = m._latlng;
			m.setLatLng(newPos);
			if (m.setZIndexOffset) {
				m.setZIndexOffset(1000000); //Make these appear on top of EVERYTHING
			}

			fg.addLayer(m);
		}
		this.setOpacity(0.3);

		group._ignoreMove = false;
		group.fire('spiderfied', {
			cluster: this,
			markers: childMarkers
		});
	},

	_animationUnspiderfy: function () {
		this._noanimationUnspiderfy();
	}
});

//Animated versions here
L.MarkerCluster.include({

	_animationSpiderfy: function (childMarkers, positions) {
		var me = this,
			group = this._group,
			map = group._map,
			fg = group._featureGroup,
			thisLayerLatLng = this._latlng,
			thisLayerPos = map.latLngToLayerPoint(thisLayerLatLng),
			svg = L.Path.SVG,
			legOptions = L.extend({}, this._group.options.spiderLegPolylineOptions), // Copy the options so that we can modify them for animation.
			finalLegOpacity = legOptions.opacity,
			i, m, leg, legPath, legLength, newPos;

		if (finalLegOpacity === undefined) {
			finalLegOpacity = L.MarkerClusterGroup.prototype.options.spiderLegPolylineOptions.opacity;
		}

		if (svg) {
			// If the initial opacity of the spider leg is not 0 then it appears before the animation starts.
			legOptions.opacity = 0;

			// Add the class for CSS transitions.
			legOptions.className = (legOptions.className || '') + ' leaflet-cluster-spider-leg';
		} else {
			// Make sure we have a defined opacity.
			legOptions.opacity = finalLegOpacity;
		}

		group._ignoreMove = true;

		// Add markers and spider legs to map, hidden at our center point.
		// Traverse in ascending order to make sure that inner circleMarkers are on top of further legs. Normal markers are re-ordered by newPosition.
		// The reverse order trick no longer improves performance on modern browsers.
		for (i = 0; i < childMarkers.length; i++) {
			m = childMarkers[i];

			newPos = map.layerPointToLatLng(positions[i]);

			// Add the leg before the marker, so that in case the latter is a circleMarker, the leg is behind it.
			leg = new L.Polyline([thisLayerLatLng, newPos], legOptions);
			map.addLayer(leg);
			m._spiderLeg = leg;

			// Explanations: https://jakearchibald.com/2013/animated-line-drawing-svg/
			// In our case the transition property is declared in the CSS file.
			if (svg) {
				legPath = leg._path;
				legLength = legPath.getTotalLength() + 0.1; // Need a small extra length to avoid remaining dot in Firefox.
				legPath.style.strokeDasharray = legLength; // Just 1 length is enough, it will be duplicated.
				legPath.style.strokeDashoffset = legLength;
			}

			// If it is a marker, add it now and we'll animate it out
			if (m.setZIndexOffset) {
				m.setZIndexOffset(1000000); // Make normal markers appear on top of EVERYTHING
			}
			if (m.clusterHide) {
				m.clusterHide();
			}
			
			// Vectors just get immediately added
			fg.addLayer(m);

			if (m._setPos) {
				m._setPos(thisLayerPos);
			}
		}

		group._forceLayout();
		group._animationStart();

		// Reveal markers and spider legs.
		for (i = childMarkers.length - 1; i >= 0; i--) {
			newPos = map.layerPointToLatLng(positions[i]);
			m = childMarkers[i];

			//Move marker to new position
			m._preSpiderfyLatlng = m._latlng;
			m.setLatLng(newPos);
			
			if (m.clusterShow) {
				m.clusterShow();
			}

			// Animate leg (animation is actually delegated to CSS transition).
			if (svg) {
				leg = m._spiderLeg;
				legPath = leg._path;
				legPath.style.strokeDashoffset = 0;
				//legPath.style.strokeOpacity = finalLegOpacity;
				leg.setStyle({opacity: finalLegOpacity});
			}
		}
		this.setOpacity(0.3);

		group._ignoreMove = false;

		setTimeout(function () {
			group._animationEnd();
			group.fire('spiderfied', {
				cluster: me,
				markers: childMarkers
			});
		}, 200);
	},

	_animationUnspiderfy: function (zoomDetails) {
		var me = this,
			group = this._group,
			map = group._map,
			fg = group._featureGroup,
			thisLayerPos = zoomDetails ? map._latLngToNewLayerPoint(this._latlng, zoomDetails.zoom, zoomDetails.center) : map.latLngToLayerPoint(this._latlng),
			childMarkers = this.getAllChildMarkers(null, true),
			svg = L.Path.SVG,
			m, i, leg, legPath, legLength, nonAnimatable;

		group._ignoreMove = true;
		group._animationStart();

		//Make us visible and bring the child markers back in
		this.setOpacity(1);
		for (i = childMarkers.length - 1; i >= 0; i--) {
			m = childMarkers[i];

			//Marker was added to us after we were spiderfied
			if (!m._preSpiderfyLatlng) {
				continue;
			}

			//Close any popup on the marker first, otherwise setting the location of the marker will make the map scroll
			m.closePopup();

			//Fix up the location to the real one
			m.setLatLng(m._preSpiderfyLatlng);
			delete m._preSpiderfyLatlng;

			//Hack override the location to be our center
			nonAnimatable = true;
			if (m._setPos) {
				m._setPos(thisLayerPos);
				nonAnimatable = false;
			}
			if (m.clusterHide) {
				m.clusterHide();
				nonAnimatable = false;
			}
			if (nonAnimatable) {
				fg.removeLayer(m);
			}

			// Animate the spider leg back in (animation is actually delegated to CSS transition).
			if (svg) {
				leg = m._spiderLeg;
				legPath = leg._path;
				legLength = legPath.getTotalLength() + 0.1;
				legPath.style.strokeDashoffset = legLength;
				leg.setStyle({opacity: 0});
			}
		}

		group._ignoreMove = false;

		setTimeout(function () {
			//If we have only <= one child left then that marker will be shown on the map so don't remove it!
			var stillThereChildCount = 0;
			for (i = childMarkers.length - 1; i >= 0; i--) {
				m = childMarkers[i];
				if (m._spiderLeg) {
					stillThereChildCount++;
				}
			}


			for (i = childMarkers.length - 1; i >= 0; i--) {
				m = childMarkers[i];

				if (!m._spiderLeg) { //Has already been unspiderfied
					continue;
				}

				if (m.clusterShow) {
					m.clusterShow();
				}
				if (m.setZIndexOffset) {
					m.setZIndexOffset(0);
				}

				if (stillThereChildCount > 1) {
					fg.removeLayer(m);
				}

				map.removeLayer(m._spiderLeg);
				delete m._spiderLeg;
			}
			group._animationEnd();
			group.fire('unspiderfied', {
				cluster: me,
				markers: childMarkers
			});
		}, 200);
	}
});


L.MarkerClusterGroup.include({
	//The MarkerCluster currently spiderfied (if any)
	_spiderfied: null,

	unspiderfy: function () {
		this._unspiderfy.apply(this, arguments);
	},

	_spiderfierOnAdd: function () {
		this._map.on('click', this._unspiderfyWrapper, this);

		if (this._map.options.zoomAnimation) {
			this._map.on('zoomstart', this._unspiderfyZoomStart, this);
		}
		//Browsers without zoomAnimation or a big zoom don't fire zoomstart
		this._map.on('zoomend', this._noanimationUnspiderfy, this);

		if (!L.Browser.touch) {
			this._map.getRenderer(this);
			//Needs to happen in the pageload, not after, or animations don't work in webkit
			//  http://stackoverflow.com/questions/8455200/svg-animate-with-dynamically-added-elements
			//Disable on touch browsers as the animation messes up on a touch zoom and isn't very noticable
		}
	},

	_spiderfierOnRemove: function () {
		this._map.off('click', this._unspiderfyWrapper, this);
		this._map.off('zoomstart', this._unspiderfyZoomStart, this);
		this._map.off('zoomanim', this._unspiderfyZoomAnim, this);
		this._map.off('zoomend', this._noanimationUnspiderfy, this);

		//Ensure that markers are back where they should be
		// Use no animation to avoid a sticky leaflet-cluster-anim class on mapPane
		this._noanimationUnspiderfy();
	},

	//On zoom start we add a zoomanim handler so that we are guaranteed to be last (after markers are animated)
	//This means we can define the animation they do rather than Markers doing an animation to their actual location
	_unspiderfyZoomStart: function () {
		if (!this._map) { //May have been removed from the map by a zoomEnd handler
			return;
		}

		this._map.on('zoomanim', this._unspiderfyZoomAnim, this);
	},

	_unspiderfyZoomAnim: function (zoomDetails) {
		//Wait until the first zoomanim after the user has finished touch-zooming before running the animation
		if (L.DomUtil.hasClass(this._map._mapPane, 'leaflet-touching')) {
			return;
		}

		this._map.off('zoomanim', this._unspiderfyZoomAnim, this);
		this._unspiderfy(zoomDetails);
	},

	_unspiderfyWrapper: function () {
		/// <summary>_unspiderfy but passes no arguments</summary>
		this._unspiderfy();
	},

	_unspiderfy: function (zoomDetails) {
		if (this._spiderfied) {
			this._spiderfied.unspiderfy(zoomDetails);
		}
	},

	_noanimationUnspiderfy: function () {
		if (this._spiderfied) {
			this._spiderfied._noanimationUnspiderfy();
		}
	},

	//If the given layer is currently being spiderfied then we unspiderfy it so it isn't on the map anymore etc
	_unspiderfyLayer: function (layer) {
		if (layer._spiderLeg) {
			this._featureGroup.removeLayer(layer);

			if (layer.clusterShow) {
				layer.clusterShow();
			}
				//Position will be fixed up immediately in _animationUnspiderfy
			if (layer.setZIndexOffset) {
				layer.setZIndexOffset(0);
			}

			this._map.removeLayer(layer._spiderLeg);
			delete layer._spiderLeg;
		}
	}
});

/**
 * Adds 1 public method to MCG and 1 to L.Marker to facilitate changing
 * markers' icon options and refreshing their icon and their parent clusters
 * accordingly (case where their iconCreateFunction uses data of childMarkers
 * to make up the cluster icon).
 */


L.MarkerClusterGroup.include({
	/**
	 * Updates the icon of all clusters which are parents of the given marker(s).
	 * In singleMarkerMode, also updates the given marker(s) icon.
	 * @param layers L.MarkerClusterGroup|L.LayerGroup|Array(L.Marker)|Map(L.Marker)|
	 * L.MarkerCluster|L.Marker (optional) list of markers (or single marker) whose parent
	 * clusters need to be updated. If not provided, retrieves all child markers of this.
	 * @returns {L.MarkerClusterGroup}
	 */
	refreshClusters: function (layers) {
		if (!layers) {
			layers = this._topClusterLevel.getAllChildMarkers();
		} else if (layers instanceof L.MarkerClusterGroup) {
			layers = layers._topClusterLevel.getAllChildMarkers();
		} else if (layers instanceof L.LayerGroup) {
			layers = layers._layers;
		} else if (layers instanceof L.MarkerCluster) {
			layers = layers.getAllChildMarkers();
		} else if (layers instanceof L.Marker) {
			layers = [layers];
		} // else: must be an Array(L.Marker)|Map(L.Marker)
		this._flagParentsIconsNeedUpdate(layers);
		this._refreshClustersIcons();

		// In case of singleMarkerMode, also re-draw the markers.
		if (this.options.singleMarkerMode) {
			this._refreshSingleMarkerModeMarkers(layers);
		}

		return this;
	},

	/**
	 * Simply flags all parent clusters of the given markers as having a "dirty" icon.
	 * @param layers Array(L.Marker)|Map(L.Marker) list of markers.
	 * @private
	 */
	_flagParentsIconsNeedUpdate: function (layers) {
		var id, parent;

		// Assumes layers is an Array or an Object whose prototype is non-enumerable.
		for (id in layers) {
			// Flag parent clusters' icon as "dirty", all the way up.
			// Dumb process that flags multiple times upper parents, but still
			// much more efficient than trying to be smart and make short lists,
			// at least in the case of a hierarchy following a power law:
			// http://jsperf.com/flag-nodes-in-power-hierarchy/2
			parent = layers[id].__parent;
			while (parent) {
				parent._iconNeedsUpdate = true;
				parent = parent.__parent;
			}
		}
	},

	/**
	 * Re-draws the icon of the supplied markers.
	 * To be used in singleMarkerMode only.
	 * @param layers Array(L.Marker)|Map(L.Marker) list of markers.
	 * @private
	 */
	_refreshSingleMarkerModeMarkers: function (layers) {
		var id, layer;

		for (id in layers) {
			layer = layers[id];

			// Make sure we do not override markers that do not belong to THIS group.
			if (this.hasLayer(layer)) {
				// Need to re-create the icon first, then re-draw the marker.
				layer.setIcon(this._overrideMarkerIcon(layer));
			}
		}
	}
});

L.Marker.include({
	/**
	 * Updates the given options in the marker's icon and refreshes the marker.
	 * @param options map object of icon options.
	 * @param directlyRefreshClusters boolean (optional) true to trigger
	 * MCG.refreshClustersOf() right away with this single marker.
	 * @returns {L.Marker}
	 */
	refreshIconOptions: function (options, directlyRefreshClusters) {
		var icon = this.options.icon;

		L.setOptions(icon, options);

		this.setIcon(icon);

		// Shortcut to refresh the associated MCG clusters right away.
		// To be used when refreshing a single marker.
		// Otherwise, better use MCG.refreshClusters() once at the end with
		// the list of modified markers.
		if (directlyRefreshClusters && this.__parent) {
			this.__parent._group.refreshClusters(this);
		}

		return this;
	}
});

exports.MarkerClusterGroup = MarkerClusterGroup;
exports.MarkerCluster = MarkerCluster;

})));
//# sourceMappingURL=leaflet.markercluster-src.js.map


/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/pages/map/map-test/map-test.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/pages/map/map-test/map-test.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"kt-main\" style=\"display: flex; flex-direction: row; align-items: stretch;height: 100%;\">\r\n    <div class=\"kt-left\" style=\"display: flex;\r\n    max-width: 800px;\r\n    flex-direction: row;\r\n    background: red;\" >\r\n        <kt-user-tree-new [userTreeName]=\"'userTreeForMap'\" [eventUpdate]=\"parrentEmiter\" [cacheSearch]=\"true\"\r\n            [storeLocal]=\"true\" [showCountDevice]=\"true\" [allowCollapse]=\"true\" (changeUser)=\"changeUser($event)\">\r\n        </kt-user-tree-new>\r\n        <kt-list-device [changeUserEmit]=\"changeUserEmiter\"></kt-list-device>\r\n    </div>\r\n    <div class=\"kt-map\" style=\"display: flex;flex-grow: 1;\" resizeObserver (resize)=\"onResize($event.contentRect)\" >        \r\n        <div class=\"map\" id=\"map\" #map leaflet (leafletMapReady)=\"onMapReady($event)\" [leafletOptions]=\"options\" >\r\n        </div>\r\n    </div>\r\n    <kt-map-control [mapService]=\"getMapService()\"></kt-map-control>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/pages/map/map.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/pages/map/map.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"kt-main\" style=\"display: flex; flex-direction: row; align-items: stretch;height: 100%;\">\r\n    <div class=\"kt-left\" style=\"display: flex;\r\n    max-width: 800px;\r\n    flex-direction: row;\r\n    background: red;\r\n    z-index:2\" >\r\n        <kt-user-tree-new [userTreeName]=\"'userTreeForMap'\" [eventUpdate]=\"parrentEmiter\" [cacheSearch]=\"true\"\r\n            [storeLocal]=\"true\" [showCountDevice]=\"false\" [allowCollapse]=\"true\" (changeUser)=\"changeUser($event)\">\r\n        </kt-user-tree-new>\r\n        <kt-list-device [changeUserEmit]=\"changeUserEmiter\"></kt-list-device>\r\n    </div>\r\n    <div class=\"kt-map\" style=\"display: flex;flex-grow: 1;border-top: 1px solid #dcdee0;border-left: 1px solid #dcdee0;\" resizeObserver (resize)=\"onResize($event.contentRect)\" >        \r\n        <div class=\"map\" id=\"map\" #map leaflet (leafletMapReady)=\"onMapReady($event)\" [leafletOptions]=\"options\" >\r\n        </div>\r\n    </div>\r\n    <kt-map-control [mapService]=\"getMapService()\"></kt-map-control>\r\n</div>"

/***/ }),

/***/ "./src/app/views/pages/map/data-service.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/views/pages/map/data-service.service.ts ***!
  \*********************************************************/
/*! exports provided: DataServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataServiceService", function() { return DataServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DataServiceService = /** @class */ (function () {
    function DataServiceService() {
    }
    DataServiceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DataServiceService);
    return DataServiceService;
}());



/***/ }),

/***/ "./src/app/views/pages/map/map-test/map-test.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/views/pages/map/map-test/map-test.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".map {\n  height: 100%;\n  padding: 0;\n  width: 100%;\n}\n\n.leaflet-pane {\n  z-index: 10 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvcGFnZXMvbWFwL21hcC10ZXN0L0Q6XFxQcm9qZWN0XFxWTkVUX0dQU1xcdm5ldF9tZXRyb25pY19za2VsZXRvbi9zcmNcXGFwcFxcdmlld3NcXHBhZ2VzXFxtYXBcXG1hcC10ZXN0XFxtYXAtdGVzdC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvdmlld3MvcGFnZXMvbWFwL21hcC10ZXN0L21hcC10ZXN0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FDQ0Y7O0FEQ0E7RUFDRSxzQkFBQTtBQ0VGIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvcGFnZXMvbWFwL21hcC10ZXN0L21hcC10ZXN0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1hcCB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIHBhZGRpbmc6IDA7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuLmxlYWZsZXQtcGFuZSB7XHJcbiAgei1pbmRleDogMTAgIWltcG9ydGFudDtcclxufVxyXG4iLCIubWFwIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICBwYWRkaW5nOiAwO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmxlYWZsZXQtcGFuZSB7XG4gIHotaW5kZXg6IDEwICFpbXBvcnRhbnQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/views/pages/map/map-test/map-test.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/views/pages/map/map-test/map-test.component.ts ***!
  \****************************************************************/
/*! exports provided: MapTestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapTestComponent", function() { return MapTestComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var leaflet_markercluster__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! leaflet.markercluster */ "./node_modules/leaflet.markercluster/dist/leaflet.markercluster-src.js");
/* harmony import */ var leaflet_markercluster__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(leaflet_markercluster__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _core_utils_map_services_map_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @core/utils/map/_services/map.service */ "./src/app/core/utils/map/_services/map.service.ts");
/* harmony import */ var _core_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @core/map */ "./src/app/core/map/index.ts");





var MapTestComponent = /** @class */ (function () {
    function MapTestComponent(mapService, cdr, trackingService) {
        this.mapService = mapService;
        this.cdr = cdr;
        this.trackingService = trackingService;
        this.data = [
            {
                "id": "2388",
                "imei": "864811036964364",
                "name": "Quỳnh Ngọc",
                "active": "1",
                "status": 2,
                "lat": "20.658936",
                "lng": "106.2652",
                "address": "106.267378",
                "gsm": "20",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "16",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "65 %",
                        "value": 65,
                        "scaleValue": 4
                    },
                    {
                        "text": 1.7,
                        "name": "Áp lực",
                        "type": "pressure",
                        "value": "781",
                        "unit": "kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "22h 15m 59s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "80159",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2388",
                "imei": "864811036964364",
                "name": "Quỳnh Ngọc 2",
                "active": "1",
                "status": 2,
                "lat": "20.658736",
                "lng": "106.2652",
                "address": "106.267378",
                "gsm": "20",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "16",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "65 %",
                        "value": 65,
                        "scaleValue": 4
                    },
                    {
                        "text": 1.7,
                        "name": "Áp lực",
                        "type": "pressure",
                        "value": "781",
                        "unit": "kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "22h 15m 59s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "80159",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2387",
                "imei": "864811036958630",
                "name": "Quỳnh Trang",
                "active": "1",
                "status": 2,
                "lat": "20.602258",
                "lng": "106.344364",
                "address": "106.344364",
                "gsm": "31",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "16",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "100 %",
                        "value": 100,
                        "scaleValue": 5
                    },
                    {
                        "text": 1.3,
                        "name": "Áp Lực",
                        "type": "pressure",
                        "value": "584",
                        "unit": "Kg/cm2",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2386",
                "imei": "864811036964257",
                "name": "Nhà máy nước Quỳnh Giao",
                "active": "1",
                "status": 1,
                "lat": "20.679656",
                "lng": "106.309173",
                "address": "106.309173",
                "gsm": "22",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "16",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "71 %",
                        "value": 71,
                        "scaleValue": 4
                    },
                    {
                        "text": 2.9,
                        "name": "Áp lực",
                        "type": "pressure",
                        "value": "1045",
                        "unit": "Kg/Cm2",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2385",
                "imei": "864811036976863",
                "name": "Quỳnh Bảo",
                "active": "1",
                "status": 1,
                "lat": "20.618107",
                "lng": "106.330302",
                "address": "106.330302",
                "gsm": "26",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "16",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "84 %",
                        "value": 84,
                        "scaleValue": 5
                    },
                    {
                        "text": 1,
                        "name": "Áp Lực",
                        "type": "pressure",
                        "value": "633",
                        "unit": "kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "19h 48m 13s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "67693",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2384",
                "imei": "864811036956139",
                "name": "Quỳnh Hưng",
                "active": "1",
                "status": 2,
                "lat": "20.624587",
                "lng": "106.338587",
                "address": "106.338587",
                "gsm": "25",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "16",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "81 %",
                        "value": 81,
                        "scaleValue": 5
                    },
                    {
                        "text": 1.1,
                        "name": "Áp lực",
                        "type": "pressure",
                        "value": "638",
                        "unit": "Kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "22h 40m 50s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "78050",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2382",
                "imei": "864811036960578",
                "name": "NMN Thanh Sơn",
                "active": "1",
                "status": 1,
                "lat": "20.870293",
                "lng": "106.446231",
                "address": "106.446231",
                "gsm": "31",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:20",
                "timestamp": "1570243340",
                "groupId": "17",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "100 %",
                        "value": 100,
                        "scaleValue": 5
                    },
                    {
                        "text": 3.1,
                        "name": "Áp lực 1",
                        "type": "pressure",
                        "value": "1069",
                        "unit": "Kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "127h 38m 22s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "455902",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2381",
                "imei": "864811036960511",
                "name": "Huyện đội Thanh Hà",
                "active": "1",
                "status": 1,
                "lat": "20.896922",
                "lng": "106.416924",
                "address": "106.416924",
                "gsm": "20",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "17",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "65 %",
                        "value": 65,
                        "scaleValue": 4
                    },
                    {
                        "text": 0.69,
                        "name": "Áp lực 1",
                        "type": "pressure",
                        "value": "462",
                        "unit": "Kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "1h 45m 52s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "2752",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2389",
                "imei": "864811036962129",
                "name": "Văn Xá - Ái Quốc",
                "active": "1",
                "status": 1,
                "lat": "20.962307",
                "lng": "106.378276",
                "address": "106.378276",
                "gsm": "22",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "19",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "71 %",
                        "value": 71,
                        "scaleValue": 4
                    },
                    {
                        "text": 0.79,
                        "name": "Áp lực",
                        "type": "pressure",
                        "value": "527",
                        "unit": "Kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "49h 28m 39s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "178119",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2377",
                "imei": "869696043528749",
                "name": "Khánh Hội - Nam Đồng",
                "active": "1",
                "status": 1,
                "lat": "20.930362",
                "lng": "106.341778",
                "address": "106.341778",
                "gsm": "26",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "19",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "84 %",
                        "value": 84,
                        "scaleValue": 5
                    },
                    {
                        "text": 0.54,
                        "name": "Áp Lực",
                        "type": "pressure",
                        "value": "446",
                        "unit": "kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "46h 59m 17s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "165557",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2375",
                "imei": "869696043503312",
                "name": "Nhà máy nước Nam Đồng",
                "active": "1",
                "status": 1,
                "lat": "20.952807",
                "lng": "106.336409",
                "address": "106.336409",
                "gsm": "31",
                "powerVolt": "11",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "19",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "100 %",
                        "value": 100,
                        "scaleValue": 5
                    },
                    {
                        "text": 3.4,
                        "name": "Áp lực",
                        "type": "pressure",
                        "value": "1046",
                        "unit": "Kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "23h 4m 27s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "83067",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            }
        ];
        this.options = this.mapService.init();
        this.parrentEmiter = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.changeUserEmiter = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.currentUserSelect = {};
    }
    MapTestComponent.prototype.ngOnInit = function () {
        this.test();
    };
    MapTestComponent.prototype.onMapReady = function (map) {
        this.mapService.setMap(map);
        var _this = this;
        // setTimeout(() => {     
        //   let items: Array<Item> = [];
        //   _this.data.forEach((x) => {
        //     let item = new Item(x);
        //     Object.assign(item, x);
        //     item.direction = Math.random() * 360;
        //     items.push(item);
        //     return item;
        //   });
        //   _this.mapService.addMarkers(items, true);
        // });   
    };
    MapTestComponent.prototype.test = function () {
        // this.trackingService.list({params:{groupId:-1}}).pipe(
        //   tap(x=>{
        //     console.log(x);
        //   })
        // ).subscribe();
    };
    MapTestComponent.prototype.onResize = function ($elm) {
        this.mapService.resize();
        this.cdr.detectChanges();
    };
    MapTestComponent.prototype.getMapService = function () {
        return this.mapService;
    };
    MapTestComponent.prototype.changeUser = function (value) {
        this.currentUserSelect = value;
        this.changeUserEmiter.emit(value);
    };
    MapTestComponent.ctorParameters = function () { return [
        { type: _core_utils_map_services_map_service__WEBPACK_IMPORTED_MODULE_3__["MapService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _core_map__WEBPACK_IMPORTED_MODULE_4__["TrackingService"] }
    ]; };
    MapTestComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-map-test',
            template: __webpack_require__(/*! raw-loader!./map-test.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/pages/map/map-test/map-test.component.html"),
            styles: [__webpack_require__(/*! ./map-test.component.scss */ "./src/app/views/pages/map/map-test/map-test.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_utils_map_services_map_service__WEBPACK_IMPORTED_MODULE_3__["MapService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _core_map__WEBPACK_IMPORTED_MODULE_4__["TrackingService"]])
    ], MapTestComponent);
    return MapTestComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/map/map.component.scss":
/*!****************************************************!*\
  !*** ./src/app/views/pages/map/map.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".map {\n  height: 100%;\n  padding: 0;\n  width: 100%;\n}\n\n.leaflet-pane {\n  z-index: 10 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvcGFnZXMvbWFwL0Q6XFxQcm9qZWN0XFxWTkVUX0dQU1xcdm5ldF9tZXRyb25pY19za2VsZXRvbi9zcmNcXGFwcFxcdmlld3NcXHBhZ2VzXFxtYXBcXG1hcC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvdmlld3MvcGFnZXMvbWFwL21hcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQ0NGOztBRENBO0VBQ0Usc0JBQUE7QUNFRiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL21hcC9tYXAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFwIHtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgcGFkZGluZzogMDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4ubGVhZmxldC1wYW5lIHtcclxuICB6LWluZGV4OiAxMCAhaW1wb3J0YW50O1xyXG59XHJcbiIsIi5tYXAge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHBhZGRpbmc6IDA7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ubGVhZmxldC1wYW5lIHtcbiAgei1pbmRleDogMTAgIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/views/pages/map/map.component.ts":
/*!**************************************************!*\
  !*** ./src/app/views/pages/map/map.component.ts ***!
  \**************************************************/
/*! exports provided: MapComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapComponent", function() { return MapComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var leaflet_markercluster__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! leaflet.markercluster */ "./node_modules/leaflet.markercluster/dist/leaflet.markercluster-src.js");
/* harmony import */ var leaflet_markercluster__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(leaflet_markercluster__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _core_utils_map_services_map_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @core/utils/map/_services/map.service */ "./src/app/core/utils/map/_services/map.service.ts");
/* harmony import */ var _core_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @core/map */ "./src/app/core/map/index.ts");





var MapComponent = /** @class */ (function () {
    function MapComponent(mapService, cdr, trackingService) {
        this.mapService = mapService;
        this.cdr = cdr;
        this.trackingService = trackingService;
        this.data = [
            {
                "id": "2388",
                "imei": "864811036964364",
                "name": "Quỳnh Ngọc",
                "active": "1",
                "status": 2,
                "lat": "20.658936",
                "lng": "106.2652",
                "address": "106.267378",
                "gsm": "20",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "16",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "65 %",
                        "value": 65,
                        "scaleValue": 4
                    },
                    {
                        "text": 1.7,
                        "name": "Áp lực",
                        "type": "pressure",
                        "value": "781",
                        "unit": "kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "22h 15m 59s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "80159",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2388",
                "imei": "864811036964364",
                "name": "Quỳnh Ngọc 2",
                "active": "1",
                "status": 2,
                "lat": "20.658736",
                "lng": "106.2652",
                "address": "106.267378",
                "gsm": "20",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "16",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "65 %",
                        "value": 65,
                        "scaleValue": 4
                    },
                    {
                        "text": 1.7,
                        "name": "Áp lực",
                        "type": "pressure",
                        "value": "781",
                        "unit": "kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "22h 15m 59s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "80159",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2387",
                "imei": "864811036958630",
                "name": "Quỳnh Trang",
                "active": "1",
                "status": 2,
                "lat": "20.602258",
                "lng": "106.344364",
                "address": "106.344364",
                "gsm": "31",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "16",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "100 %",
                        "value": 100,
                        "scaleValue": 5
                    },
                    {
                        "text": 1.3,
                        "name": "Áp Lực",
                        "type": "pressure",
                        "value": "584",
                        "unit": "Kg/cm2",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2386",
                "imei": "864811036964257",
                "name": "Nhà máy nước Quỳnh Giao",
                "active": "1",
                "status": 1,
                "lat": "20.679656",
                "lng": "106.309173",
                "address": "106.309173",
                "gsm": "22",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "16",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "71 %",
                        "value": 71,
                        "scaleValue": 4
                    },
                    {
                        "text": 2.9,
                        "name": "Áp lực",
                        "type": "pressure",
                        "value": "1045",
                        "unit": "Kg/Cm2",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2385",
                "imei": "864811036976863",
                "name": "Quỳnh Bảo",
                "active": "1",
                "status": 1,
                "lat": "20.618107",
                "lng": "106.330302",
                "address": "106.330302",
                "gsm": "26",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "16",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "84 %",
                        "value": 84,
                        "scaleValue": 5
                    },
                    {
                        "text": 1,
                        "name": "Áp Lực",
                        "type": "pressure",
                        "value": "633",
                        "unit": "kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "19h 48m 13s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "67693",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2384",
                "imei": "864811036956139",
                "name": "Quỳnh Hưng",
                "active": "1",
                "status": 2,
                "lat": "20.624587",
                "lng": "106.338587",
                "address": "106.338587",
                "gsm": "25",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "16",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "81 %",
                        "value": 81,
                        "scaleValue": 5
                    },
                    {
                        "text": 1.1,
                        "name": "Áp lực",
                        "type": "pressure",
                        "value": "638",
                        "unit": "Kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "22h 40m 50s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "78050",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2382",
                "imei": "864811036960578",
                "name": "NMN Thanh Sơn",
                "active": "1",
                "status": 1,
                "lat": "20.870293",
                "lng": "106.446231",
                "address": "106.446231",
                "gsm": "31",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:20",
                "timestamp": "1570243340",
                "groupId": "17",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "100 %",
                        "value": 100,
                        "scaleValue": 5
                    },
                    {
                        "text": 3.1,
                        "name": "Áp lực 1",
                        "type": "pressure",
                        "value": "1069",
                        "unit": "Kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "127h 38m 22s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "455902",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2381",
                "imei": "864811036960511",
                "name": "Huyện đội Thanh Hà",
                "active": "1",
                "status": 1,
                "lat": "20.896922",
                "lng": "106.416924",
                "address": "106.416924",
                "gsm": "20",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "17",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "65 %",
                        "value": 65,
                        "scaleValue": 4
                    },
                    {
                        "text": 0.69,
                        "name": "Áp lực 1",
                        "type": "pressure",
                        "value": "462",
                        "unit": "Kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "1h 45m 52s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "2752",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2389",
                "imei": "864811036962129",
                "name": "Văn Xá - Ái Quốc",
                "active": "1",
                "status": 1,
                "lat": "20.962307",
                "lng": "106.378276",
                "address": "106.378276",
                "gsm": "22",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "19",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "71 %",
                        "value": 71,
                        "scaleValue": 4
                    },
                    {
                        "text": 0.79,
                        "name": "Áp lực",
                        "type": "pressure",
                        "value": "527",
                        "unit": "Kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "49h 28m 39s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "178119",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2377",
                "imei": "869696043528749",
                "name": "Khánh Hội - Nam Đồng",
                "active": "1",
                "status": 1,
                "lat": "20.930362",
                "lng": "106.341778",
                "address": "106.341778",
                "gsm": "26",
                "powerVolt": "12",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "19",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "84 %",
                        "value": 84,
                        "scaleValue": 5
                    },
                    {
                        "text": 0.54,
                        "name": "Áp Lực",
                        "type": "pressure",
                        "value": "446",
                        "unit": "kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "46h 59m 17s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "165557",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            },
            {
                "id": "2375",
                "imei": "869696043503312",
                "name": "Nhà máy nước Nam Đồng",
                "active": "1",
                "status": 1,
                "lat": "20.952807",
                "lng": "106.336409",
                "address": "106.336409",
                "gsm": "31",
                "powerVolt": "11",
                "time": "2019-10-05 09:42:00",
                "timestamp": "1570243320",
                "groupId": "19",
                "groupName": "unknown",
                "sensors": [
                    {
                        "name": "GSM",
                        "type": "gsm",
                        "text": "100 %",
                        "value": 100,
                        "scaleValue": 5
                    },
                    {
                        "text": 3.4,
                        "name": "Áp lực",
                        "type": "pressure",
                        "value": "1046",
                        "unit": "Kg/cm2",
                        "scaleValue": ""
                    },
                    {
                        "text": "23h 4m 27s",
                        "name": "Uptime",
                        "type": "time",
                        "value": "83067",
                        "unit": "",
                        "scaleValue": ""
                    }
                ]
            }
        ];
        this.options = this.mapService.init();
        this.parrentEmiter = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.changeUserEmiter = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.currentUserSelect = {};
    }
    MapComponent.prototype.ngOnInit = function () {
        this.test();
    };
    MapComponent.prototype.onMapReady = function (map) {
        this.mapService.setMap(map);
        var _this = this;
        // setTimeout(() => {     
        //   let items: Array<Item> = [];
        //   _this.data.forEach((x) => {
        //     let item = new Item(x);
        //     Object.assign(item, x);
        //     item.direction = Math.random() * 360;
        //     items.push(item);
        //     return item;
        //   });
        //   _this.mapService.addMarkers(items, true);
        // });   
    };
    MapComponent.prototype.test = function () {
        // this.trackingService.list({params:{groupId:-1}}).pipe(
        //   tap(x=>{
        //     console.log(x);
        //   })
        // ).subscribe();
    };
    MapComponent.prototype.onResize = function ($elm) {
        this.mapService.resize();
        this.cdr.detectChanges();
    };
    MapComponent.prototype.getMapService = function () {
        return this.mapService;
    };
    MapComponent.prototype.changeUser = function (value) {
        this.currentUserSelect = value;
        this.changeUserEmiter.emit(value);
    };
    MapComponent.ctorParameters = function () { return [
        { type: _core_utils_map_services_map_service__WEBPACK_IMPORTED_MODULE_3__["MapService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _core_map__WEBPACK_IMPORTED_MODULE_4__["TrackingService"] }
    ]; };
    MapComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-map',
            template: __webpack_require__(/*! raw-loader!./map.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/pages/map/map.component.html"),
            styles: [__webpack_require__(/*! ./map.component.scss */ "./src/app/views/pages/map/map.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_utils_map_services_map_service__WEBPACK_IMPORTED_MODULE_3__["MapService"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _core_map__WEBPACK_IMPORTED_MODULE_4__["TrackingService"]])
    ], MapComponent);
    return MapComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/map/map.module.ts":
/*!***********************************************!*\
  !*** ./src/app/views/pages/map/map.module.ts ***!
  \***********************************************/
/*! exports provided: MapModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapModule", function() { return MapModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _map_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./map.component */ "./src/app/views/pages/map/map.component.ts");
/* harmony import */ var _src_app_core_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../src/app/core/auth */ "./src/app/core/auth/index.ts");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/esm5/drag-drop.es5.js");
/* harmony import */ var _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @asymmetrik/ngx-leaflet */ "./node_modules/@asymmetrik/ngx-leaflet/dist/index.js");
/* harmony import */ var _data_service_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./data-service.service */ "./src/app/views/pages/map/data-service.service.ts");
/* harmony import */ var _map_test_map_test_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./map-test/map-test.component */ "./src/app/views/pages/map/map-test/map-test.component.ts");
/* harmony import */ var _app_views_partials_partials_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @app/views/partials/partials.module */ "./src/app/views/partials/partials.module.ts");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @core/core.module */ "./src/app/core/core.module.ts");












var routes = [{
        path: '',
        component: _map_component__WEBPACK_IMPORTED_MODULE_4__["MapComponent"],
        canActivate: [_src_app_core_auth__WEBPACK_IMPORTED_MODULE_5__["ModuleGuard"], _src_app_core_auth__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"]],
        data: {
            permisison: 'ROLE_map.map',
        },
    },
];
var MapModule = /** @class */ (function () {
    function MapModule() {
    }
    MapModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _core_core_module__WEBPACK_IMPORTED_MODULE_11__["CoreModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_6__["DragDropModule"],
                _asymmetrik_ngx_leaflet__WEBPACK_IMPORTED_MODULE_7__["LeafletModule"],
                _app_views_partials_partials_module__WEBPACK_IMPORTED_MODULE_10__["PartialsModule"],
                _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_6__["DragDropModule"]
            ],
            providers: [_src_app_core_auth__WEBPACK_IMPORTED_MODULE_5__["AuthGuard"], _src_app_core_auth__WEBPACK_IMPORTED_MODULE_5__["ModuleGuard"], _data_service_service__WEBPACK_IMPORTED_MODULE_8__["DataServiceService"]],
            declarations: [_map_component__WEBPACK_IMPORTED_MODULE_4__["MapComponent"], _map_test_map_test_component__WEBPACK_IMPORTED_MODULE_9__["MapTestComponent"]],
        })
    ], MapModule);
    return MapModule;
}());



/***/ })

}]);
//# sourceMappingURL=app-views-pages-map-map-module.js.map