(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-views-pages-admin-admin-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/admin.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/pages/admin/admin.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <p>admin works!</p> -->\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/device-type/device-type.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/pages/admin/device-type/device-type.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- begin:: Content -->\r\n<!-- <div class=\"alert alert-light alert-elevate\" role=\"alert\">\r\n\t<div class=\"alert-icon\"><i class=\"flaticon-warning kt-font-brand\"></i></div>\r\n\t<div class=\"alert-text\">\r\n\t\tThe Keen Datatable component supports local or remote data source. For the local data you can pass\r\n\t\tjavascript array as data source. In this example the grid fetches its data from a javascript array data\r\n\t\tsource. It also defines\r\n\t\tthe schema model of the data source. In addition to the visualization, the Datatable provides built-in\r\n\t\tsupport for operations over data such as sorting, filtering and paging performed in user browser(frontend).\r\n\t</div>\r\n</div> -->\r\n<div class=\"kt-portlet kt-portlet--mobile kt-portlet--main kt-padding\">\r\n\t<div class=\"kt-portlet__head kt-portlet__head--lg\">\r\n\t\t<div class=\"kt-portlet__head-label\">\r\n\t\t\t<span class=\"kt-portlet__head-icon\">\r\n\t\t\t\t<i class=\"icon-setting\"></i>\r\n\t\t\t</span>\r\n\t\t\t<h3 class=\"kt-portlet__head-title\">\r\n\t\t\t\t{{'ADMIN.GENERAL.DEVICE_TYPE' | translate | uppercase }}\r\n\t\t\t</h3>\r\n\t\t</div>\r\n\t\t<div class=\"kt-portlet__head-toolbar\">\r\n\t\t\t<div class=\"kt-portlet__head-wrapper\">\r\n\t\t\t\t<button type=\"button\" class=\"btn btn-brand btn-icon-sm btn-pill\" (click)=\"open(content,'add')\"\r\n\t\t\t\t\taria-expanded=\"false\">\r\n\t\t\t\t\t<i class=\"icon-plus\"></i> {{'COMMON.ACTIONS.ADD_NEW' | translate}}\r\n\t\t\t\t\t{{'ADMIN.GENERAL.DEVICE_TYPE' | translate }}\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\t<!-- <div class=\"kt-portlet__body kt-padding-t-10 kt-padding-b-10\">\r\n\r\n\r\n\t</div> -->\r\n\t<div class=\"kt-portlet__body kt-portlet__body--fit kt-portlet__body--overflow-y\">\r\n\t\t<!--begin: Search Form -->\r\n\t\t<div class=\"kt-form kt-fork--label-right kt-padding-10 kt-padding-l-20 kt-padding-r-20\">\r\n\t\t\t<div class=\"row align-items-center\">\r\n\t\t\t\t<div class=\"col-xl-8 order-2 order-xl-1\">\r\n\t\t\t\t\t<div class=\"row align-items-center\">\r\n\t\t\t\t\t\t<div class=\"col-md-3 kt-margin-b-20-tablet-and-mobile\" id=\"formSearch\"\r\n\t\t\t\t\t\t\t(keyup.enter)=\"dataTable.search()\">\r\n\t\t\t\t\t\t\t<div class=\"kt-form__group kt-form__group--inline\">\r\n\t\t\t\t\t\t\t\t<div class=\"kt-form__label\">\r\n\t\t\t\t\t\t\t\t\t<label>{{'COMMON.COLUMN.NAME' | translate}}:</label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"kt-form__control\">\r\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" data-key=\"name\">\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t<div class=\"col-md-3 kt-margin-b-20-tablet-and-mobile\">\r\n\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-secondary btn-hover-brand\"\r\n\t\t\t\t\t\t\t\t(click)=\"dataTable.search()\">\r\n\t\t\t\t\t\t\t\t<i class=\"icon-search\"></i>{{'COMMON.SEARCH' | translate}}</button>\r\n\t\t\t\t\t\t\t&nbsp;\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<!--end: Search Form -->\r\n\r\n\t\t<!--begin: Datatable -->\r\n\t\t<div resizeObserver (resize)=\"onResize($event.contentRect)\"\r\n\t\t\tclass=\"kt_datatable kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded\"\r\n\t\t\t[ngClass]=\"{'kt-datatable--error':dataTable.isNotFound}\" id=\"api_methods\"\r\n\t\t\tstyle=\"position: static; zoom: 1;\">\r\n\t\t\t<table class=\"kt-datatable__table\" style=\"display: block; min-height: 500px;\">\r\n\t\t\t\t<!-- Header -->\r\n\t\t\t\t<thead class=\"kt-datatable__head\">\r\n\t\t\t\t\t<tr class=\"kt-datatable__row\" style=\"left: 0px;\">\r\n\t\t\t\t\t\t<!-- subRow -->\r\n\t\t\t\t\t\t<th *ngIf=\"dataTable.isShowSubRow\"\r\n\t\t\t\t\t\t\tclass=\"kt-datatable__cell kt-datatable__toggle-detail kt-datatable__cell--sort\">\r\n\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t<!-- end subRow -->\r\n\r\n\t\t\t\t\t\t<!-- checkbox -->\r\n\t\t\t\t\t\t<th *ngIf=\"dataTable.selecter\"\r\n\t\t\t\t\t\t\tclass=\"kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check\">\r\n\t\t\t\t\t\t\t<span style=\"width: 20px;\">\r\n\t\t\t\t\t\t\t\t<label class=\"kt-checkbox kt-checkbox--single kt-checkbox--all kt-checkbox--solid\">\r\n\t\t\t\t\t\t\t\t\t<input (click)=\"dataTable.setCheckBoxAll()\" type=\"checkbox\"\r\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"dataTable.isCheckBoxAll\">&nbsp;<span></span>\r\n\t\t\t\t\t\t\t\t</label>\r\n\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t<!-- end checkbox -->\r\n\r\n\t\t\t\t\t\t<ng-container *ngFor=\"let column of dataTable.getColumns()\">\r\n\t\t\t\t\t\t\t<th [ngClass]=\"{'kt-datatable__cell--sorted':column.isSort}\"\r\n\t\t\t\t\t\t\t\t[ngStyle]=\"{'display':(!column.isShow?'none':'')}\" [attr.data-field]=\"column.field\"\r\n\t\t\t\t\t\t\t\tclass=\"kt-datatable__cell kt-datatable__cell--sort {{column.class}}\"\r\n\t\t\t\t\t\t\t\t(click)=\"dataTable.sort(column.field)\">\r\n\t\t\t\t\t\t\t\t<span [ngStyle]=\"column.style\">{{column.translate | translate}}\r\n\t\t\t\t\t\t\t\t\t<i *ngIf=\"column.isSort\"\r\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'flaticon2-arrow-down':(column.isSort && column.sortable =='desc'),'flaticon2-arrow-up':(column.isSort && column.sortable =='asc')}\"></i>\r\n\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</thead>\r\n\t\t\t\t<!-- table body -->\r\n\t\t\t\t<tbody class=\"kt-datatable__body\" [ngStyle]=\"dataTable.bodyStyle\">\r\n\t\t\t\t\t<ng-container *ngFor=\"let item of dataTable.data;let i=index\">\r\n\t\t\t\t\t\t<tr [ngClass]=\"{'kt-datatable__row--active':item.dt_selecter}\" class=\"kt-datatable__row\"\r\n\t\t\t\t\t\t\tstyle=\"left: 0px;\">\r\n\t\t\t\t\t\t\t<!-- subRow -->\r\n\t\t\t\t\t\t\t<td *ngIf=\"dataTable.isShowSubRow\" class=\"kt-datatable__cell kt-datatable__toggle-detail\">\r\n\t\t\t\t\t\t\t\t<a (click)=\"dataTable.showSubRow(i)\" class=\"kt-datatable__toggle-detail\">\r\n\t\t\t\t\t\t\t\t\t<i [ngClass]=\"{'icon-caret-down' : (dataTable.lastRowShow == i), \r\n\t\t\t\t\t\t\t\t\t'icon-caret-right' : (dataTable.lastRowShow != i)}\"></i>\r\n\t\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<!-- end subRow -->\r\n\r\n\t\t\t\t\t\t\t<!-- checkbox -->\r\n\t\t\t\t\t\t\t<td *ngIf=\"dataTable.selecter\"\r\n\t\t\t\t\t\t\t\tclass=\"kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check\"\r\n\t\t\t\t\t\t\t\tdata-field=\"RecordID\"><span style=\"width: 20px;\">\r\n\t\t\t\t\t\t\t\t\t<label class=\"kt-checkbox kt-checkbox--single kt-checkbox--solid\">\r\n\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" [(ngModel)]=\"item.dt_selecter\"\r\n\t\t\t\t\t\t\t\t\t\t\t(click)=\"dataTable.setCheckBox(i)\">&nbsp;<span></span>\r\n\t\t\t\t\t\t\t\t\t</label>\r\n\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<!-- end checkbox -->\r\n\t\t\t\t\t\t\t<td [ngStyle]=\"{'display':(!dataTable.getColumns()[0].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\tclass=\"kt-datatable__cell kt-datatable__cell\" data-field=\"id\">\r\n\t\t\t\t\t\t\t\t<span\r\n\t\t\t\t\t\t\t\t\t[ngStyle]=\"dataTable.getColumns()[0].style\">{{i+dataTable.getPaginations().form}}</span>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<td [ngStyle]=\"{'display':(!dataTable.getColumns()[1].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\tdata-field=\"key_name\" class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[1].style\">{{item.name}}</span>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<td [ngStyle]=\"{'display':(!dataTable.getColumns()[2].isShow?'none':'')}\" data-field=\"name\"\r\n\t\t\t\t\t\t\t\tclass=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[2].style\">{{item.description}}</span>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<td [ngStyle]=\"{'display':(!dataTable.getColumns()[3].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\tdata-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[3].style\">{{item.modifieldBy}}</span>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<td [ngStyle]=\"{'display':(!dataTable.getColumns()[4].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\tdata-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t<span\r\n\t\t\t\t\t\t\t\t\t[ngStyle]=\"dataTable.getColumns()[4].style\">{{item.updatedDate | userDate}}</span>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<td [ngStyle]=\"{'display':(!dataTable.getColumns()[5].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\tdata-field=\"status\" class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[5].style\">\r\n\t\t\t\t\t\t\t\t\t{{item.sortOrder?item.sortOrder:\"-\"}}\r\n\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<td [ngStyle]=\"{'display':(!dataTable.getColumns()[6].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\tdata-field=\"Actions\" data-autohide-disabled=\"false\" class=\"kt-datatable__cell\"><span\r\n\t\t\t\t\t\t\t\t\t[ngStyle]=\"dataTable.getColumns()[6].style\"\r\n\t\t\t\t\t\t\t\t\tstyle=\"overflow: visible; position: relative;\">\r\n\t\t\t\t\t\t\t\t\t<a ngbTooltip=\"{{'COMMON.ACTIONS.EDIT' | translate }}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n\t\t\t\t\t\t\t\t\t\t(click)=\"open(content,'edit',item)\">\r\n\t\t\t\t\t\t\t\t\t\t<i class=\"icon-edit\"></i>\r\n\t\t\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t\t\t<a ngbTooltip=\"{{'COMMON.ACTIONS.DELETE' | translate }}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n\t\t\t\t\t\t\t\t\t\t(click)=\"open(content,'delete',item)\">\r\n\t\t\t\t\t\t\t\t\t\t<i class=\"icon-trash\"></i>\r\n\t\t\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t<!-- Sub row -->\r\n\t\t\t\t\t\t<tr *ngIf=\"dataTable.lastRowShow==i && dataTable.isShowSubRow\" class=\"kt-datatable__row-detail\">\r\n\t\t\t\t\t\t\t<td class=\"kt-datatable__detail\" colspan=\"9\">\r\n\t\t\t\t\t\t\t\t<table>\r\n\t\t\t\t\t\t\t\t\t<tr [ngStyle]=\"{'display':(dataTable.getColumns()[0].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"kt-datatable__row\">\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span>{{dataTable.getColumns()[0].translate | translate}}</span></td>\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span\r\n\t\t\t\t\t\t\t\t\t\t\t\t[ngStyle]=\"dataTable.getColumns()[0].style\">{{i+dataTable.getPaginations().form}}</span>\r\n\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t<tr [ngStyle]=\"{'display':(dataTable.getColumns()[1].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"kt-datatable__row\">\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span>{{dataTable.getColumns()[1].translate | translate}}</span></td>\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[1].style\">{{item.name}}</span>\r\n\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t<tr [ngStyle]=\"{'display':(dataTable.getColumns()[2].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"kt-datatable__row\">\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span>{{dataTable.getColumns()[2].translate | translate}}</span></td>\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[2].style\">{{item.description}}</span>\r\n\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t<tr [ngStyle]=\"{'display':(dataTable.getColumns()[3].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"kt-datatable__row\">\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span>{{dataTable.getColumns()[3].translate | translate}}</span></td>\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[3].style\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t>{{item.modifieldBy}}</span>\r\n\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t<tr [ngStyle]=\"{'display':(dataTable.getColumns()[4].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"kt-datatable__row\">\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span>{{dataTable.getColumns()[4].translate | translate}}</span></td>\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[4].style\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t>{{item.updatedDate | userDate}}</span>\r\n\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t</tr>\r\n\r\n\t\t\t\t\t\t\t\t\t<tr [ngStyle]=\"{'display':(dataTable.getColumns()[5].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"kt-datatable__row\">\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span>{{dataTable.getColumns()[5].translate | translate}}</span></td>\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[5].style\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t{{item.sortOrder?item.sortOrder:\"-\"}}\r\n\t\t\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t<tr [ngStyle]=\"{'display':(dataTable.getColumns()[6].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"kt-datatable__row\">\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span>{{dataTable.getColumns()[6].translate | translate}}</span></td>\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[6].style\"\r\n\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"overflow: visible; position: relative;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<a ngbTooltip=\"{{'COMMON.ACTIONS.EDIT' | translate }}\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\tclass=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t(click)=\"open(content,item)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-edit\"></i>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<a ngbTooltip=\"{{'COMMON.ACTIONS.DELETE' | translate }}\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\tclass=\"btn btn-sm btn-clean btn-icon btn-icon-md\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-trash\"></i>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t<!-- No records found -->\r\n\t\t\t\t\t<div *ngIf=\"dataTable.isNotFound\" class=\"kt-datatable--error\">\r\n\t\t\t\t\t\t<kt-not-found></kt-not-found>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</tbody>\r\n\r\n\t\t\t</table>\r\n\t\t\t<!-- begin pagination -->\r\n\t\t\t<div  [ngStyle]=\"{'visibility':(dataTable.isNotFound?'hidden':'visible')}\" class=\"kt-datatable__pager kt-datatable--paging-loaded\">\r\n\t\t\t\t<!-- pagination -->\r\n\t\t\t\t<ul  class=\"kt-datatable__pager-nav\">\r\n\t\t\t\t\t<li><a ngbTooltip=\"{{'COMMON.FIRST_PAGE' | translate}}\"\r\n\t\t\t\t\t\t\tclass=\"kt-datatable__pager-link kt-datatable__pager-link--first\"\r\n\t\t\t\t\t\t\t[ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getFirstPage())}\"\r\n\t\t\t\t\t\t\t(click)=\"dataTable.gotoPage(dataTable.getFirstPage())\">\r\n\t\t\t\t\t\t\t<i class=\"flaticon2-fast-back\"></i></a></li>\r\n\t\t\t\t\t<li><a ngbTooltip=\"{{'COMMON.PREVIOUS_PAGE' | translate}}\"\r\n\t\t\t\t\t\t\tclass=\"kt-datatable__pager-link kt-datatable__pager-link--prev\"\r\n\t\t\t\t\t\t\t[ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getFirstPage())}\"\r\n\t\t\t\t\t\t\t(click)=\"dataTable.gotoPage(dataTable.getCurrentPage()-1)\">\r\n\t\t\t\t\t\t\t<i class=\"flaticon2-back\"></i></a></li>\r\n\t\t\t\t\t<li></li>\r\n\t\t\t\t\t<li style=\"display: none;\"><input type=\"text\" class=\"kt-pager-input form-control\"\r\n\t\t\t\t\t\t\ttitle=\"Page number\"></li>\r\n\t\t\t\t\t<li *ngFor=\"let page of dataTable.getPageDisplay()\">\r\n\t\t\t\t\t\t<a (click)=\"dataTable.gotoPage(page)\"\r\n\t\t\t\t\t\t\tclass=\"kt-datatable__pager-link kt-datatable__pager-link-number \"\r\n\t\t\t\t\t\t\t[ngClass]=\"{'kt-datatable__pager-link--active':page==dataTable.getCurrentPage()}\"\r\n\t\t\t\t\t\t\tdata-page=\"1\">{{page}}</a>\r\n\t\t\t\t\t</li>\r\n\t\t\t\t\t<li style=\"\"></li>\r\n\t\t\t\t\t<li><a ngbTooltip=\"{{'COMMON.NEXT_PAGE' | translate}}\"\r\n\t\t\t\t\t\t\tclass=\"kt-datatable__pager-link kt-datatable__pager-link--next \"\r\n\t\t\t\t\t\t\t[ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getLastPage())}\"\r\n\t\t\t\t\t\t\t(click)=\"dataTable.gotoPage(dataTable.getCurrentPage()+1)\"><i\r\n\t\t\t\t\t\t\t\tclass=\" flaticon2-next\"></i></a></li>\r\n\t\t\t\t\t<li><a ngbTooltip=\"{{'COMMON.LAST_PAGE' | translate}}\"\r\n\t\t\t\t\t\t\t[ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getLastPage())}\"\r\n\t\t\t\t\t\t\tclass=\"kt-datatable__pager-link kt-datatable__pager-link--last\"\r\n\t\t\t\t\t\t\t(click)=\"dataTable.gotoPage(dataTable.getLastPage())\">\r\n\t\t\t\t\t\t\t<i class=\"flaticon2-fast-next\"></i></a></li>\r\n\t\t\t\t</ul>\r\n\t\t\t\t<!-- page size -->\r\n\t\t\t\t<div class=\"kt-datatable__pager-info\">\r\n\t\t\t\t\t<div class=\"dropdown bootstrap-select kt-datatable__pager-size\" id=\"dataTableSelect\"\r\n\t\t\t\t\t\tstyle=\"width: 60px;\">\r\n\t\t\t\t\t\t<select (change)=\"dataTable.setPageSize($event.target.value)\"\r\n\t\t\t\t\t\t\tclass=\"selectpicker kt-datatable__pager-size\" title=\"Select page size\" data-selected=\"30\"\r\n\t\t\t\t\t\t\tdata-width=\"60px\">\r\n\t\t\t\t\t\t\t<option *ngFor=\"let item of dataTable.getPaginationSelect()\"\r\n\t\t\t\t\t\t\t\t[selected]=\"item==dataTable.getPaginations().pageSize\" value=\"{{item}}\" title=\"{{item}}\"\r\n\t\t\t\t\t\t\t\tdata-toggle=\"tooltip\">{{item}}</option>\r\n\t\t\t\t\t\t</select>\r\n\t\t\t\t\t</div><span class=\"kt-datatable__pager-detail\">{{'COMMON.SHOW' | translate}}\r\n\t\t\t\t\t\t{{dataTable.getPaginations().form}} -\r\n\t\t\t\t\t\t{{dataTable.getPaginations().to}} {{'COMMON.OF' | translate}}\r\n\t\t\t\t\t\t{{dataTable.getPaginations().total}}</span>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<!-- end: Pagination -->\r\n\t\t</div>\r\n\r\n\t\t<!--end: Datatable -->\r\n\t\t<div *ngIf=\"dataTable.isLoading\" class=\"blockUI blockMsg blockElement\"\r\n\t\t\tstyle=\"z-index: 1011; position: absolute; padding: 0px; margin: 0px; width: 169px; top: 50%; left: 45%; text-align: center; color: rgb(0, 0, 0); border: 0px; cursor: wait;\">\r\n\t\t\t<div class=\"blockui \"><span>{{'COMMON.PLEASE_WAIT' | translate}}</span><span>\r\n\t\t\t\t\t<div class=\"kt-spinner kt-spinner--loader kt-spinner--brand \"></div>\r\n\t\t\t\t</span>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\r\n</div>\r\n<!-- Modal -->\r\n<ng-template #content let-c=\"close\" let-d=\"dismiss\">\r\n\t<div class=\"modal-header\">\r\n\t\t<h4 class=\"modal-title\">{{titlePopup | translate}} {{'ADMIN.GENERAL.DEVICE_TYPE' | translate  }}\r\n\t\t</h4>\r\n\t\t<button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\r\n\t\t\t<span aria-hidden=\"true\">&times;</span>\r\n\t\t</button>\r\n\t</div>\r\n\t<div class=\"modal-body\">\r\n\t\t<ng-container [ngSwitch]=\"action\">\r\n\t\t\t<ng-container *ngSwitchCase=\"'delete'\" [ngTemplateOutlet]=\"tmpConfirmDelete\">\r\n\t\t\t</ng-container>\r\n\t\t\t<ng-container *ngSwitchCase=\"'edit'\" [ngTemplateOutlet]=\"tmpFormEdit\">\r\n\t\t\t</ng-container>\r\n\t\t\t<ng-container *ngSwitchCase=\"'add'\" [ngTemplateOutlet]=\"tmpFormAdd\">\r\n\t\t\t</ng-container>\r\n\t\t\t<!-- <ng-container *ngSwitchDefault [ngTemplateOutlet]=\"tmpFormAdd\">\r\n\t\t\t</ng-container> -->\r\n\t\t</ng-container>\r\n\t</div>\r\n\t<div class=\"modal-footer\">\r\n\t\t<ng-container [ngSwitch]=\"action\">\r\n\t\t\t<button *ngSwitchCase=\"'edit'\" type=\"button\" class=\"btn btn-primary\"\r\n\t\t\t\t(click)=\"onSubmit()\">{{'COMMON.ACTIONS.SAVE_CHANGES' | translate}}</button>\r\n\t\t\t<button *ngSwitchCase=\"'add'\" type=\"button\" class=\"btn btn-primary\"\r\n\t\t\t\t(click)=\"onSubmit()\">{{'COMMON.ACTIONS.ADD' | translate}}</button>\r\n\t\t\t<button *ngSwitchCase=\"'delete'\" type=\"button\" class=\"btn btn-primary\"\r\n\t\t\t\t(click)=\"onSubmit()\">{{'COMMON.ACTIONS.CONFIRM' | translate}}</button>\r\n\t\t</ng-container>\r\n\t\t<button type=\"button\" class=\"btn btn-secondary\"\r\n\t\t\t(click)=\"c('Close click')\">{{'COMMON.ACTIONS.CLOSE' | translate}}</button>\r\n\t</div>\r\n</ng-template>\r\n<!-- edit form -->\r\n<ng-template #tmpFormEdit>\r\n\t<div class=\"kt-portlet__body\">\r\n\t\t<ul class=\"nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand\" role=\"tablist\">\r\n\t\t\t<li class=\"nav-item\">\r\n\t\t\t\t<a class=\"nav-link active\" data-toggle=\"tab\" href=\"#kt_tab_base\" role=\"tab\"\r\n\t\t\t\t\taria-selected=\"false\">{{ 'COMMON.GENERAL.BASE' | translate }}</a>\r\n\t\t\t</li>\r\n\t\t\t<li class=\"nav-item\">\r\n\t\t\t\t<a class=\"nav-link\" data-toggle=\"tab\" href=\"#kt_tab_command\" role=\"tab\"\r\n\t\t\t\t\taria-selected=\"true\">{{ 'COMMON.GENERAL.COMMAND' | translate }}</a>\r\n\t\t\t</li>\r\n\t\t</ul>\r\n\t\t<div class=\"tab-content \">\r\n\t\t\t<div class=\"tab-pane active\" id=\"kt_tab_base\" role=\"tabpanel\">\r\n\t\t\t\t<form class=\"kt-form\" [formGroup]=\"formEdit\" (ngSubmit)=\"onSubmit()\" novalidate=\"novalidate\">\r\n\t\t\t\t\t<div class=\"kt-portlet__body validate is-invalid\">\r\n\t\t\t\t\t\t<div class=\"form-group row\">\r\n\t\t\t\t\t\t\t<div class=\"col-md-6\">\r\n\t\t\t\t\t\t\t\t<label>{{ 'ADMIN.SIM_TYPE.GENERAL.NAME' | translate }}</label>\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" autocomplete=\"false\" name=\"name\"\r\n\t\t\t\t\t\t\t\t\tformControlName=\"name\">\r\n\t\t\t\t\t\t\t\t<div class=\"error invalid-feedback\" *ngIf=\"isControlHasError('name','required')\">\r\n\t\t\t\t\t\t\t\t\t<strong>{{ 'COMMON.VALIDATION.REQUIRED_FIELD_NAME' | translate }}\r\n\t\t\t\t\t\t\t\t\t\t{{ 'ADMIN.SIM_TYPE.GENERAL.NAME' | translate }}</strong>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"col-md-6\">\r\n\t\t\t\t\t\t\t\t<label>{{ 'COMMON.COLUMN.SORT_ORDER' | translate }}</label>\r\n\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" autocomplete=\"false\"\r\n\t\t\t\t\t\t\t\t\tformControlName=\"sortOrder\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"form-group row\">\r\n\t\t\t\t\t\t\t<div class=\"col-md-6\">\r\n\t\t\t\t\t\t\t\t<label>{{ 'COMMON.COLUMN.CREATED_BY' | translate }}</label>\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" autocomplete=\"false\"\r\n\t\t\t\t\t\t\t\t\tformControlName=\"createdBy\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"col-md-6\">\r\n\t\t\t\t\t\t\t\t<label>{{ 'COMMON.COLUMN.CREATED_DATE' | translate }}</label>\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" autocomplete=\"false\"\r\n\t\t\t\t\t\t\t\t\tformControlName=\"createdDate\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"form-group row\">\r\n\t\t\t\t\t\t\t<div class=\"col-md-6\">\r\n\t\t\t\t\t\t\t\t<label>{{ 'COMMON.COLUMN.UPDATED_BY' | translate }}</label>\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" autocomplete=\"false\"\r\n\t\t\t\t\t\t\t\t\tformControlName=\"updatedBy\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"col-md-6\">\r\n\t\t\t\t\t\t\t\t<label>{{ 'COMMON.COLUMN.UPDATED_DATE' | translate }}</label>\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" autocomplete=\"false\"\r\n\t\t\t\t\t\t\t\t\tformControlName=\"updatedDate\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"form-group row\">\r\n\t\t\t\t\t\t\t<div class=\"col-md-6\">\r\n\t\t\t\t\t\t\t\t<label>{{ 'COMMON.COLUMN.DESCRIPTION' | translate }}</label>\r\n\t\t\t\t\t\t\t\t<textarea class=\"form-control\" autocomplete=\"false\" rows=\"3\"\r\n\t\t\t\t\t\t\t\t\tformControlName=\"description\"></textarea>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"col-md-6\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</form>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"tab-pane\" id=\"kt_tab_command\" role=\"tabpanel\">\r\n\t\t\t\t<ng-container [ngTemplateOutlet]=\"commandForm\"></ng-container>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</ng-template>\r\n<!-- add form  -->\r\n<ng-template #tmpFormAdd>\r\n\t<div class=\"kt-portlet__body\">\r\n\t\t<ul class=\"nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-brand\" role=\"tablist\">\r\n\t\t\t<li class=\"nav-item\">\r\n\t\t\t\t<a class=\"nav-link active\" data-toggle=\"tab\" href=\"#kt_tab_base\" role=\"tab\"\r\n\t\t\t\t\taria-selected=\"false\">{{ 'COMMON.GENERAL.BASE' | translate }}</a>\r\n\t\t\t</li>\r\n\t\t\t<li class=\"nav-item\">\r\n\t\t\t\t<a class=\"nav-link\" data-toggle=\"tab\" href=\"#kt_tab_command\" role=\"tab\"\r\n\t\t\t\t\taria-selected=\"true\">{{ 'COMMON.GENERAL.COMMAND' | translate }}</a>\r\n\t\t\t</li>\r\n\t\t</ul>\r\n\t\t<div class=\"tab-content \">\r\n\t\t\t<div class=\"tab-pane active\" id=\"kt_tab_base\" role=\"tabpanel\">\r\n\t\t\t\t<form class=\"kt-form\" [formGroup]=\"formAdd\" (ngSubmit)=\"onSubmit()\" novalidate=\"novalidate\">\r\n\t\t\t\t\t<div class=\"kt-portlet__body\">\r\n\t\t\t\t\t\t<div class=\"form-group row\">\r\n\t\t\t\t\t\t\t<div class=\"col-md-6 validate is-invalid\">\r\n\t\t\t\t\t\t\t\t<label>{{ 'ADMIN.SIM_TYPE.GENERAL.NAME' | translate }}</label>\r\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" autocomplete=\"false\" name=\"name\"\r\n\t\t\t\t\t\t\t\t\tformControlName=\"name\">\r\n\t\t\t\t\t\t\t\t<div class=\"error invalid-feedback\" *ngIf=\"isControlHasError('name','required')\">\r\n\t\t\t\t\t\t\t\t\t<strong>{{ 'COMMON.VALIDATION.REQUIRED_FIELD_NAME' | translate }}\r\n\t\t\t\t\t\t\t\t\t\t{{ 'ADMIN.SIM_TYPE.GENERAL.NAME' | translate }}</strong>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"col-md-6 validate is-invalid\">\r\n\t\t\t\t\t\t\t\t<label>{{ 'COMMON.COLUMN.SORT_ORDER' | translate }}</label>\r\n\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" autocomplete=\"false\"\r\n\t\t\t\t\t\t\t\t\tformControlName=\"sortOrder\">\r\n\t\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"form-group row\">\r\n\t\t\t\t\t\t\t<div class=\"col-md-6 validate is-invalid\">\r\n\t\t\t\t\t\t\t\t<label>{{ 'COMMON.COLUMN.DESCRIPTION' | translate }}</label>\r\n\t\t\t\t\t\t\t\t<textarea class=\"form-control\" autocomplete=\"false\" rows=\"3\"\r\n\t\t\t\t\t\t\t\t\tformControlName=\"description\"></textarea>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</form>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"tab-pane\" id=\"kt_tab_command\" role=\"tabpanel\">\r\n\t\t\t\t<ng-container [ngTemplateOutlet]=\"commandForm\"></ng-container>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\r\n</ng-template>\r\n<!-- comfirm delete -->\r\n<ng-template #tmpConfirmDelete>\r\n\t<div class=\"alert alert-bold\" role=\"alert\">\r\n\t\t<div class=\"alert-icon\"><i class=\"icon-question-circle\"></i></div>\r\n\t\t<div class=\"alert-text\">{{'COMMON.MESSAGE.COMFIRM_DELETE' | translate}}</div>\r\n\t</div>\r\n</ng-template>\r\n\r\n<!-- end:: Content -->\r\n\r\n<ng-template #commandForm>\r\n\t<div class=\"kt-section\">\r\n\t\t<div class=\"kt-section__info\">\r\n\t\t\t<button type=\"button\" class=\"btn btn-brand btn-icon-sm btn-pill\" (click)=\"addCommand()\"\r\n\t\t\t\taria-expanded=\"false\">\r\n\t\t\t\t<i class=\"icon-plus\"></i> {{'COMMON.ACTIONS.ADD_NEW' | translate}}\r\n\t\t\t\t{{'COMMON.GENERAL.COMMAND' | translate | lowercase }}\r\n\t\t\t</button>\r\n\t\t</div>\r\n\t\t<div class=\"kt-section__content\">\r\n\t\t\t<form class=\"kt-form\" [formGroup]=\"commandsForm\" novalidate=\"novalidate\">\r\n\t\t\t\t<div class=\"kt-portlet__body\" formArrayName=\"itemRows\">\r\n\t\t\t\t\t<table class=\"table\">\r\n\t\t\t\t\t\t<colgroup>\r\n\t\t\t\t\t\t\t<col style=\"width: 50px\">\r\n\t\t\t\t\t\t\t<col>\r\n\t\t\t\t\t\t\t<col>\r\n\t\t\t\t\t\t\t<col style=\"width: 100px\">\r\n\t\t\t\t\t\t</colgroup>\r\n\t\t\t\t\t\t<thead>\r\n\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t<th>#</th>\r\n\t\t\t\t\t\t\t\t<th>{{'COMMON.COLUMN.NAME' | translate}}</th>\r\n\t\t\t\t\t\t\t\t<th>{{'COMMON.GENERAL.COMMAND' | translate}}</th>\r\n\t\t\t\t\t\t\t\t<th>{{'COMMON.ACTIONS.ACTIONS' | translate}}</th>\r\n\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t</thead>\r\n\t\t\t\t\t\t<tbody>\r\n\t\t\t\t\t\t\t<tr *ngFor=\"let row of  formArr.controls;let i=index\" [formGroupName]=\"i\">\r\n\t\t\t\t\t\t\t\t<td>{{i+1}}</td>\r\n\t\t\t\t\t\t\t\t<td>\r\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" autocomplete=\"false\" name=\"name\"\r\n\t\t\t\t\t\t\t\t\t\tformControlName=\"name\">\r\n\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t<td>\r\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" autocomplete=\"false\" name=\"commandStr\"\r\n\t\t\t\t\t\t\t\t\t\tformControlName=\"commandStr\">\r\n\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t<td><a ngbTooltip=\"{{'COMMON.ACTIONS.DELETE' | translate }}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"btn btn-sm btn-clean btn-icon btn-icon-md\" (click)=\"removeCommand(i)\">\r\n\t\t\t\t\t\t\t\t\t\t<i class=\"icon-trash\"></i></a>\r\n\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t<td colspan=\"4\" style=\"padding: 20px;text-align: center;\">\r\n\t\t\t\t\t\t\t\t\t<span *ngIf=\"formArr.controls.length==0\" class=\"kt-datatable--error\"\r\n\t\t\t\t\t\t\t\t\t\t[innerHTML]=\"'COMMON.MESSAGE.NO_RECORDS_FOUND' | translate\">No records\r\n\t\t\t\t\t\t\t\t\t\tfound</span>\r\n\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t</tbody>\r\n\t\t\t\t\t</table>\r\n\t\t\t\t</div>\r\n\t\t\t</form>\r\n\t\t</div>\r\n\t</div>\r\n</ng-template>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/icon-type/icon-type.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/pages/admin/icon-type/icon-type.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- begin:: Content -->\r\n<div class=\"kt-portlet kt-portlet--mobile kt-portlet--main\">\r\n    <div class=\"kt-portlet__head kt-portlet__head--lg\">\r\n        <div class=\"kt-portlet__head-label\">\r\n            <h3 class=\"kt-portlet__head-title\">\r\n                {{'MENU.ICON' | translate | uppercase }}\r\n            </h3>\r\n        </div>\r\n        <div class=\"kt-portlet__head-toolbar\">\r\n            <div class=\"kt-portlet__head-wrapper\">\r\n                <div class=\"dropdown dropdown-inline\">\r\n                    <button type=\"button\" class=\"btn btn-brand btn-icon-sm btn-pill\" (click)=\"buttonAddNew(content)\">\r\n                        <i class=\"flaticon2-plus\"></i> {{'COMMON.ACTIONS.ADD_NEW' | translate}}\r\n                        {{'ADMIN.DEVICE_ICON.GENERAL.ICONTYPE' | translate }}\r\n                    </button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- <div class=\"kt-portlet__body kt-padding-t-10 kt-padding-b-10\" disabled=\"true\">\r\n\r\n            </div> -->\r\n    <div class=\"kt-portlet__body kt-portlet__body--fit kt-portlet__body--overflow-y\">\r\n        <!--begin: Search Form -->\r\n        <div class=\"kt-form kt-fork--label-right kt-padding-10 kt-padding-l-20 kt-padding-r-20\">\r\n            <form class=\"\" [formGroup]=\"searchFormIcontype\" (ngSubmit)='searchIcontype(searchFormIcontype)'>\r\n\r\n                <div class=\"row align-items-center\">\r\n                    <div class=\"col-xl-8 order-2 order-xl-1\">\r\n                        <div class=\"row align-items-center\">\r\n                            <div class=\"col-md-4 kt-margin-b-20-tablet-and-mobile\">\r\n                                <div class=\"kt-form__group kt-form__group--inline\">\r\n                                    <div class=\"kt-form__label\">\r\n                                        <label\r\n                                            style=\"font-size: 15px\">{{'ADMIN.ROLE.GENERAL.NAME' | translate}}:</label>\r\n                                    </div>\r\n                                    <div class=\"kt-form__control\">\r\n                                        <input type=\"text\" class=\"form-control\"\r\n                                            placeholder=\"{{'ADMIN.DEVICE_ICON.GENERAL.ICONTYPE' | translate}}\"\r\n                                            formControlName=\"nameIcontype\">\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"col-md-4 kt-margin-b-20-tablet-and-mobile\">\r\n                                <button type=\"submit\" class=\"btn btn-secondary btn-hover-brand\">\r\n                                    <i class=\"icon-search\"></i>{{'COMMON.SEARCH' | translate}}</button>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n\r\n                </div>\r\n            </form>\r\n        </div>\r\n\r\n        <!--end: Search Form -->\r\n        <!--begin: Datatable -->\r\n        <div resizeObserver (resize)=\"onResize($event.contentRect)\"\r\n            class=\"kt_datatable kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded\"\r\n            [ngClass]=\"{'kt-datatable--error':dataTable.isNotFound}\" id=\"api_methods\"\r\n            style=\"position: static; zoom: 1;\">\r\n            <table class=\"kt-datatable__table\">\r\n                <thead class=\"kt-datatable__head\">\r\n                    <tr class=\"kt-datatable__row\" style=\"left: 0px;\">\r\n                        <!-- checkbox -->\r\n                        <th *ngIf=\"dataTable.selecter\"\r\n                            class=\"kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check\">\r\n                            <span style=\"width: 20px;\">\r\n                                <label class=\"kt-checkbox kt-checkbox--single kt-checkbox--solid\">\r\n                                    <input type=\"checkbox\" [(ngModel)]=\"dataTable.isCheckBoxAll\"\r\n                                        (click)=\"dataTable.setCheckBoxAll()\">&nbsp;<span></span>\r\n                                </label>\r\n                            </span>\r\n                        </th>\r\n                        <!-- end checkbox -->\r\n                        <th *ngIf=\"dataTable.isShowSubRow\" style=\"width: 13px;\"\r\n                            class=\"kt-datatable__cell kt-datatable__cell--sort\">\r\n                            <a class=\"kt-datatable__toggle-detail\">\r\n                            </a>\r\n                        </th>\r\n\r\n                        <ng-container *ngFor=\"let column of dataTable.getColumns()\">\r\n\r\n                            <th [ngClass]=\"{'kt-datatable__cell--sorted':column.isSort}\"\r\n                                [ngStyle]=\"{'display':(!column.isShow?'none':'')}\" [attr.data-field]=\"column.field\"\r\n                                class=\"kt-datatable__cell kt-datatable__cell--sort {{column.class}}\"\r\n                                (click)=\"dataTable.sort(column.field)\">\r\n                                <span [ngStyle]=\"column.style\">{{column.translate | translate}}\r\n                                    <i *ngIf=\"column.isSort\"\r\n                                        [ngClass]=\"{'flaticon2-arrow-down':(column.isSort && column.sortable =='desc'),'flaticon2-arrow-up':(column.isSort && column.sortable =='asc')}\"></i>\r\n                                </span>\r\n                            </th>\r\n                        </ng-container>\r\n                    </tr>\r\n                </thead>\r\n                <tbody class=\"kt-datatable__body\" [ngStyle]=\"dataTable.bodyStyle\">\r\n                    <ng-container *ngFor=\"let item of dataTable.data;let i=index\">\r\n                        <tr [ngClass]=\"{'kt-datatable__row--active':item.dt_selecter}\" class=\"kt-datatable__row\"\r\n                            style=\"left: 0px;\">\r\n                            <!-- checkbox -->\r\n                            <td *ngIf=\"dataTable.selecter\"\r\n                                class=\"kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check\"\r\n                                data-field=\"RecordID\"><span style=\"width: 20px;\">\r\n                                    <label class=\"kt-checkbox kt-checkbox--single kt-checkbox--solid\">\r\n                                        <input type=\"checkbox\" [(ngModel)]=\"item.dt_selecter\"\r\n                                            (click)=\"dataTable.setCheckBox(i)\">&nbsp;<span></span>\r\n                                    </label>\r\n                                </span>\r\n                            </td>\r\n                            <!-- end checkbox -->\r\n                            <td *ngIf=\"dataTable.isShowSubRow\" style=\"width: 13px;\"\r\n                                class=\"kt-datatable__cell kt-datatable__toggle-detail\">\r\n                                <a (click)=\"dataTable.showSubRow(i)\" class=\"kt-datatable__toggle-detail\">\r\n                                    <i [ngClass]=\"{'icon-caret-down' : (dataTable.lastRowShow == i), \r\n                                      'icon-caret-right' : (dataTable.lastRowShow != i)}\"></i>\r\n                                </a>\r\n                            </td>\r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[0].isShow?'none':'')}\"\r\n                                class=\"kt-datatable__cell kt-datatable__cell\" data-field=\"id\">\r\n                                <span\r\n                                    [ngStyle]=\"dataTable.getColumns()[0].style\">{{i+dataTable.getPaginations().form}}</span>\r\n                            </td>\r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[1].isShow?'none':'')}\"\r\n                                data-field=\"key_name\" class=\"kt-datatable__cell\">\r\n                                <span [ngStyle]=\"dataTable.getColumns()[1].style\">{{item.name}}</span>\r\n                            </td>\r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[2].isShow?'none':'')}\" data-field=\"name\"\r\n                                class=\"kt-datatable__cell\">\r\n                                <span [ngStyle]=\"dataTable.getColumns()[2].style\"\r\n                                    translate=\"{{item.nameKey}}\">{{item.nameKey}}</span>\r\n                            </td>\r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[3].isShow?'none':'')}\" data-field=\"name\"\r\n                                class=\"kt-datatable__cell\">\r\n                                <span [ngStyle]=\"dataTable.getColumns()[3].style\" translate=\"{{item.nameKey}}\">\r\n                                    <img  *ngIf= \"item.iconUrl\"style=\"height: 20px\" src=\"{{item.iconUrl}}\">\r\n                                </span>\r\n                            </td>\r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[4].isShow?'none':'')}\"\r\n                                data-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n                                <span [ngStyle]=\"dataTable.getColumns()[4].style\">\r\n                                     <img  *ngIf= \"item.iconMapUrl\"style=\"height: 20px\" src=\"{{item.iconMapUrl}}\">\r\n                                </span>\r\n                            </td>\r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[5].isShow?'none':'')}\"\r\n                                data-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n                                <span [ngStyle]=\"dataTable.getColumns()[5].style\">{{item.iconSvg}}</span>\r\n                            </td>\r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[6].isShow?'none':'')}\"\r\n                                data-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n                                <span [ngStyle]=\"dataTable.getColumns()[6].style\">{{item.iconMapSvg}}</span>\r\n                            </td>\r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[7].isShow?'none':'')}\"\r\n                                data-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n                                <span [ngStyle]=\"dataTable.getColumns()[7].style\">{{item.sortOrder}}</span>\r\n                            </td>\r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[8].isShow?'none':'')}\"\r\n                                data-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n                                <span [ngStyle]=\"dataTable.getColumns()[8].style\">{{item.modifiedBy}}</span>\r\n                            </td>\r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[9].isShow?'none':'')}\"\r\n                                data-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n                                <span [ngStyle]=\"dataTable.getColumns()[9].style\">{{item.updatedAt | userDate}}</span>\r\n                            </td>\r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[10].isShow?'none':'')}\"\r\n                                data-field=\"Actions\" data-autohide-disabled=\"false\" class=\"kt-datatable__cell\">\r\n                                <span [ngStyle]=\"dataTable.getColumns()[10].style\"\r\n                                    style=\"overflow: visible; position: relative;\">\r\n                                    <a ngbTooltip=\"{{'COMMON.ACTIONS.EDIT' | translate }}\"\r\n                                        class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                        (click)=\"editIcontype(item.id,content)\">\r\n                                        <i class=\"icon-edit\"></i>\r\n                                    </a>\r\n                                    <a ngbTooltip=\"{{'COMMON.ACTIONS.DELETE' | translate }}\"\r\n                                        class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                        (click)=\"getIdAction(item.id,'#modal-delete-icontype')\">\r\n                                        <i class=\"icon-trash\"></i>\r\n                                    </a>\r\n                                </span>\r\n                            </td>\r\n                        </tr>\r\n                        <!-- Sub row -->\r\n                        <tr *ngIf=\"dataTable.lastRowShow==i && dataTable.isShowSubRow\" class=\"kt-datatable__row-detail\">\r\n                            <td class=\"kt-datatable__detail\" colspan=\"9\">\r\n                                <table>\r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[0].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[0].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span\r\n                                                [ngStyle]=\"dataTable.getColumns()[0].style\">{{i+dataTable.getPaginations().form}}</span>\r\n                                        </td>\r\n                                    </tr>\r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[1].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[1].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span [ngStyle]=\"dataTable.getColumns()[1].style\">{{item.name}}</span>\r\n                                        </td>\r\n                                    </tr>\r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[3].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[3].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span [ngStyle]=\"dataTable.getColumns()[3].style\"\r\n                                                translate=\"{{item.nameKey}}\">{{item.nameKey}}</span>\r\n                                        </td>\r\n                                    </tr>\r\n\r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[4].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[4].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span [ngStyle]=\"dataTable.getColumns()[4].style\"\r\n                                                translate=\"{{item.iconMapUrl}}\">{{item.iconMapUrl}}</span>\r\n                                        </td>\r\n                                    </tr>\r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[5].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[5].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span [ngStyle]=\"dataTable.getColumns()[5].style\"\r\n                                                translate=\"{{item.iconSvg}}\">{{item.iconSvg}}</span>\r\n                                        </td>\r\n                                    </tr>\r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[6].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[6].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span [ngStyle]=\"dataTable.getColumns()[6].style\"\r\n                                                translate=\"{{item.iconMapSvg}}\">{{item.iconMapSvg}}</span>\r\n                                        </td>\r\n                                    </tr>\r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[7].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[7].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span [ngStyle]=\"dataTable.getColumns()[7].style\"\r\n                                                translate=\"{{item.sortOrder}}\">{{item.sortOrder}}</span>\r\n                                        </td>\r\n                                    </tr>\r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[8].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[8].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span [ngStyle]=\"dataTable.getColumns()[8].style\"\r\n                                                translate=\"{{item.modifiedBy}}\">{{item.modifiedBy}}</span>\r\n                                        </td>\r\n                                    </tr>\r\n                              \r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[9].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[9].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span [ngStyle]=\"dataTable.getColumns()[9].style\"\r\n                                                translate=\"{{item.updatedDate}}\">{{item.updatedAt | userDate}}</span>\r\n                                        </td>\r\n                                    </tr>\r\n\r\n\r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[10].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[10].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span [ngStyle]=\"dataTable.getColumns()[10].style\"\r\n                                                style=\"overflow: visible; position: relative;\">\r\n                                                <a ngbTooltip=\"{{'COMMON.ACTIONS.EDIT' | translate }}\"\r\n                                                    class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                                    (click)=\"editIcontype(item.id,content)\">\r\n                                                    <i class=\"icon-edit\"></i>\r\n                                                </a>\r\n                                                <a ngbTooltip=\"{{'COMMON.ACTIONS.DELETE' | translate }}\"\r\n                                                    class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                                    (click)=\"getIdAction(item.id,'#modal-delete-icontype')\">\r\n                                                    <i class=\"icon-trash\"></i>\r\n                                                </a>\r\n                                            </span>\r\n                                        </td>\r\n                                    </tr>\r\n                                </table>\r\n                            </td>\r\n                        </tr>\r\n                    </ng-container>\r\n                    <!-- No records found -->\r\n                    <span *ngIf=\"dataTable.isNotFound\" class=\"kt-datatable--error\"\r\n                        [innerHTML]=\"'COMMON.MESSAGE.NO_RECORDS_FOUND' | translate\">No records found</span>\r\n                </tbody>\r\n\r\n\r\n            </table>\r\n\r\n            <div class=\"kt-datatable__pager kt-datatable--paging-loaded\" *ngIf=\"dataTable.data?.length != 0\">\r\n                <ul class=\"kt-datatable__pager-nav\">\r\n                    <li><a title=\"First\" class=\"kt-datatable__pager-link kt-datatable__pager-link--first\"\r\n                            [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getFirstPage())}\"\r\n                            (click)=\"dataTable.gotoPage(dataTable.getFirstPage())\">\r\n                            <i class=\"flaticon2-fast-back\"></i></a></li>\r\n                    <li><a title=\"Previous\" class=\"kt-datatable__pager-link kt-datatable__pager-link--prev\"\r\n                            [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getFirstPage())}\"\r\n                            (click)=\"dataTable.gotoPage(dataTable.getCurrentPage()-1)\">\r\n                            <i class=\"flaticon2-back\"></i></a></li>\r\n                    <li></li>\r\n                    <li style=\"display: none;\"><input type=\"text\" class=\"kt-pager-input form-control\"\r\n                            title=\"Page number\"></li>\r\n                    <li *ngFor=\"let page of dataTable.getPageDisplay()\">\r\n                        <a (click)=\"dataTable.gotoPage(page)\"\r\n                            class=\"kt-datatable__pager-link kt-datatable__pager-link-number \"\r\n                            [ngClass]=\"{'kt-datatable__pager-link--active':page==dataTable.getCurrentPage()}\"\r\n                            data-page=\"1\" title=\"{{page}}\">{{page}}</a>\r\n                    </li>\r\n                    <li style=\"\"></li>\r\n                    <li><a title=\"Next\" class=\"kt-datatable__pager-link kt-datatable__pager-link--next \"\r\n                            [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getLastPage())}\"\r\n                            (click)=\"dataTable.gotoPage(dataTable.getCurrentPage()+1)\"><i\r\n                                class=\" flaticon2-next\"></i></a></li>\r\n                    <li><a title=\"Last\"\r\n                            [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getLastPage())}\"\r\n                            class=\"kt-datatable__pager-link kt-datatable__pager-link--last\"\r\n                            (click)=\"dataTable.gotoPage(dataTable.getLastPage())\">\r\n                            <i class=\"flaticon2-fast-next\"></i></a></li>\r\n                </ul>\r\n                <div class=\"kt-datatable__pager-info\">\r\n                    <div class=\"dropdown bootstrap-select kt-datatable__pager-size\" style=\"width: 60px;\">\r\n                        <select (change)=\"dataTable.setPageSize($event.target.value)\"\r\n                            class=\"selectpicker kt-datatable__pager-size\" title=\"Select page size\" data-selected=\"30\"\r\n                            data-width=\"60px\">\r\n                            <option *ngFor=\"let item of dataTable.getPaginationSelect()\"\r\n                                [selected]=\"item==dataTable.getPaginations().pageSize\" value=\"{{item}}\" title=\"{{item}}\"\r\n                                data-toggle=\"tooltip\">{{item}}</option>\r\n                        </select>\r\n                    </div><span class=\"kt-datatable__pager-detail\">{{'COMMON.SHOW' | translate}}\r\n                        {{dataTable.getPaginations().form}} -\r\n                        {{dataTable.getPaginations().to}} {{'COMMON.OF' | translate}}\r\n                        {{dataTable.getPaginations().total}}</span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <!--end: Datatable -->\r\n        <div *ngIf=\"dataTable.isLoading\" class=\"blockUI blockMsg blockElement\"\r\n            style=\"z-index: 1011; position: absolute; padding: 0px; margin: 0px; width: 169px; top: 50%; left: 45%; text-align: center; color: rgb(0, 0, 0); border: 0px; cursor: wait;\">\r\n            <div class=\"blockui \"><span>{{'COMMON.PLEASE_WAIT' | translate}}</span><span>\r\n                    <div class=\"kt-spinner kt-spinner--loader kt-spinner--brand \"></div>\r\n                </span>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<!-- end:: Content -->\r\n\r\n<!-- modal popup -->\r\n<ng-template #content let-modal>\r\n\r\n            <div class=\"modal-header \">\r\n                <h5 class=\"modal-title\" *ngIf=\"isEdit\">\r\n                    {{'ADMIN.DEVICE_ICON.GENERAL.TITLE_FORM_EDIT' | translate}}\r\n                </h5>\r\n                <h5 class=\"modal-title\" *ngIf=\"!isEdit\">\r\n                    {{'ADMIN.DEVICE_ICON.GENERAL.TITLE_FORM_ADD' | translate}}\r\n                </h5>\r\n                <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"modal.dismiss('Cross click')\">\r\n                        <span aria-hidden=\"true\">×</span>\r\n                </button>\r\n            </div>\r\n            <form [formGroup]=\"formIcontype\" (ngSubmit)='onSubmitIcontype(formIcontype)' class=\"\">\r\n                <div class=\"modal-body div-scrollbar modal-body-icontype\" *ngFor=\"let data of dataDefault\">\r\n                    <div class=\"form-group row\">\r\n                            <div class=\"col-lg-6\">\r\n                                <div class=\"show-error\">\r\n                                    <label class=\"\">{{'COMMON.COLUMN.NAME' | translate}}</label>\r\n                                    <app-control-messages [control]=\"f['nameIcontype']\"></app-control-messages>\r\n                                    <span class=\"text-danger\"> *</span>\r\n                                </div>\r\n                                <div class=\"input-group\">\r\n                                    <input [ngModel]=\"isEdit?data.name:''\" type=\"text\" class=\"form-control\"\r\n                                        placeholder=\"\" formControlName=\"nameIcontype\">\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-lg-6\">\r\n                                    <div class=\"show-error\">\r\n                                        <label class=\"\">{{'ADMIN.DEVICE_ICON.GENERAL.NAME_KEY' | translate}}</label>\r\n                                        <app-control-messages [control]=\"f['namekey']\"></app-control-messages>\r\n                                    <span class=\"text-danger\"> *</span>\r\n                                    </div>\r\n                                    <div class=\"input-group\">\r\n                                        <input [ngModel]=\"isEdit?data.nameKey:''\" type=\"text\" class=\"form-control\"\r\n                                            placeholder=\"\" formControlName=\"namekey\">\r\n                                    </div>\r\n                            </div>\r\n                    </div>\r\n                    <div class=\"form-group row\">\r\n                            <div class=\"col-lg-6\">\r\n                                <div class=\"show-error\">\r\n                                    <label class=\"\">{{'ADMIN.DEVICE_ICON.GENERAL.ICON' | translate}}</label>\r\n                                </div>\r\n                                <div class=\"show-img kt-mb-10\">\r\n                                        <img *ngIf = \"listIcon[0]['icon'] == null\" style=\"height: 70px\" name=\"img-icon\" id=\"img_icon\" src=\"http://admin.haduwaco.com/img/system/image-error.svg\" alt=\"Icon\">\r\n                                        <img *ngIf = \"listIcon[0]['icon'] != null\" style=\"height: 70px\" name=\"img-icon\" id=\"img_icon\" [src]=\"sanitize(listIcon[0]['icon'])\" alt=\"Icon\">\r\n\r\n                                </div>\r\n                                <div class=\"input-group\">\r\n                                        <div class=\"kt-uppy\" id=\"kt_uppy_5\">\r\n                                                <div class=\"kt-uppy__wrapper\">\r\n                                                    <div class=\"uppy-Root uppy-FileInput-container\">\r\n                                                    <input class=\"uppy-FileInput-input kt-uppy__input-control\" type=\"file\" (change)=\"onFileChange($event,'icon')\" id=\"kt_icon\"  >\r\n                                                    <label class=\"btn btn-brand btn-icon-sm\" for=\"kt_icon\">\r\n                                                        <i class=\"fa fa-cloud-upload-alt\"></i>\r\n                                                        <span>{{'COMMON.UPLOAD' | translate}}</span> \r\n                                                    </label>\r\n                                                </div>\r\n                                                </div>\r\n\r\n                                        </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-lg-6\">\r\n                                    <div class=\"show-error\">\r\n                                        <label class=\"\">{{'ADMIN.DEVICE_ICON.GENERAL.ICON_ON_MAP' | translate}}</label>\r\n                                    </div>\r\n                                    <div class=\"show-img kt-mb-10\">\r\n                                            <img *ngIf = \"listIcon[0]['iconMap'] == null\" style=\"height: 70px\" name=\"img-icon\" id=\"img_icon\" src=\"http://admin.haduwaco.com/img/system/image-error.svg\" alt=\"Icon\">\r\n                                            <img *ngIf = \"listIcon[0]['iconMap'] != null\" style=\"height: 70px\" name=\"img-icon\" id=\"img_icon\" [src]=\"sanitize(listIcon[0]['iconMap'])\" alt=\"Icon\">\r\n    \r\n                                    </div>\r\n                                    <div class=\"input-group\">\r\n                                            <div class=\"kt-uppy\" id=\"kt_uppy_5\">\r\n                                                    <div class=\"kt-uppy__wrapper\">\r\n                                                        <div class=\"uppy-Root uppy-FileInput-container\">\r\n                                                        <input class=\"uppy-FileInput-input kt-uppy__input-control\" type=\"file\"  id=\"kt_iconmap\" (change)=\"onFileChange($event,'iconMap')\">\r\n                                                        <label class=\"btn btn-brand btn-icon-sm\" for=\"kt_iconmap\">\r\n                                                                <i class=\"fa fa-cloud-upload-alt\"></i>\r\n                                                                <span>{{'COMMON.UPLOAD' | translate}}</span> \r\n                                                        </label>\r\n                                                        </div>\r\n                                                   </div>\r\n                                             \r\n                                            </div>\r\n                                    </div>\r\n                             </div>\r\n                    </div>\r\n                    <div class=\"form-group row\">\r\n                            <div class=\"col-lg-6\">\r\n                                    <div class=\"show-error\">\r\n                                    <label class=\"\">{{'ADMIN.DEVICE_ICON.GENERAL.ICON_SVG' | translate}}</label>\r\n                                    </div>\r\n                                    <div class=\"input-group\">\r\n                                      <textarea rows='3' class=\"form-control\" formControlName=\"iconSVG\" [ngModel]=\"isEdit?data.iconSVG:''\">\r\n                                      </textarea>\r\n                                    </div>\r\n                            </div>\r\n                            <div class=\"col-lg-6\">\r\n                                    <div class=\"show-error\">\r\n                                        <label class=\"\">{{'ADMIN.DEVICE_ICON.GENERAL.ICON_ON_MAP_SVG' | translate}}</label>\r\n                                    </div>\r\n                                    <div class=\"input-group\">\r\n                                      <!-- <textarea rows='3' class=\"form-control\" formControlName=\"iconMapSVG\" [ngModel]=\"isEdit?data.iconMapSVG:''\">\r\n                                          {{data.iconMapSVG}}\r\n                                      </textarea> -->\r\n                                      <textarea class=\"form-control\" rows=\"3\"\r\n                                      [ngModel]=\"isEdit?data.iconMapSVG:''\" formControlName=\"iconMapSVG\">\r\n                                              {{isEdit?data.iconMapSVG:''}}\r\n                                     </textarea>\r\n                                    </div>\r\n                            </div>\r\n                    </div>\r\n                    <div class=\"form-group row\" *ngIf =\"isEdit\">\r\n                            <div class=\"col-lg-6\">\r\n                                    <div class=\"show-error\">\r\n                                            <label class=\"\">{{'COMMON.COLUMN.MODIFIELD_BY' | translate}}</label>\r\n                                    </div>\r\n                                    <div class=\"input-group\">\r\n                                            <input value=\"{{data.modifiedBy}}\" type=\"text\" name=\"create_by\" class=\"form-control\" disabled>\r\n                                    </div>\r\n                            </div>\r\n                            <div class=\"col-lg-6\">\r\n                                    <div class=\"show-error\">\r\n                                            <label class=\"\">{{'COMMON.COLUMN.CREATED_BY' | translate}}</label>\r\n                                    </div>\r\n                                    <div class=\"input-group\">\r\n                                            <input value=\"{{data.createdBy}}\" type=\"text\" name=\"create_by\"  class=\"form-control\" disabled>\r\n                                    </div>\r\n                            </div>\r\n                    </div>\r\n                    <div class=\"form-group row\" *ngIf =\"isEdit\">\r\n                            <div class=\"col-lg-6\">\r\n                                    <div class=\"show-error\">\r\n                                            <label class=\"\">{{'COMMON.COLUMN.UPDATED_DATE' | translate}}</label>\r\n                                    </div>\r\n                                    <div class=\"input-group\">\r\n                                            <input name=\"sort-order\"  value=\"{{data.updatedAt}}\" type=\"text\"  class=\"form-control\" disabled>\r\n                                    </div>\r\n                            </div>\r\n                            <div class=\"col-lg-6\">\r\n                                    <div class=\"show-error\">\r\n                                            <label class=\"\">{{'COMMON.COLUMN.CREATED_DATE' | translate}}</label>\r\n                                    </div>\r\n                                    <div class=\"input-group\">\r\n                                            <input  type=\"text\" name=\"create_by\" value=\"{{data.createdAt}}\"  class=\"form-control\" disabled>\r\n                                    </div>\r\n                            </div>\r\n                    </div>\r\n                    <div class=\"form-group row\">\r\n                            <div class=\"col-lg-6\">\r\n                                    <div class=\"show-error\">\r\n                                            <label class=\"\">{{'ADMIN.DEVICE_ICON.GENERAL.COUNT' | translate}}</label>\r\n                                    </div>\r\n                                    <div class=\"input-group\">\r\n                                            <input name=\"sort-order\" maxlength=\"2\" [ngModel]=\"isEdit?data.sortOrder:'1'\"   type=\"number\" class=\"form-control\" formControlName=\"sortOrder\">\r\n                                    </div>\r\n                                   \r\n                            </div>\r\n                    </div>\r\n                </div>\r\n                <!--end body modal-->\r\n                <div class=\"modal-footer\">\r\n                    <span *ngIf=\"isPermissionError\" class=\"text-danger error-message-modal\">\r\n                        {{errorMessage}}\r\n                    </span>\r\n                    <button *ngIf=\"!isEdit\" type=\"submit\" [disabled]=\"formIcontype.invalid\" class=\"btn btn-primary\" >\r\n                        {{'COMMON.ACTIONS.ADD' | translate }}\r\n                    </button>\r\n                    <button *ngIf=\"isEdit\" type=\"submit\" [disabled]=\"formIcontype.invalid\" class=\"btn btn-primary \">\r\n                        {{'COMMON.ACTIONS.SAVE_CHANGES' | translate }}\r\n                    </button>\r\n                    <button type=\"button\" class=\"btn btn-secondary\" (click)=\"modal.close()\">{{'COMMON.ACTIONS.CANCEL' | translate}}</button>\r\n                    <!-- <button type=\"button\" class=\"btn btn-outline-danger\" data-dismiss='modal'>Close</button> -->\r\n                </div>\r\n            </form>\r\n</ng-template>\r\n<!-- end modal popup -->\r\n<!-- modal delete -->\r\n<div class=\"modal fade  modal-confirm kt-mt-50\" tabindex=\"-1\" icontype=\"dialog\" id=\"modal-delete-icontype\"\r\n    aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-sm\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\">{{'COMMON.ACTIONS.DELETE' | translate }}\r\n                    {{'ADMIN.ROLE.GENERAL.ROLE' | translate  }}</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">×</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <i class=\"fa fa-question-circle\"></i>{{'COMMON.MESSAGE.COMFIRM_DELETE' | translate }}\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n\r\n                <button type=\"button\" class=\"btn btn-primary\"\r\n                    (click)=\"deleteIcontype()\">{{'COMMON.ACTIONS.CONFIRM' | translate }}</button>\r\n                <button type=\"button\" class=\"btn btn-secondary\"\r\n                    data-dismiss=\"modal\">{{'COMMON.ACTIONS.CANCEL' | translate }}</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- end modal delete -->\r\n<!-- end:: Content -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/page-template/page-template.component.html":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/pages/admin/page-template/page-template.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<tree-root [nodes]=\"nodes\" [options]=\"options\"></tree-root>\r\n<kt-user-tree-new></kt-user-tree-new>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/permission/permission.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/pages/admin/permission/permission.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- begin:: Content -->\r\n<div class=\"kt-portlet kt-portlet--mobile kt-portlet--main\">\r\n  <div class=\"kt-portlet__head kt-portlet__head--lg\">\r\n    <div class=\"kt-portlet__head-label\">\r\n      <h3 class=\"kt-portlet__head-title\">\r\n        {{'MENU.MANAGE_PERMISSIONS' | translate | uppercase }}\r\n      </h3>\r\n    </div>\r\n    <div class=\"kt-portlet__head-toolbar\">\r\n      <div class=\"kt-portlet__head-wrapper\">\r\n        <div class=\"dropdown dropdown-inline\">\r\n          <button type=\"button\" class=\"btn btn-brand btn-icon-sm btn-pill\" (click)=\"buttonAddNew(content)\">\r\n            <i class=\"flaticon2-plus\"></i> {{'COMMON.ACTIONS.ADD_NEW' | translate}}\r\n            {{'ADMIN.PERMISSION.GENERAL.PERMISSION' | translate }}\r\n\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"kt-portlet__body kt-portlet__body--fit kt-portlet__body--overflow-y\">\r\n    <!--begin: Search Form -->\r\n    <div class=\"kt-form kt-fork--label-right kt-padding-10 kt-padding-l-20 kt-padding-r-20\">\r\n      <form class=\"\" [formGroup]=\"searchFormPermission\" (ngSubmit)='searchPermission(searchFormPermission)'>\r\n\r\n        <div class=\"row align-items-center\">\r\n          <div class=\"col-xl-8 order-2 order-xl-1\">\r\n            <div class=\"row align-items-center\">\r\n              <div class=\"col-md-4 kt-margin-b-20-tablet-and-mobile\">\r\n                <div class=\"kt-form__group kt-form__group--inline\">\r\n                  <div class=\"kt-form__label\">\r\n                    <label style=\"font-size: 15px\">{{'ADMIN.PERMISSION.GENERAL.NAME' | translate}}:</label>\r\n                  </div>\r\n                  <div class=\"kt-form__control\">\r\n                    <input type=\"text\" class=\"form-control\"\r\n                      placeholder=\"{{'ADMIN.PERMISSION.GENERAL.PERMISSION_NAME' | translate}} ...\"\r\n                      formControlName=\"name\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-4 kt-margin-b-20-tablet-and-mobile\">\r\n                <div class=\"kt-form__group kt-form__group--inline\">\r\n                  <div class=\"kt-form__label\">\r\n                    <label style=\"font-size: 15px\">{{'ADMIN.PERMISSION.GENERAL.GROUP' | translate}}:</label>\r\n                  </div>\r\n                  <div class=\"kt-form__control\">\r\n                    <select class=\"form-control bootstrap-select\" id=\"kt_form_type\" formControlName=\"namegroup\">\r\n                      <option value=\"\">\r\n                          {{'COMMON.MESSAGE.NOTHING_SELECTED' | translate}}\r\n                      </option>\r\n                      <option *ngFor='let namegroup of listNameGroup' value=\"{{namegroup}}\">\r\n                        {{namegroup}}\r\n                      </option>\r\n                    </select>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-md-4 kt-margin-b-20-tablet-and-mobile\">\r\n                <button type=\"submit\" class=\"btn btn-secondary btn-hover-brand\">\r\n                  <i class=\"icon-search\"></i>{{'COMMON.SEARCH' | translate}}</button>\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n      </form>\r\n    </div>\r\n\r\n    <!--end: Search Form -->\r\n    <!--begin: Datatable -->\r\n    <div resizeObserver (resize)=\"onResize($event.contentRect)\" class=\"kt_datatable kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded\" [ngClass]=\"{'kt-datatable--error':dataTable.isNotFound}\"\r\n      id=\"api_methods\">\r\n      <table class=\"kt-datatable__table\">\r\n        <thead class=\"kt-datatable__head\">\r\n            <tr class=\"kt-datatable__row\" style=\"left: 0px;\">\r\n                <!-- checkbox -->\r\n                <th *ngIf=\"dataTable.selecter\"\r\n                    class=\"kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check\">\r\n                    <span style=\"width: 20px;\">\r\n                        <label class=\"kt-checkbox kt-checkbox--single kt-checkbox--solid\">\r\n                            <input type=\"checkbox\" [(ngModel)]=\"dataTable.isCheckBoxAll\"\r\n                                (click)=\"dataTable.setCheckBoxAll()\">&nbsp;<span></span>\r\n                        </label>\r\n                    </span>\r\n                </th>\r\n                <!-- end checkbox -->\r\n                <th *ngIf=\"dataTable.isShowSubRow\" style=\"width: 13px;\"\r\n                    class=\"kt-datatable__cell kt-datatable__cell--sort\">\r\n                    <a class=\"kt-datatable__toggle-detail\">\r\n                    </a>\r\n                </th>\r\n\r\n                <ng-container *ngFor=\"let column of dataTable.getColumns()\">\r\n\r\n                    <th [ngClass]=\"{'kt-datatable__cell--sorted':column.isSort}\"\r\n                        [ngStyle]=\"{'display':(!column.isShow?'none':'')}\" [attr.data-field]=\"column.field\"\r\n                        class=\"kt-datatable__cell kt-datatable__cell--sort {{column.class}}\"\r\n                        (click)=\"dataTable.sort(column.field)\">\r\n                        <span [ngStyle]=\"column.style\">{{column.translate | translate}}\r\n                            <i *ngIf=\"column.isSort\"\r\n                                [ngClass]=\"{'flaticon2-arrow-down':(column.isSort && column.sortable =='desc'),'flaticon2-arrow-up':(column.isSort && column.sortable =='asc')}\"></i>\r\n                        </span>\r\n                    </th>\r\n                </ng-container>\r\n            </tr>\r\n        </thead>\r\n        <tbody class=\"kt-datatable__body\" [ngStyle]=\"dataTable.bodyStyle\">\r\n            <ng-container *ngFor=\"let item of dataTable.data;let i=index\">\r\n                <tr [ngClass]=\"{'kt-datatable__row--active':item.dt_selecter}\" class=\"kt-datatable__row\"\r\n                    style=\"left: 0px;\">\r\n                    <!-- checkbox -->\r\n                    <td *ngIf=\"dataTable.selecter\"\r\n                        class=\"kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check\"\r\n                        data-field=\"RecordID\"><span style=\"width: 20px;\">\r\n                            <label class=\"kt-checkbox kt-checkbox--single kt-checkbox--solid\">\r\n                                <input type=\"checkbox\" [(ngModel)]=\"item.dt_selecter\"\r\n                                    (click)=\"dataTable.setCheckBox(i)\">&nbsp;<span></span>\r\n                            </label>\r\n                        </span>\r\n                    </td>\r\n                    <!-- end checkbox -->\r\n                    <td *ngIf=\"dataTable.isShowSubRow\" style=\"width: 13px;\"\r\n                        class=\"kt-datatable__cell kt-datatable__toggle-detail\">\r\n                        <a (click)=\"dataTable.showSubRow(i)\" class=\"kt-datatable__toggle-detail\">\r\n                            <i [ngClass]=\"{'icon-caret-down' : (dataTable.lastRowShow == i), \r\n                            'icon-caret-right' : (dataTable.lastRowShow != i)}\"></i>\r\n                        </a>\r\n                    </td>\r\n                    <td [ngStyle]=\"{'display':(!dataTable.getColumns()[0].isShow?'none':'')}\"\r\n                        class=\"kt-datatable__cell kt-datatable__cell\" data-field=\"id\">\r\n                        <span\r\n                            [ngStyle]=\"dataTable.getColumns()[0].style\">{{i+dataTable.getPaginations().form}}</span>\r\n                    </td>\r\n                    <td [ngStyle]=\"{'display':(!dataTable.getColumns()[1].isShow?'none':'')}\"\r\n                        data-field=\"key_name\" class=\"kt-datatable__cell\">\r\n                        <span [ngStyle]=\"dataTable.getColumns()[1].style\">{{item.name}}</span>\r\n                    </td>\r\n                    <td [ngStyle]=\"{'display':(!dataTable.getColumns()[2].isShow?'none':'')}\"\r\n                        data-field=\"name\" class=\"kt-datatable__cell\">\r\n                        <span [ngStyle]=\"dataTable.getColumns()[2].style\"\r\n                            translate=\"{{item.nameKey}}\">{{item.description}}</span>\r\n                    </td>\r\n                    <td [ngStyle]=\"{'display':(!dataTable.getColumns()[3].isShow?'none':'')}\"\r\n                        data-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n                        <span [ngStyle]=\"dataTable.getColumns()[3].style\">{{item.groupName}}</span>\r\n                    </td>\r\n                    <td [ngStyle]=\"{'display':(!dataTable.getColumns()[4].isShow?'none':'')}\"\r\n                        data-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n                        <span\r\n                            [ngStyle]=\"dataTable.getColumns()[4].style\">{{item.createdAt | userDate}}</span>\r\n                    </td>\r\n                    <td [ngStyle]=\"{'display':(!dataTable.getColumns()[5].isShow?'none':'')}\"\r\n                        data-field=\"Actions\" data-autohide-disabled=\"false\" class=\"kt-datatable__cell\">\r\n                        <span [ngStyle]=\"dataTable.getColumns()[5].style\"\r\n                            style=\"overflow: visible; position: relative;\">\r\n                            <a ngbTooltip=\"{{'COMMON.ACTIONS.EDIT' | translate }}\"\r\n                                class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                (click)=\"editPermission(item.id,content)\">\r\n                                <i class=\"icon-edit\"></i>\r\n                            </a>\r\n                            <a ngbTooltip=\"{{'COMMON.ACTIONS.DELETE' | translate }}\"\r\n                                class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                (click)=\"getIdAction(item.id,'#modal-delete-permission')\">\r\n                                <i class=\"icon-trash\"></i>\r\n                            </a>\r\n                        </span>\r\n                    </td>\r\n                </tr>\r\n                <!-- Sub row -->\r\n                <tr *ngIf=\"dataTable.lastRowShow==i && dataTable.isShowSubRow\"\r\n                    class=\"kt-datatable__row-detail\">\r\n                    <td class=\"kt-datatable__detail\" colspan=\"9\">\r\n                        <table>\r\n                            <tr [ngStyle]=\"{'display':(dataTable.getColumns()[0].isShow?'none':'')}\"\r\n                                class=\"kt-datatable__row\">\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span>{{dataTable.getColumns()[0].translate | translate}}</span>\r\n                                </td>\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span\r\n                                        [ngStyle]=\"dataTable.getColumns()[0].style\">{{i+dataTable.getPaginations().form}}</span>\r\n                                </td>\r\n                            </tr>\r\n                            <tr [ngStyle]=\"{'display':(dataTable.getColumns()[1].isShow?'none':'')}\"\r\n                                class=\"kt-datatable__row\">\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span>{{dataTable.getColumns()[1].translate | translate}}</span>\r\n                                </td>\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span [ngStyle]=\"dataTable.getColumns()[1].style\">{{item.name}}</span>\r\n                                </td>\r\n                            </tr>\r\n                            <tr [ngStyle]=\"{'display':(dataTable.getColumns()[2].isShow?'none':'')}\"\r\n                                class=\"kt-datatable__row\">\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span>{{dataTable.getColumns()[2].translate | translate}}</span>\r\n                                </td>\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span [ngStyle]=\"dataTable.getColumns()[2].style\"\r\n                                        translate=\"{{item.nameKey}}\">{{item.description}}</span>\r\n                                </td>\r\n                            </tr>\r\n                            <tr [ngStyle]=\"{'display':(dataTable.getColumns()[3].isShow?'none':'')}\"\r\n                                class=\"kt-datatable__row\">\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span>{{dataTable.getColumns()[3].translate | translate}}</span>\r\n                                </td>\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span [ngStyle]=\"dataTable.getColumns()[3].style\"\r\n                                        translate=\"{{item.nameKey}}\">{{item.groupName}}</span>\r\n                                </td>\r\n                            </tr>\r\n                            <tr [ngStyle]=\"{'display':(dataTable.getColumns()[4].isShow?'none':'')}\"\r\n                                class=\"kt-datatable__row\">\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span>{{dataTable.getColumns()[4].translate | translate}}</span>\r\n                                </td>\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span [ngStyle]=\"dataTable.getColumns()[4].style\"\r\n                                        translate=\"{{item.nameKey}}\">{{item.createdAt | userDate}}</span>\r\n                                </td>\r\n                            </tr>\r\n\r\n\r\n                            <tr [ngStyle]=\"{'display':(dataTable.getColumns()[5].isShow?'none':'')}\"\r\n                                class=\"kt-datatable__row\">\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span>{{dataTable.getColumns()[5].translate | translate}}</span>\r\n                                </td>\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span [ngStyle]=\"dataTable.getColumns()[5].style\"\r\n                                        style=\"overflow: visible; position: relative;\">\r\n                                        <a ngbTooltip=\"{{'COMMON.ACTIONS.EDIT' | translate }}\"\r\n                                            class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                            (click)=\"editPermission(item.id,content)\">\r\n                                            <i class=\"icon-edit\"></i>\r\n                                        </a>\r\n                                        <a ngbTooltip=\"{{'COMMON.ACTIONS.DELETE' | translate }}\"\r\n                                            class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                            (click)=\"getIdAction(item.id,'#modal-delete-permission')\"\r\n                                            >\r\n                                            <i class=\"icon-trash\"></i>\r\n                                        </a>\r\n                                    </span>\r\n                                </td>\r\n                            </tr>\r\n                        </table>\r\n                    </td>\r\n                </tr>\r\n            </ng-container>\r\n            <!-- No records found -->\r\n            <span *ngIf=\"dataTable.isNotFound\" class=\"kt-datatable--error\"\r\n                [innerHTML]=\"'COMMON.MESSAGE.NO_RECORDS_FOUND' | translate\">No records found</span>\r\n        </tbody>\r\n        \r\n\r\n      </table>\r\n\r\n      <div class=\"kt-datatable__pager kt-datatable--paging-loaded\" *ngIf=\"dataTable.data?.length != 0\">\r\n        <ul class=\"kt-datatable__pager-nav\">\r\n          <li><a title=\"First\" class=\"kt-datatable__pager-link kt-datatable__pager-link--first\"\r\n              [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getFirstPage())}\"\r\n              (click)=\"dataTable.gotoPage(dataTable.getFirstPage())\">\r\n              <i class=\"flaticon2-fast-back\"></i></a></li>\r\n          <li><a title=\"Previous\" class=\"kt-datatable__pager-link kt-datatable__pager-link--prev\"\r\n              [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getFirstPage())}\"\r\n              (click)=\"dataTable.gotoPage(dataTable.getCurrentPage()-1)\">\r\n              <i class=\"flaticon2-back\"></i></a></li>\r\n          <li></li>\r\n          <li style=\"display: none;\"><input type=\"text\" class=\"kt-pager-input form-control\" title=\"Page number\"></li>\r\n          <li *ngFor=\"let page of dataTable.getPageDisplay()\">\r\n            <a (click)=\"dataTable.gotoPage(page)\" class=\"kt-datatable__pager-link kt-datatable__pager-link-number \"\r\n              [ngClass]=\"{'kt-datatable__pager-link--active':page==dataTable.getCurrentPage()}\" data-page=\"1\"\r\n              title=\"{{page}}\">{{page}}</a>\r\n          </li>\r\n          <li style=\"\"></li>\r\n          <li><a title=\"Next\" class=\"kt-datatable__pager-link kt-datatable__pager-link--next \"\r\n              [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getLastPage())}\"\r\n              (click)=\"dataTable.gotoPage(dataTable.getCurrentPage()+1)\"><i class=\" flaticon2-next\"></i></a></li>\r\n          <li><a title=\"Last\"\r\n              [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getLastPage())}\"\r\n              class=\"kt-datatable__pager-link kt-datatable__pager-link--last\"\r\n              (click)=\"dataTable.gotoPage(dataTable.getLastPage())\">\r\n              <i class=\"flaticon2-fast-next\"></i></a></li>\r\n        </ul>\r\n        <div class=\"kt-datatable__pager-info\">\r\n          <div class=\"dropdown bootstrap-select kt-datatable__pager-size\" style=\"width: 60px;\">\r\n              <select (change)=\"dataTable.setPageSize($event.target.value)\"\r\n\t\t\t\t\t\t\tclass=\"selectpicker kt-datatable__pager-size\" title=\"Select page size\" data-selected=\"30\"\r\n\t\t\t\t\t\t\tdata-width=\"60px\">\r\n\t\t\t\t\t\t\t<option *ngFor=\"let item of dataTable.getPaginationSelect()\"\r\n\t\t\t\t\t\t\t\t[selected]=\"item==dataTable.getPaginations().pageSize\" value=\"{{item}}\" title=\"{{item}}\"\r\n\t\t\t\t\t\t\t\tdata-toggle=\"tooltip\">{{item}}</option>\r\n            </select>\r\n          </div><span class=\"kt-datatable__pager-detail\">{{'COMMON.SHOW' | translate}}\r\n            {{dataTable.getPaginations().form}} -\r\n            {{dataTable.getPaginations().to}} {{'COMMON.OF' | translate}} {{dataTable.getPaginations().total}}</span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <!--end: Datatable -->\r\n    <div *ngIf=\"dataTable.isLoading\" class=\"blockUI blockMsg blockElement\"\r\n      style=\"z-index: 1011; position: absolute; padding: 0px; margin: 0px; width: 169px; top: 50%; left: 45%; text-align: center; color: rgb(0, 0, 0); border: 0px; cursor: wait;\">\r\n\t\t\t<div class=\"blockui \"><span>{{'COMMON.PLEASE_WAIT' | translate}}</span><span>\r\n          <div class=\"kt-spinner kt-spinner--loader kt-spinner--brand \"></div>\r\n        </span>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!-- end:: Content -->\r\n\r\n<!-- modal popup -->\r\n<ng-template #content let-modal>\r\n      <div class=\"modal-header \">\r\n        <h5 class=\"modal-title\" *ngIf=\"isEdit\">\r\n          {{'ADMIN.PERMISSION.GENERAL.TITLE_FORM_EDIT' | translate}}\r\n        </h5>\r\n        <h5 class=\"modal-title\" *ngIf=\"!isEdit\">\r\n          {{'ADMIN.PERMISSION.GENERAL.TITLE_FORM_ADD' | translate}}\r\n        </h5>\r\n        <button type=\"button\" class=\"close\" (click)=\"modal.dismiss('Cross click')\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">×</span>\r\n        </button>\r\n      </div>\r\n      <form [formGroup]=\"formPermission\" (ngSubmit)='onSubmitPermission(formPermission)' class=\"\">\r\n        <div class=\"modal-body\" *ngFor=\"let data of dataDefault\">\r\n          <div class=\"form-group row\">\r\n            <div class=\"col-lg-12\">\r\n              <div class=\"show-error\">\r\n                <label class=\"\">{{'ADMIN.PERMISSION.GENERAL.NAME' | translate}} </label>\r\n                <app-control-messages [control]=\"f['name']\"></app-control-messages>\r\n                <span class=\"text-danger\"> *</span>\r\n              </div>\r\n              <div class=\"input-group\">\r\n              \r\n                <input [ngModel]=\"isEdit?data.name:''\" type=\"text\" class=\"form-control\" placeholder=\"\"\r\n                  formControlName=\"name\">\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <div class=\"col-lg-12\">\r\n              <div class=\"show-error\">\r\n                <label class=\"\">{{'ADMIN.PERMISSION.GENERAL.GROUP' | translate}}</label>\r\n              </div>\r\n              <div class=\"input-group autocomplete-form\">\r\n               \r\n                <div class=\"ng-autocomplete ng-autocomplete-form ng-autocomplete-form-permission\">\r\n                  <ng-autocomplete [data]=\"listNameGroupObj\" [searchKeyword]=\"keyword\" (selected)='selectEvent($event)'\r\n                    (inputChanged)='onChangeSearch($event)' [itemTemplate]=\"itemTemplate\" [debounceTime]=500\r\n                    [initialValue]=\"isEdit?nameGroupEdit:''\" [notFoundTemplate]=\"notFoundTemplate\">\r\n                  </ng-autocomplete>\r\n                  <ng-template #itemTemplate let-item>\r\n                    <span class=\"font-weight-bold\" [innerHTML]=\"item.name\"></span>\r\n                  </ng-template>\r\n\r\n                  <ng-template #notFoundTemplate let-notFound>\r\n                    <div>{{'COMMON.MESSAGE.NO_RECORDS_FOUND' | translate}}</div>\r\n                  </ng-template>\r\n                </div>\r\n                <!-- <input  [ngModel]=\"isEdit?data.groupName:''\" type=\"text\" class=\"form-control\" placeholder=\"\" formControlName=\"namegroup\"> -->\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <div class=\"col-lg-12\">\r\n              <div class=\"show-error\">\r\n                <label class=\"\">{{'ADMIN.PERMISSION.GENERAL.DESCRIPTION' | translate}} </label>\r\n                <!-- <app-control-messages [control]=\"f['username']\"></app-control-messages> -->\r\n              </div>\r\n              <div class=\"input-group\">\r\n                <textarea class=\"form-control\" rows=\"5\" [ngModel]=\"isEdit?data.description:''\"\r\n                  formControlName=\"description\">\r\n                            {{isEdit?data.description:''}}\r\n                   </textarea>\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!--end body modal-->\r\n        <div class=\"modal-footer\">\r\n          <span *ngIf=\"isPermissionError\" class=\"text-danger error-message-modal\">\r\n            {{errorMessage}}\r\n          </span>\r\n          <button *ngIf=\"!isEdit\" type=\"submit\" [disabled]=\"formPermission.invalid\" class=\"btn btn-primary \">\r\n            {{'COMMON.ACTIONS.ADD' | translate }}\r\n          </button>\r\n          <button *ngIf=\"isEdit\" type=\"submit\" [disabled]=\"formPermission.invalid\" class=\"btn btn-primary \">\r\n            {{'COMMON.ACTIONS.SAVE_CHANGES' | translate }}\r\n          </button>\r\n\r\n          <button type=\"button\" class=\"btn btn-secondary\"\r\n          (click)=\"modal.close()\">{{'COMMON.ACTIONS.CANCEL' | translate}}</button>\r\n          <!-- <button type=\"button\" class=\"btn btn-outline-danger\" data-dismiss='modal'>Close</button> -->\r\n        </div>\r\n      </form>\r\n</ng-template>\r\n<!-- end modal popup -->\r\n<!-- modal delete -->\r\n<div class=\"modal fade  modal-confirm\" tabindex=\"-1\" role=\"dialog\" id=\"modal-delete-permission\"\r\n  aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog modal-sm\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\">{{'COMMON.ACTIONS.DELETE' | translate }}\r\n          {{'ADMIN.PERMISSION.GENERAL.PERMISSION' | translate  }}</h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">×</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <i class=\"fa fa-question-circle\"></i>{{'COMMON.MESSAGE.COMFIRM_DELETE' | translate }}\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n\r\n        <button type=\"button\" class=\"btn btn-primary\"\r\n          (click)=\"deletePermission()\">{{'COMMON.ACTIONS.CONFIRM' | translate }}</button>\r\n        <button type=\"button\" class=\"btn btn-secondary\"\r\n          data-dismiss=\"modal\">{{'COMMON.ACTIONS.CANCEL' | translate }}</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- end modal delete -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/role/role.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/pages/admin/role/role.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- begin:: Content -->\r\n<div class=\"kt-portlet kt-portlet--mobile kt-portlet--main\">\r\n  <div class=\"kt-portlet__head kt-portlet__head--lg\">\r\n    <div class=\"kt-portlet__head-label\">\r\n  \r\n      <h3 class=\"kt-portlet__head-title\">\r\n        {{'MENU.MANAGE_ROLES' | translate | uppercase }}\r\n      </h3>\r\n    </div>\r\n    <div class=\"kt-portlet__head-toolbar\">\r\n      <div class=\"kt-portlet__head-wrapper\">\r\n        <div class=\"dropdown dropdown-inline\">\r\n          <button type=\"button\" class=\"btn btn-brand btn-icon-sm btn-pill\" (click)=\"buttonAddNew(content)\">\r\n            <i class=\"flaticon2-plus\"></i> {{'COMMON.ACTIONS.ADD_NEW' | translate}}\r\n            {{'ADMIN.ROLE.GENERAL.ROLE' | translate }}\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- <div class=\"kt-portlet__body kt-padding-t-10 kt-padding-b-10\" disabled=\"true\">\r\n\r\n  </div> -->\r\n  <div class=\"kt-portlet__body kt-portlet__body--fit kt-portlet__body--overflow-y\">\r\n    <!--begin: Search Form -->\r\n    <div class=\"kt-form kt-fork--label-right kt-padding-10 kt-padding-l-20 kt-padding-r-20\">\r\n        <form class=\"\" [formGroup]=\"searchFormRole\" (ngSubmit)='searchRole(searchFormRole)'>\r\n  \r\n          <div class=\"row align-items-center\">\r\n            <div class=\"col-xl-8 order-2 order-xl-1\">\r\n              <div class=\"row align-items-center\">\r\n                <div class=\"col-md-4 kt-margin-b-20-tablet-and-mobile\">\r\n                  <div class=\"kt-form__group kt-form__group--inline\">\r\n                    <div class=\"kt-form__label\">\r\n                      <label style=\"font-size: 15px\">{{'ADMIN.ROLE.GENERAL.NAME' | translate}}:</label>\r\n                    </div>\r\n                    <div class=\"kt-form__control\">\r\n                      <input type=\"text\" class=\"form-control\"\r\n                        placeholder=\"{{'ADMIN.ROLE.GENERAL.ROLE_NAME' | translate}} ...\" formControlName=\"name\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                \r\n                <div class=\"col-md-4 kt-margin-b-20-tablet-and-mobile\">\r\n                  <button type=\"submit\" class=\"btn btn-secondary btn-hover-brand\">\r\n                    <i class=\"icon-search\"></i>{{'COMMON.SEARCH' | translate}}</button>\r\n                </div>\r\n  \r\n              </div>\r\n            </div>\r\n  \r\n          </div>\r\n        </form>\r\n      </div>\r\n  \r\n      <!--end: Search Form -->\r\n    <!--begin: Datatable -->\r\n    <div resizeObserver (resize)=\"onResize($event.contentRect)\" class=\"kt_datatable kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded \" [ngClass]=\"{'kt-datatable--error':dataTable.isNotFound}\"\r\n      id=\"api_methods\">\r\n      <table class=\"kt-datatable__table\">\r\n        <thead class=\"kt-datatable__head\">\r\n            <tr class=\"kt-datatable__row\" style=\"left: 0px;\">\r\n                <!-- checkbox -->\r\n                <th *ngIf=\"dataTable.selecter\"\r\n                    class=\"kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check\">\r\n                    <span style=\"width: 20px;\">\r\n                        <label class=\"kt-checkbox kt-checkbox--single kt-checkbox--solid\">\r\n                            <input type=\"checkbox\" [(ngModel)]=\"dataTable.isCheckBoxAll\"\r\n                                (click)=\"dataTable.setCheckBoxAll()\">&nbsp;<span></span>\r\n                        </label>\r\n                    </span>\r\n                </th>\r\n                <!-- end checkbox -->\r\n                <th *ngIf=\"dataTable.isShowSubRow\" style=\"width: 13px;\"\r\n                    class=\"kt-datatable__cell kt-datatable__cell--sort\">\r\n                    <a class=\"kt-datatable__toggle-detail\">\r\n                    </a>\r\n                </th>\r\n\r\n                <ng-container *ngFor=\"let column of dataTable.getColumns()\">\r\n\r\n                    <th [ngClass]=\"{'kt-datatable__cell--sorted':column.isSort}\"\r\n                        [ngStyle]=\"{'display':(!column.isShow?'none':'')}\" [attr.data-field]=\"column.field\"\r\n                        class=\"kt-datatable__cell kt-datatable__cell--sort {{column.class}}\"\r\n                        (click)=\"dataTable.sort(column.field)\">\r\n                        <span [ngStyle]=\"column.style\">{{column.translate | translate}}\r\n                            <i *ngIf=\"column.isSort\"\r\n                                [ngClass]=\"{'flaticon2-arrow-down':(column.isSort && column.sortable =='desc'),'flaticon2-arrow-up':(column.isSort && column.sortable =='asc')}\"></i>\r\n                        </span>\r\n                    </th>\r\n                </ng-container>\r\n            </tr>\r\n        </thead>\r\n        <tbody class=\"kt-datatable__body\" [ngStyle]=\"dataTable.bodyStyle\">\r\n            <ng-container *ngFor=\"let item of dataTable.data;let i=index\">\r\n                <tr [ngClass]=\"{'kt-datatable__row--active':item.dt_selecter}\" class=\"kt-datatable__row\"\r\n                    style=\"left: 0px;\">\r\n                    <!-- checkbox -->\r\n                    <td *ngIf=\"dataTable.selecter\"\r\n                        class=\"kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check\"\r\n                        data-field=\"RecordID\"><span style=\"width: 20px;\">\r\n                            <label class=\"kt-checkbox kt-checkbox--single kt-checkbox--solid\">\r\n                                <input type=\"checkbox\" [(ngModel)]=\"item.dt_selecter\"\r\n                                    (click)=\"dataTable.setCheckBox(i)\">&nbsp;<span></span>\r\n                            </label>\r\n                        </span>\r\n                    </td>\r\n                    <!-- end checkbox -->\r\n                    <td *ngIf=\"dataTable.isShowSubRow\" style=\"width: 13px;\"\r\n                        class=\"kt-datatable__cell kt-datatable__toggle-detail\">\r\n                        <a (click)=\"dataTable.showSubRow(i)\" class=\"kt-datatable__toggle-detail\">\r\n                            <i [ngClass]=\"{'icon-caret-down' : (dataTable.lastRowShow == i), \r\n                            'icon-caret-right' : (dataTable.lastRowShow != i)}\"></i>\r\n                        </a>\r\n                    </td>\r\n                    <td [ngStyle]=\"{'display':(!dataTable.getColumns()[0].isShow?'none':'')}\"\r\n                        class=\"kt-datatable__cell kt-datatable__cell\" data-field=\"id\">\r\n                        <span\r\n                            [ngStyle]=\"dataTable.getColumns()[0].style\">{{i+dataTable.getPaginations().form}}</span>\r\n                    </td>\r\n                    <td [ngStyle]=\"{'display':(!dataTable.getColumns()[1].isShow?'none':'')}\"\r\n                        data-field=\"key_name\" class=\"kt-datatable__cell\">\r\n                        <span [ngStyle]=\"dataTable.getColumns()[1].style\">{{item.name}}</span>\r\n                    </td>\r\n                    <td [ngStyle]=\"{'display':(!dataTable.getColumns()[2].isShow?'none':'')}\"\r\n                        data-field=\"name\" class=\"kt-datatable__cell\">\r\n                        <span [ngStyle]=\"dataTable.getColumns()[2].style\"\r\n                            translate=\"{{item.nameKey}}\">{{item.parentName}}</span>\r\n                    </td>\r\n                    <td [ngStyle]=\"{'display':(!dataTable.getColumns()[3].isShow?'none':'')}\"\r\n                        data-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n                        <span [ngStyle]=\"dataTable.getColumns()[3].style\">{{item.description}}</span>\r\n                    </td>\r\n                    <td [ngStyle]=\"{'display':(!dataTable.getColumns()[4].isShow?'none':'')}\"\r\n                        data-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n                        <span\r\n                            [ngStyle]=\"dataTable.getColumns()[4].style\">{{item.createdAt | userDate}}</span>\r\n                    </td>\r\n                    <td [ngStyle]=\"{'display':(!dataTable.getColumns()[5].isShow?'none':'')}\"\r\n                        data-field=\"Actions\" data-autohide-disabled=\"false\" class=\"kt-datatable__cell\">\r\n                        <span [ngStyle]=\"dataTable.getColumns()[5].style\"\r\n                            style=\"overflow: visible; position: relative;\">\r\n                            <a ngbTooltip=\"{{'COMMON.ACTIONS.EDIT' | translate }}\"\r\n                                class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                (click)=\"editRole(item.id,content)\">\r\n                                <i class=\"icon-edit\"></i>\r\n                            </a>\r\n                            <a ngbTooltip=\"{{'COMMON.ACTIONS.DELETE' | translate }}\"\r\n                                class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                (click)=\"getIdAction(item.id,'#modal-delete-role')\">\r\n                                <i class=\"icon-trash\"></i>\r\n                            </a>\r\n                        </span>\r\n                    </td>\r\n                </tr>\r\n                <!-- Sub row -->\r\n                <tr *ngIf=\"dataTable.lastRowShow==i && dataTable.isShowSubRow\"\r\n                    class=\"kt-datatable__row-detail\">\r\n                    <td class=\"kt-datatable__detail\" colspan=\"9\">\r\n                        <table>\r\n                            <tr [ngStyle]=\"{'display':(dataTable.getColumns()[0].isShow?'none':'')}\"\r\n                                class=\"kt-datatable__row\">\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span>{{dataTable.getColumns()[0].translate | translate}}</span>\r\n                                </td>\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span\r\n                                        [ngStyle]=\"dataTable.getColumns()[0].style\">{{i+dataTable.getPaginations().form}}</span>\r\n                                </td>\r\n                            </tr>\r\n                            <tr [ngStyle]=\"{'display':(dataTable.getColumns()[1].isShow?'none':'')}\"\r\n                                class=\"kt-datatable__row\">\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span>{{dataTable.getColumns()[1].translate | translate}}</span>\r\n                                </td>\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span [ngStyle]=\"dataTable.getColumns()[1].style\">{{item.name}}</span>\r\n                                </td>\r\n                            </tr>\r\n                            <tr [ngStyle]=\"{'display':(dataTable.getColumns()[2].isShow?'none':'')}\"\r\n                                class=\"kt-datatable__row\">\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span>{{dataTable.getColumns()[2].translate | translate}}</span>\r\n                                </td>\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span [ngStyle]=\"dataTable.getColumns()[2].style\"\r\n                                        translate=\"{{item.nameKey}}\">{{item.parentName}}</span>\r\n                                </td>\r\n                            </tr>\r\n                            <tr [ngStyle]=\"{'display':(dataTable.getColumns()[3].isShow?'none':'')}\"\r\n                                class=\"kt-datatable__row\">\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span>{{dataTable.getColumns()[3].translate | translate}}</span>\r\n                                </td>\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span [ngStyle]=\"dataTable.getColumns()[3].style\"\r\n                                        translate=\"{{item.nameKey}}\">{{item.description}}</span>\r\n                                </td>\r\n                            </tr>\r\n                            <tr [ngStyle]=\"{'display':(dataTable.getColumns()[4].isShow?'none':'')}\"\r\n                                class=\"kt-datatable__row\">\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span>{{dataTable.getColumns()[4].translate | translate}}</span>\r\n                                </td>\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span [ngStyle]=\"dataTable.getColumns()[4].style\"\r\n                                        translate=\"{{item.nameKey}}\">{{item.createdAt}}</span>\r\n                                </td>\r\n                            </tr>\r\n\r\n\r\n                            <tr [ngStyle]=\"{'display':(dataTable.getColumns()[5].isShow?'none':'')}\"\r\n                                class=\"kt-datatable__row\">\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span>{{dataTable.getColumns()[5].translate | translate}}</span>\r\n                                </td>\r\n                                <td class=\"kt-datatable__cell\">\r\n                                    <span [ngStyle]=\"dataTable.getColumns()[5].style\"\r\n                                        style=\"overflow: visible; position: relative;\">\r\n                                        <a ngbTooltip=\"{{'COMMON.ACTIONS.EDIT' | translate }}\"\r\n                                            class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                            (click)=\"editRole(item.id,content)\">\r\n                                            <i class=\"icon-edit\"></i>\r\n                                        </a>\r\n                                        <a ngbTooltip=\"{{'COMMON.ACTIONS.DELETE' | translate }}\"\r\n                                            class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                            (click)=\"getIdAction(item.id,'#modal-delete-role')\"\r\n                                            >\r\n                                            <i class=\"icon-trash\"></i>\r\n                                        </a>\r\n                                    </span>\r\n                                </td>\r\n                            </tr>\r\n                        </table>\r\n                    </td>\r\n                </tr>\r\n            </ng-container>\r\n            <!-- No records found -->\r\n            <span *ngIf=\"dataTable.isNotFound\" class=\"kt-datatable--error\"\r\n                [innerHTML]=\"'COMMON.MESSAGE.NO_RECORDS_FOUND' | translate\">No records found</span>\r\n        </tbody>\r\n        \r\n\r\n      </table>\r\n\r\n      <div class=\"kt-datatable__pager kt-datatable--paging-loaded\" *ngIf=\"dataTable.data?.length != 0\">\r\n        <ul class=\"kt-datatable__pager-nav\">\r\n          <li><a title=\"First\" class=\"kt-datatable__pager-link kt-datatable__pager-link--first\"\r\n              [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getFirstPage())}\"\r\n              (click)=\"dataTable.gotoPage(dataTable.getFirstPage())\">\r\n              <i class=\"flaticon2-fast-back\"></i></a></li>\r\n          <li><a title=\"Previous\" class=\"kt-datatable__pager-link kt-datatable__pager-link--prev\"\r\n              [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getFirstPage())}\"\r\n              (click)=\"dataTable.gotoPage(dataTable.getCurrentPage()-1)\">\r\n              <i class=\"flaticon2-back\"></i></a></li>\r\n          <li></li>\r\n          <li style=\"display: none;\"><input type=\"text\" class=\"kt-pager-input form-control\" title=\"Page number\"></li>\r\n          <li *ngFor=\"let page of dataTable.getPageDisplay()\">\r\n            <a (click)=\"dataTable.gotoPage(page)\" class=\"kt-datatable__pager-link kt-datatable__pager-link-number \"\r\n              [ngClass]=\"{'kt-datatable__pager-link--active':page==dataTable.getCurrentPage()}\" data-page=\"1\"\r\n              title=\"{{page}}\">{{page}}</a>\r\n          </li>\r\n          <li style=\"\"></li>\r\n          <li><a title=\"Next\" class=\"kt-datatable__pager-link kt-datatable__pager-link--next \"\r\n              [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getLastPage())}\"\r\n              (click)=\"dataTable.gotoPage(dataTable.getCurrentPage()+1)\"><i class=\" flaticon2-next\"></i></a></li>\r\n          <li><a title=\"Last\"\r\n              [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getLastPage())}\"\r\n              class=\"kt-datatable__pager-link kt-datatable__pager-link--last\"\r\n              (click)=\"dataTable.gotoPage(dataTable.getLastPage())\">\r\n              <i class=\"flaticon2-fast-next\"></i></a></li>\r\n        </ul>\r\n        <div class=\"kt-datatable__pager-info\">\r\n          <div class=\"dropdown bootstrap-select kt-datatable__pager-size\" style=\"width: 60px;\">\r\n              <select (change)=\"dataTable.setPageSize($event.target.value)\"\r\n\t\t\t\t\t\t\tclass=\"selectpicker kt-datatable__pager-size\" title=\"Select page size\" data-selected=\"30\"\r\n\t\t\t\t\t\t\tdata-width=\"60px\">\r\n\t\t\t\t\t\t\t<option *ngFor=\"let item of dataTable.getPaginationSelect()\"\r\n\t\t\t\t\t\t\t\t[selected]=\"item==dataTable.getPaginations().pageSize\" value=\"{{item}}\" title=\"{{item}}\"\r\n\t\t\t\t\t\t\t\tdata-toggle=\"tooltip\">{{item}}</option>\r\n            </select>\r\n          </div><span class=\"kt-datatable__pager-detail\">{{'COMMON.SHOW' | translate}}\r\n            {{dataTable.getPaginations().form}} -\r\n            {{dataTable.getPaginations().to}} {{'COMMON.OF' | translate}} {{dataTable.getPaginations().total}}</span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <!--end: Datatable -->\r\n    <div *ngIf=\"dataTable.isLoading\" class=\"blockUI blockMsg blockElement\"\r\n      style=\"z-index: 1011; position: absolute; padding: 0px; margin: 0px; width: 169px; top: 50%; left: 45%; text-align: center; color: rgb(0, 0, 0); border: 0px; cursor: wait;\">\r\n\t\t\t<div class=\"blockui \"><span>{{'COMMON.PLEASE_WAIT' | translate}}</span><span>\r\n          <div class=\"kt-spinner kt-spinner--loader kt-spinner--brand \"></div>\r\n        </span>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!-- end:: Content -->\r\n\r\n<!-- modal popup -->\r\n<ng-template #content let-modal>\r\n      <div class=\"modal-header \">\r\n        <h5 class=\"modal-title\" *ngIf=\"isEdit\">\r\n          {{'ADMIN.ROLE.GENERAL.TITLE_FORM_EDIT' | translate}}\r\n        </h5>\r\n        <h5 class=\"modal-title\" *ngIf=\"!isEdit\">\r\n          {{'ADMIN.ROLE.GENERAL.TITLE_FORM_ADD' | translate}}\r\n        </h5>\r\n        <button type=\"button\" class=\"close\" (click)=\"modal.dismiss('Cross click')\">\r\n          <span aria-hidden=\"true\">×</span>\r\n        </button>\r\n      </div>\r\n      <form [formGroup]=\"formRole\" (ngSubmit)='onSubmitRole(formRole)'>\r\n        <div class=\"modal-body div-scrollbar modal-body-role\" *ngFor=\"let data of dataDefault\">\r\n          <div class=\"row\">\r\n            <div class=\"col-lg-6\">\r\n              <div class=\"row\">\r\n                <div class=\"form-group col-lg-12\">\r\n                    <div class=\"show-error\">\r\n                        <label >{{'ADMIN.ROLE.GENERAL.NAME' | translate}} </label>\r\n                          <app-control-messages [control]=\"f['name']\"></app-control-messages>\r\n                        <span class=\"text-danger\"> *</span>\r\n                      </div>\r\n                      <div class=\"input-group\">\r\n                        <input [ngModel]=\"isEdit?data.name:''\" type=\"text\" class=\"form-control\" placeholder=\"\"\r\n                          formControlName=\"name\">\r\n                      </div>\r\n                </div>\r\n                <div class=\"form-group col-lg-12\">\r\n                    <div class=\"show-error\">\r\n                        <label >{{'ADMIN.ROLE.GENERAL.NAME_PARENT' | translate}}</label>\r\n                        <!-- <app-control-messages [control]=\"f['name']\"></app-control-messages> -->\r\n                        <span class=\"text-danger\"> *</span>\r\n                      </div>\r\n                      <div class=\"input-group autocomplete-form\">\r\n                        <select [ngModel]=\"isEdit?data.parentId:'0'\"   name=\"kt_table_1_length\" aria-controls=\"kt_table_1\"\r\n                          class=\" form-control kt_selectpicker padding-7\" formControlName=\"nameParent\">\r\n                          <option value=\"0\">\r\n                              {{'COMMON.MESSAGE.NOTHING_SELECTED' | translate}}\r\n                          </option>\r\n                          <option *ngFor='let itemParent of listParentRoles' [ngValue]=\"itemParent.id\">\r\n                            {{itemParent.name}}\r\n                          </option>\r\n                        </select>\r\n                      </div>                \r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group col-lg-6\">\r\n                <div class=\"show-error\">\r\n                    <label>{{'ADMIN.ROLE.GENERAL.DESCRIPTION' | translate}} </label>\r\n                    <!-- <app-control-messages [control]=\"f['username']\"></app-control-messages> -->\r\n                  </div>\r\n                  <div class=\"input-group\">\r\n                    <textarea class=\"form-control\" rows=\"5\" [ngModel]=\"isEdit?data.description:''\"\r\n                      formControlName=\"description\">\r\n                                  {{isEdit?data.description:''}}\r\n                         </textarea>\r\n                  </div>\r\n  \r\n            </div>\r\n          </div>\r\n          <div class=\"form-group row\">\r\n            <div class=\"col-lg-12\">\r\n              <div class=\"show-error\">\r\n                <label>\r\n                    {{'ADMIN.ROLE.GENERAL.CHOICE_PERMISSIONS' | translate}}\r\n                </label>\r\n                <!-- <app-control-messages [control]=\"f['username']\"></app-control-messages> -->\r\n              </div>\r\n              <div class=\"lists-group-permission\">\r\n                <div class=\"kt-portlet\" *ngFor=\"let list of data.permissions\">\r\n                  <h3 class=\"kt-portlet__head-title\">\r\n                       {{list.permissionGroup}}     \r\n                       <span class=\"kt-switch kt-switch--sm kt-switch--icon float-right\" style=\"height: 0px;margin:0px\">\r\n                          <label >\r\n                            <input type=\"checkbox\"  [(ngModel)]=\"list.checked\"  (change)=\"checkUncheckAll(list.permissionGroup)\" formControlName=\"{{list.permissionGroup}}\">\r\n                            <span></span>\r\n                          </label>\r\n                      </span>     \r\n                   </h3>\r\n                  <div class=\"kt-portlet__body\">\r\n                    <div class=\"form-group row\" >\r\n                      <div class=\"col-lg-4\" formArrayName=\"permissions\" *ngFor=\"let item of  list.permissons\" >\r\n                          <span class=\"kt-switch kt-switch--sm kt-switch--icon float-left\">\r\n                              <label >\r\n                                <input type=\"checkbox\"  [(ngModel)]=\"item.checked\" name=\"permissions\" formControlName=\"id{{item.id}}\"  (change)=\"isCheckSelected(list.permissionGroup)\">\r\n                                <span></span>\r\n                              </label>\r\n                            </span>\r\n                            <label class=\"col-form-label\">\r\n                              {{item.permissionName}}\r\n                            </label>\r\n                      </div>\r\n\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!--end body modal-->\r\n        <div class=\"modal-footer\">\r\n          <span *ngIf=\"isRoleError\" class=\"text-danger error-message-modal\">\r\n            {{errorMessage}}\r\n          </span>\r\n          <button *ngIf=\"!isEdit\" type=\"submit\" [disabled]=\"formRole.invalid\" class=\"btn btn-primary \">\r\n            {{'COMMON.ACTIONS.ADD' | translate }}\r\n          </button>\r\n          <button *ngIf=\"isEdit\" type=\"submit\" [disabled]=\"formRole.invalid\" class=\"btn btn-primary \">\r\n            {{'COMMON.ACTIONS.SAVE_CHANGES' | translate }}\r\n          </button>\r\n\r\n          <button type=\"button\" class=\"btn btn-secondary\"\r\n          (click)=\"modal.close()\">{{'COMMON.ACTIONS.CANCEL' | translate}}</button>\r\n          <!-- <button type=\"button\" class=\"btn btn-outline-danger\" data-dismiss='modal'>Close</button> -->\r\n        </div>\r\n      </form>\r\n</ng-template>\r\n<!-- end modal popup -->\r\n<!-- modal delete -->\r\n<div class=\"modal fade  modal-confirm kt-mt-50\" tabindex=\"-1\" role=\"dialog\" id=\"modal-delete-role\"\r\n  aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\r\n  <div class=\"modal-dialog modal-sm\">\r\n    <div class=\"modal-content\">\r\n      <div class=\"modal-header\">\r\n        <h5 class=\"modal-title\">{{'COMMON.ACTIONS.DELETE' | translate }}\r\n          {{'ADMIN.ROLE.GENERAL.ROLE' | translate  }}</h5>\r\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n          <span aria-hidden=\"true\">×</span>\r\n        </button>\r\n      </div>\r\n      <div class=\"modal-body\">\r\n        <i class=\"fa fa-question-circle\"></i>{{'COMMON.MESSAGE.COMFIRM_DELETE' | translate }}\r\n      </div>\r\n      <div class=\"modal-footer\">\r\n\r\n        <button type=\"button\" class=\"btn btn-primary\"\r\n          (click)=\"deleteRole()\">{{'COMMON.ACTIONS.CONFIRM' | translate }}</button>\r\n        <button type=\"button\" class=\"btn btn-secondary\"\r\n          data-dismiss=\"modal\">{{'COMMON.ACTIONS.CANCEL' | translate }}</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- end modal delete -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/sensor-type/sensor-type.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/pages/admin/sensor-type/sensor-type.component.html ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- begin:: Content -->\r\n<div class=\"kt-portlet kt-portlet--mobile kt-portlet--main\">\r\n    <div class=\"kt-portlet__head kt-portlet__head--lg\">\r\n        <div class=\"kt-portlet__head-label\">\r\n            <h3 class=\"kt-portlet__head-title\">\r\n                {{'MENU.SENSOR_MODEL' | translate | uppercase }}\r\n            </h3>\r\n        </div>\r\n        <div class=\"kt-portlet__head-toolbar\">\r\n            <div class=\"kt-portlet__head-wrapper\">\r\n                <div class=\"dropdown dropdown-inline\">\r\n                    <button type=\"button\" class=\"btn btn-brand btn-icon-sm btn-pill\" (click)=\"buttonAddNew(content)\">\r\n                        <i class=\"flaticon2-plus\"></i> {{'COMMON.ACTIONS.ADD_NEW' | translate}}\r\n                        {{'ADMIN.SENSOR.GENERAL.SENSOR' | translate }}\r\n                    </button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- <div class=\"kt-portlet__body kt-padding-t-10 kt-padding-b-10\" disabled=\"true\">\r\n      \r\n        </div> -->\r\n    <div class=\"kt-portlet__body kt-portlet__body--fit kt-portlet__body--overflow-y\">\r\n        <!--begin: Search Form -->\r\n        <div class=\"kt-form kt-fork--label-right kt-padding-10 kt-padding-l-20 kt-padding-r-20\">\r\n            <form class=\"\" [formGroup]=\"searchFormSensor\" (ngSubmit)='searchSensor(searchFormSensor)'>\r\n\r\n                <div class=\"row align-items-center\">\r\n                    <div class=\"col-xl-8 order-2 order-xl-1\">\r\n                        <div class=\"row align-items-center\">\r\n                            <div class=\"col-md-4 kt-margin-b-20-tablet-and-mobile\">\r\n                                <div class=\"kt-form__group kt-form__group--inline\">\r\n                                    <div class=\"kt-form__label\">\r\n                                        <label\r\n                                            style=\"font-size: 15px\">{{'ADMIN.ROLE.GENERAL.NAME' | translate}}:</label>\r\n                                    </div>\r\n                                    <div class=\"kt-form__control\">\r\n                                        <input type=\"text\" class=\"form-control\"\r\n                                            placeholder=\"{{'ADMIN.SENSOR.GENERAL.SENSOR_NAME' | translate}} ...\"\r\n                                            formControlName=\"nameSensor\">\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-md-4 kt-margin-b-20-tablet-and-mobile\">\r\n                                <button type=\"submit\" class=\"btn btn-secondary btn-hover-brand\">\r\n                                    <i class=\"icon-search\"></i>{{'COMMON.SEARCH' | translate}}</button>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n\r\n                </div>\r\n            </form>\r\n        </div>\r\n\r\n        <!--end: Search Form -->\r\n        <!--begin: Datatable -->\r\n\t\t<div resizeObserver (resize)=\"onResize($event.contentRect)\"\r\n\t\t\tclass=\"kt_datatable kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded\"\r\n\t\t\t[ngClass]=\"{'kt-datatable--error':dataTable.isNotFound}\" id=\"api_methods\"\r\n\t\t\tstyle=\"position: static; zoom: 1;\">\r\n            <table class=\"kt-datatable__table\">\r\n                <thead class=\"kt-datatable__head\">\r\n                    <tr class=\"kt-datatable__row\" style=\"left: 0px;\">\r\n                        <!-- checkbox -->\r\n                        <th *ngIf=\"dataTable.selecter\"\r\n                            class=\"kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check\">\r\n                            <span style=\"width: 20px;\">\r\n                                <label class=\"kt-checkbox kt-checkbox--single kt-checkbox--solid\">\r\n                                    <input type=\"checkbox\" [(ngModel)]=\"dataTable.isCheckBoxAll\"\r\n                                        (click)=\"dataTable.setCheckBoxAll()\">&nbsp;<span></span>\r\n                                </label>\r\n                            </span>\r\n                        </th>\r\n                        <!-- end checkbox -->\r\n                        <th *ngIf=\"dataTable.isShowSubRow\" style=\"width: 13px;\"\r\n                            class=\"kt-datatable__cell kt-datatable__cell--sort\">\r\n                            <a class=\"kt-datatable__toggle-detail\">\r\n                            </a>\r\n                        </th>\r\n\r\n                        <ng-container *ngFor=\"let column of dataTable.getColumns()\">\r\n\r\n                            <th [ngClass]=\"{'kt-datatable__cell--sorted':column.isSort}\"\r\n                                [ngStyle]=\"{'display':(!column.isShow?'none':'')}\" [attr.data-field]=\"column.field\"\r\n                                class=\"kt-datatable__cell kt-datatable__cell--sort {{column.class}}\"\r\n                                (click)=\"dataTable.sort(column.field)\">\r\n                                <span [ngStyle]=\"column.style\">{{column.translate | translate}}\r\n                                    <i *ngIf=\"column.isSort\"\r\n                                        [ngClass]=\"{'flaticon2-arrow-down':(column.isSort && column.sortable =='desc'),'flaticon2-arrow-up':(column.isSort && column.sortable =='asc')}\"></i>\r\n                                </span>\r\n                            </th>\r\n                        </ng-container>\r\n                    </tr>\r\n                </thead>\r\n                <tbody class=\"kt-datatable__body\" [ngStyle]=\"dataTable.bodyStyle\">\r\n                    <ng-container *ngFor=\"let item of dataTable.data;let i=index\">\r\n                        <tr [ngClass]=\"{'kt-datatable__row--active':item.dt_selecter}\" class=\"kt-datatable__row\"\r\n                            style=\"left: 0px;\">\r\n                            <!-- checkbox -->\r\n                            <td *ngIf=\"dataTable.selecter\"\r\n                                class=\"kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check\"\r\n                                data-field=\"RecordID\"><span style=\"width: 20px;\">\r\n                                    <label class=\"kt-checkbox kt-checkbox--single kt-checkbox--solid\">\r\n                                        <input type=\"checkbox\" [(ngModel)]=\"item.dt_selecter\"\r\n                                            (click)=\"dataTable.setCheckBox(i)\">&nbsp;<span></span>\r\n                                    </label>\r\n                                </span>\r\n                            </td>\r\n                            <!-- end checkbox -->\r\n                            <td *ngIf=\"dataTable.isShowSubRow\" style=\"width: 13px;\"\r\n                                class=\"kt-datatable__cell kt-datatable__toggle-detail\">\r\n                                <a (click)=\"dataTable.showSubRow(i)\" class=\"kt-datatable__toggle-detail\">\r\n                                    <i [ngClass]=\"{'icon-caret-down' : (dataTable.lastRowShow == i), \r\n                                  'icon-caret-right' : (dataTable.lastRowShow != i)}\"></i>\r\n                                </a>\r\n                            </td>\r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[0].isShow?'none':'')}\"\r\n                                class=\"kt-datatable__cell kt-datatable__cell\" data-field=\"id\">\r\n                                <span\r\n                                    [ngStyle]=\"dataTable.getColumns()[0].style\">{{i+dataTable.getPaginations().form}}</span>\r\n                            </td>\r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[1].isShow?'none':'')}\"\r\n                                data-field=\"key_name\" class=\"kt-datatable__cell\">\r\n                                <span [ngStyle]=\"dataTable.getColumns()[1].style\">{{item.name}}</span>\r\n                            </td>\r\n                           \r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[2].isShow?'none':'')}\" data-field=\"name\"\r\n                                class=\"kt-datatable__cell\">\r\n                                <span [ngStyle]=\"dataTable.getColumns()[2].style\"\r\n                                    translate=\"{{item.nameKey}}\">{{item.modifiedBy}}</span>\r\n                            </td>\r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[3].isShow?'none':'')}\"\r\n                                data-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n                                <span [ngStyle]=\"dataTable.getColumns()[3].style\">{{item.description}}</span>\r\n                            </td>\r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[4].isShow?'none':'')}\"\r\n                                data-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n                                <span [ngStyle]=\"dataTable.getColumns()[4].style\">{{item.updatedAt | userDate}}</span>\r\n                            </td>\r\n                            <td [ngStyle]=\"{'display':(!dataTable.getColumns()[5].isShow?'none':'')}\"\r\n                                data-field=\"Actions\" data-autohide-disabled=\"false\" class=\"kt-datatable__cell\">\r\n                                <span [ngStyle]=\"dataTable.getColumns()[5].style\"\r\n                                    style=\"overflow: visible; position: relative;\">\r\n                                    <a ngbTooltip=\"{{'COMMON.ACTIONS.EDIT' | translate }}\"\r\n                                        class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                        (click)=\"editSensor(item.id,content)\">\r\n                                        <i class=\"icon-edit\"></i>\r\n                                    </a>\r\n                                    <a ngbTooltip=\"{{'COMMON.ACTIONS.DELETE' | translate }}\"\r\n                                        class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                        (click)=\"getIdAction(item.id,'#modal-delete-sensor')\">\r\n                                        <i class=\"icon-trash\"></i>\r\n                                    </a>\r\n                                </span>\r\n                            </td>\r\n                        </tr>\r\n                        <!-- Sub row -->\r\n                        <tr *ngIf=\"dataTable.lastRowShow==i && dataTable.isShowSubRow\" class=\"kt-datatable__row-detail\">\r\n                            <td class=\"kt-datatable__detail\" colspan=\"9\">\r\n                                <table>\r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[0].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[0].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span\r\n                                                [ngStyle]=\"dataTable.getColumns()[0].style\">{{i+dataTable.getPaginations().form}}</span>\r\n                                        </td>\r\n                                    </tr>\r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[1].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[1].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span [ngStyle]=\"dataTable.getColumns()[1].style\">{{item.name}}</span>\r\n                                        </td>\r\n                                    </tr>\r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[2].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[2].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span [ngStyle]=\"dataTable.getColumns()[2].style\"\r\n                                                translate=\"{{item.nameKey}}\">{{item.typeSensor}}</span>\r\n                                        </td>\r\n                                    </tr>\r\n                                    \r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[3].isShow?'none':'')}\"\r\n                                    class=\"kt-datatable__row\">\r\n                                    <td class=\"kt-datatable__cell\">\r\n                                        <span>{{dataTable.getColumns()[3].translate | translate}}</span>\r\n                                    </td>\r\n                                    <td class=\"kt-datatable__cell\">\r\n                                        <span [ngStyle]=\"dataTable.getColumns()[3].style\"\r\n                                            translate=\"{{item.nameKey}}\">{{item.modifiedBy}}</span>\r\n                                    </td>\r\n                                </tr>\r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[4].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[4].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span [ngStyle]=\"dataTable.getColumns()[4].style\"\r\n                                                translate=\"{{item.nameKey}}\">{{item.description}}</span>\r\n                                        </td>\r\n                                    </tr>\r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[5].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[5].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span [ngStyle]=\"dataTable.getColumns()[5].style\"\r\n                                                translate=\"{{item.nameKey}}\">{{item.updatedAt | userDate}}</span>\r\n                                        </td>\r\n                                    </tr>\r\n\r\n\r\n                                    <tr [ngStyle]=\"{'display':(dataTable.getColumns()[6].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[5].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span [ngStyle]=\"dataTable.getColumns()[6].style\"\r\n                                                style=\"overflow: visible; position: relative;\">\r\n                                                <a ngbTooltip=\"{{'COMMON.ACTIONS.EDIT' | translate }}\"\r\n                                                    class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                                    (click)=\"editSensor(item.id,'#modal-sensor')\">\r\n                                                    <i class=\"icon-edit\"></i>\r\n                                                </a>\r\n                                                <a ngbTooltip=\"{{'COMMON.ACTIONS.DELETE' | translate }}\"\r\n                                                    class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                                    (click)=\"getIdAction(item.id,'#modal-delete-sensor')\">\r\n                                                    <i class=\"icon-trash\"></i>\r\n                                                </a>\r\n                                            </span>\r\n                                        </td>\r\n                                    </tr>\r\n                                </table>\r\n                            </td>\r\n                        </tr>\r\n                    </ng-container>\r\n                    <!-- No records found -->\r\n                    <div *ngIf=\"dataTable.isNotFound\" class=\"kt-datatable--error\">\r\n                        <kt-not-found></kt-not-found>\r\n                    </div>\r\n                </tbody>\r\n\r\n\r\n            </table>\r\n\r\n            <div class=\"kt-datatable__pager kt-datatable--paging-loaded\" *ngIf=\"dataTable.data?.length != 0\">\r\n                <ul class=\"kt-datatable__pager-nav\">\r\n                    <li><a title=\"First\" class=\"kt-datatable__pager-link kt-datatable__pager-link--first\"\r\n                            [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getFirstPage())}\"\r\n                            (click)=\"dataTable.gotoPage(dataTable.getFirstPage())\">\r\n                            <i class=\"flaticon2-fast-back\"></i></a></li>\r\n                    <li><a title=\"Previous\" class=\"kt-datatable__pager-link kt-datatable__pager-link--prev\"\r\n                            [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getFirstPage())}\"\r\n                            (click)=\"dataTable.gotoPage(dataTable.getCurrentPage()-1)\">\r\n                            <i class=\"flaticon2-back\"></i></a></li>\r\n                    <li></li>\r\n                    <li style=\"display: none;\"><input type=\"text\" class=\"kt-pager-input form-control\"\r\n                            title=\"Page number\"></li>\r\n                    <li *ngFor=\"let page of dataTable.getPageDisplay()\">\r\n                        <a (click)=\"dataTable.gotoPage(page)\"\r\n                            class=\"kt-datatable__pager-link kt-datatable__pager-link-number \"\r\n                            [ngClass]=\"{'kt-datatable__pager-link--active':page==dataTable.getCurrentPage()}\"\r\n                            data-page=\"1\" title=\"{{page}}\">{{page}}</a>\r\n                    </li>\r\n                    <li style=\"\"></li>\r\n                    <li><a title=\"Next\" class=\"kt-datatable__pager-link kt-datatable__pager-link--next \"\r\n                            [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getLastPage())}\"\r\n                            (click)=\"dataTable.gotoPage(dataTable.getCurrentPage()+1)\"><i\r\n                                class=\" flaticon2-next\"></i></a></li>\r\n                    <li><a title=\"Last\"\r\n                            [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getLastPage())}\"\r\n                            class=\"kt-datatable__pager-link kt-datatable__pager-link--last\"\r\n                            (click)=\"dataTable.gotoPage(dataTable.getLastPage())\">\r\n                            <i class=\"flaticon2-fast-next\"></i></a></li>\r\n                </ul>\r\n                <div class=\"kt-datatable__pager-info\">\r\n                    <div class=\"dropdown bootstrap-select kt-datatable__pager-size\" style=\"width: 60px;\">\r\n                        <select (change)=\"dataTable.setPageSize($event.target.value)\"\r\n                            class=\"selectpicker kt-datatable__pager-size\" title=\"Select page size\" data-selected=\"30\"\r\n                            data-width=\"60px\">\r\n                            <option *ngFor=\"let item of dataTable.getPaginationSelect()\"\r\n                                [selected]=\"item==dataTable.getPaginations().pageSize\" value=\"{{item}}\" title=\"{{item}}\"\r\n                                data-toggle=\"tooltip\">{{item}}</option>\r\n                        </select>\r\n                    </div><span class=\"kt-datatable__pager-detail\">{{'COMMON.SHOW' | translate}}\r\n                        {{dataTable.getPaginations().form}} -\r\n                        {{dataTable.getPaginations().to}} {{'COMMON.OF' | translate}}\r\n                        {{dataTable.getPaginations().total}}</span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <!--end: Datatable -->\r\n        <div *ngIf=\"dataTable.isLoading\" class=\"blockUI blockMsg blockElement\"\r\n            style=\"z-index: 1011; position: absolute; padding: 0px; margin: 0px; width: 169px; top: 50%; left: 45%; text-align: center; color: rgb(0, 0, 0); border: 0px; cursor: wait;\">\r\n            <div class=\"blockui \"><span>{{'COMMON.PLEASE_WAIT' | translate}}</span><span>\r\n                    <div class=\"kt-spinner kt-spinner--loader kt-spinner--brand \"></div>\r\n                </span>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<!-- end:: Content -->\r\n\r\n<!-- modal popup -->\r\n<ng-template #content let-modal>\r\n            <div class=\"modal-header \">\r\n                <h5 class=\"modal-title\" *ngIf=\"isEdit\">\r\n                    {{'ADMIN.SENSOR.GENERAL.TITLE_FORM_EDIT' | translate}}\r\n                </h5>\r\n                <h5 class=\"modal-title\" *ngIf=\"!isEdit\">\r\n                    {{'ADMIN.SENSOR.GENERAL.ADD_SENSOR_NAME' | translate}}\r\n                </h5>\r\n                <button type=\"button\" class=\"close\" (click)=\"modal.dismiss('Cross click')\">\r\n                    <span aria-hidden=\"true\">×</span>\r\n                </button>\r\n            </div>\r\n            <form [formGroup]=\"formSensor\" (ngSubmit)='onSubmitSensor(formSensor)' class=\"\">\r\n                <div class=\"modal-body div-scrollbar modal-body-sensor\" *ngFor=\"let data of dataDefault\">\r\n        \r\n                        <div class=\"tab-title-modal\">\r\n                            <ul class=\"nav nav-tabs nav-tabs-line nav-tabs-line-primary\" sensor=\"tablist\">\r\n                                <li class=\"nav-item\">\r\n                                    <a class=\"nav-link active\" data-toggle=\"tab\" href=\"#info_global_sensor\" sensor=\"tab\">\r\n                                        {{'COMMON.GENERAL.BASE' | translate}}\r\n                                    </a>\r\n                                </li>\r\n                                <li class=\"nav-item\">\r\n                                    <a class=\"nav-link \" data-toggle=\"tab\" href=\"#info_paramset\" sensor=\"tab\">\r\n                                        {{'COMMON.GENERAL.PARAMETER' | translate}}\r\n                                    </a>\r\n                                </li>\r\n                            </ul>\r\n                        </div>\r\n                        <div class=\"tab-content\">\r\n                            <div class=\"tab-pane active\" id=\"info_global_sensor\" sensor=\"tabpanel\">\r\n                                <div class=\"form-group row\">\r\n                                    <div class=\"col-lg-12\">\r\n                                        <div class=\"show-error\">\r\n                                            <label class=\"\">{{'ADMIN.PERMISSION.GENERAL.NAME' | translate}} </label>\r\n                                            <app-control-messages [control]=\"f['nameSensor']\"></app-control-messages>\r\n                                            <span class=\"text-danger\"> *</span>\r\n                                        </div>\r\n                                        <div class=\"input-group\">\r\n                                          \r\n                                            <input [ngModel]=\"isEdit?data.name:''\" type=\"text\" class=\"form-control\"\r\n                                                placeholder=\"\" formControlName=\"nameSensor\">\r\n                                        </div>\r\n                                    </div>\r\n                               </div>\r\n                                <!-- <div class=\"form-group row\">\r\n                                    <div class=\"col-lg-12\">\r\n                                        <div class=\"show-error\">\r\n                                            <label class=\"\">{{'ADMIN.SENSOR.GENERAL.SENSOR_TYPE' | translate}} </label>\r\n                                        </div>\r\n                                        <div class=\"input-group autocomplete-form\">\r\n                                    \r\n                                            <div class=\"ng-autocomplete ng-autocomplete-form\">\r\n                                                <ng-autocomplete [data]=\"listTypeSensor\" [searchKeyword]=\"keyword\"\r\n                                                    (selected)='selectEvent($event)'\r\n                                                    (inputChanged)='onChangeSearch($event)'\r\n                                                    [itemTemplate]=\"itemTemplate\" [debounceTime]=500\r\n                                                    [initialValue]=\"isEdit?sensorTypeEdit:''\"\r\n                                                    [notFoundTemplate]=\"notFoundTemplate\">\r\n                                                </ng-autocomplete>\r\n                                                <ng-template #itemTemplate let-item>\r\n                                                    <span class=\"font-weight-bold\" [innerHTML]=\"item.name\"></span>\r\n                                                </ng-template>\r\n\r\n                                                <ng-template #notFoundTemplate let-notFound>\r\n                                                    <div>{{'COMMON.MESSAGE.NO_RECORDS_FOUND' | translate}}</div>\r\n                                                </ng-template>\r\n                                            </div>\r\n                                         <input  [ngModel]=\"isEdit?data.groupName:''\" type=\"text\" class=\"form-control\" placeholder=\"\" formControlName=\"namegroup\">\r\n                                        </div>\r\n\r\n                                    </div>\r\n                                </div>  -->\r\n                                <div class=\"form-group row\">\r\n                                        <div class=\"col-lg-12\">\r\n                                            <div class=\"show-error\">\r\n                                                <label class=\"\">{{'ADMIN.PERMISSION.GENERAL.DESCRIPTION' | translate}}\r\n                                                </label>\r\n                                                <!-- <app-control-messages [control]=\"f['username']\"></app-control-messages> -->\r\n                                            </div>\r\n                                            <div class=\"input-group\">\r\n                                                <textarea class=\"form-control\" rows=\"2\"\r\n                                                    [ngModel]=\"isEdit?data.description:''\" formControlName=\"description\">\r\n                                                            {{isEdit?data.description:''}}\r\n                                                   </textarea>\r\n                                            </div>\r\n    \r\n                                        </div>\r\n                                    </div>\r\n                                <div class=\"form-group row\" *ngIf= \"isEdit\">\r\n                                        <div class=\"col-lg-12\">\r\n                                            <div class=\"show-error\">\r\n                                                <label class=\"\">{{'COMMON.COLUMN.CREATED_BY' | translate}} </label>\r\n                                            </div>\r\n                                            <div class=\"input-group\">\r\n                                                \r\n                                                <input type=\"text\" class=\"form-control\" disabled\r\n                                                placeholder=\"\" value=\"{{data.createBy}}\">\r\n                                            </div>\r\n                                        </div>\r\n                                </div>\r\n                                <div class=\"form-group row\" *ngIf= \"isEdit\">\r\n                                        <div class=\"col-lg-12\">\r\n                                            <div class=\"show-error\">\r\n                                                <label class=\"\">{{'COMMON.COLUMN.MODIFIELD_BY' | translate}} </label>\r\n                                            </div>\r\n                                            <div class=\"input-group\">\r\n                                            \r\n                                                <input type=\"text\" class=\"form-control\" disabled\r\n                                                placeholder=\"\" value=\"{{data.modifiedBy}}\">\r\n                                            </div>\r\n                                        </div>\r\n                                </div>\r\n                                <div class=\"form-group row\" *ngIf= \"isEdit\">\r\n                                        <div class=\"col-lg-12\">\r\n                                            <div class=\"show-error\">\r\n                                                <label class=\"\">{{'COMMON.COLUMN.UPDATED_DATE' | translate}} </label>\r\n                                            </div>\r\n                                            <div class=\"input-group\">\r\n                                                \r\n                                                <input type=\"text\" class=\"form-control\" disabled\r\n                                                    placeholder=\"\" value=\"{{data.updatedAt | userDate}}\">\r\n                                            </div>\r\n                                        </div>\r\n                                </div>\r\n                                \r\n                            </div>\r\n                            <div class=\"tab-pane\" id=\"info_paramset\" sensor=\"tabpanel\">\r\n                                    <div id=\"kt_repeater_1\">\r\n                                            <table class=\"table table-bordered table-sensor\"  id=\"table-sensor\">\r\n                                                    <thead>\r\n                                                      <tr>  \r\n                                                            <th data-width=\"40%\" style=\"width:40%\">{{'COMMON.COLUMN.NAME' | translate}}</th>\r\n                                                            <th data-width=\"40%\" style=\"width:30%\">{{'COMMON.COLUMN.VALUE' | translate}}</th>\r\n                                                            <th data-width=\"20%\" style=\"width:30%\">{{'COMMON.ACTIONS.ACTIONS' | translate}}</th>\r\n                                                      </tr>\r\n                                                    </thead>\r\n                                                    <tbody data-repeater-list>\r\n                                                      <tr data-repeater-item  style=\"display: none\">\r\n                                                          <td  contenteditable=\"true\"></td>\r\n                                                          <td  contenteditable=\"true\"></td>\r\n                                                          <td>\r\n                                                                <a  data-repeater-delete class=\"btn.btn-clean\" ng-reflect-ngb-tooltip=\"Xóa\">\r\n                                                                  <i class=\"icon-trash\"></i>\r\n                                                               </a>\r\n                                                            </td>\r\n                                                      </tr>                                           \r\n                                                      <tr  *ngFor = 'let item  of parameters'  data-repeater-item>                                                    \r\n                                                            <td  [textContent]=\"item.key\"  name =\"text-input\" contenteditable=\"true\">{{item.key}}</td>\r\n                                                            <td contenteditable=\"true\">{{item.value}}</td>\r\n                                                            <td>\r\n                                                                <a  data-repeater-delete class=\"btn.btn-clean\" ng-reflect-ngb-tooltip=\"Xóa\">\r\n                                                                  <i  class=\"icon-trash\"></i>\r\n                                                               </a>\r\n                                                            </td>\r\n                                                      </tr>\r\n                                                      <!-- <tr *ngIf = \"parameters.length == 0\">\r\n                                                          not found\r\n                                                      </tr> -->\r\n                                                 \r\n                                                    </tbody>\r\n                                              </table>\r\n                                              <button data-repeater-create class=\"btn btn-brand btn-icon-sm\" type=\"button\" (click)=\"createParamsets()\" id=\"create-paramsets\">\r\n                                                  <i _ngcontent-oek-c15=\"\"  class=\"flaticon2-plus\" ></i> {{'COMMON.ACTIONS.ADD_NEW' | translate}} </button>\r\n                                       \r\n                                    </div>\r\n                                   \r\n                                <!-- code repeater-->\r\n                            </div>\r\n                        </div>\r\n                </div>\r\n                <!--end body modal-->\r\n                <div class=\"modal-footer\">\r\n                    <span *ngIf=\"isPermissionError\" class=\"text-danger error-message-modal\">\r\n                        {{errorMessage}}\r\n                    </span>\r\n                    <button *ngIf=\"!isEdit\" type=\"submit\" [disabled]=\"formSensor.invalid\" class=\"btn btn-primary \">\r\n                        {{'COMMON.ACTIONS.ADD' | translate }}\r\n                    </button>\r\n                    <button *ngIf=\"isEdit\" type=\"submit\" [disabled]=\"formSensor.invalid\" class=\"btn btn-primary \">\r\n                        {{'COMMON.ACTIONS.SAVE_CHANGES' | translate }}\r\n                    </button>\r\n\r\n                    <button type=\"button\" class=\"btn btn-secondary\"\r\n                    (click)=\"modal.close()\">{{'COMMON.ACTIONS.CANCEL' | translate}}</button>\r\n                    <!-- <button type=\"button\" class=\"btn btn-outline-danger\" data-dismiss='modal'>Close</button> -->\r\n                </div>\r\n            </form>\r\n</ng-template>\r\n<!-- end modal popup -->\r\n<!-- modal delete -->\r\n<div class=\"modal fade  modal-confirm kt-mt-50\" tabindex=\"-1\" sensor=\"dialog\" id=\"modal-delete-sensor\"\r\n    aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog modal-sm\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\">{{'COMMON.ACTIONS.DELETE' | translate }}\r\n                    {{'ADMIN.ROLE.GENERAL.ROLE' | translate  }}</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">×</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <i class=\"fa fa-question-circle\"></i>{{'COMMON.MESSAGE.COMFIRM_DELETE' | translate }}\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n\r\n                <button type=\"button\" class=\"btn btn-primary\"\r\n                    (click)=\"deleteSensor()\">{{'COMMON.ACTIONS.CONFIRM' | translate }}</button>\r\n                <button type=\"button\" class=\"btn btn-secondary\"\r\n                    data-dismiss=\"modal\">{{'COMMON.ACTIONS.CANCEL' | translate }}</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!-- end modal delete -->\r\n<!-- end:: Content -->\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/sim-type/sim-type.component.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/pages/admin/sim-type/sim-type.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- begin:: Content -->\r\n<!-- <div class=\"alert alert-light alert-elevate\" role=\"alert\">\r\n\t<div class=\"alert-icon\"><i class=\"flaticon-warning kt-font-brand\"></i></div>\r\n\t<div class=\"alert-text\">\r\n\t\tThe Keen Datatable component supports local or remote data source. For the local data you can pass\r\n\t\tjavascript array as data source. In this example the grid fetches its data from a javascript array data\r\n\t\tsource. It also defines\r\n\t\tthe schema model of the data source. In addition to the visualization, the Datatable provides built-in\r\n\t\tsupport for operations over data such as sorting, filtering and paging performed in user browser(frontend).\r\n\t</div>\r\n</div> -->\r\n<div class=\"kt-portlet kt-portlet--mobile kt-portlet--main\">\r\n\t<div class=\"kt-portlet__head kt-portlet__head--lg\">\r\n\t\t<div class=\"kt-portlet__head-label\">\r\n\t\t\t<span class=\"kt-portlet__head-icon\">\r\n\t\t\t\t<i class=\"icon-setting\"></i>\r\n\t\t\t</span>\r\n\t\t\t<h3 class=\"kt-portlet__head-title\">\r\n\t\t\t\t{{'ADMIN.GENERAL.SIM_TYPE' | translate | uppercase }}\r\n\t\t\t</h3>\r\n\t\t</div>\r\n\t\t<div class=\"kt-portlet__head-toolbar\">\r\n\t\t\t<div class=\"kt-portlet__head-wrapper\">\r\n\t\t\t\t<button type=\"button\" class=\"btn btn-brand btn-icon-sm btn-pill\" (click)=\"open(content,'add')\"\r\n\t\t\t\t\taria-expanded=\"false\">\r\n\t\t\t\t\t<i class=\"icon-plus\"></i> {{'COMMON.ACTIONS.ADD_NEW' | translate}}\r\n\t\t\t\t\t{{'ADMIN.GENERAL.SIM_TYPE' | translate }}\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\t<div class=\"kt-portlet__body kt-portlet__body--fit kt-portlet__body--overflow-y\">\r\n\t\t<!--begin: Search Form -->\r\n\t\t<div class=\"kt-form kt-fork--label-right kt-padding-10 kt-padding-l-20 kt-padding-r-20\">\r\n\t\t\t<div class=\"row align-items-center\">\r\n\t\t\t\t<div class=\"col-xl-8 order-2 order-xl-1\">\r\n\t\t\t\t\t<div class=\"row align-items-center\">\r\n\t\t\t\t\t\t<div class=\"col-md-3 kt-margin-b-20-tablet-and-mobile\" id=\"formSearch\"\r\n\t\t\t\t\t\t\t(keyup.enter)=\"dataTable.search()\">\r\n\t\t\t\t\t\t\t<div class=\"kt-form__group kt-form__group--inline\">\r\n\t\t\t\t\t\t\t\t<div class=\"kt-form__label\">\r\n\t\t\t\t\t\t\t\t\t<label>{{'ADMIN.SIM_TYPE.GENERAL.NAME_KEY' | translate}}:</label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"kt-form__control\">\r\n\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" data-key=\"nameKey\">\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t<div class=\"col-md-3 kt-margin-b-20-tablet-and-mobile\">\r\n\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-secondary btn-hover-brand\"\r\n\t\t\t\t\t\t\t\t(click)=\"dataTable.search()\">\r\n\t\t\t\t\t\t\t\t<i class=\"icon-search\"></i>{{'COMMON.SEARCH' | translate}}</button>\r\n\t\t\t\t\t\t\t&nbsp;\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<!--end: Search Form -->\r\n\t\t<!--begin: Datatable -->\r\n\t\t<div resizeObserver (resize)=\"onResize($event.contentRect)\"\r\n\t\t\tclass=\"kt_datatable kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded\"\r\n\t\t\t[ngClass]=\"{'kt-datatable--error':dataTable.isNotFound}\" id=\"api_methods\"\r\n\t\t\tstyle=\"position: static; zoom: 1;\">\r\n\t\t\t<table class=\"kt-datatable__table\" style=\"display: block; min-height: 500px;\">\r\n\t\t\t\t<!-- Header -->\r\n\t\t\t\t<thead class=\"kt-datatable__head \">\r\n\t\t\t\t\t<tr class=\"kt-datatable__row\" style=\"left: 0px;\">\r\n\t\t\t\t\t\t<!-- subRow -->\r\n\t\t\t\t\t\t<th *ngIf=\"dataTable.isShowSubRow\"\r\n\t\t\t\t\t\t\tclass=\"kt-datatable__cell kt-datatable__toggle-detail kt-datatable__cell--sort\">\r\n\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t<!-- end subRow -->\r\n\r\n\t\t\t\t\t\t<!-- checkbox -->\r\n\t\t\t\t\t\t<th *ngIf=\"dataTable.selecter\"\r\n\t\t\t\t\t\t\tclass=\"kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check\">\r\n\t\t\t\t\t\t\t<span style=\"width: 20px;\">\r\n\t\t\t\t\t\t\t\t<label class=\"kt-checkbox kt-checkbox--single kt-checkbox--all kt-checkbox--solid\">\r\n\t\t\t\t\t\t\t\t\t<input (click)=\"dataTable.setCheckBoxAll()\" type=\"checkbox\"\r\n\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"dataTable.isCheckBoxAll\">&nbsp;<span></span>\r\n\t\t\t\t\t\t\t\t</label>\r\n\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t<!-- end checkbox -->\r\n\t\t\t\t\t\t<ng-container *ngFor=\"let column of dataTable.getColumns()\">\r\n\t\t\t\t\t\t\t<th [ngClass]=\"{'kt-datatable__cell--sorted':column.isSort}\"\r\n\t\t\t\t\t\t\t\t[ngStyle]=\"{'display':(!column.isShow?'none':'')}\" [attr.data-field]=\"column.field\"\r\n\t\t\t\t\t\t\t\tclass=\"kt-datatable__cell kt-datatable__cell--sort {{column.class}}\"\r\n\t\t\t\t\t\t\t\t(click)=\"dataTable.sort(column.field)\">\r\n\t\t\t\t\t\t\t\t<span [ngStyle]=\"column.style\">{{column.translate | translate}}\r\n\t\t\t\t\t\t\t\t\t<i *ngIf=\"column.isSort\"\r\n\t\t\t\t\t\t\t\t\t\t[ngClass]=\"{'flaticon2-arrow-down':(column.isSort && column.sortable =='desc'),'flaticon2-arrow-up':(column.isSort && column.sortable =='asc')}\"></i>\r\n\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</thead>\r\n\t\t\t\t<!-- table body -->\r\n\t\t\t\t<tbody class=\"kt-datatable__body\" [ngStyle]=\"dataTable.bodyStyle\">\r\n\t\t\t\t\t<ng-container *ngFor=\"let item of dataTable.data;let i=index\">\r\n\t\t\t\t\t\t<tr [ngClass]=\"{'kt-datatable__row--active':item.dt_selecter}\" class=\"kt-datatable__row\"\r\n\t\t\t\t\t\t\tstyle=\"left: 0px;\">\r\n\t\t\t\t\t\t\t<!-- subRow -->\r\n\t\t\t\t\t\t\t<td *ngIf=\"dataTable.isShowSubRow\" class=\"kt-datatable__cell kt-datatable__toggle-detail\">\r\n\t\t\t\t\t\t\t\t<a (click)=\"dataTable.showSubRow(i)\" class=\"kt-datatable__toggle-detail\">\r\n\t\t\t\t\t\t\t\t\t<i [ngClass]=\"{'icon-caret-down' : (dataTable.lastRowShow == i), \r\n\t\t\t\t\t\t\t\t\t\t'icon-caret-right' : (dataTable.lastRowShow != i)}\"></i>\r\n\t\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<!-- end subRow -->\r\n\r\n\t\t\t\t\t\t\t<!-- checkbox -->\r\n\t\t\t\t\t\t\t<td *ngIf=\"dataTable.selecter\"\r\n\t\t\t\t\t\t\t\tclass=\"kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check\"\r\n\t\t\t\t\t\t\t\tdata-field=\"RecordID\"><span style=\"width: 20px;\">\r\n\t\t\t\t\t\t\t\t\t<label class=\"kt-checkbox kt-checkbox--single kt-checkbox--solid\">\r\n\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" [(ngModel)]=\"item.dt_selecter\"\r\n\t\t\t\t\t\t\t\t\t\t\t(click)=\"dataTable.setCheckBox(i)\">&nbsp;<span></span>\r\n\t\t\t\t\t\t\t\t\t</label>\r\n\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<!-- end checkbox -->\r\n\t\t\t\t\t\t\t<td [ngStyle]=\"{'display':(!dataTable.getColumns()[0].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\tclass=\"kt-datatable__cell kt-datatable__cell\" data-field=\"id\">\r\n\t\t\t\t\t\t\t\t<span\r\n\t\t\t\t\t\t\t\t\t[ngStyle]=\"dataTable.getColumns()[0].style\">{{i+dataTable.getPaginations().form}}</span>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<td [ngStyle]=\"{'display':(!dataTable.getColumns()[1].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\tdata-field=\"key_name\" class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[1].style\">{{item.name}}</span>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<td [ngStyle]=\"{'display':(!dataTable.getColumns()[2].isShow?'none':'')}\" data-field=\"name\"\r\n\t\t\t\t\t\t\t\tclass=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[2].style\">{{item.nameKey | translate}}</span>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<td [ngStyle]=\"{'display':(!dataTable.getColumns()[3].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\tdata-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t<span\r\n\t\t\t\t\t\t\t\t\t[ngStyle]=\"dataTable.getColumns()[3].style\">{{item.createdAt | date:'dd/MM/yyyy'}}</span>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<td [ngStyle]=\"{'display':(!dataTable.getColumns()[4].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\tdata-field=\"status\" class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[4].style\">\r\n\t\t\t\t\t\t\t\t\t{{item.sortOrder?item.sortOrder:\"-\"}}\r\n\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t<td [ngStyle]=\"{'display':(!dataTable.getColumns()[5].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\tdata-field=\"Actions\" data-autohide-disabled=\"false\" class=\"kt-datatable__cell\"><span\r\n\t\t\t\t\t\t\t\t\t[ngStyle]=\"dataTable.getColumns()[5].style\"\r\n\t\t\t\t\t\t\t\t\tstyle=\"overflow: visible; position: relative;\">\r\n\t\t\t\t\t\t\t\t\t<a ngbTooltip=\"{{'COMMON.ACTIONS.EDIT' | translate }}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n\t\t\t\t\t\t\t\t\t\t(click)=\"open(content,'edit',item)\">\r\n\t\t\t\t\t\t\t\t\t\t<i class=\"icon-edit\"></i>\r\n\t\t\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t\t\t<a ngbTooltip=\"{{'COMMON.ACTIONS.DELETE' | translate }}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n\t\t\t\t\t\t\t\t\t\t(click)=\"open(content,'delete',item)\">\r\n\t\t\t\t\t\t\t\t\t\t<i class=\"icon-trash\"></i>\r\n\t\t\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t<!-- Sub row -->\r\n\t\t\t\t\t\t<tr *ngIf=\"dataTable.lastRowShow==i && dataTable.isShowSubRow\" class=\"kt-datatable__row-detail\">\r\n\t\t\t\t\t\t\t<td class=\"kt-datatable__detail\" colspan=\"9\">\r\n\t\t\t\t\t\t\t\t<table>\r\n\t\t\t\t\t\t\t\t\t<tr [ngStyle]=\"{'display':(dataTable.getColumns()[0].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"kt-datatable__row\">\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span>{{dataTable.getColumns()[0].translate | translate}}</span></td>\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span\r\n\t\t\t\t\t\t\t\t\t\t\t\t[ngStyle]=\"dataTable.getColumns()[0].style\">{{i+dataTable.getPaginations().form}}</span>\r\n\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t<tr [ngStyle]=\"{'display':(dataTable.getColumns()[1].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"kt-datatable__row\">\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span>{{dataTable.getColumns()[1].translate | translate}}</span></td>\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[1].style\">{{item.name}}</span>\r\n\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t<tr [ngStyle]=\"{'display':(dataTable.getColumns()[2].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"kt-datatable__row\">\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span>{{dataTable.getColumns()[2].translate | translate}}</span></td>\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[2].style\">{{item.nameKey | translate}}</span>\r\n\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t<tr [ngStyle]=\"{'display':(dataTable.getColumns()[3].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"kt-datatable__row\">\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span>{{dataTable.getColumns()[3].translate | translate}}</span></td>\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span\r\n\t\t\t\t\t\t\t\t\t\t\t\t[ngStyle]=\"dataTable.getColumns()[3].style\">{{item.createdAt | date:'dd/MM/yyyy'}}</span>\r\n\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t<tr [ngStyle]=\"{'display':(dataTable.getColumns()[4].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"kt-datatable__row\">\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span>{{dataTable.getColumns()[4].translate | translate}}</span></td>\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[4].style\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t{{item.sortOrder?item.sortOrder:\"-\"}}\r\n\t\t\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t<tr [ngStyle]=\"{'display':(dataTable.getColumns()[5].isShow?'none':'')}\"\r\n\t\t\t\t\t\t\t\t\t\tclass=\"kt-datatable__row\">\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span>{{dataTable.getColumns()[5].translate | translate}}</span></td>\r\n\t\t\t\t\t\t\t\t\t\t<td class=\"kt-datatable__cell\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span [ngStyle]=\"dataTable.getColumns()[5].style\"\r\n\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"overflow: visible; position: relative;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<a ngbTooltip=\"{{'COMMON.ACTIONS.EDIT' | translate }}\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\tclass=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t(click)=\"open(content,item)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-edit\"></i>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<a ngbTooltip=\"{{'COMMON.ACTIONS.DELETE' | translate }}\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\tclass=\"btn btn-sm btn-clean btn-icon btn-icon-md\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"icon-trash\"></i>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t<!-- No records found -->\r\n\t\t\t\t\t<div *ngIf=\"dataTable.isNotFound\" class=\"kt-datatable--error\">\r\n\t\t\t\t\t\t<kt-not-found></kt-not-found>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<!-- <span *ngIf=\"dataTable.isNotFound\" class=\"kt-datatable--error\"\r\n\t\t\t\t\t\t[innerHTML]=\"'COMMON.MESSAGE.NO_RECORDS_FOUND' | translate\">No records found</span> -->\r\n\t\t\t\t</tbody>\r\n\r\n\t\t\t</table>\r\n\t\t\t<!-- begin pagination -->\r\n\t\t\t<div  [ngStyle]=\"{'visibility':(dataTable.isNotFound?'hidden':'visible')}\" class=\"kt-datatable__pager kt-datatable--paging-loaded\">\r\n\t\t\t\t<!-- pagination -->\r\n\t\t\t\t<ul class=\"kt-datatable__pager-nav\">\r\n\t\t\t\t\t<li><a ngbTooltip=\"{{'COMMON.FIRST_PAGE' | translate}}\"\r\n\t\t\t\t\t\t\tclass=\"kt-datatable__pager-link kt-datatable__pager-link--first\"\r\n\t\t\t\t\t\t\t[ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getFirstPage())}\"\r\n\t\t\t\t\t\t\t(click)=\"dataTable.gotoPage(dataTable.getFirstPage())\">\r\n\t\t\t\t\t\t\t<i class=\"flaticon2-fast-back\"></i></a></li>\r\n\t\t\t\t\t<li><a ngbTooltip=\"{{'COMMON.PREVIOUS_PAGE' | translate}}\"\r\n\t\t\t\t\t\t\tclass=\"kt-datatable__pager-link kt-datatable__pager-link--prev\"\r\n\t\t\t\t\t\t\t[ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getFirstPage())}\"\r\n\t\t\t\t\t\t\t(click)=\"dataTable.gotoPage(dataTable.getCurrentPage()-1)\">\r\n\t\t\t\t\t\t\t<i class=\"flaticon2-back\"></i></a></li>\r\n\t\t\t\t\t<li></li>\r\n\t\t\t\t\t<li style=\"display: none;\"><input type=\"text\" class=\"kt-pager-input form-control\"\r\n\t\t\t\t\t\t\ttitle=\"Page number\"></li>\r\n\t\t\t\t\t<li *ngFor=\"let page of dataTable.getPageDisplay()\">\r\n\t\t\t\t\t\t<a (click)=\"dataTable.gotoPage(page)\"\r\n\t\t\t\t\t\t\tclass=\"kt-datatable__pager-link kt-datatable__pager-link-number \"\r\n\t\t\t\t\t\t\t[ngClass]=\"{'kt-datatable__pager-link--active':page==dataTable.getCurrentPage()}\"\r\n\t\t\t\t\t\t\tdata-page=\"1\">{{page}}</a>\r\n\t\t\t\t\t</li>\r\n\t\t\t\t\t<li style=\"\"></li>\r\n\t\t\t\t\t<li><a ngbTooltip=\"{{'COMMON.NEXT_PAGE' | translate}}\"\r\n\t\t\t\t\t\t\tclass=\"kt-datatable__pager-link kt-datatable__pager-link--next \"\r\n\t\t\t\t\t\t\t[ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getLastPage())}\"\r\n\t\t\t\t\t\t\t(click)=\"dataTable.gotoPage(dataTable.getCurrentPage()+1)\"><i\r\n\t\t\t\t\t\t\t\tclass=\" flaticon2-next\"></i></a></li>\r\n\t\t\t\t\t<li><a ngbTooltip=\"{{'COMMON.LAST_PAGE' | translate}}\"\r\n\t\t\t\t\t\t\t[ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getLastPage())}\"\r\n\t\t\t\t\t\t\tclass=\"kt-datatable__pager-link kt-datatable__pager-link--last\"\r\n\t\t\t\t\t\t\t(click)=\"dataTable.gotoPage(dataTable.getLastPage())\">\r\n\t\t\t\t\t\t\t<i class=\"flaticon2-fast-next\"></i></a></li>\r\n\t\t\t\t</ul>\r\n\t\t\t\t<!-- page size -->\r\n\t\t\t\t<div class=\"kt-datatable__pager-info\">\r\n\t\t\t\t\t<div class=\"dropdown bootstrap-select kt-datatable__pager-size\" id=\"dataTableSelect\"\r\n\t\t\t\t\t\tstyle=\"width: 60px;\">\r\n\t\t\t\t\t\t<select (change)=\"dataTable.setPageSize($event.target.value)\"\r\n\t\t\t\t\t\t\tclass=\"selectpicker kt-datatable__pager-size\" title=\"Select page size\" data-selected=\"30\"\r\n\t\t\t\t\t\t\tdata-width=\"60px\">\r\n\t\t\t\t\t\t\t<option *ngFor=\"let item of dataTable.getPaginationSelect()\"\r\n\t\t\t\t\t\t\t\t[selected]=\"item==dataTable.getPaginations().pageSize\" value=\"{{item}}\" title=\"{{item}}\"\r\n\t\t\t\t\t\t\t\tdata-toggle=\"tooltip\">{{item}}</option>\r\n\t\t\t\t\t\t</select>\r\n\t\t\t\t\t</div><span class=\"kt-datatable__pager-detail\">{{'COMMON.SHOW' | translate}}\r\n\t\t\t\t\t\t{{dataTable.getPaginations().form}} -\r\n\t\t\t\t\t\t{{dataTable.getPaginations().to}} {{'COMMON.OF' | translate}}\r\n\t\t\t\t\t\t{{dataTable.getPaginations().total}}</span>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<!-- end: Pagination -->\r\n\t\t</div>\r\n\r\n\t\t<!--end: Datatable -->\r\n\t\t<div *ngIf=\"dataTable.isLoading\" class=\"blockUI blockMsg blockElement\"\r\n\t\t\tstyle=\"z-index: 1011; position: absolute; padding: 0px; margin: 0px; width: 169px; top: 50%; left: 45%; text-align: center; color: rgb(0, 0, 0); border: 0px; cursor: wait;\">\r\n\t\t\t<div class=\"blockui \"><span>{{'COMMON.PLEASE_WAIT' | translate}}</span><span>\r\n\t\t\t\t\t<div class=\"kt-spinner kt-spinner--loader kt-spinner--brand \"></div>\r\n\t\t\t\t</span>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\r\n</div>\r\n<!-- Modal -->\r\n<ng-template #content let-c=\"close\" let-d=\"dismiss\">\r\n\t<div class=\"modal-header\">\r\n\t\t<h4 class=\"modal-title\">{{titlePopup | translate}} {{'ADMIN.GENERAL.SIM_TYPE' | translate  }}\r\n\t\t</h4>\r\n\t\t<button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\r\n\t\t\t<span aria-hidden=\"true\">&times;</span>\r\n\t\t</button>\r\n\t</div>\r\n\t<div class=\"modal-body\">\r\n\t\t<ng-container [ngSwitch]=\"action\">\r\n\t\t\t<ng-container *ngSwitchCase=\"'delete'\" [ngTemplateOutlet]=\"comfimDelete\"\r\n\t\t\t\t[ngTemplateOutletContext]=\"{ item: 'name' }\">\r\n\t\t\t</ng-container>\r\n\t\t\t<ng-container *ngSwitchDefault [ngTemplateOutlet]=\"formEdit\" [ngTemplateOutletContext]=\"{ item: 'name' }\">\r\n\t\t\t</ng-container>\r\n\t\t</ng-container>\r\n\r\n\t</div>\r\n\t<div class=\"modal-footer\">\r\n\t\t<ng-container [ngSwitch]=\"action\">\r\n\t\t\t<button *ngSwitchCase=\"'edit'\" type=\"button\" class=\"btn btn-primary\" [disabled]=\"!simTypeForm.valid\"\r\n\t\t\t\t(click)=\"onSubmit()\">{{'COMMON.ACTIONS.SAVE_CHANGES' | translate}}</button>\r\n\t\t\t<button *ngSwitchCase=\"'add'\" type=\"button\" class=\"btn btn-primary\" [disabled]=\"!simTypeForm.valid\"\r\n\t\t\t\t(click)=\"onSubmit()\">{{'COMMON.ACTIONS.ADD' | translate}}</button>\r\n\t\t\t<button *ngSwitchCase=\"'delete'\" type=\"button\" class=\"btn btn-primary\"\r\n\t\t\t\t(click)=\"onSubmit()\">{{'COMMON.ACTIONS.CONFIRM' | translate}}</button>\r\n\t\t</ng-container>\r\n\t\t<button type=\"button\" class=\"btn btn-secondary\"\r\n\t\t\t(click)=\"c('Close click')\">{{'COMMON.ACTIONS.CLOSE' | translate}}</button>\r\n\t</div>\r\n</ng-template>\r\n<!-- edit form and create form -->\r\n<ng-template #formEdit>\r\n\t<form class=\"kt-form\" [formGroup]=\"simTypeForm\" (ngSubmit)=\"onSubmit()\" novalidate=\"novalidate\">\r\n\t\t<div class=\"kt-portlet__body\">\r\n\t\t\t<div class=\"form-group validate is-invalid\">\r\n\t\t\t\t<label>{{ 'ADMIN.SIM_TYPE.GENERAL.NAME_KEY' | translate }}</label>\r\n\t\t\t\t<input type=\"text\" class=\"form-control\" autocomplete=\"false\" name=\"name_key\" formControlName=\"nameKey\">\r\n\t\t\t\t<div class=\"error invalid-feedback\" *ngIf=\"isControlHasError('nameKey','required')\">\r\n\t\t\t\t\t<strong>{{ 'COMMON.VALIDATION.REQUIRED_FIELD_NAME' | translate }}\r\n\t\t\t\t\t\t{{ 'ADMIN.SIM_TYPE.GENERAL.NAME_KEY' | translate }}</strong>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"form-group\">\r\n\t\t\t\t<label>{{ 'ADMIN.SIM_TYPE.GENERAL.SORT_ORDER' | translate }}</label>\r\n\t\t\t\t<input type=\"number\" class=\"form-control\" autocomplete=\"false\" formControlName=\"sortOrder\">\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</form>\r\n</ng-template>\r\n<ng-template #comfimDelete>\r\n\t<div class=\"alert alert-bold\" role=\"alert\">\r\n\t\t<div class=\"alert-icon\"><i class=\"icon-question-circle\"></i></div>\r\n\t\t<div class=\"alert-text\">{{'COMMON.MESSAGE.COMFIRM_DELETE' | translate}}</div>\r\n\t</div>\r\n</ng-template>\r\n\r\n<!-- end:: Content -->"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/transport-type/transport-type.component.html":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/views/pages/admin/transport-type/transport-type.component.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- begin:: Content -->\r\n<div class=\"kt-portlet kt-portlet--mobile kt-portlet--main\">\r\n        <div class=\"kt-portlet__head kt-portlet__head--lg\">\r\n            <div class=\"kt-portlet__head-label\">\r\n                <h3 class=\"kt-portlet__head-title\">\r\n                    {{'ADMIN.GENERAL.TRANSPORT_TYPE' | translate | uppercase }}\r\n                </h3>\r\n            </div>\r\n            <div class=\"kt-portlet__head-toolbar\">\r\n                <div class=\"kt-portlet__head-wrapper\">\r\n                    <div class=\"dropdown dropdown-inline\">\r\n                        <button type=\"button\" class=\"btn btn-brand btn-icon-sm btn-pill\" (click)=\"buttonAddNew(content)\">\r\n                            <i class=\"flaticon2-plus\"></i> {{'COMMON.ACTIONS.ADD_NEW' | translate}}\r\n                            {{'ADMIN.GENERAL.TRANSPORT_TYPE' | translate }}\r\n                        </button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <!-- <div class=\"kt-portlet__body kt-padding-t-10 kt-padding-b-10\" disabled=\"true\">\r\n          \r\n            </div> -->\r\n        <div class=\"kt-portlet__body kt-portlet__body--fit kt-portlet__body--overflow-y\">\r\n            <!--begin: Search Form -->\r\n            <div class=\"kt-form kt-fork--label-right kt-padding-10 kt-padding-l-20 kt-padding-r-20\">\r\n                <form class=\"\" [formGroup]=\"searchFormTransportType\" (ngSubmit)='searchTransportType(searchFormTransportType)'>\r\n    \r\n                    <div class=\"row align-items-center\">\r\n                        <div class=\"col-xl-8 order-2 order-xl-1\">\r\n                            <div class=\"row align-items-center\">\r\n                                <div class=\"col-md-4 kt-margin-b-20-tablet-and-mobile\">\r\n                            \r\n                                        <div class=\"kt-form__control\">\r\n                                            <input type=\"text\" class=\"form-control\"\r\n                                                placeholder=\"{{'ADMIN.TRANSPORT_TYPE.GENERAL.TRANSPORT_TYPE' | translate}} ...\"\r\n                                                formControlName=\"nameTransportType\">\r\n                                        </div>\r\n                                </div>\r\n                                <div class=\"col-md-4 kt-margin-b-20-tablet-and-mobile\">\r\n                                    <button type=\"submit\" class=\"btn btn-secondary btn-hover-brand\">\r\n                                        <i class=\"icon-search\"></i>{{'COMMON.SEARCH' | translate}}</button>\r\n                                </div>\r\n    \r\n                            </div>\r\n                        </div>\r\n    \r\n                    </div>\r\n                </form>\r\n            </div>\r\n    \r\n            <!--end: Search Form -->\r\n            <!--begin: Datatable -->\r\n            <div resizeObserver (resize)=\"onResize($event.contentRect)\"\r\n                class=\"kt_datatable kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded\"\r\n                [ngClass]=\"{'kt-datatable--error':dataTable.isNotFound}\" id=\"api_methods\"\r\n                style=\"position: static; zoom: 1;\">\r\n                <table class=\"kt-datatable__table\">\r\n                    <thead class=\"kt-datatable__head\">\r\n                        <tr class=\"kt-datatable__row\" style=\"left: 0px;\">\r\n                            <!-- checkbox -->\r\n                            <th *ngIf=\"dataTable.selecter\"\r\n                                class=\"kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check\">\r\n                                <span style=\"width: 20px;\">\r\n                                    <label class=\"kt-checkbox kt-checkbox--single kt-checkbox--solid\">\r\n                                        <input type=\"checkbox\" [(ngModel)]=\"dataTable.isCheckBoxAll\"\r\n                                            (click)=\"dataTable.setCheckBoxAll()\">&nbsp;<span></span>\r\n                                    </label>\r\n                                </span>\r\n                            </th>\r\n                            <!-- end checkbox -->\r\n                            <th *ngIf=\"dataTable.isShowSubRow\" style=\"width: 13px;\"\r\n                                class=\"kt-datatable__cell kt-datatable__cell--sort\">\r\n                                <a class=\"kt-datatable__toggle-detail\">\r\n                                </a>\r\n                            </th>\r\n    \r\n                            <ng-container *ngFor=\"let column of dataTable.getColumns()\">\r\n    \r\n                                <th [ngClass]=\"{'kt-datatable__cell--sorted':column.isSort}\"\r\n                                    [ngStyle]=\"{'display':(!column.isShow?'none':'')}\" [attr.data-field]=\"column.field\"\r\n                                    class=\"kt-datatable__cell kt-datatable__cell--sort {{column.class}}\"\r\n                                    (click)=\"dataTable.sort(column.field)\">\r\n                                    <span [ngStyle]=\"column.style\">{{column.translate | translate}}\r\n                                        <i *ngIf=\"column.isSort\"\r\n                                            [ngClass]=\"{'flaticon2-arrow-down':(column.isSort && column.sortable =='desc'),'flaticon2-arrow-up':(column.isSort && column.sortable =='asc')}\"></i>\r\n                                    </span>\r\n                                </th>\r\n                            </ng-container>\r\n                        </tr>\r\n                    </thead>\r\n                    <tbody class=\"kt-datatable__body\" [ngStyle]=\"dataTable.bodyStyle\">\r\n                        <ng-container *ngFor=\"let item of dataTable.data;let i=index\">\r\n                            <tr [ngClass]=\"{'kt-datatable__row--active':item.dt_selecter}\" class=\"kt-datatable__row\"\r\n                                style=\"left: 0px;\">\r\n                                <!-- checkbox -->\r\n                                <td *ngIf=\"dataTable.selecter\"\r\n                                    class=\"kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check\"\r\n                                    data-field=\"RecordID\"><span style=\"width: 20px;\">\r\n                                        <label class=\"kt-checkbox kt-checkbox--single kt-checkbox--solid\">\r\n                                            <input type=\"checkbox\" [(ngModel)]=\"item.dt_selecter\"\r\n                                                (click)=\"dataTable.setCheckBox(i)\">&nbsp;<span></span>\r\n                                        </label>\r\n                                    </span>\r\n                                </td>\r\n                                <!-- end checkbox -->\r\n                                <td *ngIf=\"dataTable.isShowSubRow\" style=\"width: 13px;\"\r\n                                    class=\"kt-datatable__cell kt-datatable__toggle-detail\">\r\n                                    <a (click)=\"dataTable.showSubRow(i)\" class=\"kt-datatable__toggle-detail\">\r\n                                        <i [ngClass]=\"{'icon-caret-down' : (dataTable.lastRowShow == i), \r\n                                      'icon-caret-right' : (dataTable.lastRowShow != i)}\"></i>\r\n                                    </a>\r\n                                </td>\r\n                                <td [ngStyle]=\"{'display':(!dataTable.getColumns()[0].isShow?'none':'')}\"\r\n                                    class=\"kt-datatable__cell kt-datatable__cell\" data-field=\"id\">\r\n                                    <span\r\n                                        [ngStyle]=\"dataTable.getColumns()[0].style\">{{i+dataTable.getPaginations().form}}</span>\r\n                                </td>\r\n                                <td [ngStyle]=\"{'display':(!dataTable.getColumns()[1].isShow?'none':'')}\"\r\n                                    data-field=\"key_name\" class=\"kt-datatable__cell\">\r\n                                    <span [ngStyle]=\"dataTable.getColumns()[1].style\">{{item.name}}</span>\r\n                                </td>\r\n                               \r\n                                <td [ngStyle]=\"{'display':(!dataTable.getColumns()[2].isShow?'none':'')}\" data-field=\"name\"\r\n                                    class=\"kt-datatable__cell\">\r\n                                    <span [ngStyle]=\"dataTable.getColumns()[2].style\"\r\n                                        translate=\"{{item.limitSpeed}}\">{{item.limitSpeed}}</span>\r\n                                </td>\r\n                                <td [ngStyle]=\"{'display':(!dataTable.getColumns()[3].isShow?'none':'')}\"\r\n                                    data-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n                                    <span [ngStyle]=\"dataTable.getColumns()[3].style\">{{getNameQCVN(item.qcvnCode)}}</span>\r\n                                </td>\r\n                                <td [ngStyle]=\"{'display':(!dataTable.getColumns()[4].isShow?'none':'')}\"\r\n                                    data-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n                                    <span [ngStyle]=\"dataTable.getColumns()[4].style\">{{item.createdBy}}</span>\r\n                                </td>\r\n                                <td [ngStyle]=\"{'display':(!dataTable.getColumns()[5].isShow?'none':'')}\"\r\n                                    data-field=\"hire_date\" class=\"kt-datatable__cell\">\r\n                                <span [ngStyle]=\"dataTable.getColumns()[5].style\">{{item.updatedAt | userDate}}</span>\r\n                                </td>\r\n                                \r\n                                <td [ngStyle]=\"{'display':(!dataTable.getColumns()[6].isShow?'none':'')}\"\r\n                                    data-field=\"Actions\" data-autohide-disabled=\"false\" class=\"kt-datatable__cell\">\r\n                                    <span [ngStyle]=\"dataTable.getColumns()[6].style\"\r\n                                        style=\"overflow: visible; position: relative;\">\r\n                                        <a ngbTooltip=\"{{'COMMON.ACTIONS.EDIT' | translate }}\"\r\n                                            class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                            (click)=\"editTransportType(item.id,content)\">\r\n                                            <i class=\"icon-edit\"></i>\r\n                                        </a>\r\n                                        <a ngbTooltip=\"{{'COMMON.ACTIONS.DELETE' | translate }}\"\r\n                                            class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                            (click)=\"getIdAction(item.id,'#modal-delete-TransportType')\">\r\n                                            <i class=\"icon-trash\"></i>\r\n                                        </a>\r\n                                    </span>\r\n                                </td>\r\n                            </tr>\r\n                            <!-- Sub row -->\r\n                            <tr *ngIf=\"dataTable.lastRowShow==i && dataTable.isShowSubRow\" class=\"kt-datatable__row-detail\">\r\n                                <td class=\"kt-datatable__detail\" colspan=\"9\">\r\n                                    <table>\r\n                                        <tr [ngStyle]=\"{'display':(dataTable.getColumns()[0].isShow?'none':'')}\"\r\n                                            class=\"kt-datatable__row\">\r\n                                            <td class=\"kt-datatable__cell\">\r\n                                                <span>{{dataTable.getColumns()[0].translate | translate}}</span>\r\n                                            </td>\r\n                                            <td class=\"kt-datatable__cell\">\r\n                                                <span\r\n                                                    [ngStyle]=\"dataTable.getColumns()[0].style\">{{i+dataTable.getPaginations().form}}</span>\r\n                                            </td>\r\n                                        </tr>\r\n                                        <tr [ngStyle]=\"{'display':(dataTable.getColumns()[1].isShow?'none':'')}\"\r\n                                            class=\"kt-datatable__row\">\r\n                                            <td class=\"kt-datatable__cell\">\r\n                                                <span>{{dataTable.getColumns()[1].translate | translate}}</span>\r\n                                            </td>\r\n                                            <td class=\"kt-datatable__cell\">\r\n                                                <span [ngStyle]=\"dataTable.getColumns()[1].style\">{{item.name}}</span>\r\n                                            </td>\r\n                                        </tr>\r\n                                        <tr [ngStyle]=\"{'display':(dataTable.getColumns()[2].isShow?'none':'')}\"\r\n                                            class=\"kt-datatable__row\">\r\n                                            <td class=\"kt-datatable__cell\">\r\n                                                <span>{{dataTable.getColumns()[2].translate | translate}}</span>\r\n                                            </td>\r\n                                            <td class=\"kt-datatable__cell\">\r\n                                                <span [ngStyle]=\"dataTable.getColumns()[2].style\"\r\n                                                    translate=\"{{item.limitSpeed}}\">{{item.limitSpeed}}</span>\r\n                                            </td>\r\n                                        </tr>\r\n                                        \r\n                                        <tr [ngStyle]=\"{'display':(dataTable.getColumns()[3].isShow?'none':'')}\"\r\n                                        class=\"kt-datatable__row\">\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span>{{dataTable.getColumns()[3].translate | translate}}</span>\r\n                                        </td>\r\n                                        <td class=\"kt-datatable__cell\">\r\n                                            <span [ngStyle]=\"dataTable.getColumns()[3].style\"\r\n                                                >{{getNameQCVN(item.qcvnCode)}}</span>\r\n                                        </td>\r\n                                    </tr>\r\n                                        <tr [ngStyle]=\"{'display':(dataTable.getColumns()[4].isShow?'none':'')}\"\r\n                                            class=\"kt-datatable__row\">\r\n                                            <td class=\"kt-datatable__cell\">\r\n                                                <span>{{dataTable.getColumns()[4].translate | translate}}</span>\r\n                                            </td>\r\n                                            <td class=\"kt-datatable__cell\">\r\n                                                <span [ngStyle]=\"dataTable.getColumns()[4].style\"\r\n                                                    translate=\"{{item.createdBy}}\">{{item.createdBy}}</span>\r\n                                            </td>\r\n                                        </tr>\r\n                                        <tr [ngStyle]=\"{'display':(dataTable.getColumns()[5].isShow?'none':'')}\"\r\n                                            class=\"kt-datatable__row\">\r\n                                            <td class=\"kt-datatable__cell\">\r\n                                                <span>{{dataTable.getColumns()[5].translate | translate}}</span>\r\n                                            </td>\r\n                                            <td class=\"kt-datatable__cell\">\r\n                                                <span [ngStyle]=\"dataTable.getColumns()[5].style\"\r\n                                                    translate=\"{{item.updatedAt}}\">{{item.updatedAt | userDate}}</span>\r\n                                            </td>\r\n                                        </tr>\r\n    \r\n    \r\n                                        <tr [ngStyle]=\"{'display':(dataTable.getColumns()[6].isShow?'none':'')}\"\r\n                                            class=\"kt-datatable__row\">\r\n                                            <td class=\"kt-datatable__cell\">\r\n                                                <span>{{dataTable.getColumns()[5].translate | translate}}</span>\r\n                                            </td>\r\n                                            <td class=\"kt-datatable__cell\">\r\n                                                <span [ngStyle]=\"dataTable.getColumns()[6].style\"\r\n                                                    style=\"overflow: visible; position: relative;\">\r\n                                                    <a ngbTooltip=\"{{'COMMON.ACTIONS.EDIT' | translate }}\"\r\n                                                        class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                                        (click)=\"editTransportType(item.id,'#modal-TransportType')\">\r\n                                                        <i class=\"icon-edit\"></i>\r\n                                                    </a>\r\n                                                    <a ngbTooltip=\"{{'COMMON.ACTIONS.DELETE' | translate }}\"\r\n                                                        class=\"btn btn-sm btn-clean btn-icon btn-icon-md\"\r\n                                                        (click)=\"getIdAction(item.id,'#modal-delete-TransportType')\">\r\n                                                        <i class=\"icon-trash\"></i>\r\n                                                    </a>\r\n                                                </span>\r\n                                            </td>\r\n                                        </tr>\r\n                                    </table>\r\n                                </td>\r\n                            </tr>\r\n                        </ng-container>\r\n                        <!-- No records found -->\r\n                        <div *ngIf=\"dataTable.isNotFound\" class=\"kt-datatable--error\">\r\n                            <kt-not-found></kt-not-found>\r\n                        </div>\r\n                    </tbody>\r\n    \r\n    \r\n                </table>\r\n    \r\n                <div class=\"kt-datatable__pager kt-datatable--paging-loaded\" *ngIf=\"dataTable.data?.length != 0\">\r\n                    <ul class=\"kt-datatable__pager-nav\">\r\n                        <li><a title=\"First\" class=\"kt-datatable__pager-link kt-datatable__pager-link--first\"\r\n                                [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getFirstPage())}\"\r\n                                (click)=\"dataTable.gotoPage(dataTable.getFirstPage())\">\r\n                                <i class=\"flaticon2-fast-back\"></i></a></li>\r\n                        <li><a title=\"Previous\" class=\"kt-datatable__pager-link kt-datatable__pager-link--prev\"\r\n                                [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getFirstPage())}\"\r\n                                (click)=\"dataTable.gotoPage(dataTable.getCurrentPage()-1)\">\r\n                                <i class=\"flaticon2-back\"></i></a></li>\r\n                        <li></li>\r\n                        <li style=\"display: none;\"><input type=\"text\" class=\"kt-pager-input form-control\"\r\n                                title=\"Page number\"></li>\r\n                        <li *ngFor=\"let page of dataTable.getPageDisplay()\">\r\n                            <a (click)=\"dataTable.gotoPage(page)\"\r\n                                class=\"kt-datatable__pager-link kt-datatable__pager-link-number \"\r\n                                [ngClass]=\"{'kt-datatable__pager-link--active':page==dataTable.getCurrentPage()}\"\r\n                                data-page=\"1\" title=\"{{page}}\">{{page}}</a>\r\n                        </li>\r\n                        <li style=\"\"></li>\r\n                        <li><a title=\"Next\" class=\"kt-datatable__pager-link kt-datatable__pager-link--next \"\r\n                                [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getLastPage())}\"\r\n                                (click)=\"dataTable.gotoPage(dataTable.getCurrentPage()+1)\"><i\r\n                                    class=\" flaticon2-next\"></i></a></li>\r\n                        <li><a title=\"Last\"\r\n                                [ngClass]=\"{'kt-datatable__pager-link--disabled':(dataTable.getCurrentPage()==dataTable.getLastPage())}\"\r\n                                class=\"kt-datatable__pager-link kt-datatable__pager-link--last\"\r\n                                (click)=\"dataTable.gotoPage(dataTable.getLastPage())\">\r\n                                <i class=\"flaticon2-fast-next\"></i></a></li>\r\n                    </ul>\r\n                    <div class=\"kt-datatable__pager-info\">\r\n                        <div class=\"dropdown bootstrap-select kt-datatable__pager-size\" style=\"width: 60px;\">\r\n                            <select (change)=\"dataTable.setPageSize($event.target.value)\"\r\n                                class=\"selectpicker kt-datatable__pager-size\" title=\"Select page size\" data-selected=\"30\"\r\n                                data-width=\"60px\">\r\n                                <option *ngFor=\"let item of dataTable.getPaginationSelect()\"\r\n                                    [selected]=\"item==dataTable.getPaginations().pageSize\" value=\"{{item}}\" title=\"{{item}}\"\r\n                                    data-toggle=\"tooltip\">{{item}}</option>\r\n                            </select>\r\n                        </div><span class=\"kt-datatable__pager-detail\">{{'COMMON.SHOW' | translate}}\r\n                            {{dataTable.getPaginations().form}} -\r\n                            {{dataTable.getPaginations().to}} {{'COMMON.OF' | translate}}\r\n                            {{dataTable.getPaginations().total}}</span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n    \r\n            <!--end: Datatable -->\r\n            <div *ngIf=\"dataTable.isLoading\" class=\"blockUI blockMsg blockElement\"\r\n                style=\"z-index: 1011; position: absolute; padding: 0px; margin: 0px; width: 169px; top: 50%; left: 45%; text-align: center; color: rgb(0, 0, 0); border: 0px; cursor: wait;\">\r\n                <div class=\"blockui \"><span>{{'COMMON.PLEASE_WAIT' | translate}}</span><span>\r\n                        <div class=\"kt-spinner kt-spinner--loader kt-spinner--brand \"></div>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    \r\n    <!-- end:: Content -->\r\n    \r\n    <!-- modal popup -->\r\n    <ng-template #content let-modal>\r\n                <div class=\"modal-header \">\r\n                    <h5 class=\"modal-title\" *ngIf=\"isEdit\">\r\n                        {{'ADMIN.TRANSPORT_TYPE.GENERAL.TITLE_FORM_EDIT' | translate}}\r\n                    </h5>\r\n                    <h5 class=\"modal-title\" *ngIf=\"!isEdit\">\r\n                        {{'ADMIN.TRANSPORT_TYPE.GENERAL.TITLE_FORM_ADD' | translate}}\r\n                    </h5>\r\n                    <button type=\"button\" class=\"close\" (click)=\"modal.dismiss('Cross click')\">\r\n                        <span aria-hidden=\"true\">×</span>\r\n                    </button>\r\n                </div>\r\n                <form [formGroup]=\"formTransportType\" (ngSubmit)='onSubmitTransportType(formTransportType)' class=\"form-transport\">\r\n                    <div class=\"modal-body div-scrollbar modal-body-TransportType\" *ngFor=\"let data of dataDefault\">\r\n            \r\n                                    <div class=\"form-group row\">\r\n                                        <div class=\"col-lg-12\">\r\n                                            <div class=\"show-error\">\r\n                                                <label class=\"\">{{'ADMIN.PERMISSION.GENERAL.NAME' | translate}} </label>\r\n                                                <app-control-messages [control]=\"f['nameTransportType']\"></app-control-messages>\r\n                                                <span class=\"text-danger\"> *</span>\r\n                                            </div>\r\n                                            <div class=\"input-group\">\r\n                                              \r\n                                                <input [ngModel]=\"isEdit?data.name:''\" type=\"text\" class=\"form-control\"\r\n                                                    placeholder=\"\" formControlName=\"nameTransportType\">\r\n                                            </div>\r\n                                        </div>\r\n                                   </div>\r\n\r\n                                   <div class=\"form-group row\">\r\n                                        <div class=\"col-lg-12\">\r\n                                            <div class=\"show-error\">\r\n                                                <label class=\"\">{{'COMMON.COLUMN.NAME_KEY' | translate}} </label>\r\n                                                <app-control-messages [control]=\"f['nameKey']\"></app-control-messages>\r\n                                                <span class=\"text-danger\"> *</span>\r\n                                            </div>\r\n                                            <div class=\"input-group\">\r\n                                              \r\n                                                <input [ngModel]=\"isEdit?data.nameKey:''\" type=\"text\" class=\"form-control\"\r\n                                                    placeholder=\"\" formControlName=\"nameKey\">\r\n                                            </div>\r\n                                        </div>\r\n                                   </div>\r\n                                \r\n                                \r\n            \r\n                                    <div class=\"form-group row\">\r\n                                            <div class=\"col-lg-12\">\r\n                                                <div class=\"show-error\">\r\n                                                    <label class=\"\">{{'ADMIN.TRANSPORT_TYPE.COLUMN.LIMITSPEDD' | translate}}\r\n                                                    </label>\r\n                                                    <!-- <app-control-messages [control]=\"f['username']\"></app-control-messages> -->\r\n                                                </div>\r\n                                                <div class=\"input-group\">\r\n                                                    <input [ngModel]=\"isEdit?data.limitSpeed:''\" type=\"text\" class=\"form-control\"\r\n                                                    placeholder=\"\" formControlName=\"limitSpeed\">\r\n                                                </div>\r\n        \r\n                                            </div>\r\n                                    </div>\r\n                                    \r\n                                    <div class=\"form-group row\">\r\n                                        <div class=\"col-lg-12\">\r\n                                                <div class=\"show-error\">\r\n                                                        <label class=\"\">{{'ADMIN.TRANSPORT_TYPE.COLUMN.QNCN_CODE' | translate}}\r\n                                                        </label>\r\n                                                        <!-- <app-control-messages [control]=\"f['username']\"></app-control-messages> -->\r\n                                                    </div>\r\n                                                    <div class=\"input-group\">\r\n                                                            <select [ngModel]=\"isEdit?data.qcvnCode:'-1'\" \r\n                                                            class=\"form-control kt_selectpicker padding-7\"\r\n                                                            formControlName='qcvnCode'>\r\n                                                            <option value=\"-1\">\r\n                                                                {{'COMMON.MESSAGE.NOTHING_SELECTED' | translate}}\r\n                                                            </option>\r\n                                                            <option  *ngFor='let qcvn of listQCVN' [ngValue]=\"qcvn.id\">\r\n                                                                {{qcvn.name}}\r\n                                                            </option>\r\n                                                        </select>\r\n                                                    </div>\r\n                                               \r\n                                        </div>\r\n                                    </div>\r\n\r\n                                    <div class=\"form-group row\" *ngIf= \"isEdit\">\r\n                                            <div class=\"col-lg-12\">\r\n                                                <div class=\"show-error\">\r\n                                                    <label class=\"\">{{'COMMON.COLUMN.CREATED_BY' | translate}} </label>\r\n                                                </div>\r\n                                                <div class=\"input-group\">\r\n\r\n                                                    <input type=\"text\" class=\"form-control\" disabled\r\n                                                    placeholder=\"\" value=\"{{data.createdBy}}\">\r\n                                                </div>\r\n                                            </div>\r\n                                    </div>\r\n                                   \r\n                                    <div class=\"form-group row\" *ngIf= \"isEdit\">\r\n                                            <div class=\"col-lg-12\">\r\n                                                <div class=\"show-error\">\r\n                                                    <label class=\"\">{{'COMMON.COLUMN.UPDATED_DATE' | translate}} </label>\r\n                                                </div>\r\n                                                <div class=\"input-group\">\r\n                                                    \r\n                                                    <input type=\"text\" class=\"form-control\" disabled\r\n                                                        placeholder=\"\" value=\"{{data.updatedAt | userDate}}\">\r\n                                                </div>\r\n                                            </div>\r\n                                    </div>\r\n                                    \r\n                            \r\n                          \r\n                    </div>\r\n                    <!--end body modal-->\r\n                    <div class=\"modal-footer\">\r\n                        <span *ngIf=\"isPermissionError\" class=\"text-danger error-message-modal\">\r\n                            {{errorMessage}}\r\n                        </span>\r\n                        <button *ngIf=\"!isEdit\" type=\"submit\" [disabled]=\"formTransportType.invalid\" class=\"btn btn-primary \">\r\n                            {{'COMMON.ACTIONS.ADD' | translate }}\r\n                        </button>\r\n                        <button *ngIf=\"isEdit\" type=\"submit\" [disabled]=\"formTransportType.invalid\" class=\"btn btn-primary \">\r\n                            {{'COMMON.ACTIONS.SAVE_CHANGES' | translate }}\r\n                        </button>\r\n    \r\n                        <button type=\"button\" class=\"btn btn-secondary\"\r\n                        (click)=\"modal.close()\">{{'COMMON.ACTIONS.CANCEL' | translate}}</button>\r\n                        <!-- <button type=\"button\" class=\"btn btn-outline-danger\" data-dismiss='modal'>Close</button> -->\r\n                    </div>\r\n                </form>\r\n    </ng-template>\r\n    <!-- end modal popup -->\r\n    <!-- modal delete -->\r\n    <div class=\"modal fade  modal-confirm kt-mt-50\" tabindex=\"-1\" TransportType=\"dialog\" id=\"modal-delete-TransportType\"\r\n        aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\r\n        <div class=\"modal-dialog modal-sm\">\r\n            <div class=\"modal-content\">\r\n                <div class=\"modal-header\">\r\n                    <h5 class=\"modal-title\">{{'COMMON.ACTIONS.DELETE' | translate }}\r\n                        {{'ADMIN.ROLE.GENERAL.ROLE' | translate  }}</h5>\r\n                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\r\n                        <span aria-hidden=\"true\">×</span>\r\n                    </button>\r\n                </div>\r\n                <div class=\"modal-body\">\r\n                    <i class=\"fa fa-question-circle\"></i>{{'COMMON.MESSAGE.COMFIRM_DELETE' | translate }}\r\n                </div>\r\n                <div class=\"modal-footer\">\r\n    \r\n                    <button type=\"button\" class=\"btn btn-primary\"\r\n                        (click)=\"deleteTransportType()\">{{'COMMON.ACTIONS.CONFIRM' | translate }}</button>\r\n                    <button type=\"button\" class=\"btn btn-secondary\"\r\n                        data-dismiss=\"modal\">{{'COMMON.ACTIONS.CANCEL' | translate }}</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <!-- end modal delete -->\r\n    <!-- end:: Content -->\r\n    "

/***/ }),

/***/ "./src/app/views/pages/admin/admin.component.scss":
/*!********************************************************!*\
  !*** ./src/app/views/pages/admin/admin.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL2FkbWluL2FkbWluLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/pages/admin/admin.component.ts":
/*!******************************************************!*\
  !*** ./src/app/views/pages/admin/admin.component.ts ***!
  \******************************************************/
/*! exports provided: AdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminComponent", function() { return AdminComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AdminComponent = /** @class */ (function () {
    function AdminComponent() {
    }
    AdminComponent.prototype.ngOnInit = function () {
    };
    AdminComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-admin',
            template: __webpack_require__(/*! raw-loader!./admin.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/admin.component.html"),
            styles: [__webpack_require__(/*! ./admin.component.scss */ "./src/app/views/pages/admin/admin.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AdminComponent);
    return AdminComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/admin/admin.module.ts":
/*!***************************************************!*\
  !*** ./src/app/views/pages/admin/admin.module.ts ***!
  \***************************************************/
/*! exports provided: AdminModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminModule", function() { return AdminModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular-ng-autocomplete */ "./node_modules/angular-ng-autocomplete/fesm5/angular-ng-autocomplete.js");
/* harmony import */ var _app_core_common_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @app/core/common/shared.module */ "./src/app/core/common/shared.module.ts");
/* harmony import */ var _permission_permission_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./permission/permission.component */ "./src/app/views/pages/admin/permission/permission.component.ts");
/* harmony import */ var _role_role_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./role/role.component */ "./src/app/views/pages/admin/role/role.component.ts");
/* harmony import */ var _login_page_login_page_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./login-page/login-page.component */ "./src/app/views/pages/admin/login-page/login-page.component.ts");
/* harmony import */ var _sim_type_sim_type_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./sim-type/sim-type.component */ "./src/app/views/pages/admin/sim-type/sim-type.component.ts");
/* harmony import */ var _device_type_device_type_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./device-type/device-type.component */ "./src/app/views/pages/admin/device-type/device-type.component.ts");
/* harmony import */ var _sensor_type_sensor_type_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./sensor-type/sensor-type.component */ "./src/app/views/pages/admin/sensor-type/sensor-type.component.ts");
/* harmony import */ var _icon_type_icon_type_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./icon-type/icon-type.component */ "./src/app/views/pages/admin/icon-type/icon-type.component.ts");
/* harmony import */ var _admin_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./admin.component */ "./src/app/views/pages/admin/admin.component.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _page_template_page_template_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./page-template/page-template.component */ "./src/app/views/pages/admin/page-template/page-template.component.ts");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _app_views_partials_content_widgets_widget_module__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @app/views/partials/content/widgets/widget.module */ "./src/app/views/partials/content/widgets/widget.module.ts");
/* harmony import */ var angular_tree_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! angular-tree-component */ "./node_modules/angular-tree-component/dist/angular-tree-component.js");
/* harmony import */ var _transport_type_transport_type_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./transport-type/transport-type.component */ "./src/app/views/pages/admin/transport-type/transport-type.component.ts");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @core/auth */ "./src/app/core/auth/index.ts");

// Angular




// Translate

// ng autocomplete

//services

// Components















var routes = [
    {
        path: '',
        component: _admin_component__WEBPACK_IMPORTED_MODULE_15__["AdminComponent"],
        children: [
            {
                path: 'permission',
                component: _permission_permission_component__WEBPACK_IMPORTED_MODULE_8__["PermissionComponent"],
                canActivate: [_core_auth__WEBPACK_IMPORTED_MODULE_22__["AuthGuard"], _core_auth__WEBPACK_IMPORTED_MODULE_22__["ModuleGuard"]],
                data: {
                    permisison: 'ROLE_admin.permission',
                }
            },
            {
                path: 'role',
                component: _role_role_component__WEBPACK_IMPORTED_MODULE_9__["RoleComponent"],
                canActivate: [_core_auth__WEBPACK_IMPORTED_MODULE_22__["AuthGuard"], _core_auth__WEBPACK_IMPORTED_MODULE_22__["ModuleGuard"]],
                data: {
                    permisison: 'ROLE_admin.role',
                }
            },
            {
                path: 'login-page',
                component: _login_page_login_page_component__WEBPACK_IMPORTED_MODULE_10__["LoginPageComponent"],
                canActivate: [_core_auth__WEBPACK_IMPORTED_MODULE_22__["AuthGuard"], _core_auth__WEBPACK_IMPORTED_MODULE_22__["ModuleGuard"]],
                data: {
                    permisison: 'ROLE_admin.login_page',
                }
            },
            {
                path: 'device-type',
                component: _device_type_device_type_component__WEBPACK_IMPORTED_MODULE_12__["DeviceTypeComponent"],
                canActivate: [_core_auth__WEBPACK_IMPORTED_MODULE_22__["AuthGuard"], _core_auth__WEBPACK_IMPORTED_MODULE_22__["ModuleGuard"]],
                data: {
                    permisison: 'ROLE_admin.device_type',
                }
            },
            {
                path: 'sim-type',
                component: _sim_type_sim_type_component__WEBPACK_IMPORTED_MODULE_11__["SimTypeComponent"],
                canActivate: [_core_auth__WEBPACK_IMPORTED_MODULE_22__["AuthGuard"], _core_auth__WEBPACK_IMPORTED_MODULE_22__["ModuleGuard"]],
                data: {
                    permisison: 'ROLE_admin.sim_type',
                }
            },
            {
                path: 'sensor-type',
                component: _sensor_type_sensor_type_component__WEBPACK_IMPORTED_MODULE_13__["SensorTypeComponent"],
                canActivate: [_core_auth__WEBPACK_IMPORTED_MODULE_22__["AuthGuard"], _core_auth__WEBPACK_IMPORTED_MODULE_22__["ModuleGuard"]],
                data: {
                    permisison: 'ROLE_admin.sensor_template',
                }
            },
            {
                path: 'icon-type',
                component: _icon_type_icon_type_component__WEBPACK_IMPORTED_MODULE_14__["IconTypeComponent"],
                canActivate: [_core_auth__WEBPACK_IMPORTED_MODULE_22__["AuthGuard"], _core_auth__WEBPACK_IMPORTED_MODULE_22__["ModuleGuard"]],
                data: {
                    permisison: 'ROLE_admin.icon',
                }
            },
            {
                path: 'page-template',
                component: _page_template_page_template_component__WEBPACK_IMPORTED_MODULE_17__["PageTemplateComponent"],
            },
            {
                path: 'transport-type',
                component: _transport_type_transport_type_component__WEBPACK_IMPORTED_MODULE_21__["TransportTypeComponent"],
                canActivate: [_core_auth__WEBPACK_IMPORTED_MODULE_22__["AuthGuard"], _core_auth__WEBPACK_IMPORTED_MODULE_22__["ModuleGuard"]],
                data: {
                    permisison: 'ROLE_admin.transport_type',
                }
            },
        ]
    }
];
var AdminModule = /** @class */ (function () {
    function AdminModule() {
    }
    AdminModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _core_core_module__WEBPACK_IMPORTED_MODULE_18__["CoreModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forChild(),
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_16__["NgbModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_16__["NgbDropdownModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_16__["NgbModalModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                angular_ng_autocomplete__WEBPACK_IMPORTED_MODULE_6__["AutocompleteLibModule"],
                _app_core_common_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"],
                _app_views_partials_content_widgets_widget_module__WEBPACK_IMPORTED_MODULE_19__["WidgetModule"],
                angular_tree_component__WEBPACK_IMPORTED_MODULE_20__["TreeModule"].forRoot(),
            ],
            providers: [],
            declarations: [
                _transport_type_transport_type_component__WEBPACK_IMPORTED_MODULE_21__["TransportTypeComponent"],
                _permission_permission_component__WEBPACK_IMPORTED_MODULE_8__["PermissionComponent"],
                _role_role_component__WEBPACK_IMPORTED_MODULE_9__["RoleComponent"],
                _login_page_login_page_component__WEBPACK_IMPORTED_MODULE_10__["LoginPageComponent"],
                _sim_type_sim_type_component__WEBPACK_IMPORTED_MODULE_11__["SimTypeComponent"],
                _device_type_device_type_component__WEBPACK_IMPORTED_MODULE_12__["DeviceTypeComponent"],
                _sensor_type_sensor_type_component__WEBPACK_IMPORTED_MODULE_13__["SensorTypeComponent"],
                _icon_type_icon_type_component__WEBPACK_IMPORTED_MODULE_14__["IconTypeComponent"],
                _admin_component__WEBPACK_IMPORTED_MODULE_15__["AdminComponent"],
                _page_template_page_template_component__WEBPACK_IMPORTED_MODULE_17__["PageTemplateComponent"],
            ],
        })
    ], AdminModule);
    return AdminModule;
}());



/***/ }),

/***/ "./src/app/views/pages/admin/device-type/device-type.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/views/pages/admin/device-type/device-type.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL2FkbWluL2RldmljZS10eXBlL2RldmljZS10eXBlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/pages/admin/device-type/device-type.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/views/pages/admin/device-type/device-type.component.ts ***!
  \************************************************************************/
/*! exports provided: DeviceTypeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceTypeComponent", function() { return DeviceTypeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _core_admin__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @core/admin */ "./src/app/core/admin/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _core_base_layout_models_datatable_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @core/_base/layout/models/datatable.model */ "./src/app/core/_base/layout/models/datatable.model.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");









var TITLE_FORM_EDIT = "COMMON.ACTIONS.EDIT";
var TITLE_FORM_ADD = "COMMON.ACTIONS.ADD";
var TITLE_FORM_DELETE = "COMMON.ACTIONS.DELETE";
var ACTION;
(function (ACTION) {
    ACTION[ACTION["edit"] = 0] = "edit";
    ACTION[ACTION["add"] = 1] = "add";
    ACTION[ACTION["delete"] = 2] = "delete";
})(ACTION || (ACTION = {}));
;
var DeviceTypeComponent = /** @class */ (function () {
    function DeviceTypeComponent(cdr, modalService, fb, deviceTypeService, currentService, http) {
        this.cdr = cdr;
        this.modalService = modalService;
        this.fb = fb;
        this.deviceTypeService = deviceTypeService;
        this.currentService = currentService;
        this.http = http;
        this.dataTable = new _core_base_layout_models_datatable_model__WEBPACK_IMPORTED_MODULE_7__["DataTable"]();
        this.titlePopup = TITLE_FORM_ADD;
        this.action = "";
        this.tableConfig = {
            pagination: [
                10, 20, 30, 50
            ],
            columns: [
                {
                    title: '#',
                    field: 'id',
                    allowSort: false,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '20px' },
                    width: 20,
                    class: 't-datatable__cell--center',
                    translate: '#',
                    autoHide: false,
                },
                {
                    title: 'Name',
                    field: 'nameKey',
                    allowSort: true,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '182px' },
                    width: 182,
                    class: '',
                    translate: 'COMMON.COLUMN.NAME',
                    autoHide: false,
                },
                {
                    title: 'Description',
                    field: 'description',
                    allowSort: false,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '182px' },
                    width: 182,
                    class: '',
                    translate: 'COMMON.COLUMN.DESCRIPTION',
                    autoHide: true,
                },
                {
                    title: 'Modifield By',
                    field: 'modifieldBy',
                    allowSort: true,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '182px' },
                    width: 182,
                    class: '',
                    translate: 'COMMON.COLUMN.MODIFIELD_BY',
                    autoHide: true,
                },
                {
                    title: 'Updated',
                    field: 'updatedDate',
                    allowSort: true,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '182px' },
                    width: 182,
                    class: '',
                    translate: 'COMMON.COLUMN.UPDATED_DATE',
                    autoHide: true,
                },
                {
                    title: 'Sort Order',
                    field: 'sortOrder',
                    allowSort: true,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '182px' },
                    class: '',
                    width: 182,
                    translate: 'COMMON.COLUMN.SORT_ORDER',
                    autoHide: true,
                },
                {
                    title: 'Actions',
                    field: 'action',
                    allowSort: false,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '110px' },
                    class: '',
                    width: 110,
                    translate: 'COMMON.ACTIONS.ACTIONS',
                    autoHide: false,
                },
            ]
        };
        this.unsubscribe = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.dataTable.init({
            data: [],
            totalRecod: 0,
            paginationSelect: [1, 2, 5, 10, 20],
            columns: this.tableConfig.columns,
            formSearch: '#formSearch',
            isDebug: true,
            layout: {
                body: {
                    scrollable: false,
                    maxHeight: 600,
                },
            }
        });
        this.currentReason = {};
    }
    DeviceTypeComponent.prototype.ngOnInit = function () {
        var _this_1 = this;
        // listening dataTable event
        this.dataTable.eventUpdate.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (option) {
            _this_1.getData(option);
        })).subscribe();
        this.dataTable.reload({});
        $(function () {
            $('select').selectpicker();
        });
    };
    /**
     * Create form
     * @param DeviceType model
     */
    DeviceTypeComponent.prototype.createFormAdd = function (model) {
        this.formAdd = this.fb.group({
            name: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            description: [""],
            sortOrder: [1],
        });
        this.commandsForm = this.fb.group({
            itemRows: this.fb.array([])
        });
    };
    DeviceTypeComponent.prototype.createFormEdit = function (model) {
        this.formEdit = this.fb.group({
            name: [model.name, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            description: [model.description],
            createdBy: [model.createdBy],
            createdDate: [model.createdDate],
            updatedBy: [model.updatedBy],
            updatedDate: [model.updatedDate],
            sortOrder: [model.sortOrder],
        });
        var contentForm = [];
        var _this = this;
        if (model['command']) {
            var command_1 = model['command'];
            Object.keys(model['command']).forEach(function (x) {
                contentForm.push(_this.fb.group({
                    name: [x],
                    commandStr: [command_1[x]],
                }));
            });
        }
        this.commandsForm = this.fb.group({
            itemRows: this.fb.array(contentForm)
        });
    };
    /**
     * Get list by data option
     * @param option option search list
     */
    DeviceTypeComponent.prototype.getData = function (option) {
        var _this_1 = this;
        this.deviceTypeService.list({ params: option }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (data) {
            _this_1.dataTable.update({
                data: data.result.content,
                totalRecod: data.result.totalRecord
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
            _this_1.cdr.markForCheck();
        })).subscribe();
    };
    /**
     * Open popup template
     * @param content template
     * @param type action type
     * @param data data for content
     */
    DeviceTypeComponent.prototype.open = function (content, type, item) {
        this.action = type;
        switch (type) {
            case 'edit':
                this.editFnc(item.id, content);
                break;
            case 'add':
                this.addFnc(content);
                break;
            case 'delete':
                this.deleteFnc(item.id, content);
                break;
        }
    };
    DeviceTypeComponent.prototype.editFnc = function (id, content) {
        var _this_1 = this;
        this.titlePopup = TITLE_FORM_EDIT;
        this.currentForm = this.formEdit;
        this.deviceTypeService.detail(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (body) {
            _this_1.currentModel = body.result;
            _this_1.createFormEdit(_this_1.currentModel);
            _this_1.currentForm = _this_1.formEdit;
            _this_1.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', size: 'lg', backdrop: 'static' }).result.then(function (result) {
                _this_1.closeResult = "Closed with: " + result;
            }, function (reason) {
                _this_1.closeResult = "Dismissed " + _this_1.getDismissReason(reason);
                _this_1.currentReason = reason;
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
            _this_1.cdr.markForCheck();
        })).subscribe();
    };
    DeviceTypeComponent.prototype.addFnc = function (content) {
        var _this_1 = this;
        this.titlePopup = TITLE_FORM_ADD;
        this.createFormAdd();
        this.currentForm = this.formAdd;
        this.currentModel = new _core_admin__WEBPACK_IMPORTED_MODULE_5__["DeviceType"]();
        this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', size: 'lg', backdrop: 'static' }).result.then(function (result) {
            _this_1.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this_1.closeResult = "Dismissed " + _this_1.getDismissReason(reason);
            _this_1.currentReason = reason;
        });
    };
    DeviceTypeComponent.prototype.deleteFnc = function (id, content) {
        var _this_1 = this;
        this.titlePopup = TITLE_FORM_DELETE;
        this.currentModel = new _core_admin__WEBPACK_IMPORTED_MODULE_5__["DeviceType"]();
        this.currentModel.id = id;
        this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable', backdrop: 'static' }).result.then(function (result) {
            _this_1.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this_1.closeResult = "Dismissed " + _this_1.getDismissReason(reason);
            _this_1.currentReason = reason;
        });
    };
    /**
     * Submit actions
     */
    DeviceTypeComponent.prototype.onSubmit = function () {
        switch (this.action) {
            case "edit":
                if (!this.currentForm.invalid) {
                    this.editAction();
                }
                break;
            case "add":
                if (!this.currentForm.invalid) {
                    this.addAction();
                }
                break;
            case "delete":
                this.deleteAction();
                break;
        }
    };
    DeviceTypeComponent.prototype.addAction = function () {
        var _this_1 = this;
        this.currentModel.name = this.currentForm.get('name').value;
        this.currentModel.sortOrder = this.currentForm.get('sortOrder').value;
        this.currentModel.description = this.currentForm.get('description').value;
        this.currentModel.commands = this.formArr.value;
        this.currentService.create(this.currentModel, { notifyGlobal: true }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (data) {
            if (data.status == 201) {
                _this_1.modalService.dismissAll(_this_1.currentReason);
                _this_1.dataTable.reload({});
            }
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
            _this_1.cdr.markForCheck();
        })).subscribe();
    };
    DeviceTypeComponent.prototype.editAction = function () {
        var _this_1 = this;
        this.currentModel.name = this.currentForm.get('name').value;
        this.currentModel.sortOrder = this.currentForm.get('sortOrder').value;
        this.currentModel.description = this.currentForm.get('description').value;
        this.currentModel.commands = this.formArr.value;
        this.currentService.update(this.currentModel, { notifyGlobal: true }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (data) {
            if (data.status == 200) {
                _this_1.modalService.dismissAll(_this_1.currentReason);
                _this_1.dataTable.reload({});
            }
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
            _this_1.cdr.markForCheck();
        })).subscribe();
    };
    DeviceTypeComponent.prototype.deleteAction = function () {
        var _this_1 = this;
        this.deviceTypeService.delete(this.currentModel.id, { notifyGlobal: true }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (data) {
            if (data.status == 200) {
                _this_1.modalService.dismissAll(_this_1.currentReason);
                _this_1.dataTable.reload({});
            }
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
            _this_1.cdr.markForCheck();
        })).subscribe();
    };
    DeviceTypeComponent.prototype.removeCommand = function (index) {
        this.formArr.removeAt(index);
    };
    DeviceTypeComponent.prototype.addCommand = function () {
        this.formArr.push(this.fb.group({
            name: [''],
            commandStr: [''],
        }));
    };
    Object.defineProperty(DeviceTypeComponent.prototype, "formArr", {
        get: function () {
            if (this.commandsForm) {
                return this.commandsForm.get('itemRows');
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Checking control validation
     *
     * @param controlName: string => Equals to formControlName
     * @param validationType: string => Equals to valitors name
     */
    DeviceTypeComponent.prototype.isControlHasError = function (controlName, validationType) {
        var control = this.currentForm.controls[controlName];
        if (!control) {
            return false;
        }
        var result = control.hasError(validationType) && (control.dirty || control.touched);
        return result;
    };
    /**
     * Change size databale
     * @param $elm
     */
    DeviceTypeComponent.prototype.onResize = function ($elm) {
        this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    };
    /**
   * Dismiss Reason Popup
   * @param reason
   */
    DeviceTypeComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    DeviceTypeComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"] },
        { type: _core_admin__WEBPACK_IMPORTED_MODULE_5__["DeviceTypeService"] },
        { type: _core_admin__WEBPACK_IMPORTED_MODULE_5__["DeviceTypeService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"] }
    ]; };
    DeviceTypeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-device-type',
            template: __webpack_require__(/*! raw-loader!./device-type.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/device-type/device-type.component.html"),
            styles: [__webpack_require__(/*! ./device-type.component.scss */ "./src/app/views/pages/admin/device-type/device-type.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
            _core_admin__WEBPACK_IMPORTED_MODULE_5__["DeviceTypeService"],
            _core_admin__WEBPACK_IMPORTED_MODULE_5__["DeviceTypeService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"]])
    ], DeviceTypeComponent);
    return DeviceTypeComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/admin/icon-type/icon-type.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/views/pages/admin/icon-type/icon-type.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL2FkbWluL2ljb24tdHlwZS9pY29uLXR5cGUuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/views/pages/admin/icon-type/icon-type.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/views/pages/admin/icon-type/icon-type.component.ts ***!
  \********************************************************************/
/*! exports provided: IconTypeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IconTypeComponent", function() { return IconTypeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _core_base_layout_models_datatable_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @core/_base/layout/models/datatable.model */ "./src/app/core/_base/layout/models/datatable.model.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _core_admin__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @core/admin */ "./src/app/core/admin/index.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");











var IconTypeComponent = /** @class */ (function () {
    function IconTypeComponent(icontype, formBuilder, toastr, cdr, translate, sanitizer, modalService) {
        this.icontype = icontype;
        this.formBuilder = formBuilder;
        this.toastr = toastr;
        this.cdr = cdr;
        this.translate = translate;
        this.sanitizer = sanitizer;
        this.modalService = modalService;
        this.dataTable = new _core_base_layout_models_datatable_model__WEBPACK_IMPORTED_MODULE_6__["DataTable"]();
        this.columns = [];
        this.data = [];
        this.totalRecod = 0;
        this.paginationSelect = [];
        this.isEdit = false;
        this.dataDefault = [{}];
        this.isIcontypeError = false;
        this.selectedFile = [
            {
                icon: File,
                iconMap: File
            }
        ];
        this.listIcon = [{
                icon: null,
                iconMap: null
            }];
        this.unsubscribe = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
        this.buildForm();
        this.setColumns();
        this.buildPagination();
        this.setPaginationSelect();
        this.setDataTable();
    }
    IconTypeComponent.prototype.ngOnInit = function () {
        this.updateDataTable();
        this.getData();
    };
    IconTypeComponent.prototype.buildForm = function () {
        this.formIcontype = this.formBuilder.group({
            nameIcontype: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            namekey: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            icon: [''],
            iconMap: [''],
            iconSVG: [''],
            iconMapSVG: [''],
            sortOrder: [''],
        });
        this.searchFormIcontype = this.formBuilder.group({
            nameIcontype: [''],
        });
    };
    IconTypeComponent.prototype.buttonAddNew = function (content) {
        this.listIcon = [
            {
                icon: null,
                iconMap: null
            }
        ];
        this.isEdit = false;
        this.formIcontype.reset();
        this.open(content);
    };
    IconTypeComponent.prototype.open = function (content) {
        var _this_1 = this;
        this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder modal-lg-800', size: 'lg' }).result.then(function (result) {
            _this_1.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this_1.closeResult = "Dismissed " + _this_1.getDismissReason(reason);
        });
    };
    IconTypeComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_10__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_10__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    IconTypeComponent.prototype.onSubmitIcontype = function (form) {
        var _this_1 = this;
        if (this.formIcontype.invalid) {
            return;
        }
        var params = {
            name: form.value.nameIcontype,
            nameKey: form.value.namekey,
            iconSVG: form.value.iconSVG,
            iconMapSVG: form.value.iconMapSVG,
            sortOrder: form.value.sortOrder
        };
        if (this.isEdit) {
            params.id = this.idIcontypeEdit;
        }
        var formData = this.convertFormData(params);
        if (this.isEdit) {
            params.id = this.idIcontypeEdit;
            this.icontype.update(formData, { notifyGlobal: true }).subscribe(function (result) {
                if (result.status == 200) {
                    _this_1.getData();
                    _this_1.closeModal();
                }
            });
            return;
        }
        this.icontype.create(formData, { notifyGlobal: true })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () {
            _this_1.cdr.markForCheck();
        }))
            .subscribe(function (result) {
            if (result.status == 201) {
                _this_1.getData();
                _this_1.closeModal();
            }
        });
    };
    IconTypeComponent.prototype.convertFormData = function (params) {
        var formData = new FormData();
        Object.keys(params).forEach(function (x) {
            formData.set(x, params[x]);
        });
        if (this.selectedFile.icon)
            formData.append("icon", this.selectedFile.icon, this.selectedFile.icon.name);
        if (this.selectedFile.iconMap)
            formData.append("iconMap", this.selectedFile.iconMap, this.selectedFile.iconMap.name);
        return formData;
    };
    IconTypeComponent.prototype.buildPagination = function () {
        this.filter = {
            pageNo: 1,
            pageSize: 10
        };
    };
    IconTypeComponent.prototype.onHideModal = function (id) {
        var idModal = '#' + id;
        $(idModal).modal('hide');
    };
    IconTypeComponent.prototype.setPaginationSelect = function () {
        this.paginationSelect = [10, 20, 30, 40, 50];
    };
    IconTypeComponent.prototype.getIdAction = function (id, idModal) {
        this.idIcontypeDelete = id;
        this.openModal({ idModal: idModal, confirm: true });
    };
    IconTypeComponent.prototype.openModal = function (params) {
        var idModal = params.idModal;
        var confirm = params.confirm;
        if (confirm) {
            $(idModal).appendTo('body').modal({ backdrop: 'static', keyboard: false });
        }
        else {
            $(idModal).appendTo('body').modal('show');
        }
    };
    IconTypeComponent.prototype.closeModal = function () {
        this.modalService.dismissAll();
    };
    IconTypeComponent.prototype.deleteIcontype = function () {
        var _this_1 = this;
        var id = this.idIcontypeDelete;
        this.icontype.delete(id, { notifyGlobal: true }).subscribe(function (data) {
            if (data.status == 200) {
                _this_1.getData();
            }
        });
        this.onHideModal('modal-delete-icontype');
    };
    IconTypeComponent.prototype.editIcontype = function (id, content) {
        var _this_1 = this;
        this.isEdit = true;
        this.idIcontypeEdit = id;
        var params = {
            id: id
        };
        this.icontype.list(params).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            _this_1.dataDefault = data.result.content;
            var iconUrl = data.result.content[0].iconUrl;
            if (iconUrl != null && iconUrl != '' && iconUrl != undefined) {
                iconUrl = data.result.content[0].iconUrl;
            }
            else {
                iconUrl = null;
            }
            var iconMapUrl = data.result.content[0].iconMapUrl;
            if (iconMapUrl != null && iconMapUrl != '' && iconMapUrl != 'undefined') {
                iconMapUrl = data.result.content[0].iconMapUrl;
            }
            else {
                iconMapUrl = null;
            }
            _this_1.listIcon = [{
                    icon: iconUrl,
                    iconMap: iconMapUrl
                }];
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () {
            _this_1.cdr.markForCheck();
        })).subscribe();
        this.open(content);
    };
    IconTypeComponent.prototype.loadSelectBootstrap = function () {
        $('.bootstrap-select').selectpicker();
    };
    IconTypeComponent.prototype.searchIcontype = function (form) {
        this.filter.name = form.value.nameIcontype;
        this.getData();
    };
    IconTypeComponent.prototype.updateDataTable = function () {
        var _this_1 = this;
        this.dataTable.eventUpdate.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (option) {
            _this_1.filter = option;
            _this_1.getData();
        })).subscribe();
    };
    Object.defineProperty(IconTypeComponent.prototype, "f", {
        get: function () {
            if (this.formIcontype != undefined)
                return this.formIcontype.controls;
        },
        enumerable: true,
        configurable: true
    });
    IconTypeComponent.prototype.getData = function () {
        var _this_1 = this;
        this.icontype.list(this.filter)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["debounceTime"])(1000), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () {
            _this_1.cdr.markForCheck();
        }))
            .subscribe(function (result) {
            if (result.status == 200) {
                _this_1.data = result.result.content;
                _this_1.totalRecod = result.result.totalRecord;
                _this_1.dataTable.update({
                    data: _this_1.data,
                    totalRecod: _this_1.totalRecod
                });
                $(function () {
                    $('select').selectpicker();
                });
            }
        });
    };
    IconTypeComponent.prototype.onFileChange = function (event, name) {
        if (name === void 0) { name = ''; }
        if (event.target.files.length > 0) {
            var file = event.target.files[0];
            this.selectedFile[name] = event.target.files.item(0);
            var mimeType = event.target.files[0].type;
            if (mimeType.match(/image\/*/) == null) {
                return;
            }
            this.convertFileToBase64(file, name);
        }
    };
    IconTypeComponent.prototype.convertFileToBase64 = function (file, name) {
        var _this = this;
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            _this.cdr.markForCheck();
            _this.listIcon[0][name] = reader.result;
        };
    };
    IconTypeComponent.prototype.sanitize = function (url) {
        return this.sanitizer.bypassSecurityTrustUrl(url);
    };
    IconTypeComponent.prototype.setDataTable = function () {
        this.dataTable.init({
            data: this.data,
            totalRecod: this.totalRecod,
            paginationSelect: this.paginationSelect,
            columns: this.columns,
            layout: {
                body: {
                    scrollable: false,
                    maxHeight: 600,
                },
                selecter: true,
            }
        });
    };
    IconTypeComponent.prototype.onResize = function ($elm) {
        this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
        this.cdr.detectChanges();
    };
    IconTypeComponent.prototype.setColumns = function () {
        this.columns = [
            {
                title: '#',
                field: 'no',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '40px' },
                class: 't-datatable__cell--center',
                translate: '#',
                autoHide: false,
                width: 20,
            },
            {
                title: 'Name',
                field: 'name',
                allowSort: true,
                isSort: true,
                dataSort: '',
                style: { 'width': '100px' },
                class: '',
                translate: 'COMMON.COLUMN.NAME',
                autoHide: false,
                width: 100,
            },
            {
                title: 'Key name',
                field: 'keyName',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '100px' },
                class: '',
                translate: 'ADMIN.DEVICE_ICON.GENERAL.NAME_KEY',
                autoHide: false,
                width: 100,
            },
            {
                title: 'ICON',
                field: 'icon',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '100px', 'text-align': 'center' },
                class: '',
                translate: 'ADMIN.DEVICE_ICON.GENERAL.ICON',
                autoHide: false,
                width: 100,
            },
            {
                title: 'Icon On Map',
                field: 'iconOnMap',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '100px', 'text-align': 'center' },
                class: '',
                translate: 'ADMIN.DEVICE_ICON.GENERAL.ICON_ON_MAP',
                autoHide: true,
                width: 100,
            },
            {
                title: 'Icon - SVG',
                field: 'icon',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '100px', 'text-align': 'center' },
                class: '',
                translate: 'ADMIN.DEVICE_ICON.GENERAL.ICON_SVG',
                autoHide: true,
                width: 100,
            },
            {
                title: 'Icon on map SVG',
                field: 'iconOnMapSVG',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '100px', 'text-align': 'center' },
                class: '',
                translate: 'ADMIN.DEVICE_ICON.GENERAL.ICON_ON_MAP_SVG',
                autoHide: true,
                width: 100,
            },
            {
                title: 'Count',
                field: 'count',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '100px', 'text-align': 'center' },
                class: '',
                translate: 'ADMIN.DEVICE_ICON.GENERAL.COUNT',
                autoHide: true,
                width: 100,
            },
            {
                title: 'Edit By',
                field: 'editBy',
                allowSort: true,
                isSort: false,
                dataSort: '',
                style: { 'width': '100px', 'text-align': 'center' },
                class: '',
                translate: 'COMMON.EDIT_BY',
                autoHide: false,
                width: 100,
            },
            {
                title: 'Created update',
                field: 'created',
                allowSort: true,
                isSort: false,
                dataSort: '',
                style: { 'width': '100px', 'text-align': 'center' },
                class: '',
                translate: 'COMMON.COLUMN.UPDATED_DATE',
                autoHide: true,
                width: 100,
            },
            {
                title: 'Actions',
                field: 'action',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '110px' },
                class: '',
                translate: 'COMMON.ACTIONS.ACTIONS',
                autoHide: false,
                width: 100,
            },
        ];
    };
    IconTypeComponent.ctorParameters = function () { return [
        { type: _core_admin__WEBPACK_IMPORTED_MODULE_8__["IconTypeService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateService"] },
        { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_9__["DomSanitizer"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_10__["NgbModal"] }
    ]; };
    IconTypeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-icon-type',
            template: __webpack_require__(/*! raw-loader!./icon-type.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/icon-type/icon-type.component.html"),
            styles: [__webpack_require__(/*! ./icon-type.component.scss */ "./src/app/views/pages/admin/icon-type/icon-type.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_admin__WEBPACK_IMPORTED_MODULE_8__["IconTypeService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_9__["DomSanitizer"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_10__["NgbModal"]])
    ], IconTypeComponent);
    return IconTypeComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/admin/login-page/login-page.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/views/pages/admin/login-page/login-page.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL2FkbWluL2xvZ2luLXBhZ2UvbG9naW4tcGFnZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/views/pages/admin/login-page/login-page.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/views/pages/admin/login-page/login-page.component.ts ***!
  \**********************************************************************/
/*! exports provided: LoginPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageComponent", function() { return LoginPageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");



var LoginPageComponent = /** @class */ (function () {
    function LoginPageComponent(fb) {
        this.fb = fb;
        this.control = fb.control({ value: 'my val', disabled: true });
    }
    LoginPageComponent.prototype.ngOnInit = function () {
    };
    LoginPageComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] }
    ]; };
    LoginPageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-login-page',
            template: "\n    <input [formControl]=\"control\" placeholder=\"First\">\n  ",
            styles: [__webpack_require__(/*! ./login-page.component.scss */ "./src/app/views/pages/admin/login-page/login-page.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
    ], LoginPageComponent);
    return LoginPageComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/admin/page-template/page-template.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/views/pages/admin/page-template/page-template.component.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL2FkbWluL3BhZ2UtdGVtcGxhdGUvcGFnZS10ZW1wbGF0ZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/views/pages/admin/page-template/page-template.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/views/pages/admin/page-template/page-template.component.ts ***!
  \****************************************************************************/
/*! exports provided: PageTemplateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageTemplateComponent", function() { return PageTemplateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PageTemplateComponent = /** @class */ (function () {
    function PageTemplateComponent() {
        this.nodes = [
            {
                "id": "234",
                "name": "Nhat",
                "children": [
                    {
                        "id": "5564",
                        "parentId": "234",
                        "aliasId": "0",
                        "path": "234,",
                        "username": "hoàng",
                        "passWord": null,
                        "type": "1",
                        "name": "Nguyễn Văn Định 7",
                        "surname": "VnetGps1",
                        "email": "indsdfo@vn-et1.com",
                        "phone": "0224d36400767d",
                        "description": "",
                        "status": "1",
                        "active": "1",
                        "createdAt": "2019-08-13T07:45:56",
                        "editedAt": "2019-09-11T09:20:53",
                        "stockDevice": "0",
                        "totalDevice": "0",
                        "totalPoint": "1000",
                        "currentPoint": "0",
                        "isRenewed": "1",
                        "timezone": "UTC+7",
                        "language": "en",
                        "unitDistance": "kilometer",
                        "unitVolume": "litre",
                        "unitTemperature": "celsius",
                        "unitWeight": "kilogram",
                        "dateFormat": "dd/MM/yyyy",
                        "timeFormat": "HH:mm",
                        "weekFirstDay": 1,
                        "markerStyle": "html",
                        "carSorting": "plate",
                        "decimalSeprerator": ",",
                        "permission": null,
                        "roleId": 2,
                        "roleName": "Distributor-Normal",
                        "hasChildren": true
                    },
                    {
                        "id": "5562",
                        "parentId": "234",
                        "aliasId": "0",
                        "path": "234,",
                        "username": "okkkk",
                        "passWord": null,
                        "type": "1",
                        "name": "Nguyễn Văn Định 6",
                        "surname": "VnetGps1",
                        "email": "infoádsad.com",
                        "phone": "02436400dsa767d",
                        "description": "",
                        "status": "1",
                        "active": "1",
                        "createdAt": "2019-08-13T07:39:35",
                        "editedAt": "2019-09-12T03:52:46",
                        "stockDevice": "0",
                        "totalDevice": "0",
                        "totalPoint": "1000",
                        "currentPoint": "0",
                        "isRenewed": "1",
                        "timezone": "UTC+7",
                        "language": "en",
                        "unitDistance": "kilometer",
                        "unitVolume": "litre",
                        "unitTemperature": "celsius",
                        "unitWeight": "kilogram",
                        "dateFormat": "dd/MM/yyyy",
                        "timeFormat": "HH:mm",
                        "weekFirstDay": 1,
                        "markerStyle": "html",
                        "carSorting": "plate",
                        "decimalSeprerator": ",",
                        "permission": null,
                        "roleId": 2,
                        "roleName": "Distributor-Normal",
                        "hasChildren": true
                    },
                    {
                        "id": "5561",
                        "parentId": "234",
                        "aliasId": "0",
                        "path": "234,",
                        "username": "dinhshit",
                        "passWord": null,
                        "type": "1",
                        "name": "Nguyễn Văn Định 01",
                        "surname": "",
                        "email": "dinhshit@vn-et1.com",
                        "phone": "02436400767d",
                        "description": "",
                        "status": "1",
                        "active": "1",
                        "createdAt": "2019-08-12T04:22:12",
                        "editedAt": "2019-09-09T01:05:43",
                        "stockDevice": "0",
                        "totalDevice": "0",
                        "totalPoint": "0",
                        "currentPoint": "0",
                        "isRenewed": "1",
                        "timezone": "UTC+7",
                        "language": "en",
                        "unitDistance": "kilometer",
                        "unitVolume": "litre",
                        "unitTemperature": "celsius",
                        "unitWeight": "kilogram",
                        "dateFormat": "dd/MM/yyyy",
                        "timeFormat": "HH:mm",
                        "weekFirstDay": 1,
                        "markerStyle": "html",
                        "carSorting": "plate",
                        "decimalSeprerator": ",",
                        "permission": null,
                        "roleId": 3,
                        "roleName": "User - Normal",
                        "hasChildren": true
                    },
                    {
                        "id": "5560",
                        "parentId": "234",
                        "aliasId": "0",
                        "path": "234,",
                        "username": "children",
                        "passWord": null,
                        "type": "1",
                        "name": "Nguyễn Văn Định 5",
                        "surname": "",
                        "email": "info@vn-et1.com",
                        "phone": "02436400767d",
                        "description": "",
                        "status": "1",
                        "active": "1",
                        "createdAt": "2019-08-12T04:16:24",
                        "editedAt": "2019-08-27T04:53:06",
                        "stockDevice": "0",
                        "totalDevice": "0",
                        "totalPoint": "0",
                        "currentPoint": "0",
                        "isRenewed": "1",
                        "timezone": "UTC+7",
                        "language": "vi",
                        "unitDistance": "kilometer",
                        "unitVolume": "litre",
                        "unitTemperature": "celsius",
                        "unitWeight": "kilogram",
                        "dateFormat": "dd/MM/yyyy",
                        "timeFormat": "HH:mm",
                        "weekFirstDay": 1,
                        "markerStyle": "html",
                        "carSorting": "plate",
                        "decimalSeprerator": ",",
                        "permission": null,
                        "roleId": 2,
                        "roleName": "Distributor-Normal",
                        "hasChildren": true
                    },
                    {
                        "id": "5305",
                        "parentId": "234",
                        "aliasId": "0",
                        "path": "234,",
                        "username": "vnet",
                        "passWord": null,
                        "type": "4",
                        "name": "nguyễn văn định 1",
                        "surname": "",
                        "email": "dinhnv@vn",
                        "phone": "",
                        "description": "",
                        "status": "1",
                        "active": "1",
                        "createdAt": null,
                        "editedAt": "2019-08-03T06:41:24",
                        "stockDevice": "1",
                        "totalDevice": "3",
                        "totalPoint": "0",
                        "currentPoint": "0",
                        "isRenewed": "1",
                        "timezone": "UTC+7",
                        "language": "en",
                        "unitDistance": "kilometer",
                        "unitVolume": "litre",
                        "unitTemperature": "celsius",
                        "unitWeight": "kilogram",
                        "dateFormat": "dd/MM/yyyy",
                        "timeFormat": "HH:mm",
                        "weekFirstDay": 1,
                        "markerStyle": "html",
                        "carSorting": "plate",
                        "decimalSeprerator": ",",
                        "permission": null,
                        "roleId": 2,
                        "roleName": "Distributor-Normal",
                        "hasChildren": true
                    },
                    {
                        "id": "5306",
                        "parentId": "234",
                        "aliasId": "0",
                        "path": "234,",
                        "username": "haduwaco",
                        "passWord": null,
                        "type": "1",
                        "name": "Nuoc Sach Hai Duong ",
                        "surname": "",
                        "email": "haduwaco@gmail.com",
                        "phone": null,
                        "description": "",
                        "status": "1",
                        "active": "1",
                        "createdAt": null,
                        "editedAt": "2019-09-09T04:02:31",
                        "stockDevice": "0",
                        "totalDevice": "0",
                        "totalPoint": "0",
                        "currentPoint": "930",
                        "isRenewed": "1",
                        "timezone": "UTC+7",
                        "language": "vi",
                        "unitDistance": "kilometer",
                        "unitVolume": "litre",
                        "unitTemperature": "celsius",
                        "unitWeight": "kilogram",
                        "dateFormat": "dd/MM/yyyy",
                        "timeFormat": "HH:mm",
                        "weekFirstDay": 1,
                        "markerStyle": "html",
                        "carSorting": "plate",
                        "decimalSeprerator": ",",
                        "permission": null,
                        "roleId": 2,
                        "roleName": "Distributor-Normal",
                        "hasChildren": true
                    }
                ],
                "root": true,
                "hasChildren": true,
                "username": "admin",
                "path": "",
                "stockDevice": "21",
                "totalDevice": "5",
                "tree_root": true,
                "password": "demo",
                "email": "android@vn-et.com",
                "accessToken": null,
                "refreshToken": "access-token-f8c137a2c98743f48b643e71161d90aa",
                "roles": [
                    1
                ],
                "pic": "./assets/media/users/300_25.jpg",
                "fullname": "Admin",
                "occupation": "CEO",
                "companyName": "Keenthemes",
                "phone": "02436400767",
                "address": {
                    "addressLine": "L-12-20 Vertex, Cybersquare",
                    "city": "San Francisco",
                    "state": "California",
                    "postCode": "45000"
                },
                "socialNetworks": {
                    "linkedIn": "https://linkedin.com/admin",
                    "facebook": "https://facebook.com/admin",
                    "twitter": "https://twitter.com/admin",
                    "instagram": "https://instagram.com/admin"
                },
                "parentId": "0",
                "aliasId": "0",
                "passWord": null,
                "type": "0",
                "surname": "VnetGps",
                "description": "",
                "status": "1",
                "active": "1",
                "createdAt": "2010-07-05T00:00",
                "editedAt": "2019-06-21T11:29:52",
                "totalPoint": "1000",
                "currentPoint": "712",
                "isRenewed": "1",
                "timezone": "UTC+7",
                "language": "vi",
                "unitDistance": "kilometer",
                "unitVolume": "litre",
                "unitTemperature": "celsius",
                "unitWeight": "kilogram",
                "dateFormat": "dd/MM/yyyy",
                "timeFormat": "HH:mm",
                "weekFirstDay": 1,
                "markerStyle": "html",
                "carSorting": "plate",
                "decimalSeprerator": ",",
                "permission": "ROLE_user.delete,ROLE_user.create,ROLE_user.update,ROLE_profile.own.view,ROLE_profile.own.view2,ROLE_geofence.manage,ROLE_loginPage.manage,ROLE_device.manage,ROLE_device.action.edit,ROLE_device.action.command,ROLE_device.edit.type,ROLE_device.edit.active_status,ROLE_device.edit.active_customer,ROLE_device.edit.description_admin,ROLE_device.edit.add_group,ROLE_device.edit.inactive_customer,ROLE_device.edit.icon,ROLE_device.action.import_device,ROLE_device.action.renewed,ROLE_device.action.sell,ROLE_permission.list,ROLE_permission.manage,ROLE_role.list,ROLE_role.manage,ROLE_user.list",
                "roleId": 1,
                "roleName": "Administrator"
            }
        ];
        this.options = {};
    }
    PageTemplateComponent.prototype.ngOnInit = function () {
    };
    PageTemplateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-page-template',
            template: __webpack_require__(/*! raw-loader!./page-template.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/page-template/page-template.component.html"),
            styles: [__webpack_require__(/*! ./page-template.component.scss */ "./src/app/views/pages/admin/page-template/page-template.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PageTemplateComponent);
    return PageTemplateComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/admin/permission/permission.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/views/pages/admin/permission/permission.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL2FkbWluL3Blcm1pc3Npb24vcGVybWlzc2lvbi5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/views/pages/admin/permission/permission.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/views/pages/admin/permission/permission.component.ts ***!
  \**********************************************************************/
/*! exports provided: PermissionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PermissionComponent", function() { return PermissionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @core/auth */ "./src/app/core/auth/index.ts");
/* harmony import */ var _core_base_layout_models_datatable_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @core/_base/layout/models/datatable.model */ "./src/app/core/_base/layout/models/datatable.model.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");










var PermissionComponent = /** @class */ (function () {
    function PermissionComponent(auth, formBuilder, toastr, cdr, translate, modalService) {
        this.auth = auth;
        this.formBuilder = formBuilder;
        this.toastr = toastr;
        this.cdr = cdr;
        this.translate = translate;
        this.modalService = modalService;
        this.isLoading = false;
        this.dataTable = new _core_base_layout_models_datatable_model__WEBPACK_IMPORTED_MODULE_7__["DataTable"]();
        this.columns = [];
        this.data = [];
        this.totalRecod = 0;
        this.paginationSelect = [];
        this.isEdit = false;
        this.dataDefault = [{}];
        this.loadingSave = false;
        this.loadingData = false;
        this.listNameGroupObj = [];
        this.isPermissionError = false;
        this.errorMessage = '';
        this.listNameGroup = [];
        this.keyword = 'name';
        this.nameGroupEdit = { name: '' };
        this.translationsArr = [];
        this.unsubscribe = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
        this.translations();
    }
    PermissionComponent.prototype.ngOnInit = function () {
        this.buildForm();
        this.buildPagination();
        this.loadListPermissionGroup();
        this.setColumns();
        this.setPaginationSelect();
        this.updateDataTable();
        this.setDataTable();
        this.getData();
    };
    PermissionComponent.prototype.buildForm = function () {
        this.formPermission = this.formBuilder.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            description: ['']
        });
        this.searchFormPermission = this.formBuilder.group({
            name: [''],
            namegroup: ['']
        });
    };
    PermissionComponent.prototype.buttonAddNew = function (content) {
        this.isEdit = false;
        this.formPermission.reset();
        this.open(content);
    };
    PermissionComponent.prototype.closeModal = function () {
        $('.modal').modal('hide');
    };
    PermissionComponent.prototype.onSubmitPermission = function (form) {
        var _this = this;
        if (this.formPermission.invalid) {
            return;
        }
        var nameGroup = this.nameGroupEdit.name;
        var params = {
            "name": form.value.name,
            "groupName": nameGroup,
            "description": form.value.description
        };
        this.loadingSave = true;
        if (this.isEdit) {
            this.auth.updatePermission(this.idPermissonEdit, params).subscribe(function (result) {
                _this.loadingSave = true;
                if (result.status == 200) {
                    _this.loadingSave = false;
                    _this.onshowToastSuccess(_this.translationsArr['SUCCESS'], _this.translationsArr['UPDATE_PERMISSION_NAME'] + form.value.name);
                    _this.getData();
                    // this.dataTable.reload({})
                }
                else {
                    _this.loadingSave = false;
                    _this.isPermissionError = true;
                    _this.errorMessage = result.message;
                    _this.onshowToastFail(_this.translationsArr['FAILED'], _this.translationsArr['UPDATE_PERMISSION_NAME'] + form.value.name);
                }
            }, function (err) {
                _this.isPermissionError = true;
            });
            this.modalService.dismissAll();
            return;
        }
        this.auth.addPermission(params)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () {
            _this.cdr.markForCheck();
        }))
            .subscribe(function (result) {
            if (result.status == 201) {
                _this.loadingSave = false;
                _this.onshowToastSuccess(_this.translationsArr['SUCCESS'], _this.translationsArr['ADD_PERMISSION_NAME'] + form.value.name);
                _this.getData();
            }
            else {
                _this.loadingSave = false;
                _this.isPermissionError = true;
                _this.errorMessage = result.message;
            }
        }, function (err) {
            _this.isPermissionError = true;
        });
        this.modalService.dismissAll();
    };
    PermissionComponent.prototype.buildPagination = function () {
        this.filter = {
            pageNo: 1,
            pageSize: 10
        };
    };
    PermissionComponent.prototype.open = function (content) {
        var _this = this;
        this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder' }).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    PermissionComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    PermissionComponent.prototype.translations = function () {
        var _this = this;
        this.buildTranslations();
        this.translate.onLangChange.subscribe(function (event) {
            _this.translationsArr['SUCCESS'] = event.translations.COMMON.SUCCESS;
            _this.translationsArr['FAILED'] = event.translations.COMMON.FAILED;
            _this.translationsArr['UPDATE_PERMISSION_NAME'] = event.translations.ADMIN.PERMISSION.GENERAL.UPDATE_PERMISSION_NAME;
            _this.translationsArr['UPDATE_PERMISSION_FAILED'] = event.translations.ADMIN.PERMISSION.GENERAL.UPDATE_PERMISSION_FAILED;
            _this.translationsArr['ADD_PERMISSION_NAME'] = event.translations.ADMIN.PERMISSION.GENERAL.ADD_PERMISSION_NAME;
            _this.translationsArr['DELETE_PERMISSION_SUCCESS'] = _this.translate.instant('ADMIN.PERMISSION.GENERAL.DELETE_PERMISSION_SUCCESS');
            _this.translationsArr['DELETE_PERMISSION_FAILED'] = _this.translate.instant('ADMIN.PERMISSION.GENERAL.DELETE_PERMISSION_FAILED');
        });
    };
    PermissionComponent.prototype.buildTranslations = function () {
        this.translationsArr['SUCCESS'] = this.translate.instant('COMMON.SUCCESS');
        this.translationsArr['FAILED'] = this.translate.instant('COMMON.FAILED');
        this.translationsArr['UPDATE_PERMISSION_NAME'] = this.translate.instant('ADMIN.PERMISSION.GENERAL.UPDATE_PERMISSION_NAME');
        this.translationsArr['UPDATE_PERMISSION_FAILED'] = this.translate.instant('ADMIN.PERMISSION.GENERAL.UPDATE_PERMISSION_FAILED');
        this.translationsArr['ADD_PERMISSION_NAME'] = this.translate.instant('ADMIN.PERMISSION.GENERAL.ADD_PERMISSION_NAME');
        this.translationsArr['DELETE_PERMISSION_SUCCESS'] = this.translate.instant('ADMIN.PERMISSION.GENERAL.DELETE_PERMISSION_SUCCESS');
        this.translationsArr['DELETE_PERMISSION_FAILED'] = this.translate.instant('ADMIN.PERMISSION.GENERAL.DELETE_PERMISSION_FAILED');
    };
    PermissionComponent.prototype.onshowToastSuccess = function (content, title) {
        this.toastr.success(title, content, {
            timeOut: 2000,
            positionClass: 'toast-bottom-right'
        });
    };
    PermissionComponent.prototype.onshowToastFail = function (content, title) {
        this.toastr.error(title, content, {
            timeOut: 2000,
            positionClass: 'toast-bottom-right'
        });
    };
    Object.defineProperty(PermissionComponent.prototype, "f", {
        get: function () {
            if (this.formPermission != undefined)
                return this.formPermission.controls;
        },
        enumerable: true,
        configurable: true
    });
    PermissionComponent.prototype.onHideModal = function (id) {
        var idModal = '#' + id;
        $(idModal).modal('hide');
    };
    PermissionComponent.prototype.selectEvent = function (item) {
        this.nameGroupEdit = item;
    };
    PermissionComponent.prototype.onChangeSearch = function (val) {
        this.nameGroupEdit = {
            name: val
        };
    };
    PermissionComponent.prototype.setPaginationSelect = function () {
        this.paginationSelect = [10, 20, 30, 40, 50];
    };
    PermissionComponent.prototype.getIdAction = function (id, idModal) {
        this.idPermissonDelete = id;
        this.openModal({ idModal: idModal, confirm: true });
    };
    PermissionComponent.prototype.openModal = function (params) {
        var idModal = params.idModal;
        var confirm = params.confirm;
        if (confirm) {
            $(idModal).appendTo('body').modal({ backdrop: 'static', keyboard: false });
        }
        else {
            $(idModal).appendTo('body').modal('show');
        }
    };
    PermissionComponent.prototype.deletePermission = function () {
        var _this = this;
        var id = this.idPermissonDelete;
        this.auth.deletePermission(id).subscribe(function (data) {
            if (data.status == 200) {
                _this.getData();
                // this.dataTable.reload({});
                _this.onshowToastSuccess(_this.translationsArr['SUCCESS'], _this.translationsArr['DELETE_PERMISSION_SUCCESS']);
            }
        }, function (err) {
            _this.onshowToastFail(_this.translationsArr['FAILED'], _this.translationsArr['DELETE_PERMISSION_FAILED']);
        });
        $('.modal').modal('hide');
    };
    PermissionComponent.prototype.editPermission = function (id, content) {
        var _this = this;
        this.isEdit = true;
        this.idPermissonEdit = id;
        this.auth.getPermissionById(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            _this.dataDefault = [data.result];
            _this.nameGroupEdit = {
                name: data.result.groupName
            };
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () {
            _this.cdr.markForCheck();
        })).subscribe();
        this.open(content);
    };
    PermissionComponent.prototype.loadSelectBootstrap = function () {
        $('.bootstrap-select').selectpicker();
    };
    PermissionComponent.prototype.convertToObj = function (data) {
        for (var i = 0; i < data.length; i++) {
            var obj = {
                name: data[i]
            };
            this.listNameGroupObj.push(obj);
        }
    };
    PermissionComponent.prototype.loadListPermissionGroup = function () {
        var _this = this;
        this.auth.getListPermissionGroup()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () {
            _this.cdr.markForCheck();
        }))
            .subscribe(function (data) {
            if (data.status == 200) {
                _this.listNameGroup = data.result;
                _this.convertToObj(_this.listNameGroup);
                setTimeout(function () {
                    _this.loadSelectBootstrap();
                }, 100);
            }
        });
    };
    PermissionComponent.prototype.searchPermission = function (form) {
        this.filter.name = form.value.name;
        this.filter.groupName = form.value.namegroup;
        this.getData();
    };
    PermissionComponent.prototype.updateDataTable = function () {
        var _this = this;
        this.dataTable.eventUpdate.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (option) {
            _this.filter = option;
            _this.getData();
        })).subscribe();
    };
    PermissionComponent.prototype.getData = function () {
        var _this = this;
        this.auth.searchPermissions(this.filter)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["debounceTime"])(1000), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () {
            _this.cdr.markForCheck();
        }))
            .subscribe(function (result) {
            if (result.status == 200) {
                _this.data = result.result.content;
                _this.totalRecod = result.result.totalRecord;
                _this.dataTable.update({
                    data: _this.data,
                    totalRecod: _this.totalRecod
                });
                $(function () {
                    $('select').selectpicker();
                });
            }
        });
    };
    PermissionComponent.prototype.setDataTable = function () {
        this.dataTable.init({
            data: this.data,
            totalRecod: this.totalRecod,
            paginationSelect: this.paginationSelect,
            columns: this.columns,
            layout: {
                body: {
                    scrollable: false,
                    maxHeight: 600,
                },
                selecter: false,
            }
        });
    };
    PermissionComponent.prototype.onResize = function ($elm) {
        this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
        this.cdr.detectChanges();
    };
    PermissionComponent.prototype.setColumns = function () {
        this.columns = [
            {
                title: '#',
                field: 'no',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '40px' },
                class: '',
                translate: '#',
                autoHide: false,
                width: 20,
            },
            {
                title: 'Name',
                field: 'name',
                allowSort: true,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'ADMIN.PERMISSION.GENERAL.NAME',
                autoHide: false,
                width: 182,
            },
            {
                title: 'Description',
                field: 'description',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'ADMIN.PERMISSION.GENERAL.DESCRIPTION',
                autoHide: true,
                width: 182,
            },
            {
                title: 'Name group	',
                field: 'groupName',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'ADMIN.PERMISSION.GENERAL.GROUP',
                autoHide: false,
                width: 182,
            },
            {
                title: 'Create At',
                field: 'createdAt',
                allowSort: true,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'COMMON.CREATED_DATE',
                autoHide: true,
                width: 182,
            },
            {
                title: 'Actions',
                field: 'action',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '110px' },
                class: '',
                translate: 'COMMON.ACTIONS.ACTIONS',
                autoHide: false,
                width: 182,
            },
        ];
    };
    PermissionComponent.ctorParameters = function () { return [
        { type: _core_auth__WEBPACK_IMPORTED_MODULE_6__["AuthService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__["TranslateService"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__["NgbModal"] }
    ]; };
    PermissionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-permission',
            template: __webpack_require__(/*! raw-loader!./permission.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/permission/permission.component.html"),
            styles: [__webpack_require__(/*! ./permission.component.scss */ "./src/app/views/pages/admin/permission/permission.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_auth__WEBPACK_IMPORTED_MODULE_6__["AuthService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__["TranslateService"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__["NgbModal"]])
    ], PermissionComponent);
    return PermissionComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/admin/role/role.component.scss":
/*!************************************************************!*\
  !*** ./src/app/views/pages/admin/role/role.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL2FkbWluL3JvbGUvcm9sZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/views/pages/admin/role/role.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/views/pages/admin/role/role.component.ts ***!
  \**********************************************************/
/*! exports provided: RoleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleComponent", function() { return RoleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _core_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @core/auth */ "./src/app/core/auth/index.ts");
/* harmony import */ var _core_base_layout_models_datatable_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @core/_base/layout/models/datatable.model */ "./src/app/core/_base/layout/models/datatable.model.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");








var RoleComponent = /** @class */ (function () {
    function RoleComponent(auth, formBuilder, cdr, modalService) {
        this.auth = auth;
        this.formBuilder = formBuilder;
        this.cdr = cdr;
        this.modalService = modalService;
        this.dataTable = new _core_base_layout_models_datatable_model__WEBPACK_IMPORTED_MODULE_6__["DataTable"]();
        this.columns = [];
        this.data = [];
        this.totalRecod = 0;
        this.paginationSelect = [];
        this.isEdit = false;
        this.dataDefault = [{}];
        this.listNameGroupObj = [];
        this.listNameGroup = [];
        this.nameGroupEdit = { name: '' };
        this.checked = true;
        this.listPermission = [];
        this.listParentRoles = [];
        this.listCheckedPermission = [];
        this.unsubscribe = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
        this.buildForm();
        this.setColumns();
        this.buildPagination();
        this.setPaginationSelect();
        this.setDataTable();
    }
    RoleComponent.prototype.ngOnInit = function () {
        this.getListRoleParent();
        this.updateDataTable();
        this.getData();
    };
    //gán permission group và list permission vào formArray 
    RoleComponent.prototype.addIdToFormArray = function () {
        this.formRole.controls.permissions = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]([]);
        for (var i = 0; i < this.listNameGroup.length; i++) {
            var group = this.listNameGroup[i].permissionGroup;
            var permissions = this.listNameGroup[i].permissons;
            this.formRole['controls'][group] = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]();
            for (var index = 0; index < permissions.length; index++) {
                var id = 'id' + permissions[index].id;
                this.formRole.controls.permissions['controls'][id] = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]();
            }
        }
    };
    // lấy list permission mặc định false
    RoleComponent.prototype.getAllPermissions = function () {
        var _this = this;
        var id = 0;
        this.auth.getRoleById(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (data) {
            _this.dataDefault = [data.result];
            _this.listNameGroup = data.result.permissions;
            _this.addIdToFormArray();
            setTimeout(function () {
                $('.kt_selectpicker ').selectpicker();
            }, 300);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(function () {
            _this.cdr.markForCheck();
        })).subscribe();
    };
    // Tích chọn tất cả theo nhóm permission
    RoleComponent.prototype.checkUncheckAll = function (group) {
        for (var i = 0; i < this.listNameGroup.length; i++) {
            if (this.listNameGroup[i].permissionGroup == group) {
                var groupChecked = this.listNameGroup[i].checked;
                var listPermissions = this.listNameGroup[i].permissons;
                for (var j = 0; j < listPermissions.length; j++) {
                    listPermissions[j].checked = groupChecked;
                }
                break;
            }
        }
    };
    //Tích chọn từng permission
    RoleComponent.prototype.isCheckSelected = function (group) {
        if (group === void 0) { group = ''; }
        for (var i = 0; i < this.listNameGroup.length; i++) {
            if (group == '') {
                var listPermissions = this.listNameGroup[i].permissons;
                this.listNameGroup[i].checked = listPermissions.every(function (item) {
                    return item.checked == true;
                });
            }
            if (this.listNameGroup[i].permissionGroup == group) {
                var listPermissions = this.listNameGroup[i].permissons;
                this.listNameGroup[i].checked = listPermissions.every(function (item) {
                    return item.checked == true;
                });
                break;
            }
        }
    };
    //lấy tất cả giá trị permisson đã chọn
    RoleComponent.prototype.getIsSelectedItemList = function () {
        this.listCheckedPermission = [];
        for (var i = 0; i < this.listNameGroup.length; i++) {
            var permissons = this.listNameGroup[i].permissons;
            for (var index = 0; index < permissons.length; index++) {
                if (permissons[index].checked) {
                    this.listCheckedPermission.push(permissons[index].id);
                }
            }
        }
    };
    RoleComponent.prototype.buildForm = function () {
        this.formRole = this.formBuilder.group({
            name: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
            nameParent: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            description: [''],
            group_user: ['']
        });
        this.searchFormRole = this.formBuilder.group({
            name: [''],
            namegroup: ['']
        });
    };
    RoleComponent.prototype.buttonAddNew = function (content) {
        this.isEdit = false;
        this.getAllPermissions();
        this.formRole.reset();
        this.open(content);
    };
    RoleComponent.prototype.open = function (content) {
        var _this = this;
        this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder', size: 'lg' }).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    RoleComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    RoleComponent.prototype.onSubmitRole = function (form) {
        var _this = this;
        if (this.formRole.invalid) {
            return;
        }
        this.getIsSelectedItemList();
        var params = {
            "name": form.value.name,
            "parentId": form.value.nameParent,
            "description": form.value.description,
            "listPermission": this.listCheckedPermission
        };
        if (this.isEdit) {
            params.id = this.idRoleEdit;
            this.auth.updateRoleById(params, { notifyGlobal: true }).subscribe(function (result) {
                if (result.status == 200) {
                    _this.getData();
                    _this.closeModal();
                }
            });
            return;
        }
        this.auth.addRole(params, { notifyGlobal: true })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(function () {
            _this.cdr.markForCheck();
        }))
            .subscribe(function (result) {
            if (result.status == 201) {
                _this.getData();
                _this.closeModal();
            }
        });
    };
    RoleComponent.prototype.buildPagination = function () {
        this.filter = {
            pageNo: 1,
            pageSize: 10
        };
    };
    RoleComponent.prototype.closeModal = function () {
        this.modalService.dismissAll();
    };
    RoleComponent.prototype.onHideModal = function (id) {
        var idModal = '#' + id;
        $(idModal).modal('hide');
    };
    RoleComponent.prototype.selectEvent = function (item) {
        this.nameGroupEdit = item;
    };
    RoleComponent.prototype.onChangeSearch = function (val) {
        this.nameGroupEdit = {
            name: val
        };
    };
    RoleComponent.prototype.setPaginationSelect = function () {
        this.paginationSelect = [10, 20, 30, 40, 50];
    };
    RoleComponent.prototype.getIdAction = function (id, idModal) {
        this.idRoleDelete = id;
        this.openModal({ idModal: idModal, confirm: true });
    };
    RoleComponent.prototype.openModal = function (params) {
        var idModal = params.idModal;
        var confirm = params.confirm;
        if (confirm) {
            $(idModal).appendTo('body').modal({ backdrop: 'static', keyboard: false });
        }
        else {
            $(idModal).appendTo('body').modal('show');
        }
    };
    RoleComponent.prototype.deleteRole = function () {
        var _this = this;
        var id = this.idRoleDelete;
        this.auth.deleteRoleById(id, { notifyGlobal: true }).subscribe(function (data) {
            if (data.status == 200) {
                _this.getData();
            }
        });
        $('.modal').modal('hide');
    };
    RoleComponent.prototype.editRole = function (id, content) {
        var _this = this;
        this.isEdit = true;
        this.idRoleEdit = id;
        this.auth.getRoleById(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (data) {
            setTimeout(function () {
                $('.kt_selectpicker ').selectpicker();
            }, 100);
            _this.dataDefault = [data.result];
            _this.listNameGroup = data.result.permissions;
            _this.addIdToFormArray();
            _this.isCheckSelected();
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(function () {
            _this.cdr.markForCheck();
        })).subscribe();
        this.open(content);
    };
    RoleComponent.prototype.loadSelectBootstrap = function () {
        $('.bootstrap-select').selectpicker();
    };
    RoleComponent.prototype.convertToObj = function (data) {
        for (var i = 0; i < data.length; i++) {
            var obj = {
                name: data[i]
            };
            this.listNameGroupObj.push(obj);
        }
    };
    RoleComponent.prototype.getListRoleParent = function () {
        var _this = this;
        var params = {
            pageNo: -1
        };
        this.auth.getListRoles(params)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(function () {
            _this.cdr.markForCheck();
        }))
            .subscribe(function (result) {
            if (result.status == 200) {
                _this.listParentRoles = result.result;
                setTimeout(function () {
                    $('.kt_selectpicker ').selectpicker();
                }, 300);
            }
        });
    };
    RoleComponent.prototype.searchRole = function (form) {
        this.filter.name = form.value.name;
        this.filter.groupName = form.value.namegroup;
        this.getData();
    };
    RoleComponent.prototype.updateDataTable = function () {
        var _this = this;
        this.dataTable.eventUpdate.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (option) {
            _this.filter = option;
            _this.getData();
        })).subscribe();
    };
    Object.defineProperty(RoleComponent.prototype, "f", {
        get: function () {
            if (this.formRole != undefined)
                return this.formRole.controls;
        },
        enumerable: true,
        configurable: true
    });
    RoleComponent.prototype.getData = function () {
        var _this = this;
        this.auth.searchRoles(this.filter)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["debounceTime"])(1000), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(function () {
            _this.cdr.markForCheck();
        }))
            .subscribe(function (result) {
            if (result.status == 200) {
                _this.data = result.result.content;
                _this.totalRecod = result.result.totalRecord;
                _this.dataTable.update({
                    data: _this.data,
                    totalRecod: _this.totalRecod
                });
                $(function () {
                    $('select').selectpicker();
                });
            }
        });
    };
    RoleComponent.prototype.setDataTable = function () {
        this.dataTable.init({
            data: this.data,
            totalRecod: this.totalRecod,
            paginationSelect: this.paginationSelect,
            columns: this.columns,
            layout: {
                body: {
                    scrollable: false,
                    maxHeight: 600,
                },
                selecter: false,
            }
        });
    };
    RoleComponent.prototype.onResize = function ($elm) {
        this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
        this.cdr.detectChanges();
    };
    RoleComponent.prototype.setColumns = function () {
        this.columns = [
            {
                title: '#',
                field: 'no',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '40px' },
                class: 't-datatable__cell--center',
                translate: '#',
                autoHide: false,
                width: 20,
            },
            {
                title: 'Name',
                field: 'name',
                allowSort: true,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'ADMIN.ROLE.GENERAL.NAME',
                autoHide: false,
                width: 182,
            },
            {
                title: 'Name Parent',
                field: 'parentName',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'ADMIN.ROLE.GENERAL.NAME_PARENT',
                autoHide: false,
                width: 182,
            },
            {
                title: 'Description',
                field: 'description',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'ADMIN.ROLE.GENERAL.DESCRIPTION',
                autoHide: true,
                width: 182,
            },
            {
                title: 'Create At',
                field: 'createdAt',
                allowSort: true,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'COMMON.CREATED_DATE',
                autoHide: true,
                width: 182,
            },
            {
                title: 'Actions',
                field: 'action',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '110px' },
                class: '',
                translate: 'COMMON.ACTIONS.ACTIONS',
                autoHide: false,
                width: 182,
            },
        ];
    };
    RoleComponent.ctorParameters = function () { return [
        { type: _core_auth__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModal"] }
    ]; };
    RoleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-role',
            template: __webpack_require__(/*! raw-loader!./role.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/role/role.component.html"),
            styles: [__webpack_require__(/*! ./role.component.scss */ "./src/app/views/pages/admin/role/role.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_auth__WEBPACK_IMPORTED_MODULE_5__["AuthService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModal"]])
    ], RoleComponent);
    return RoleComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/admin/sensor-type/sensor-type.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/views/pages/admin/sensor-type/sensor-type.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL2FkbWluL3NlbnNvci10eXBlL3NlbnNvci10eXBlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/pages/admin/sensor-type/sensor-type.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/views/pages/admin/sensor-type/sensor-type.component.ts ***!
  \************************************************************************/
/*! exports provided: SensorTypeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SensorTypeComponent", function() { return SensorTypeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _core_base_layout_models_datatable_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @core/_base/layout/models/datatable.model */ "./src/app/core/_base/layout/models/datatable.model.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _core_admin__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @core/admin */ "./src/app/core/admin/index.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");










var SensorTypeComponent = /** @class */ (function () {
    function SensorTypeComponent(sensor, formBuilder, toastr, cdr, translate, modalService) {
        this.sensor = sensor;
        this.formBuilder = formBuilder;
        this.toastr = toastr;
        this.cdr = cdr;
        this.translate = translate;
        this.modalService = modalService;
        this.dataTable = new _core_base_layout_models_datatable_model__WEBPACK_IMPORTED_MODULE_6__["DataTable"]();
        this.columns = [];
        this.data = [];
        this.totalRecod = 0;
        this.paginationSelect = [];
        this.isEdit = false;
        this.dataDefault = [{}];
        this.translationsArr = [];
        this.parameters = [{}];
        this.isRepeater = false;
        this.unsubscribe = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
        this.buildForm();
        this.setColumns();
        this.buildPagination();
        this.setPaginationSelect();
        this.setDataTable();
    }
    SensorTypeComponent.prototype.ngOnInit = function () {
        this.updateDataTable();
        this.getData();
    };
    SensorTypeComponent.prototype.buildForm = function () {
        this.formSensor = this.formBuilder.group({
            nameSensor: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            typeSensor: [''],
            description: [''],
        });
        this.searchFormSensor = this.formBuilder.group({
            nameSensor: ['']
        });
    };
    SensorTypeComponent.prototype.buttonAddNew = function (content) {
        this.parameters = [];
        this.isEdit = false;
        this.formSensor.reset();
        this.open(content);
        this.addRepeaterForm('#kt_repeater_1', true);
    };
    SensorTypeComponent.prototype.addRepeaterForm = function (classDiv, initEmpty) {
        if (initEmpty === void 0) { initEmpty = false; }
        setTimeout(function () {
            $(classDiv).repeater({
                initEmpty: initEmpty,
                defaultValues: {},
                show: function () {
                    $(this).slideDown();
                },
                hide: function (deleteElement) {
                    $(this).slideUp(deleteElement);
                }
            });
        }, 600);
    };
    SensorTypeComponent.prototype.createParamsets = function () {
        setTimeout(function () {
            document.getElementById('create-paramsets').scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start' });
        }, 300);
    };
    SensorTypeComponent.prototype.open = function (content) {
        var _this = this;
        this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder' }).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    SensorTypeComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    SensorTypeComponent.prototype.getContentTable = function (elementTable) {
        var content = {};
        for (var i = 0; i < elementTable.length; i++) {
            var key = elementTable[i].firstChild.textContent;
            if (key != '') {
                content[elementTable[i].firstChild.textContent] = elementTable[i].childNodes[1].textContent;
            }
        }
        return JSON.stringify(content);
    };
    SensorTypeComponent.prototype.onSubmitSensor = function (form) {
        var _this = this;
        var elements = $("#table-sensor tbody tr");
        var parameters = this.getContentTable(elements);
        if (this.formSensor.invalid) {
            return;
        }
        var sensorType = new _core_admin__WEBPACK_IMPORTED_MODULE_8__["SensorType"]({
            "name": form.value.nameSensor,
            "description": form.value.description,
            "parameters": parameters,
        });
        if (this.isEdit) {
            sensorType.id = this.idSensorEdit;
            this.sensor.update(sensorType, { notifyGlobal: true }).subscribe(function (result) {
                if (result.status == 200) {
                    _this.getData();
                }
            });
            this.modalService.dismissAll();
            return;
        }
        this.sensor.create(sensorType, { notifyGlobal: true })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () {
            _this.cdr.markForCheck();
        }))
            .subscribe(function (result) {
            if (result.status == 201) {
                _this.getData();
            }
        });
        this.modalService.dismissAll();
    };
    SensorTypeComponent.prototype.buildPagination = function () {
        this.filter = {
            pageNo: 1,
            pageSize: 10
        };
    };
    SensorTypeComponent.prototype.onHideModal = function (id) {
        var idModal = '#' + id;
        $(idModal).modal('hide');
    };
    SensorTypeComponent.prototype.setPaginationSelect = function () {
        this.paginationSelect = [10, 20, 30, 40, 50];
    };
    SensorTypeComponent.prototype.getIdAction = function (id, idModal) {
        this.idSensorDelete = id;
        this.openModal({ idModal: idModal, confirm: true });
    };
    SensorTypeComponent.prototype.openModal = function (params) {
        var idModal = params.idModal;
        var confirm = params.confirm;
        if (confirm) {
            $(idModal).appendTo('body').modal({ backdrop: 'static', keyboard: false });
        }
        else {
            $(idModal).appendTo('body').modal('show');
        }
    };
    SensorTypeComponent.prototype.deleteSensor = function () {
        var _this = this;
        var id = this.idSensorDelete;
        this.sensor.delete(id, { notifyGlobal: true }).subscribe(function (data) {
            if (data.status == 200) {
                _this.getData();
            }
        });
        $('.modal').modal('hide');
    };
    SensorTypeComponent.prototype.editSensor = function (id, content) {
        var _this = this;
        this.addRepeaterForm('#kt_repeater_1');
        this.isEdit = true;
        this.idSensorEdit = id;
        var params = {
            id: id
        };
        this.sensor.list(params).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
            _this.parameters = _this.convertParameters(data.result.content[0].parameters);
            _this.dataDefault = data.result.content;
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () {
            _this.cdr.markForCheck();
        })).subscribe();
        this.open(content);
    };
    SensorTypeComponent.prototype.convertParameters = function (parameters) {
        var result = [];
        for (var property in parameters) {
            var obj = {
                key: property,
                value: parameters[property]
            };
            result.push(obj);
        }
        return result;
    };
    SensorTypeComponent.prototype.loadSelectBootstrap = function () {
        $('.bootstrap-select').selectpicker();
    };
    SensorTypeComponent.prototype.searchSensor = function (form) {
        this.filter.name = form.value.nameSensor;
        this.getData();
    };
    SensorTypeComponent.prototype.updateDataTable = function () {
        var _this = this;
        this.dataTable.eventUpdate.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (option) {
            _this.filter = option;
            _this.getData();
        })).subscribe();
    };
    Object.defineProperty(SensorTypeComponent.prototype, "f", {
        get: function () {
            if (this.formSensor != undefined)
                return this.formSensor.controls;
        },
        enumerable: true,
        configurable: true
    });
    SensorTypeComponent.prototype.getData = function () {
        var _this = this;
        this.sensor.list(this.filter)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["debounceTime"])(1000), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["finalize"])(function () {
            _this.cdr.markForCheck();
        }))
            .subscribe(function (result) {
            if (result.status == 200) {
                _this.data = result.result.content;
                _this.totalRecod = result.result.totalRecord;
                _this.dataTable.update({
                    data: _this.data,
                    totalRecod: _this.totalRecod
                });
                $(function () {
                    $('select').selectpicker();
                });
            }
        });
    };
    SensorTypeComponent.prototype.setDataTable = function () {
        this.dataTable.init({
            data: this.data,
            totalRecod: this.totalRecod,
            paginationSelect: this.paginationSelect,
            columns: this.columns,
            layout: {
                body: {
                    scrollable: false,
                    maxHeight: 600,
                },
                selecter: false,
            }
        });
    };
    SensorTypeComponent.prototype.onResize = function ($elm) {
        this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
        this.cdr.detectChanges();
    };
    SensorTypeComponent.prototype.setColumns = function () {
        this.columns = [
            {
                title: '#',
                field: 'no',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '40px' },
                class: 't-datatable__cell--center',
                translate: '#',
                autoHide: false,
                width: 20,
            },
            {
                title: 'Name',
                field: 'name',
                allowSort: true,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'COMMON.COLUMN.NAME',
                autoHide: false,
                width: 182,
            },
            {
                title: 'Edit By',
                field: 'editBy',
                allowSort: true,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'COMMON.EDIT_BY',
                autoHide: false,
                width: 182,
            },
            {
                title: 'Description',
                field: 'description',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'COMMON.COLUMN.DESCRIPTION',
                autoHide: true,
                width: 182,
            },
            {
                title: 'Created update',
                field: 'created',
                allowSort: true,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'COMMON.COLUMN.UPDATED_DATE',
                autoHide: true,
                width: 182,
            },
            {
                title: 'Actions',
                field: 'action',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '110px' },
                class: '',
                translate: 'COMMON.ACTIONS.ACTIONS',
                autoHide: false,
                width: 182,
            },
        ];
    };
    SensorTypeComponent.ctorParameters = function () { return [
        { type: _core_admin__WEBPACK_IMPORTED_MODULE_8__["SensorTypeService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateService"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__["NgbModal"] }
    ]; };
    SensorTypeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-sensor-type',
            template: __webpack_require__(/*! raw-loader!./sensor-type.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/sensor-type/sensor-type.component.html"),
            styles: [__webpack_require__(/*! ./sensor-type.component.scss */ "./src/app/views/pages/admin/sensor-type/sensor-type.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_admin__WEBPACK_IMPORTED_MODULE_8__["SensorTypeService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateService"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_9__["NgbModal"]])
    ], SensorTypeComponent);
    return SensorTypeComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/admin/sim-type/sim-type.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/views/pages/admin/sim-type/sim-type.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL2FkbWluL3NpbS10eXBlL3NpbS10eXBlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/pages/admin/sim-type/sim-type.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/views/pages/admin/sim-type/sim-type.component.ts ***!
  \******************************************************************/
/*! exports provided: SimTypeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SimTypeComponent", function() { return SimTypeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _core_admin__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @core/admin */ "./src/app/core/admin/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _core_base_layout_models_datatable_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @core/_base/layout/models/datatable.model */ "./src/app/core/_base/layout/models/datatable.model.ts");








var TITLE_FORM_EDIT = "COMMON.ACTIONS.EDIT";
var TITLE_FORM_ADD = "COMMON.ACTIONS.ADD";
var TITLE_FORM_DELETE = "COMMON.ACTIONS.DELETE";
var ACTION;
(function (ACTION) {
    ACTION[ACTION["edit"] = 0] = "edit";
    ACTION[ACTION["add"] = 1] = "add";
    ACTION[ACTION["delete"] = 2] = "delete";
})(ACTION || (ACTION = {}));
;
var SimTypeComponent = /** @class */ (function () {
    function SimTypeComponent(cdr, modalService, fb, simTypeService) {
        this.cdr = cdr;
        this.modalService = modalService;
        this.fb = fb;
        this.simTypeService = simTypeService;
        this.dataTable = new _core_base_layout_models_datatable_model__WEBPACK_IMPORTED_MODULE_7__["DataTable"]();
        this.titlePopup = TITLE_FORM_ADD;
        this.check = true;
        this.valueTest = "";
        this.action = "";
        this.tableConfig = {
            pagination: [
                10, 20, 30, 50
            ],
            columns: [
                {
                    title: '#',
                    field: 'no',
                    allowSort: false,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '20px' },
                    width: 20,
                    class: 't-datatable__cell--center',
                    translate: '#',
                    autoHide: false,
                },
                {
                    title: 'Key',
                    field: 'nameKey',
                    allowSort: true,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '250px' },
                    width: 182,
                    class: '',
                    translate: 'ADMIN.SIM_TYPE.GENERAL.NAME_KEY',
                    autoHide: false,
                },
                {
                    title: 'Name',
                    field: 'nameKey2',
                    allowSort: false,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '250px' },
                    width: 182,
                    class: '',
                    translate: 'ADMIN.SIM_TYPE.GENERAL.NAME',
                },
                {
                    title: 'Created Date',
                    field: 'createdDate',
                    allowSort: false,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '182px' },
                    width: 182,
                    class: '',
                    translate: 'ADMIN.SIM_TYPE.GENERAL.CREATED_DATE',
                },
                {
                    title: 'Sort Order',
                    field: 'sortOrder',
                    allowSort: true,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '182px' },
                    class: '',
                    width: 182,
                    translate: 'ADMIN.SIM_TYPE.GENERAL.SORT_ORDER',
                },
                {
                    title: 'Actions',
                    field: 'action',
                    allowSort: false,
                    isSort: false,
                    dataSort: '',
                    style: { 'width': '110px' },
                    class: '',
                    width: 110,
                    translate: 'COMMON.ACTIONS.ACTIONS',
                    autoHide: false,
                },
            ]
        };
        this.unsubscribe = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.dataTable.init({
            data: [],
            totalRecod: 0,
            paginationSelect: [1, 2, 5, 10, 20],
            columns: this.tableConfig.columns,
            formSearch: '#formSearch',
            isDebug: true,
            layout: {
                body: {
                    scrollable: false,
                    maxHeight: 600,
                },
            }
        });
        this.popupEdit = {};
    }
    SimTypeComponent.prototype.ngOnInit = function () {
        var _this = this;
        // listening dataTable event
        this.dataTable.eventUpdate.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (option) {
            _this.getData(option);
        })).subscribe();
        this.dataTable.reload({});
        this.createForm();
        $(function () {
            $('select').selectpicker();
        });
    };
    /**
     * Create form
     * @param simType model
     */
    SimTypeComponent.prototype.createForm = function (simType) {
        this.simTypeForm = this.fb.group({
            nameKey: [(simType ? simType.nameKey : ""), _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            sortOrder: [(simType ? simType.sortOrder : 1)],
        });
    };
    /**
     * Get list by data option
     * @param option option search list
     */
    SimTypeComponent.prototype.getData = function (option) {
        var _this = this;
        this.simTypeService.list({ params: option }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (data) {
            _this.dataTable.update({
                data: data.result.content,
                totalRecod: data.result.totalRecord
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
            _this.cdr.markForCheck();
        })).subscribe();
    };
    /**
     * Open popup template
     * @param content template
     * @param type action type
     * @param data data for content
     */
    SimTypeComponent.prototype.open = function (content, type, data) {
        var _this = this;
        this.action = type;
        switch (type) {
            case 'edit':
                this.titlePopup = TITLE_FORM_EDIT;
                this.popupEdit.simType = new _core_admin__WEBPACK_IMPORTED_MODULE_5__["SimType"]();
                this.popupEdit.simType.id = data.id;
                this.popupEdit.simType.nameKey = data.name;
                this.popupEdit.simType.sortOrder = data.sortOrder;
                this.createForm(this.popupEdit.simType);
                break;
            case 'add':
                this.titlePopup = TITLE_FORM_ADD;
                this.popupEdit.simType = new _core_admin__WEBPACK_IMPORTED_MODULE_5__["SimType"]();
                this.createForm();
                break;
            case 'delete':
                this.titlePopup = TITLE_FORM_DELETE;
                this.popupEdit.simType = new _core_admin__WEBPACK_IMPORTED_MODULE_5__["SimType"]();
                this.popupEdit.simType.id = data.id;
                break;
        }
        this.modalService.open(content, { windowClass: 'kt-mt-50', backdrop: 'static' }).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
            _this.popupEdit.reason = reason;
        });
    };
    /**
     * Dismiss Reason Popup
     * @param reason
     */
    SimTypeComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    /**
     * Submit actions
     */
    SimTypeComponent.prototype.onSubmit = function () {
        var _this = this;
        switch (this.action) {
            case "edit":
                if (!this.simTypeForm.invalid) {
                    this.popupEdit.simType.nameKey = this.simTypeForm.get('nameKey').value;
                    this.popupEdit.simType.sortOrder = this.simTypeForm.get('sortOrder').value;
                    this.simTypeService.update(this.popupEdit.simType, { notifyGlobal: true }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (data) {
                        if (data.status == 200) {
                            _this.modalService.dismissAll(_this.popupEdit.reason);
                            _this.dataTable.reload({});
                        }
                    }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
                        _this.cdr.markForCheck();
                    })).subscribe();
                }
                break;
            case "add":
                if (!this.simTypeForm.invalid) {
                    this.popupEdit.simType.nameKey = this.simTypeForm.get('nameKey').value;
                    this.popupEdit.simType.sortOrder = this.simTypeForm.get('sortOrder').value;
                    this.simTypeService.create(this.popupEdit.simType, {
                        notifyGlobal: true
                    }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (data) {
                        if (data.status == 201) {
                            _this.modalService.dismissAll(_this.popupEdit.reason);
                            _this.dataTable.reload({});
                        }
                    }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
                        _this.cdr.markForCheck();
                    })).subscribe();
                }
                break;
            case "delete":
                this.simTypeService.delete(this.popupEdit.simType.id, { notifyGlobal: true }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (data) {
                    if (data.status == 200) {
                        _this.modalService.dismissAll(_this.popupEdit.reason);
                        _this.dataTable.reload({});
                    }
                }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
                    _this.cdr.markForCheck();
                })).subscribe();
                break;
        }
    };
    /**
     * Checking control validation
     *
     * @param controlName: string => Equals to formControlName
     * @param validationType: string => Equals to valitors name
     */
    SimTypeComponent.prototype.isControlHasError = function (controlName, validationType) {
        var control = this.simTypeForm.controls[controlName];
        if (!control) {
            return false;
        }
        var result = control.hasError(validationType) && (control.dirty || control.touched);
        return result;
    };
    SimTypeComponent.prototype.test = function () {
        this.check = !this.check;
        if (this.check) {
            this.dataTable.updateLayout({ width: 1024, height: 800 });
        }
        else {
            this.dataTable.updateLayout({ width: 600, height: 800 });
        }
    };
    SimTypeComponent.prototype.onResize = function ($elm) {
        // this.check= !this.check;
        // if(this.check){
        // 	this.valueTest = "123";
        // }
        // else{
        // 	this.valueTest = "abc";
        // }
        this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
        // this.cdr.markForCheck();
    };
    SimTypeComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"] },
        { type: _core_admin__WEBPACK_IMPORTED_MODULE_5__["SimTypeService"] }
    ]; };
    SimTypeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-sim-type',
            template: __webpack_require__(/*! raw-loader!./sim-type.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/sim-type/sim-type.component.html"),
            styles: [__webpack_require__(/*! ./sim-type.component.scss */ "./src/app/views/pages/admin/sim-type/sim-type.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModal"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
            _core_admin__WEBPACK_IMPORTED_MODULE_5__["SimTypeService"]])
    ], SimTypeComponent);
    return SimTypeComponent;
}());



/***/ }),

/***/ "./src/app/views/pages/admin/transport-type/transport-type.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/views/pages/admin/transport-type/transport-type.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3BhZ2VzL2FkbWluL3RyYW5zcG9ydC10eXBlL3RyYW5zcG9ydC10eXBlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/pages/admin/transport-type/transport-type.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/views/pages/admin/transport-type/transport-type.component.ts ***!
  \******************************************************************************/
/*! exports provided: TransportTypeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransportTypeComponent", function() { return TransportTypeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _core_base_layout_models_datatable_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @core/_base/layout/models/datatable.model */ "./src/app/core/_base/layout/models/datatable.model.ts");
/* harmony import */ var _core_admin__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @core/admin */ "./src/app/core/admin/index.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");








var TransportTypeComponent = /** @class */ (function () {
    function TransportTypeComponent(transport, formBuilder, cdr, modalService) {
        this.transport = transport;
        this.formBuilder = formBuilder;
        this.cdr = cdr;
        this.modalService = modalService;
        this.dataTable = new _core_base_layout_models_datatable_model__WEBPACK_IMPORTED_MODULE_5__["DataTable"]();
        this.columns = [];
        this.data = [];
        this.totalRecod = 0;
        this.paginationSelect = [];
        this.isEdit = false;
        this.dataDefault = [{}];
        // qcvn
        this.listQCVN = [
            { id: 100, name: 'Khach = 100' },
            { id: 200, name: 'Bus = 200' },
            { id: 300, name: 'HopDong = 300' },
            { id: 400, name: 'DuLich = 400' },
            { id: 500, name: 'Container = 500' },
            { id: 600, name: 'XeTai = 600' },
            { id: 700, name: 'Taxi = 700' },
            { id: 800, name: 'TaxiTai = 800' },
            { id: 900, name: 'XeDauKeo = 900' },
        ];
        this.unsubscribe = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subject"]();
        this.buildForm();
        this.setColumns();
        this.buildPagination();
        this.setPaginationSelect();
        this.setDataTable();
    }
    TransportTypeComponent.prototype.ngOnInit = function () {
        this.updateDataTable();
        this.getData();
    };
    TransportTypeComponent.prototype.buildForm = function () {
        this.formTransportType = this.formBuilder.group({
            nameTransportType: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            nameKey: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            limitSpeed: [''],
            qcvnCode: [''],
        });
        this.searchFormTransportType = this.formBuilder.group({
            nameTransportType: ['']
        });
    };
    TransportTypeComponent.prototype.buttonAddNew = function (content) {
        this.isEdit = false;
        this.formTransportType.reset();
        this.open(content);
        setTimeout(function () {
            $('.kt_selectpicker').selectpicker('refresh');
        }, 100);
    };
    TransportTypeComponent.prototype.open = function (content) {
        var _this = this;
        this.modalService.open(content, { windowClass: 'kt-mt-50 modal-dialog-scrollable modal-holder' }).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    TransportTypeComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    TransportTypeComponent.prototype.onSubmitTransportType = function (form) {
        var _this = this;
        if (this.formTransportType.invalid) {
            return;
        }
        var TransportType = {
            name: form.value.nameTransportType,
            nameKey: form.value.nameKey,
            limitSpeed: form.value.limitSpeed,
            qcvnCode: form.value.qcvnCode,
        };
        if (this.isEdit) {
            TransportType.id = this.idTransportTypeEdit;
            this.transport.update(TransportType, { notifyGlobal: true }).subscribe(function (result) {
                if (result.status == 200) {
                    _this.getData();
                    _this.modalService.dismissAll();
                }
            });
            return;
        }
        this.transport.create(TransportType, { notifyGlobal: true })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(function () {
            _this.cdr.markForCheck();
        }))
            .subscribe(function (result) {
            if (result.status == 201) {
                _this.getData();
                _this.modalService.dismissAll();
            }
        });
    };
    TransportTypeComponent.prototype.buildPagination = function () {
        this.filter = {
            pageNo: 1,
            pageSize: 10
        };
    };
    TransportTypeComponent.prototype.onHideModal = function (id) {
        var idModal = '#' + id;
        $(idModal).modal('hide');
    };
    TransportTypeComponent.prototype.setPaginationSelect = function () {
        this.paginationSelect = [10, 20, 30, 40, 50];
    };
    TransportTypeComponent.prototype.getNameQCVN = function (id) {
        var name = '';
        var item = this.listQCVN.find(function (x) { return x.id === id; });
        if (item != undefined)
            name = item.name;
        return name;
    };
    TransportTypeComponent.prototype.getIdAction = function (id, idModal) {
        this.idTransportTypeDelete = id;
        this.openModal({ idModal: idModal, confirm: true });
    };
    TransportTypeComponent.prototype.openModal = function (params) {
        var idModal = params.idModal;
        var confirm = params.confirm;
        if (confirm) {
            $(idModal).appendTo('body').modal({ backdrop: 'static', keyboard: false });
        }
        else {
            $(idModal).appendTo('body').modal('show');
        }
    };
    TransportTypeComponent.prototype.deleteTransportType = function () {
        var _this = this;
        var id = this.idTransportTypeDelete;
        this.transport.delete(id, { notifyGlobal: true }).subscribe(function (data) {
            if (data.status == 200) {
                _this.getData();
            }
        });
        $('.modal').modal('hide');
    };
    TransportTypeComponent.prototype.editTransportType = function (id, content) {
        var _this = this;
        this.isEdit = true;
        this.idTransportTypeEdit = id;
        var params = {
            id: id
        };
        this.transport.list(params).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (data) {
            _this.dataDefault = data.result.content;
            setTimeout(function () {
                $('.kt_selectpicker').selectpicker('refresh');
            }, 100);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(function () {
            _this.cdr.markForCheck();
        })).subscribe();
        this.open(content);
    };
    TransportTypeComponent.prototype.loadSelectBootstrap = function () {
        $('.bootstrap-select').selectpicker();
    };
    TransportTypeComponent.prototype.searchTransportType = function (form) {
        this.filter.name = form.value.nameTransportType;
        this.getData();
    };
    TransportTypeComponent.prototype.updateDataTable = function () {
        var _this = this;
        this.dataTable.eventUpdate.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (option) {
            _this.filter = option;
            _this.getData();
        })).subscribe();
    };
    Object.defineProperty(TransportTypeComponent.prototype, "f", {
        get: function () {
            if (this.formTransportType != undefined)
                return this.formTransportType.controls;
        },
        enumerable: true,
        configurable: true
    });
    TransportTypeComponent.prototype.getData = function () {
        var _this = this;
        this.transport.list(this.filter)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["debounceTime"])(1000), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["takeUntil"])(this.unsubscribe), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(function () {
            _this.cdr.markForCheck();
        }))
            .subscribe(function (result) {
            if (result.status == 200) {
                _this.data = result.result.content;
                _this.totalRecod = result.result.totalRecord;
                _this.dataTable.update({
                    data: _this.data,
                    totalRecod: _this.totalRecod
                });
                $(function () {
                    $('select').selectpicker();
                });
            }
        });
    };
    TransportTypeComponent.prototype.setDataTable = function () {
        this.dataTable.init({
            data: this.data,
            totalRecod: this.totalRecod,
            paginationSelect: this.paginationSelect,
            columns: this.columns,
            layout: {
                body: {
                    scrollable: false,
                    maxHeight: 600,
                },
                selecter: false,
            }
        });
    };
    TransportTypeComponent.prototype.onResize = function ($elm) {
        this.dataTable.updateLayout({ width: $elm.width, height: $elm.height });
    };
    TransportTypeComponent.prototype.setColumns = function () {
        this.columns = [
            {
                title: '#',
                field: 'no',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '40px' },
                class: 't-datatable__cell--center',
                translate: '#',
                autoHide: false,
                width: 20,
            },
            {
                title: 'Name',
                field: 'name',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'COMMON.COLUMN.NAME',
                autoHide: false,
                width: 182,
            },
            {
                title: 'Limit Speed',
                field: 'limitspeed',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'ADMIN.TRANSPORT_TYPE.COLUMN.LIMITSPEDD',
                autoHide: true,
                width: 182,
            },
            {
                title: 'QNCN',
                field: 'qcvnCode',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'ADMIN.TRANSPORT_TYPE.COLUMN.QNCN_CODE',
                autoHide: true,
                width: 182,
            },
            {
                title: 'Create by',
                field: 'createdBy',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'COMMON.COLUMN.CREATED_BY',
                autoHide: true,
                width: 182,
            },
            {
                title: 'Created update',
                field: 'created',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '182px' },
                class: '',
                translate: 'COMMON.COLUMN.UPDATED_DATE',
                autoHide: true,
                width: 182,
            },
            {
                title: 'Actions',
                field: 'action',
                allowSort: false,
                isSort: false,
                dataSort: '',
                style: { 'width': '110px' },
                class: '',
                translate: 'COMMON.ACTIONS.ACTIONS',
                autoHide: false,
                width: 182,
            },
        ];
    };
    TransportTypeComponent.ctorParameters = function () { return [
        { type: _core_admin__WEBPACK_IMPORTED_MODULE_6__["TransportTypeService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
        { type: _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModal"] }
    ]; };
    TransportTypeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'kt-transport-type',
            template: __webpack_require__(/*! raw-loader!./transport-type.component.html */ "./node_modules/raw-loader/index.js!./src/app/views/pages/admin/transport-type/transport-type.component.html"),
            styles: [__webpack_require__(/*! ./transport-type.component.scss */ "./src/app/views/pages/admin/transport-type/transport-type.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_admin__WEBPACK_IMPORTED_MODULE_6__["TransportTypeService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModal"]])
    ], TransportTypeComponent);
    return TransportTypeComponent;
}());



/***/ })

}]);
//# sourceMappingURL=app-views-pages-admin-admin-module.js.map