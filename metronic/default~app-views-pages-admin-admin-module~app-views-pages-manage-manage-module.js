(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~app-views-pages-admin-admin-module~app-views-pages-manage-manage-module"],{

/***/ "./src/app/core/_base/layout/models/datatable.model.ts":
/*!*************************************************************!*\
  !*** ./src/app/core/_base/layout/models/datatable.model.ts ***!
  \*************************************************************/
/*! exports provided: DataTable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataTable", function() { return DataTable; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");

var className = "DataTable";
/**
 * DataTable manage page
 * 1. Manage pagination
 * 2. Sort order by field name
 * 3. Search by data option and formSearch
 */
var DataTable = /** @class */ (function () {
    /**
     * Creates an instance of this class that can
     * Set default value
     */
    function DataTable() {
        this.lastWidth = 0;
        this.lastChecked = 0;
        this.lastRowShow = -1;
        this.isShowSubRow = false;
        this.eventUpdate = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.isLoading = false;
        this.isDebug = false;
        this.lastColumnSort = '';
        this.api = {
            pagination: {
                currentPage: 1,
                pageSize: 10,
            },
            query: [],
            sorts: {
                sort: "",
                field: ""
            }
        };
        this.paginations = {
            total: 0,
            pageSize: 10,
            currentPage: 1,
            maxPageDisplay: 5,
            pageDisplays: [1, 2, 3, 4, 5],
            firstPage: 1,
            lastPage: 1,
            form: 0,
            to: 0,
        };
        this.config = {
            paginationSelect: [10, 20, 30, 50, 100]
        };
        this.isNotFound = false;
        this.layout = {
            body: {
                scrollable: true,
                maxHeight: 500,
            },
            selecter: false,
        };
        this.isCheckBoxAll = false;
    }
    /**
     * @returns all page display
     */
    DataTable.prototype.getPageDisplay = function () {
        return this.paginations.pageDisplays;
    };
    /**
     * @returns first page
     */
    DataTable.prototype.getFirstPage = function () {
        return this.paginations.firstPage;
    };
    /**
     * @returns last page
     */
    DataTable.prototype.getLastPage = function () {
        return this.paginations.lastPage;
    };
    /**
     * @returns current page select
     */
    DataTable.prototype.getCurrentPage = function () {
        return this.paginations.currentPage;
    };
    /**
     * @returns pagination select
     */
    DataTable.prototype.getPaginationSelect = function () {
        return this.config.paginationSelect;
    };
    /**
     * @returns paginations config
     */
    DataTable.prototype.getPaginations = function () {
        return this.paginations;
    };
    /**
     * @returns columns config
     */
    DataTable.prototype.getColumns = function () {
        return this.columns;
    };
    DataTable.prototype.setColumns = function (columns) {
        var columnDefault = {
            field: '',
            title: "",
            sortable: "",
            width: "0",
            type: "",
            selector: false,
            textAlign: "",
            format: "",
            class: "",
            style: "",
            template: "",
            allowSort: false,
            isSort: false,
            autoHide: true,
            isShow: true
        };
        var columnNew;
        columnNew = [];
        columns.forEach(function (element) {
            columnNew.push(Object.assign({}, columnDefault, element));
        });
        this.columns = columnNew;
    };
    /**
     * Go to page number
     * @param page
     */
    DataTable.prototype.gotoPage = function (page) {
        if (this.isDebug) {
            console.info("%s call gotoPage()", className);
        }
        if (page >= this.paginations.firstPage && page <= this.paginations.lastPage && page != this.paginations.currentPage) {
            this.reload({ currentPage: page });
        }
    };
    /**
       * Set page size
       * Reload page and set default currentPage = 1
       * @param pageSize number pagesize
       */
    DataTable.prototype.setPageSize = function (pageSize) {
        if (this.isDebug) {
            console.info("%s call setPageSize()", className);
        }
        if (pageSize > 0 && pageSize != this.paginations.pageSize) {
            this.reload({ pageSize: pageSize, currentPage: 1 });
        }
    };
    DataTable.prototype.getDataSearch = function () {
        return Object.assign({}, this.dataSearch);
    };
    /**
     * Init this class
     * @param option data setting
     */
    DataTable.prototype.init = function (option) {
        if (option.isDebug) {
            console.info("%s call init()", className);
        }
        // set value with data option
        if (option.data)
            this.setData(option.data);
        if (option.currentPage)
            this.paginations.currentPage = option.currentPage;
        if (option.totalRecod)
            this.paginations.total = option.totalRecod;
        if (option.pageSize)
            this.paginations.pageSize = option.pageSize;
        if (option.paginationSelect)
            this.config.paginationSelect = option.paginationSelect;
        if (option.maxPageDisplay) {
            if (option.maxPageDisplay > 0 && option.maxPageDisplay < 10)
                this.paginations.maxPageDisplay = option.maxPageDisplay;
        }
        if (option.columns)
            this.setColumns(option.columns);
        if (option.formSearch)
            this.formSearch = option.formSearch;
        if (option.isDebug)
            this.isDebug = option.isDebug;
        if (option.layout)
            this.layout = Object.assign({}, this.layout, option.layout);
        // const
        // //update data
        // console.log(document.querySelector('.datatable'));
        // resizeObserver.observe(document.querySelector('.datatable'));      
        this.update(option);
    };
    ;
    /**
     * Process and update data table by option
     * @param option data config
     */
    DataTable.prototype.update = function (option) {
        if (this.isDebug) {
            console.info("%s call update()", className);
        }
        if (option.data) {
            this.setData(option.data);
            // this.data = option.data;
        }
        if (option.totalRecod != undefined) {
            this.paginations.total = option.totalRecod;
        }
        if (option.pageSize) {
            this.paginations.pageSize = option.pageSize;
        }
        // set first page and last page
        this.paginations.firstPage = 1;
        if (this.paginations.total % this.paginations.pageSize == 0) {
            this.paginations.lastPage = Math.trunc(this.paginations.total / this.paginations.pageSize);
        }
        else {
            this.paginations.lastPage = Math.trunc(this.paginations.total / this.paginations.pageSize) + 1;
        }
        // check end set current page
        if (option.currentPage) {
            if (option.currentPage < this.paginations.lastPage && option.currentPage > 0)
                this.paginations.currentPage = option.currentPage;
        }
        if (this.paginations.currentPage > this.paginations.lastPage && this.paginations.lastPage > 0)
            this.paginations.currentPage = this.paginations.lastPage;
        else if (this.paginations.currentPage < 1)
            this.paginations.currentPage = 1;
        // set from to.
        if (this.paginations.total == 0) {
            this.paginations.form = 0;
            this.paginations.to = 0;
        }
        else {
            this.paginations.form = 1 + (this.paginations.currentPage - 1) * this.paginations.pageSize;
            var to = this.paginations.pageSize * this.paginations.currentPage;
            if (this.paginations.total < to)
                this.paginations.to = this.paginations.total;
            else
                this.paginations.to = to;
        }
        // splipt array if data > page size.
        if (this.data.length > this.paginations.pageSize) {
            this.data = this.data.slice(0, this.paginations.pageSize);
        }
        // else{
        //     let checkSize = this.paginations.form-this.paginations.to;
        //     if(this.data.length>checkSize)
        //     {
        //         this.data = this.data.slice(0,checkSize+1); 
        //     }
        // }
        // set pagination display
        if (this.data.length >= 0) {
            this.paginations.pageDisplays = [1];
            if (this.paginations.lastPage < this.paginations.maxPageDisplay) {
                this.paginations.pageDisplays = [];
                for (var i = 1; i <= this.paginations.lastPage; i++) {
                    this.paginations.pageDisplays.push(i);
                }
            }
            else {
                var pageStart = 1;
                this.paginations.pageDisplays = [];
                if (this.paginations.currentPage % this.paginations.maxPageDisplay == 0) {
                    pageStart = (Math.trunc(this.paginations.currentPage / this.paginations.maxPageDisplay) - 1) * this.paginations.maxPageDisplay + 1;
                }
                else {
                    pageStart = (Math.trunc(this.paginations.currentPage / this.paginations.maxPageDisplay)) * this.paginations.maxPageDisplay + 1;
                }
                for (var i = 0; i < this.paginations.maxPageDisplay; i++) {
                    if ((pageStart + i) <= this.paginations.lastPage) {
                        this.paginations.pageDisplays.push(pageStart + i);
                    }
                }
            }
        }
        else
            this.paginations.pageDisplays = [1];
        // off loading datatable  
        this.isLoading = false;
        if (this.data.length == 0) {
            this.isNotFound = true;
        }
        else
            this.isNotFound = false;
        $(function () {
            $('bootstrap-select').selectpicker();
            // auto format text imei
            $(".auto-format-text").change(function () {
                var val = $(this).val().replace(/,/g, "\n").replace(/ /g, "");
                $(this).val(val);
            });
        });
    };
    ;
    /**
     * Reload data table after change page
     * @param option setting
     */
    DataTable.prototype.reload = function (option) {
        if (this.isDebug) {
            console.info("%s call reload()", className);
        }
        // on loading datatable     
        this.isLoading = true;
        this.isCheckBoxAll = false;
        // set value by value option
        if (option.data) {
            this.setData(option.data);
            // this.data = option.data;
        }
        if (option.currentPage) {
            this.paginations.currentPage = option.currentPage;
        }
        if (option.totalRecod) {
            this.paginations.total = option.totalRecod;
        }
        if (option.paginationSelect) {
            this.config.paginationSelect = option.paginationSelect;
        }
        if (option.pageSize) {
            this.paginations.pageSize = option.pageSize;
        }
        if (option.maxPageDisplay) {
            if (option.maxPageDisplay > 0 && option.maxPageDisplay < 10)
                this.paginations.maxPageDisplay = option.maxPageDisplay;
        }
        // set value for api
        this.api.pagination.currentPage = this.paginations.currentPage;
        this.api.pagination.pageSize = this.paginations.pageSize;
        var dataSearch = {};
        dataSearch['pageNo'] = this.api.pagination.currentPage;
        dataSearch['pageSize'] = this.api.pagination.pageSize;
        if (this.api.query) {
            this.api.query.forEach(function (value) {
                dataSearch[value.key] = value.value;
            });
        }
        if (this.api.sorts.field.length > 0) {
            dataSearch['orderBy'] = this.api.sorts.field;
            dataSearch['orderType'] = this.api.sorts.sort;
        }
        this.dataSearch = dataSearch;
        // send emit
        this.eventUpdate.emit(dataSearch);
    };
    /**
     * Search option
     * 1. Important searh with query
     * 2. If search param is undefined => search with formSearch
     * @param query  query search
     */
    DataTable.prototype.search = function (query, searchByform) {
        var _this = this;
        if (this.isDebug) {
            console.info("%s call search()", className);
        }
        var newQuery = [];
        // check search with query
        if (query) {
            var orderBy_1 = query.find(function (x) { return x.key == "orderBy"; });
            var sort = query.find(function (x) { return x.key == "orderType"; });
            if (orderBy_1 != undefined) {
                this.lastColumnSort = orderBy_1.value;
                for (var i = 0; i < this.columns.length; i++) {
                    this.columns[i]['isSort'] = false;
                }
                var index = this.columns.findIndex(function (x) { return x.field == orderBy_1.value; });
                if (index >= 0) {
                    this.columns[index].isSort = true;
                }
                this.api.sorts.field = orderBy_1.value;
                this.api.sorts.sort = sort ? sort.value : "asc";
            }
            query.forEach(function (item) {
                if (typeof item.value == "string" && item.value.trim().length > 0) {
                    newQuery.push({
                        key: item.key,
                        value: item.value.trim()
                    });
                }
                else {
                    newQuery.push({
                        key: item.key,
                        value: item.value
                    });
                }
            });
        }
        if (searchByform == undefined || searchByform === true) {
            // search  with formSearch
            if (this.formSearch) {
                $(this.formSearch + " :input").each(function () {
                    if ($(this).attr('data-key') != undefined) {
                        // console.log($(this).attr('type'));
                        switch (this.type) {
                            case "checkbox":
                                newQuery.push({
                                    key: $(this).attr('data-key'),
                                    value: $(this).prop("checked")
                                });
                                break;
                            case "textarea":
                                if ($(this).val().trim().length > 0) {
                                    newQuery.push({
                                        key: $(this).attr('data-key'),
                                        value: $(this).val().trim().replace(/\n/g, ",")
                                    });
                                }
                                break;
                            default:
                                switch (typeof $(this).val()) {
                                    case "string":
                                        if ($(this).attr('data-date-format') != undefined) {
                                            var format = $(this).attr('data-date-format');
                                            var str = _this.stringToISODateStr($(this).val(), format);
                                            if (str.length > 0) {
                                                newQuery.push({
                                                    key: $(this).attr('data-key'),
                                                    value: str
                                                });
                                            }
                                        }
                                        else {
                                            if ($(this).val().trim().length > 0) {
                                                newQuery.push({
                                                    key: $(this).attr('data-key'),
                                                    value: $(this).val().trim()
                                                });
                                            }
                                        }
                                        break;
                                    case "object":
                                        if (Array.isArray($(this).val()) && $(this).val().length > 0) {
                                            newQuery.push({
                                                key: $(this).attr('data-key'),
                                                value: $(this).val().toString()
                                            });
                                        }
                                        else if (Object.keys($(this).val()).length > 0) {
                                            newQuery.push({
                                                key: $(this).attr('data-key'),
                                                value: JSON.stringify($(this).val())
                                            });
                                        }
                                        break;
                                    case "number":
                                        newQuery.push({
                                            key: $(this).attr('data-key'),
                                            value: $(this).val()
                                        });
                                        break;
                                    default:
                                        newQuery.push({
                                            key: $(this).attr('data-key'),
                                            value: $(this).val()
                                        });
                                        break;
                                }
                                break;
                        }
                    }
                });
            }
        }
        // compare newQuery and api.query, set api query and reload
        if (JSON.stringify(newQuery) != JSON.stringify(this.api.query)) {
            this.api.query = newQuery;
            this.reload({ currentPage: 1 });
        }
    };
    /**
     * Sort data table
     * @param columnName column name order
     */
    DataTable.prototype.sort = function (columnName) {
        if (this.isDebug) {
            console.info("%s call sort()", className);
        }
        var lastColumn = this.lastColumnSort;
        // compare with lastcolumn
        if (columnName == lastColumn) {
            var index = this.columns.findIndex(function (x) { return x.field == lastColumn; });
            if (index != -1) {
                if (this.columns[index].allowSort) {
                    this.columns[index].isSort = true;
                    switch (this.columns[index].sortable) {
                        case 'asc':
                            this.columns[index].sortable = 'desc';
                            break;
                        case 'desc':
                            this.columns[index].sortable = 'asc';
                            break;
                        default:
                            this.columns[index].sortable = 'asc';
                            break;
                    }
                    this.api.sorts.field = this.columns[index].field;
                    this.api.sorts.sort = this.columns[index].sortable;
                    this.reload({});
                }
            }
        }
        else {
            var indexNew = this.columns.findIndex(function (x) { return x.field == columnName; });
            if (indexNew != -1) {
                if (this.columns[indexNew].allowSort) {
                    if (lastColumn.length > 0) {
                        var index = this.columns.findIndex(function (x) { return x.field == lastColumn; });
                        if (index != -1) {
                            this.columns[index].sortable = '';
                            this.columns[index].isSort = false;
                        }
                    }
                    this.columns[indexNew].sortable = 'asc';
                    this.columns[indexNew].isSort = true;
                    this.lastColumnSort = columnName;
                    this.api.sorts.field = this.columns[indexNew].field;
                    this.api.sorts.sort = this.columns[indexNew].sortable;
                    this.reload({});
                }
            }
        }
    };
    /**
     *
     * @param option option width height
     */
    DataTable.prototype.updateLayout = function (option) {
        // console.log(option);
        // console.log(option.contentRect);        
        if (!this.isLoading && this.lastWidth != option.width) {
            this.lastWidth = option.width;
            this.isShowSubRow = false;
            this.isLoading = true;
            var columnsNew = void 0;
            columnsNew = this.columns.map(function (x) { return x; });
            // check column
            var width = option.width;
            var widthMin = 0;
            var offset = 32;
            for (var i = 0; i < columnsNew.length; i++) {
                if (!columnsNew[i].autoHide) {
                    widthMin += columnsNew[i].width + offset;
                }
            }
            for (var i = 0; i < columnsNew.length; i++) {
                if (columnsNew[i].autoHide) {
                    var calculator = widthMin + columnsNew[i].width + offset;
                    if (calculator <= width) {
                        widthMin = widthMin + columnsNew[i].width + offset;
                        columnsNew[i].isShow = true;
                    }
                    else {
                        columnsNew[i].isShow = false;
                        this.isShowSubRow = true;
                    }
                }
            }
            this.columns = columnsNew;
            this.isLoading = false;
            this.lastRowShow = -1;
            // this.crd.detectChanges();
            // this.update({});
        }
    };
    /**
     *
     * @param i
     */
    DataTable.prototype.showSubRow = function (i) {
        if (i == this.lastRowShow)
            this.lastRowShow = -1;
        else
            this.lastRowShow = i;
    };
    Object.defineProperty(DataTable.prototype, "bodyStyle", {
        /**
         * set style body table by layout.body.scrolable config
         */
        get: function () {
            var style = {};
            if (this.layout.body.scrollable) {
                style = { 'max-height': this.layout.body.maxHeight + 'px', 'overflow-y': 'auto', 'overflow-x': 'hidden', 'right': '1px' };
            }
            return style;
        },
        enumerable: true,
        configurable: true
    });
    // ------ Checkbox process ------
    /**
     * Checkbox all
     */
    DataTable.prototype.checkCheckBoxAll = function () {
        var selected = this.data.filter(function (val) {
            return val['dt_selecter'] === true;
        });
        if (selected && selected.length == this.data.length)
            this.isCheckBoxAll = true;
        else
            this.isCheckBoxAll = false;
    };
    DataTable.prototype.setCheckBox = function (index) {
        this.data[index].dt_selecter = !this.data[index].dt_selecter;
        this.checkCheckBoxAll();
    };
    DataTable.prototype.setCheckBoxAll = function () {
        this.isCheckBoxAll = !this.isCheckBoxAll;
        var checked = this.isCheckBoxAll;
        this.data = this.data.map(function (val) {
            val.dt_selecter = checked;
            return val;
        });
    };
    Object.defineProperty(DataTable.prototype, "dataSelected", {
        get: function () {
            return this.data.filter(function (val) {
                return val.dt_selecter === true;
            });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DataTable.prototype, "selecter", {
        get: function () {
            return this.layout.selecter;
        },
        enumerable: true,
        configurable: true
    });
    // ------ End checkbox process ------
    /**
     * Custom set data
     * @param data
     */
    DataTable.prototype.setData = function (data) {
        if (this.layout.selecter) {
            this.data = data.map(function (val) {
                return Object.assign({}, val, { dt_selecter: false });
            });
        }
        else
            this.data = data;
    };
    DataTable.prototype.stringToISODateStr = function (str, format) {
        var strResult = "";
        var temp;
        switch (format) {
            case "dd/mm/yyyy":
                strResult = str.split("/").reverse().join('-');
                break;
            case "mm/dd/yyyy":
                temp = str.split("/");
                if (temp.length == 2) {
                    strResult = temp[2] + "/" + temp[1] + "/" + temp[0];
                }
                break;
            case "yyyy/mm/dd":
                strResult = str;
                break;
            default:
                break;
        }
        return strResult;
    };
    return DataTable;
}());



/***/ })

}]);
//# sourceMappingURL=default~app-views-pages-admin-admin-module~app-views-pages-manage-manage-module.js.map